export JAVA_HOME=/home/ec2-user/jdk
export PATH=$PATH:$JAVA_HOME/bin
export M2_HOME=/home/ec2-user/maven
export M2=$M2_HOME/bin
export PATH=$M2:$PATH 
mvn clean install -DskipTests
