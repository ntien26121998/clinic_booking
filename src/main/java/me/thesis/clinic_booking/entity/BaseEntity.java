package me.thesis.clinic_booking.entity;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.util.Date;

@MappedSuperclass
@Data
@NoArgsConstructor
public class BaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private boolean isDeleted;
    private Date createdTime;
    private Date updatedTime;
    
    private Long createdBy;
    private Long updatedBy;

    public BaseEntity(boolean isDeleted, Date createdTime, Date updatedTime) {
        this.isDeleted = isDeleted;
        this.createdTime = createdTime;
        this.updatedTime = updatedTime;
    }
}
