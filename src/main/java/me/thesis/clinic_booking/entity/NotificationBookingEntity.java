package me.thesis.clinic_booking.entity;

import lombok.Data;
import me.thesis.clinic_booking.entity.enums.NotificationUsingForType;
import org.hibernate.annotations.Where;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "notification")
@Data
@Where(clause = "is_deleted = false")
public class NotificationBookingEntity extends BaseEntity {

    private Long serviceDepartmentId;
    private Long staffId;
    private String clientPhone;
    @Convert(converter = NotificationUsingForType.Converter.class)
    private NotificationUsingForType type;
    private String content;
    private Date createdTime;

}
