package me.thesis.clinic_booking.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;

@Entity
@Table(name = "role")
@Getter
@Setter
@Where(clause = "is_deleted = false")
public class RoleEntity extends BaseEntity {

//    private String name;
//
//    @JsonIgnore
//    @ManyToMany(mappedBy = "roleEntities")
//    private Set<UserEntity> userEntitySet;
//
//    public RoleEntity(boolean deleted, Date createdTime, Date updatedTime, String name) {
//        super(deleted, createdTime, updatedTime);
//        this.name = "ROLE_" + name;
//    }
@Id
@GeneratedValue(strategy = GenerationType.IDENTITY)
private Long id;
    private String name;
    private Long companyId;
    private Long storeId;
    private Integer type; // 1:Company 2: store
    private Date createdTime;

    @Transient
    private String storeCode;

    public RoleEntity(String name, Long idType, Integer type) {
        this.name = name;
        this.type = type;
        if (type == 1) {
            this.companyId = idType;
        } else if (type == 2) {
            this.storeId = idType;
        }
        this.createdTime = new Date();
    }

    public RoleEntity(String name, Long companyId, Long storeId, Integer type) {
        this.name = name;
        this.type = type;
        this.storeId = storeId;
        this.companyId = companyId;
        this.createdTime = new Date();
    }

    public RoleEntity(boolean deleted, Date createdTime, Date updatedTime, String name) {
        super(deleted, createdTime, updatedTime);
        this.name = "ROLE_" + name;
    }

    public RoleEntity() {}

    public RoleEntity(RoleEntity roleEntity, String storeCode) {
        this.id = roleEntity.getId();
        this.name = roleEntity.name;
        this.type = roleEntity.type;
        this.storeId = roleEntity.storeId;
        this.companyId = roleEntity.companyId;
        this.createdTime = roleEntity.getCreatedTime();
        this.storeCode = storeCode;
    }
}
