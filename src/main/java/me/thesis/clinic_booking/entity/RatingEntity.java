package me.thesis.clinic_booking.entity;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Where;

import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.util.List;

@Entity
@Table(name = "rating_service",
        indexes = {
                @Index(name="find_by_service",columnList = "serviceDepartmentId"),
                @Index(name="find_by_store",columnList = "storeId")
        })
@Data
@NoArgsConstructor
@Where(clause = "is_deleted = false")
public class RatingEntity extends BaseEntity {

    private Integer ratingValue;
    private String phoneNumber;
    private Long serviceDepartmentId;
    private Long storeId;
    private Long bookingId;

    @Transient
    private List<CommentOfRatingEntity> listComment;
}
