package me.thesis.clinic_booking.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import me.thesis.clinic_booking.entity.enums.StatusUserType;
import org.hibernate.annotations.Formula;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;

@Entity
@Table(name = "user")
@Data
@NoArgsConstructor
@Where(clause = "is_deleted = false")
public class UserEntity extends BaseEntity{
    private String name;
    private String email;

    @JsonIgnore
    private String password;
    private String phone;
    private Integer gender;
    private Date birthday;
    private String address;

    @Convert(converter = StatusUserType.Converter.class)
    private StatusUserType status;
    private Long companyId;
    private Long storeId;
    private Long serviceDepartmentId;
    private String avatarUrl;

    @Column(columnDefinition="text")
    private String description;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "user_role",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "role_id"))
    private Set<RoleEntity> roleEntities;

    @Column(columnDefinition="boolean default false")
    private boolean enableServe;

    private Date latestTimeReadingNotification;

//    @Formula(value = "(select count(b.id) from booking b where b.deleted = false and b.staff_id = id and b.status = 4)")
//    private int numberDoneBooking;

    @Builder
    public UserEntity(boolean isDeleted, Date createdTime, Date updatedTime,
                      String name, String email, String password, String phone, Integer gender, StatusUserType status,
                      Set<RoleEntity> roleEntities, Long storeId, Long serviceDepartmentId, String avatarUrl) {
        super(isDeleted, createdTime, updatedTime);
        this.name = name;
        this.email = email;
        this.password = password;
        this.phone = phone;
        this.gender = gender;
        this.roleEntities = roleEntities;
        this.storeId = storeId;
        this.serviceDepartmentId = serviceDepartmentId;
        this.avatarUrl = avatarUrl;
        this.status = status;
    }
}
