package me.thesis.clinic_booking.entity;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import me.thesis.clinic_booking.entity.enums.CommentRatingType;
import org.hibernate.annotations.Where;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "comment_rating")
@Data
@NoArgsConstructor
@Where(clause = "is_deleted = false")
public class CommentOfRatingEntity extends BaseEntity {
    private String comment;
    private String imageUrl;

    @Convert(converter = CommentRatingType.Converter.class)
    private CommentRatingType type;
    private Long ratingServiceId;
}
