package me.thesis.clinic_booking.entity.company;

import lombok.Data;
import me.thesis.clinic_booking.entity.BaseEntity;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Data
@Table(name = "company")
public class CompanyEntity extends BaseEntity {

    private String companyAuthId;
    private String name;
    private String phoneNumber;
    private String address;
    private String email;
    private String city;
    private String district;

}
