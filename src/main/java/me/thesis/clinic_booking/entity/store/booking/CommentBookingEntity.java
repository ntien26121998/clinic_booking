package me.thesis.clinic_booking.entity.store.booking;

import lombok.Data;
import me.thesis.clinic_booking.entity.BaseEntity;
import me.thesis.clinic_booking.entity.enums.CommentFromType;
import me.thesis.clinic_booking.entity.enums.CommentType;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.util.Date;

@Data
@Entity
@Table(name = "comment_booking")
public class CommentBookingEntity extends BaseEntity {

    private Long bookingId;
    private Long customerId;
    private Long memberId;
    private String content;

    @Convert(converter = CommentFromType.Converter.class)
    private CommentFromType typeUser; // 1: admin ; 2: customer

    @Convert(converter = CommentType.Converter.class)
    private CommentType typeInput; // 1: text, 2: image; 3:pdf
    private String fileUrl;
    private boolean isDeleted;
    private Date createdTime;
    private Date updatedTime;

    private String pdfFileName;

    @Transient
    private String updatedByName;
}
