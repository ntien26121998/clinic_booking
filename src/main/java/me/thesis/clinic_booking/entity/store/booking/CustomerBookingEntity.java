package me.thesis.clinic_booking.entity.store.booking;

import lombok.Data;
import me.thesis.clinic_booking.entity.BaseEntity;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Entity
@Data
@Table(name = "customer" ,
        uniqueConstraints={
                @UniqueConstraint(columnNames = {"storeId", "patientCode"})
        })
public class CustomerBookingEntity extends BaseEntity {
    private String name;
    private Integer gender; //0 Không xác định, 1 Nam, 2 Nữ
    private Integer age;
    private String phone;
    private String email;
    private Long memberId;
    private Long medicalRecordId;

    private Long patientCode;
    private Long storeId;
    private Long serviceDepartmentId;
    private boolean isPatientCodeFromUser; //user create this patient code
    private boolean isCreatedBooking;

    @Transient
    private String listStoreNameHasBooked;
    @Transient
    private Long numberBookingHasBookedInStore;
    @Transient
    private String latestNote;
    @Transient
    private List<Long> latestListCategoryIdHasChosen;
    @Transient
    private String storeAuthId;
    @Transient
    private String serviceDepartmentAuthId;

    public void setLatestListCategoryIdHasChosen(String latestListCategoryIdHasChosenStr) {
        if (latestListCategoryIdHasChosenStr == null) {
            this.latestListCategoryIdHasChosen = Collections.emptyList();
        } else {
            this.latestListCategoryIdHasChosen =
                    Stream.of(latestListCategoryIdHasChosenStr.split(","))
                            .map(categoryId -> Long.parseLong(categoryId.trim())).collect(Collectors.toList());
        }
    }
}
