package me.thesis.clinic_booking.entity.store.booking;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Data
@Table(name = "frame_setting")
public class FrameSettingEntity extends BaseConfigEntity {

    private int frameSlot;
    private int slotTimeUnit; // minute

}
