package me.thesis.clinic_booking.entity.store.booking;

import lombok.Data;
import me.thesis.clinic_booking.entity.enums.WorkOffDayEnum;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Data
@Table(name = "work_off_day_setting")
public class WorkOffDaySettingEntity extends BaseConfigEntity {

    @Convert(converter = WorkOffDayEnum.Converter.class)
    private WorkOffDayEnum workOffFixed;
    private boolean isHoliday;
}
