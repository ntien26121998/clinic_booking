package me.thesis.clinic_booking.entity.store.booking;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import me.thesis.clinic_booking.entity.enums.WorkingTimeConfigType;

import javax.persistence.*;
import java.util.Date;

@MappedSuperclass
@Data
public class BaseConfigEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Long serviceDepartmentId;

    @JsonFormat(pattern = "yyyy/MM/dd")
    private Date startActiveRange;

    @JsonFormat(pattern = "yyyy/MM/dd")
    private Date endActiveRange;
    private boolean isDeleted;
    private Date createdTime;
    private Date updatedTime;
    private Long updatedBy;

    @Convert(converter = WorkingTimeConfigType.Converter.class)
    private WorkingTimeConfigType typeConfig; // 1: common, 2: private
}