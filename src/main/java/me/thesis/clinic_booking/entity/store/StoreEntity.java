package me.thesis.clinic_booking.entity.store;

import lombok.Data;
import me.thesis.clinic_booking.entity.BaseEntity;
import me.thesis.clinic_booking.entity.enums.PageRedirect;
import org.hibernate.annotations.Formula;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Data
@Table(name = "store")
public class StoreEntity extends BaseEntity {
    private String storeAuthId;
    private Long companyId;
    private String storeName;
    private String storeCode;
    private String cityName;
    private String districtName;
    private String address;
    private String phone;
    private String imageUrl;
    @Column(columnDefinition="text")
    private String description;
    private String email;

    @Transient
    private PageRedirect redirectScreen;

    @Formula(value = "(select count(b.id) from booking b where b.is_deleted = false and b.store_id = id and b.status = 4)")
    private int numberDoneBooking;
    @Formula(value = "(select count(s.id) from service_department s where s.is_deleted = false and s.store_id = id)")
    private int numberServiceDepartment;
}
