package me.thesis.clinic_booking.entity.store.booking;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.Valid;
import javax.validation.constraints.Pattern;

@Entity
@Data
@Table(name = "time_setting")
public class TimeSettingEntity extends BaseConfigEntity {

    @Valid
    @Pattern(regexp = "([01]?[0-9]|2[0-3]):[0-5][0-9]")
    private String startWorkingTime;

    @Valid
    @Pattern(regexp = "([01]?[0-9]|2[0-3]):[0-5][0-9]")
    private String endWorkingTime;

    @Valid
    @Pattern(regexp = "([01]?[0-9]|2[0-3]):[0-5][0-9]")
    private String startBreakTime;

    @Valid
    @Pattern(regexp = "([01]?[0-9]|2[0-3]):[0-5][0-9]")
    private String endBreakTime;

    private int dayInWeek; //2,3,4,5,6,7,1 : 1 = sunday

}
