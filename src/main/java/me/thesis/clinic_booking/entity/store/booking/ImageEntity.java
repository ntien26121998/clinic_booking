package me.thesis.clinic_booking.entity.store.booking;

import lombok.Data;
import lombok.NoArgsConstructor;
import me.thesis.clinic_booking.entity.BaseEntity;
import org.hibernate.annotations.Where;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "image")
@Data
@NoArgsConstructor
@Where(clause = "is_deleted = false")
public class ImageEntity extends BaseEntity {
    private String imageUrl;
    private Long serviceDepartmentId;
    private String title;
    private String description;
}
