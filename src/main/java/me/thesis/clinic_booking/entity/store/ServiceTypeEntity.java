package me.thesis.clinic_booking.entity.store;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import me.thesis.clinic_booking.entity.BaseEntity;
import org.hibernate.annotations.Where;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "type_service")
@Setter
@Getter
@NoArgsConstructor
@Where(clause = "is_deleted = false")
public class ServiceTypeEntity extends BaseEntity {
    private String name;
    private Long storeId;
}
