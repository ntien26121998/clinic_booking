package me.thesis.clinic_booking.entity.store.booking;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@Table(name = "booking_category")
public class BookingCategoryEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Long bookingId;
    private Long categoryId;
    private Long moneyExcludeTax;
    private Date createdTime;
}
