package me.thesis.clinic_booking.entity.store;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import me.thesis.clinic_booking.entity.BaseEntity;
import org.hibernate.annotations.Formula;
import org.hibernate.annotations.Where;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;

@Entity
@Data
@Table(name = "service_department", indexes = {
        @Index(name="find_by_name",columnList = "name,serviceTypeId,city")
        })
@Where(clause = "is_deleted = false")

public class ServiceDepartmentEntity extends BaseEntity {
    private String authId;
    private String name;
    private String phone;
    private String email;

    @Column(columnDefinition = "text")
    private String description;

    private Long serviceTypeId;
    private String city;

    @JsonIgnore
    private Long storeId;
    private String imageUrl;

    @Formula(value = "(select t.name from type_service t where t.is_deleted = false and t.id = service_type_id)")
    private String serviceTypeName;

    @Formula(value = "(select s.store_name from store s where s.is_deleted = false and s.id = store_id)")
    private String storeName;

    @Formula(value = "(select s.store_auth_id from store s where s.is_deleted = false and s.id = store_id)")
    private String storeAuthId;

    @Formula(value = "(select count(b.id) from booking b where b.is_deleted = false and b.service_department_id = id and b.status = 4 and b.type_start_frame = 1 )")
//    @Formula(value = "(Select count(b.id) from service_department sd left join booking b on b.service_department_id = sd.id and b.is_deleted = false and b.status = 2 group by sd.id)")
    private int numberDoneBooking;
//
//    @Formula(value = "(select count(c.id) from service_department sd left join category_booking c on c.service_department_id = sd.id where c.is_deleted = false group by sd.id")
//    private int numberCategory;

    private String priceList;
}
