package me.thesis.clinic_booking.entity.store.booking;

import lombok.Data;
import me.thesis.clinic_booking.dto.CommonBookingInfoDto;
import me.thesis.clinic_booking.entity.enums.StatusBookingType;
import me.thesis.clinic_booking.entity.enums.TypeStartFrame;
import me.thesis.clinic_booking.entity.store.booking.category.CategoryBookingEntity;
import me.thesis.clinic_booking.utils.DateUtils;
import org.hibernate.annotations.Formula;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Data
@Table(name = "booking")
public class BookingEntity extends CommonBookingInfoDto{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Long storeId;
    private Long serviceDepartmentId;
    private String note;
    private String title;
    private Integer numberFrameConsecutive;
    private String startFrameAndDate; //frame start _ date without hour _ booking start id
    private boolean isDeleted;
    private Date createdTime;
    private Date updatedTime;

    private Long createdBy;
    private Long updatedBy;

    @ManyToOne
    @JoinColumn(name = "customer_id")
    private CustomerBookingEntity customer;
    private boolean isSpecial;

    private String timeStartToEnd;
    @Convert(converter = TypeStartFrame.Converter.class)
    private TypeStartFrame typeStartFrame;
    @Convert(converter = StatusBookingType.Converter.class)
    private StatusBookingType status;

    @Transient
    private String serviceDepartmentAuthId;
    @Transient
    private String storeName;
    @Transient
    private String storeAuthId;
    @Transient
    private List<Long> listCategoryIdChosen;
    @Transient
    private List<CategoryBookingEntity> listCategory;

    @Column(columnDefinition = "text")
    private String description;
    @Transient
    private Date startOfDay;
    @Transient
    private Date endOfDay;

    public void setStartOfDay(Date bookingTime) {
        this.startOfDay = DateUtils.getStartOfDay(bookingTime);
    }

    public void setEndOfDay(Date bookingTime) {
        this.endOfDay = DateUtils.getEndOfDay(bookingTime);
    }

    @Formula(value = "(select concat( sd.name, ' - ', s.store_name ) " +
            "from service_department sd, store s where sd.is_deleted = false and sd.id = service_department_id " +
            "and s.is_deleted = false and s.id = store_id)")
    private String serviceDepartmentName;

    @Formula(value = "(select sd.image_url from service_department sd where sd.is_deleted = false and sd.id = service_department_id)")
    private String serviceDepartmentImageUrl;

    @Formula(value = "(select sd.auth_id from service_department sd where sd.is_deleted = false and sd.id = service_department_id)")
    private String serviceDepartmentAuthIdForClient;

    private boolean isRatedByPhone;

    private boolean isPayCheck;

    private boolean isSendMailToClient;

}
