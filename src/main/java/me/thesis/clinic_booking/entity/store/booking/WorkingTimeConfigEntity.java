package me.thesis.clinic_booking.entity.store.booking;

import lombok.Data;
import me.thesis.clinic_booking.entity.BaseEntity;
import me.thesis.clinic_booking.entity.enums.WorkOffDayEnum;
import me.thesis.clinic_booking.entity.enums.WorkingTimeConfigType;

import javax.persistence.*;
import java.util.Date;

@Entity
@Data
@Table(name = "working_time_config")
public class WorkingTimeConfigEntity extends BaseEntity {

    private Long storeId;
    private Long serviceDepartmentId;
    private String startWorkingTime;
    private String endWorkingTime;
    private String startBreakTime;
    private String endBreakTime;

    @Convert(converter = WorkOffDayEnum.Converter.class)
    private WorkOffDayEnum workOffFixed;
    private Date activeDay;
    private boolean isHoliday;

    @Convert(converter = WorkingTimeConfigType.Converter.class)
    private WorkingTimeConfigType typeConfig; // 1: common, 2: private
    private int slotPerFrame;
    private int slotTimeUnit; // minute

    @Transient
    private int commonSlotTimeUnit;
    @Column(columnDefinition="text")
    private String customSlotPerFrame; // frame1:numberSlotOfFrame1;frame2:numberSlotOfFrame2;...
    private boolean isDeleted;
    private Date createdTime;
    private Date updatedTime;

    @Transient
    private boolean isWorkOffDay;
}
