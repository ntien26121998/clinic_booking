package me.thesis.clinic_booking.entity.store.booking.category;

import lombok.Data;
import me.thesis.clinic_booking.entity.BaseEntity;

import javax.persistence.*;
import java.util.Date;
import java.util.Objects;

@Data
@Entity
@Table(name = "category_booking")
public class CategoryBookingEntity extends BaseEntity implements Comparable<CategoryBookingEntity> {
    private Long storeId;
    private Long serviceDepartmentId;
    private String categoryName;
    private int numberFrameConsecutiveAutoFill;
    private Long moneyExcludeTax;
    private Long money;
    private boolean isActive;
    @Transient
    private String serviceDepartmentAuthId;

    @Override
    public int compareTo(CategoryBookingEntity o) {
        return categoryName.compareTo(o.categoryName);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CategoryBookingEntity that = (CategoryBookingEntity) o;
        return Objects.equals(storeId, that.storeId) &&
                Objects.equals(categoryName, that.categoryName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(storeId, categoryName);
    }
}
