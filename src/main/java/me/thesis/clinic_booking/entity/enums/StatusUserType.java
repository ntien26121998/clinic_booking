package me.thesis.clinic_booking.entity.enums;

import me.thesis.clinic_booking.utils.AbstractEnumConverter;

public enum  StatusUserType implements ValueEnum<Integer> {
    ACTIVE(1), IN_ACTIVE(2);

    int value;
    StatusUserType(int value) {
        this.value = value;
    }

    public static class Converter extends AbstractEnumConverter<StatusUserType, Integer> {
        public Converter() {
            super(StatusUserType.class);
        }
    }
    @Override
    public Integer getValue() {
        return value;
    }

    @Override
    public void setValue(Integer value) {
        this.value = value;
    }

}