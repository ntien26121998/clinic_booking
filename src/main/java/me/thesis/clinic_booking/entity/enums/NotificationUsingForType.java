package me.thesis.clinic_booking.entity.enums;

import me.thesis.clinic_booking.utils.AbstractEnumConverter;

public enum NotificationUsingForType implements ValueEnum<Integer> {
    SERVICE_DEPARTMENT_ADMIN(1), SERVICE_DEPARTMENT_STAFF(2), CLIENT(3);

    int value;

    NotificationUsingForType(int value) {
        this.value = value;
    }

    public static class Converter extends AbstractEnumConverter<NotificationUsingForType, Integer> {
        public Converter() {
            super(NotificationUsingForType.class);
        }
    }

    @Override
    public Integer getValue() {
        return value;
    }

    @Override
    public void setValue(Integer value) {
        this.value = value;
    }
}

