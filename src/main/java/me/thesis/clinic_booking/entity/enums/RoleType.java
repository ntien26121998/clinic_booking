package me.thesis.clinic_booking.entity.enums;

import me.thesis.clinic_booking.utils.AbstractEnumConverter;

public enum RoleType implements ValueEnum<String> {
    SYSTEM_ADMIN("SYSTEM_ADMIN"), STORE_ADMIN("STORE_ADMIN"),
    SERVICE_DEPARTMENT_ADMIN("SERVICE_DEPARTMENT_ADMIN"),
    SERVICE_DEPARTMENT_STAFF("SERVICE_DEPARTMENT_STAFF"),
    SERVICE_DEPARTMENT_CASHIER("SERVICE_DEPARTMENT_CASHIER"),
    CLIENT_WITH_PHONE("CLIENT_WITH_PHONE");

    String name;

    RoleType(String name) {
        this.name = name;
    }

    @Override
    public String getValue() {
        return name;
    }

    @Override
    public void setValue(String value) {
        this.name = value;
    }

    public static class Converter extends AbstractEnumConverter<RoleType, String> {
        public Converter() {
            super(RoleType.class);
        }
    }
}
