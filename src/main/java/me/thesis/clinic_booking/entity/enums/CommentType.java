package me.thesis.clinic_booking.entity.enums;

import me.thesis.clinic_booking.utils.AbstractEnumConverter;

public enum CommentType implements ValueEnum<Integer> {

    TEXT(1), IMAGE(2), PDF(3);
    int value;

    CommentType(int value) {
        this.value = value;
    }

    public static class Converter extends AbstractEnumConverter<CommentType, Integer> {
        public Converter() {
            super(CommentType.class);
        }
    }

    @Override
    public Integer getValue() {
        return value;
    }

    @Override
    public void setValue(Integer value) {
        this.value = value;
    }
}
