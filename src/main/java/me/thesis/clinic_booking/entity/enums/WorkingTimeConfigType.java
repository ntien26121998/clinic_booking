package me.thesis.clinic_booking.entity.enums;

import me.thesis.clinic_booking.utils.AbstractEnumConverter;

public enum WorkingTimeConfigType implements ValueEnum<Integer> {
    COMMON_CONFIG(1), SPECIFIC_DAY_CONFIG(2);
    int value;

    WorkingTimeConfigType(int value) {
        this.value = value;
    }

    public static class Converter extends AbstractEnumConverter<WorkingTimeConfigType, Integer> {
        public Converter() {
            super(WorkingTimeConfigType.class);
        }
    }

    @Override
    public Integer getValue() {
        return value;
    }

    @Override
    public void setValue(Integer value) {
        this.value = value;
    }
}
