package me.thesis.clinic_booking.entity.enums;

import me.thesis.clinic_booking.utils.AbstractEnumConverter;

public enum WorkOffDayEnum implements ValueEnum<Integer>{
    NOT_CONFIG(0),
    MONDAY(2), TUESDAY(3),
    WEDNESDAY(4), THURSDAY(5), FRIDAY(6),
    SATURDAY(7), SUNDAY(1);

    int value;

    WorkOffDayEnum(int value) {
        this.value = value;
    }

    public static class Converter extends AbstractEnumConverter<WorkOffDayEnum, Integer> {
        public Converter() {
            super(WorkOffDayEnum.class);
        }
    }

    @Override
    public Integer getValue() {
        return value;
    }

    @Override
    public void setValue(Integer value) {
        this.value = value;
    }
}
