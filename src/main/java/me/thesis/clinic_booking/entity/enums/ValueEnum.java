package me.thesis.clinic_booking.entity.enums;

public interface ValueEnum<E> {

    E getValue();
    void setValue(E value);
}
