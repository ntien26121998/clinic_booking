package me.thesis.clinic_booking.entity.enums;

import me.thesis.clinic_booking.utils.AbstractEnumConverter;

public enum CommentRatingType implements ValueEnum<Integer> {

    CLIENT_COMMENT(1), ADMIN_COMMENT(2);

    int value;

    CommentRatingType(int value) {
        this.value = value;
    }

    public static class Converter extends AbstractEnumConverter<CommentRatingType, Integer> {
        public Converter() {
            super(CommentRatingType.class);
        }
    }

    @Override
    public Integer getValue() {
        return value;
    }

    @Override
    public void setValue(Integer value) {
        this.value = value;
    }
}
