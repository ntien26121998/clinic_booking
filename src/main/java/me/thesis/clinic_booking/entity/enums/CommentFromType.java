package me.thesis.clinic_booking.entity.enums;

import me.thesis.clinic_booking.utils.AbstractEnumConverter;

public enum  CommentFromType implements ValueEnum<Integer> {

    VIA_ADMIN(1), VIA_CUSTOMER(2);
    int value;

    CommentFromType(int value) {
        this.value = value;
    }

    public static class Converter extends AbstractEnumConverter<CommentFromType, Integer> {
        public Converter() {
            super(CommentFromType.class);
        }
    }

    @Override
    public Integer getValue() {
        return value;
    }

    @Override
    public void setValue(Integer value) {
        this.value = value;
    }
}