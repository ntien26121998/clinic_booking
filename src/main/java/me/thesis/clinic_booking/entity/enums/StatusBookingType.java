package me.thesis.clinic_booking.entity.enums;

import me.thesis.clinic_booking.utils.AbstractEnumConverter;

public enum StatusBookingType implements ValueEnum<Integer> {

    WAITING_CONFIRM(1), SUCCESS(2), IN_PROCESS(3), DONE(4), CANCELED(5);

    int value;

    StatusBookingType(int value) {
        this.value = value;
    }

    public static class Converter extends AbstractEnumConverter<StatusBookingType, Integer> {
        public Converter() {
            super(StatusBookingType.class);
        }
    }

    @Override
    public Integer getValue() {
        return value;
    }

    @Override
    public void setValue(Integer value) {
        this.value = value;
    }
}
