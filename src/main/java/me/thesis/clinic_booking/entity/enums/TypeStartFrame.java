package me.thesis.clinic_booking.entity.enums;

import me.thesis.clinic_booking.utils.AbstractEnumConverter;

import java.util.HashMap;
import java.util.Map;

public enum TypeStartFrame implements ValueEnum<Integer> {
    START_FRAME(1), CONSECUTIVE_FRAME(2);
    int value;

    TypeStartFrame(int value) {
        this.value = value;
    }

    public static class Converter extends AbstractEnumConverter<TypeStartFrame, Integer> {
        public Converter() {
            super(TypeStartFrame.class);
        }
    }

    @Override
    public Integer getValue() {
        return value;
    }

    @Override
    public void setValue(Integer value) {
        this.value = value;
    }

    private static final Map<Integer, TypeStartFrame> map = new HashMap<>(values().length, 1);

    static {
        for (TypeStartFrame c : values()) map.put(c.value, c);
    }

    public static TypeStartFrame of(Integer value) {
        TypeStartFrame result = map.get(value);
        if (result == null) {
            throw new IllegalArgumentException("Invalid value for TypeStartFrame: " + value);
        }
        return result;
    }
}
