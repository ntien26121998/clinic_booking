package me.thesis.clinic_booking.entity.enums;

public enum PageRedirect {
    BOOKING_SCREEN,
    WORKING_TIME_CONFIG_SCREEN,
    LIST_CUSTOMER_STORE_SCREEN,
    STATISTIC_BOOKING_SCREEN,
    CAN_NOT_ACCESS_ALL_SCREEN_STORE
}
