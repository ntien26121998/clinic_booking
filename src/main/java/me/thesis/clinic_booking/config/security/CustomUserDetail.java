package me.thesis.clinic_booking.config.security;

import lombok.Data;
import me.thesis.clinic_booking.entity.UserEntity;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import java.util.Collection;

@Data
public class CustomUserDetail extends User {

    private UserEntity userEntity;

    public CustomUserDetail(String username, String password,
                            Collection<? extends GrantedAuthority> authorities, UserEntity userEntity) {
        super(username, password, authorities);
        this.userEntity = userEntity;
    }

    @Override
    public String toString() {
        return "CustomUserDetail{" +
                "userEntity=" + userEntity.toString() +
                '}';
    }


}
