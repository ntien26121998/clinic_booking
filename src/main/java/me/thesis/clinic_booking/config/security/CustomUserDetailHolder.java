package me.thesis.clinic_booking.config.security;

import me.thesis.clinic_booking.entity.UserEntity;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;

import javax.annotation.PostConstruct;

@Component
@Scope(value = WebApplicationContext.SCOPE_REQUEST, proxyMode = ScopedProxyMode.TARGET_CLASS)
public class CustomUserDetailHolder {

    private CustomUserDetail userDetails;

    @PostConstruct
    public void init() {
        if (!isAuthenticated()) {
            this.userDetails = null;
            return;
        }
        this.userDetails = (CustomUserDetail) getAuthentication().getPrincipal();
    }

    public CustomUserDetail getUserDetails() {
        return userDetails;
    }

    public UserEntity getUserEntity() {
        return userDetails.getUserEntity();
    }

    private static boolean isAuthenticated() {
        Authentication authentication = getAuthentication();
        return authentication != null && !(authentication instanceof AnonymousAuthenticationToken) && authentication.isAuthenticated();

    }

    private static Authentication getAuthentication(){
        return SecurityContextHolder.getContext().getAuthentication();
    }
}
