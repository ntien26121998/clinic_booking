package me.thesis.clinic_booking.config.security;

import me.thesis.clinic_booking.entity.UserEntity;
import me.thesis.clinic_booking.service.RoleService;
import me.thesis.clinic_booking.service.store.ServiceDepartmentService;
import me.thesis.clinic_booking.service.store.StoreService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collection;

@Component
public class SimpleAuthenticationSuccessHandler implements AuthenticationSuccessHandler {

    private RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();

    private static final Logger LOGGER = LoggerFactory.getLogger(SimpleAuthenticationSuccessHandler.class);

    @Autowired
    private StoreService storeService;

    @Autowired
    private ServiceDepartmentService serviceDepartmentService;

    @Autowired
    private RoleService roleService;

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
        Collection<? extends GrantedAuthority> authorities = authentication.getAuthorities();
        CustomUserDetail customUserDetail = (CustomUserDetail) authentication.getPrincipal();
        response.getWriter().print(getLinkRedirect(customUserDetail.getUserEntity()));

    }

    public String getLinkRedirect(UserEntity userEntity) {
        Long storeId = userEntity.getStoreId();
        Long serviceDepartmentId = userEntity.getServiceDepartmentId();
        if(storeId == null && serviceDepartmentId == null) {
            return "/system/store";
//            return "system/company/companies";
        }

        // store admin
        if(storeId != null && serviceDepartmentId == null) {
            String storeAuthId = storeService.findById(storeId).getStoreAuthId();
            return "/store/" + storeAuthId +"/service_department";
        }

        // service_department admin or staff
        if(storeId != null && serviceDepartmentId != null) {
            String serviceDepartmentAuthId = serviceDepartmentService.findById(serviceDepartmentId).getAuthId();
            if(roleService.isServiceDepartmentStaff(userEntity.getId(), storeId, serviceDepartmentId)) {
                return "/service_staff/booking_management";
            }
            return "/service_department/" + serviceDepartmentAuthId +"/booking";
        } else {
            return "/401";
        }
    }
}
