package me.thesis.clinic_booking.config.security;

import me.thesis.clinic_booking.entity.enums.RoleType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Bean
    @Override
    protected AuthenticationManager authenticationManager() throws Exception {
        return super.authenticationManager();
    }

    @Autowired
    private AuthenticationSuccessHandler authenticationSuccessHandler;

    @Autowired
    private UserDetailsService userDetailsService;

    @Autowired
    private RESTAuthenticationEntryPoint authenticationEntryPoint;

    @Autowired
    private RESTAuthenticationFailureHandler authenticationFailureHandler;

    @Autowired
    private AuthenticationProvider customAuthenticationProvider;

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    @Override
    public void configure(WebSecurity web) {
        web.ignoring().antMatchers("/libs/**", "/custom/**", "/js/**", "/icon/**", "/images/**", "/favicon.ico/**"
                , "libs/**", "custom/**", "js/**", "icon/**", "images/**", "favicon.ico/**");
    }

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder builder) throws Exception {
        builder.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder);
        builder.inMemoryAuthentication()
                .withUser("user").password("password").roles("USER")
                .and()
                .withUser("admin").password("password").roles(RoleType.SYSTEM_ADMIN.name());
    }


    @Override
    protected void configure(HttpSecurity http) throws Exception {
        String[] rolesCanAccessWebAPI = new String[]{RoleType.SYSTEM_ADMIN.name(), RoleType.STORE_ADMIN.name(), RoleType.SERVICE_DEPARTMENT_ADMIN.name(), RoleType.SERVICE_DEPARTMENT_STAFF.name(), RoleType.SERVICE_DEPARTMENT_CASHIER.name()};
        String[] listRolesCanAccessStorePage = new String[]{RoleType.SYSTEM_ADMIN.name(), RoleType.STORE_ADMIN.name()};
        String[] listRolesCanAccessServicePage = new String[]{RoleType.SYSTEM_ADMIN.name(), RoleType.STORE_ADMIN.name(), RoleType.SERVICE_DEPARTMENT_ADMIN.name(), RoleType.SERVICE_DEPARTMENT_CASHIER.name()};

        http.authorizeRequests()
                .antMatchers("/api/v1/web/user/forgotPassword").permitAll()
                .antMatchers("/api/v1/web/user/signUp").permitAll()
                .antMatchers("/api/v1/web/user/setPassword").permitAll()
                .antMatchers("/api/v1/web/user/changePassword").permitAll()
                .antMatchers("/api/v1/web/**").hasAnyRole(rolesCanAccessWebAPI)
                .antMatchers("/system/**").hasAnyRole(RoleType.SYSTEM_ADMIN.getValue())
                .antMatchers("/store/**").hasAnyRole(listRolesCanAccessStorePage)
                .antMatchers("/service_department/**").hasAnyRole(listRolesCanAccessServicePage)
                .antMatchers("/service_staff/**").hasAnyRole(RoleType.SERVICE_DEPARTMENT_STAFF.name())
//                .antMatchers("/swagger-ui.html").hasAnyRole(RoleType.SYSTEM_ADMIN.name());
                .antMatchers("/swagger-ui.html").permitAll();

        http.authenticationProvider(customAuthenticationProvider);

        http.formLogin()
                .loginPage("/login")
                .usernameParameter("email")
                .passwordParameter("password")
                .loginProcessingUrl("/login")
                .successHandler(authenticationSuccessHandler)
                .failureHandler(authenticationFailureHandler)
                .permitAll();

        http.exceptionHandling()
                .authenticationEntryPoint(authenticationEntryPoint)
                .accessDeniedPage("/401");

        http.logout()
                .logoutUrl("/logout")
                .logoutSuccessUrl("/login");
        http.csrf().disable();
    }
}

