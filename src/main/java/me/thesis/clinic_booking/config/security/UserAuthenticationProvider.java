package me.thesis.clinic_booking.config.security;

import me.thesis.clinic_booking.entity.UserEntity;
import me.thesis.clinic_booking.service.security.AuthenticationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.HashSet;

@Component
public class UserAuthenticationProvider implements AuthenticationProvider {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserAuthenticationProvider.class);

    private final AuthenticationService authenticationService;

    public UserAuthenticationProvider(AuthenticationService authenticationService) {
        this.authenticationService = authenticationService;
    }

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        String email = authentication.getName();
        String password = authentication.getCredentials().toString();

        LOGGER.info("Validate user with username:  {}", email);
        UserEntity userEntity = authenticationService.validateUser(email, password);

        if (userEntity == null) {
            throw new BadCredentialsException("Username or password is incorrect!");
        }

        Collection<SimpleGrantedAuthority> authorities = new HashSet<>();
        userEntity.getRoleEntities().forEach(
                roleEntity -> authorities.add(new SimpleGrantedAuthority( roleEntity.getName()))
        );

        CustomUserDetail userDetails = new CustomUserDetail(email, password, authorities, userEntity);
        return new UsernamePasswordAuthenticationToken(userDetails, password, authorities);
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return UsernamePasswordAuthenticationToken.class.isAssignableFrom(authentication);
    }
}
