package me.thesis.clinic_booking.config.data_seeder;

import me.thesis.clinic_booking.entity.RoleEntity;
import me.thesis.clinic_booking.entity.UserEntity;
import me.thesis.clinic_booking.entity.enums.RoleType;
import me.thesis.clinic_booking.entity.enums.StatusUserType;
import me.thesis.clinic_booking.repository.RoleRepository;
import me.thesis.clinic_booking.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.HashSet;

@Component
public class UserDataSeeder implements ApplicationListener<ContextRefreshedEvent> {


    @Autowired
    private UserRepository userRepository;

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    @Autowired
    private RoleRepository roleRepository;

    @Override
    public void onApplicationEvent(ContextRefreshedEvent arg0) {
        if (roleRepository.findByName("ROLE_" + RoleType.SYSTEM_ADMIN.name()) == null) {
            roleRepository.save(new RoleEntity(false, new Date(), null, RoleType.SYSTEM_ADMIN.getValue()));
        }
        // System Admin account
        if (userRepository.findByEmail("sys_admin@gmail.com") == null) {
            HashSet<RoleEntity> roles = new HashSet<>();
            roles.add(roleRepository.findByName("ROLE_" + RoleType.SYSTEM_ADMIN.getValue()));

            UserEntity systemAdmin = UserEntity.builder()
                    .email("sys_admin@gmail.com")
                    .password(passwordEncoder.encode("123456789"))
                    .name("SYSTEM_ADMIN")
                    .phone("123456789")
                    .roleEntities(roles)
                    .createdTime(new Date())
                    .isDeleted(false)
                    .status(StatusUserType.ACTIVE)
                    .build();
            userRepository.save(systemAdmin);
        }
    }
}