package me.thesis.clinic_booking.dto;

import lombok.Data;
import me.thesis.clinic_booking.entity.enums.StatusBookingType;

import javax.persistence.Convert;
import javax.persistence.MappedSuperclass;
import java.util.Date;

@MappedSuperclass
@Data
public class CommonBookingInfoDto {

    private Date bookingTime;
    private String frame;

    @Convert(converter = StatusBookingType.Converter.class)
    private StatusBookingType status;
}
