package me.thesis.clinic_booking.dto;

import me.thesis.clinic_booking.entity.store.booking.CustomerBookingEntity;

import java.util.Collection;
import java.util.List;

public class ImportCustomerBookingDto {
    Collection<CustomerBookingEntity> listCustomerBooking;
    List<String> listCustomerBookingError;

    public Collection<CustomerBookingEntity> getListCustomerBooking() {
        return listCustomerBooking;
    }

    public void setListCustomerBooking(Collection<CustomerBookingEntity> listCustomerBooking) {
        this.listCustomerBooking = listCustomerBooking;
    }

    public List<String> getListCustomerBookingError() {
        return listCustomerBookingError;
    }

    public void setListCustomerBookingError(List<String> listCustomerBookingError) {
        this.listCustomerBookingError = listCustomerBookingError;
    }
}
