package me.thesis.clinic_booking.dto;

import lombok.Data;

import java.util.Date;

@Data
public class SummaryBookingOfCustomerDto {
    private Long bookingId;
    private Date bookingTime;
    private int numberFrameConsecutive;
    private String timeStartToEnd;
    private String listCategory;
    private Date createdTime;
}
