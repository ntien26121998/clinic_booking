package me.thesis.clinic_booking.dto;

import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

@Data
public class ServiceDepartmentDto {
    private Long id;
    private String name;
    private String phone;
    private String email;
    private String description;
    private Long serviceTypeId;
    private Long storeId;
    private MultipartFile image;
    private String priceList;
    private String city;
}
