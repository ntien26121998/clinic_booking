package me.thesis.clinic_booking.dto;

import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

@Data
public class StoreDto {
    private Long id;
    private Long companyId;
    private String name;
    private String phone;
    private String address;
    private String city;
    private String districtName;
    private String storeAuthId;
    private MultipartFile image;
    private String description;
    private String email;
    private String storeCode;

    @Override
    public String toString() {
        return "StoreDto{" +
                "id=" + id +
                ", companyId=" + companyId +
                ", name='" + name + '\'' +
                ", phone='" + phone + '\'' +
                ", address='" + address + '\'' +
                ", city='" + city + '\'' +
                ", districtName='" + districtName + '\'' +
                ", storeAuthId='" + storeAuthId + '\'' +
                ", image=" + image +
                ", description='" + description + '\'' +
                ", email='" + email + '\'' +
                '}';
    }
}
