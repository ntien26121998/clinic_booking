package me.thesis.clinic_booking.dto;

public interface ResponseCase {
    ResponseStatus SUCCESS = new ResponseStatus(200, "Success");
    ResponseStatus DELETE_SUCCESS = new ResponseStatus(201, "Delete success");
    ResponseStatus ADD_SUCCESS = new ResponseStatus(202, "Add success");
    ResponseStatus UPDATE_SUCCESS = new ResponseStatus(203, "Update success");
    ResponseStatus ERROR = new ResponseStatus(4, "Error");
    ResponseStatus NOT_FOUND_ENTITY = new ResponseStatus(100, "Not found Entity");
    ResponseStatus DELETE_FAIL = new ResponseStatus(401, "Delete fail");
    ResponseStatus INVALID_DATE_RANGE = new ResponseStatus(402, "Invalid date range");
    ResponseStatus DATE_MUST_NOT_OVERLAP = new ResponseStatus(403, "Date must not overlap");

    ResponseStatus INVALID_FILES_SIZE = new ResponseStatus(404, "Invalid number of files!");
    ResponseStatus NOT_FOUND_CUSTOMER = new ResponseStatus(405, "Not found customer");

    ResponseStatus HOLIDAY_CONFIG = new ResponseStatus(406, "This day is a holiday!");
    ResponseStatus HAS_NO_CONFIG = new ResponseStatus(407, "Cant find common config on this day!");
    ResponseStatus WORK_OFF_DAY = new ResponseStatus(408, "This is work off day!");
    ResponseStatus OUT_OF_FRAME_CAN_CONSECUTIVE = new ResponseStatus(409, "Out of frame can consecutive!");

    ResponseStatus DUPLICATE_TIME_BOOKING = new ResponseStatus(410, "Duplicate time booking of customer");
    ResponseStatus DUPLICATE = new ResponseStatus(411, "Duplicate patient code of customer");

}
