package me.thesis.clinic_booking.dto;

import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

@Data
public class ImageServiceDto {
    private Long id;
    private MultipartFile image;
    private String title;
    private String description;
}
