package me.thesis.clinic_booking.dto;

import lombok.Data;

import java.util.Date;

@Data
public class CustomerBookingForWebDto {
    private Long id;
    private String name;
    private String email;
    private String phone;
    private Integer gender;
    private Integer age;
    private Long patientCode;
    private Long numberBookingHasBookedInStore;
    private String listStoreNameHasBooked;
    private String storeAuthId;
    private String whoCreatedName;
    private String whoUpdatedName;
    private Date createdTime;
    private Date updatedTime;
    private Long storeId;
    private boolean isPatientCodeFromUser; //user create this patient code
    private boolean isCreatedBooking;
}
