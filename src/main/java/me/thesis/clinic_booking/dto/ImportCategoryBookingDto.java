package me.thesis.clinic_booking.dto;

import me.thesis.clinic_booking.entity.store.booking.category.CategoryBookingEntity;

import java.util.Collection;
import java.util.List;

public class ImportCategoryBookingDto {
    private Collection<CategoryBookingEntity> listCategoryBooking;
    private List<String> listCategoryBookingError;

    public Collection<CategoryBookingEntity> getListCategoryBooking() {
        return listCategoryBooking;
    }

    public void setListCategoryBooking(Collection<CategoryBookingEntity> listCategoryBooking) {
        this.listCategoryBooking = listCategoryBooking;
    }

    public List<String> getListCategoryBookingError() {
        return listCategoryBookingError;
    }

    public void setListCategoryBookingError(List<String> listCategoryBookingError) {
        this.listCategoryBookingError = listCategoryBookingError;
    }
}
