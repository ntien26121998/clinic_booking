package me.thesis.clinic_booking.dto;

import lombok.Data;
import me.thesis.clinic_booking.entity.enums.StatusBookingType;

import java.util.Date;
import java.util.Map;
import java.util.Set;

@Data
public class SaveBookingDto {
    private Long id;
    private Long storeId;
    private String storeAuthId;
    private Long serviceDepartmentId;
    private String title;
    private String customerPhone;
    private String customerName;
    private String customerEmail;
    private Long customerId;
    private String frame;
    private Date bookingDate;
    private StatusBookingType status;
    private String note;
    private String noteFromClient;
    private boolean isSpecial;
    private String description;
    private Long staffId;

    private String serviceDepartmentAuthId;
    private int typeBooking;
    private String changeReason;
    private Long memberId;
    private int numberFrameConsecutive;
    private String timeStartToEnd;

    private Set<Long> setCategoryBookingId;

    private Map<Date, String> mapBookingTimeAndStartFrame;
    private Date startDayOfBookingTime;
//    private Integer bookingFor; // 1: for self, 2: for other
//    private String customerBookingForName;
//    private String customerBookingForPhone;
//    private String customerBookingForEmail;
}
