package me.thesis.clinic_booking.dto;

import lombok.Data;
import me.thesis.clinic_booking.entity.RoleEntity;
import me.thesis.clinic_booking.entity.enums.StatusUserType;
import org.springframework.web.multipart.MultipartFile;

@Data
public class UserDto {
    private Long id;
    private String name;
    private String email;
    private String password;
    private String phone;
    private Long companyId;
    private Long storeId;
    private Long serviceDepartmentId;
//    private MultipartFile multipartFile;
    private StatusUserType status;
    private RoleEntity roleEntity;
    private boolean enableServe;
    private String description;
    private MultipartFile avatar;
}
