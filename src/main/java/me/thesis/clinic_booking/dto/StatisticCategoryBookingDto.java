package me.thesis.clinic_booking.dto;

import lombok.Data;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Data
public class StatisticCategoryBookingDto {
    private Long categoryId;
    private String categoryName;
    private Long numberFrameConsecutiveAutoFill;
    private Long numberMoney;
    private Long numberBooking;

    private Date bookingDate;

    private List<Long> listBookingId;

    public StatisticCategoryBookingDto() {

    }
    public StatisticCategoryBookingDto (long numberBooking, long numberMoney) {
        this.numberBooking = numberBooking;
        this.numberMoney = numberMoney;
    }

    public void setListBookingId(String listBookingIdString) {
        if (listBookingIdString == null || listBookingIdString.equals("")) {
            this.listBookingId = null;
        } else {
            this.listBookingId = Arrays.stream(listBookingIdString.split(",")).map(Long::parseLong).collect(Collectors.toList());
        }
    }
}
