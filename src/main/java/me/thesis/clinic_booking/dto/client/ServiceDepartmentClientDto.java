package me.thesis.clinic_booking.dto.client;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import me.thesis.clinic_booking.entity.store.ServiceDepartmentEntity;

@Data
public class ServiceDepartmentClientDto {

    private ServiceDepartmentEntity serviceDepartment;
}
