package me.thesis.clinic_booking.dto.client;

import lombok.Data;

import java.util.LinkedHashMap;
import java.util.Map;

@Data
public class SlotBookingDto {

    private boolean hasAvailableSlot;
    private Map<String, Integer> numberAvailableSlotOfFrames = new LinkedHashMap<>();

    @Override
    public String toString() {
        return "SlotBookingDto{" +
                "hasAvailableSlot=" + hasAvailableSlot +
                ", numberAvailableSlotOfFrames=" + numberAvailableSlotOfFrames +
                '}';
    }
}
