package me.thesis.clinic_booking.dto.client;

import lombok.Data;
import lombok.NoArgsConstructor;
import me.thesis.clinic_booking.entity.NotificationBookingEntity;

import java.util.List;

@Data
@NoArgsConstructor
public class NotificationBookingDto {
    List<NotificationBookingEntity> listNotification;
    private Integer numberUnread;

    public NotificationBookingDto(List<NotificationBookingEntity> listNotification, Integer numberUnread) {
        this.listNotification = listNotification;
        this.numberUnread = numberUnread;
    }
}
