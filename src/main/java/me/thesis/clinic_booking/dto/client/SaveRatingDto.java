package me.thesis.clinic_booking.dto.client;

import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

@Data
public class SaveRatingDto {
    private Long bookingId;
    private Long serviceDepartmentId;
    private String serviceDepartmentAuthId;
    private Long storeId;
    private String comment;
    private String phoneNumber;
    private MultipartFile imageCommentClient;
    private String imageUrl;
}