package me.thesis.clinic_booking.dto.client;

import lombok.Data;
import me.thesis.clinic_booking.entity.RatingEntity;

import java.util.List;

@Data
public class ServiceDepartmentRatingDto {
    private List<RatingEntity> listRatingEntity;
    private Double ratingAverageValue;
}
