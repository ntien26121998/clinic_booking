package me.thesis.clinic_booking.dto.client;

import lombok.Data;
import me.thesis.clinic_booking.entity.store.ServiceDepartmentEntity;
import me.thesis.clinic_booking.entity.store.StoreEntity;
import me.thesis.clinic_booking.entity.store.booking.BookingEntity;

import java.util.List;

@Data
public class BookingClientDetailDto {

    private BookingEntity bookingEntity;
    private StoreEntity storeEntity;
    private ServiceDepartmentEntity serviceDepartmentEntity;
    private List<Long> listCategoryIdHasChosen;

}