package me.thesis.clinic_booking.dto.client;

import lombok.Data;

import java.util.Date;

@Data
public class NewBookingMessageDto {

    private String message;
    private Date bookingDate;

    public NewBookingMessageDto(String message, Date date) {
        this.message = message;
        this.bookingDate = date;
    }
}