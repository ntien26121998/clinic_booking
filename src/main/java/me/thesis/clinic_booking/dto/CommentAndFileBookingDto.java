package me.thesis.clinic_booking.dto;

import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@Data
public class CommentAndFileBookingDto {
    private Long bookingId;
    private Long customerId;
    private Long storeId;
    private Long serviceDepartmentId;
    private String comment;
    private List<MultipartFile> listAttachFile;
}
