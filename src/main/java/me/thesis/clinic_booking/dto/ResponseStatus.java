package me.thesis.clinic_booking.dto;

import lombok.Data;

@Data
public class ResponseStatus {
    public int code;
    public String message;

    public ResponseStatus() {
    }

    public ResponseStatus(int code, String message) {
        this.code = code;
        this.message = message;
    }
}
