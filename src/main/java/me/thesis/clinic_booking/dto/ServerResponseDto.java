package me.thesis.clinic_booking.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
//@AllArgsConstructor
//@NoArgsConstructor
public class ServerResponseDto {
    private ResponseStatus status;
    private Object data;

    public ServerResponseDto(ResponseStatus responseStatus) {
        this.status = responseStatus;
    }

    public ServerResponseDto(ResponseStatus responseStatus, Object data) {
        this.status = responseStatus;
        this.data = data;
    }
//
//    public ServerResponseDto(ResponseStatus responseStatus, boolean isSuccess) {
//        this.status = responseStatus;
//        this.data = true;
//    }

}
