package me.thesis.clinic_booking.dto;

import lombok.Data;
import me.thesis.clinic_booking.entity.store.booking.BookingEntity;
import me.thesis.clinic_booking.entity.store.booking.CommentBookingEntity;
import me.thesis.clinic_booking.entity.store.booking.CustomerBookingEntity;
import me.thesis.clinic_booking.entity.store.booking.category.CategoryBookingEntity;

import java.util.List;

@Data
public class DetailBookingInfoDto {
    private BookingEntity bookingEntity;
    private CustomerBookingEntity customer;
    private List<CategoryBookingEntity> listCategory;
    private List<Long> listCategoryIdHasChosen;
    private List<CommentBookingEntity> commentBookingEntities;

    public DetailBookingInfoDto(BookingEntity bookingEntity, CustomerBookingEntity customer) {
        this.bookingEntity = bookingEntity;
        this.customer = customer;
    }
}
