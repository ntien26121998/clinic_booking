package me.thesis.clinic_booking.service;

import me.thesis.clinic_booking.dto.client.SaveRatingDto;
import me.thesis.clinic_booking.dto.client.ServiceDepartmentRatingDto;
import me.thesis.clinic_booking.entity.CommentOfRatingEntity;
import me.thesis.clinic_booking.entity.RatingEntity;
import me.thesis.clinic_booking.entity.enums.CommentRatingType;
import me.thesis.clinic_booking.repository.CommentOfRatingRepository;
import me.thesis.clinic_booking.repository.RatingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class RatingService {

    @Autowired
    private RatingRepository ratingRepository;

    @Autowired
    private CommentOfRatingRepository commentOfRatingRepository;

    @Autowired
    private UploadFileLocalService uploadFileService;


    public boolean save(SaveRatingDto ratingDto, HttpServletRequest request) {
        RatingEntity ratingEntityAfterSaving = ratingRepository.save(convertFromDto(ratingDto));
        String comment = ratingDto.getComment();
        if (comment != null && !"".equals(comment)) {
            CommentOfRatingEntity commentOfRatingEntity = new CommentOfRatingEntity();
            commentOfRatingEntity.setType(CommentRatingType.CLIENT_COMMENT);
            commentOfRatingEntity.setRatingServiceId(ratingEntityAfterSaving.getId());
            commentOfRatingEntity.setComment(comment);
            commentOfRatingEntity.setCreatedTime(new Date());
            if(ratingDto.getImageCommentClient() != null) {
                String url = uploadFileService.uploadFile("commentClient", ratingDto.getImageCommentClient(), request);
                commentOfRatingEntity.setImageUrl(url);
            }
            commentOfRatingRepository.save(commentOfRatingEntity);
        }
        return true;
    }

    private RatingEntity convertFromDto(SaveRatingDto dto) {
        RatingEntity ratingEntity = new RatingEntity();
        ratingEntity.setPhoneNumber(dto.getPhoneNumber());
        ratingEntity.setServiceDepartmentId(dto.getServiceDepartmentId());
        ratingEntity.setStoreId(dto.getStoreId());
        ratingEntity.setBookingId(dto.getBookingId());
        ratingEntity.setCreatedTime(new Date());
        return ratingEntity;
    }

    public ServiceDepartmentRatingDto findByServiceDepartment(Long serviceDepartmentId) {
        ServiceDepartmentRatingDto serviceDepartmentRatingDto = new ServiceDepartmentRatingDto();
        List<RatingEntity> listRating = ratingRepository.findByServiceDepartmentIdOrderByCreatedTimeDesc(serviceDepartmentId);
        if (listRating.size() > 0) {
            List<CommentOfRatingEntity> allComment = commentOfRatingRepository.findAllByOrderByCreatedTimeDesc();
            Map<Long, List<CommentOfRatingEntity>> mapCommentAndRatingId
                    = allComment.stream().collect(Collectors.groupingBy(CommentOfRatingEntity::getRatingServiceId));

            listRating.forEach(ratingEntity -> {
                ratingEntity.setListComment(mapCommentAndRatingId.get(ratingEntity.getId()));
                String currentPhoneNumber = ratingEntity.getPhoneNumber();
                String phoneWithHideLast4Number = currentPhoneNumber.replaceAll(currentPhoneNumber.substring(3,6), "****");
                ratingEntity.setPhoneNumber(phoneWithHideLast4Number);
            });
        }
        serviceDepartmentRatingDto.setListRatingEntity(listRating);
        return serviceDepartmentRatingDto;
    }

    public List<RatingEntity> findByPhone(String phone) {
        return ratingRepository.findByPhoneNumber(phone);
    }
}

