package me.thesis.clinic_booking.service;

import me.thesis.clinic_booking.entity.BaseEntity;
import me.thesis.clinic_booking.repository.BaseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Optional;

@Service
public abstract class BaseService<T extends BaseEntity, R extends BaseRepository<T>> {
    @Autowired
    protected R repository;

//
//    @Autowired
//    private AuthenticateInfoHolder authenticateInfoHolder;
//

    public Optional<T> findById(Long entityId) {
        return repository.findById(entityId);
    }

    public boolean deleteEntity(Long entityId) {
        Optional<T> entityOptional = findById(entityId);
        if (entityOptional.isEmpty()) {
            return false;
        }
        T entity = entityOptional.get();
        entity.setDeleted(true);
        entity.setUpdatedTime(new Date());
        repository.save(entity);
        return true;
    }

    public boolean saveOrUpdate(T entityAfterSettingItSelfValue) {
        Long entityId = entityAfterSettingItSelfValue.getId();

        if (entityId == null) {
//            entityAfterSettingItSelfValue.setCreatedBy(getCurrentUserIdLoggedIn());
            entityAfterSettingItSelfValue.setCreatedTime(new Date());
        } else {
            if (findById(entityId).isEmpty()) {
                return false;
            }
            entityAfterSettingItSelfValue.setUpdatedTime(new Date());
        }
        repository.save(entityAfterSettingItSelfValue);
        return true;
    }

//    private CustomerUserDetails getCurrentUserLoggedIn(){
//        return authenticateInfoHolder.getAuthenxxx();
//    }
//
//    private Long getCurrentUserIdLoggedIn(){
//        return authenticateInfoHolder.getAuthenxxx().getId();
//    }
}


