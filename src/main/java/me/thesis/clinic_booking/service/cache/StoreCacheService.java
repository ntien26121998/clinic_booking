package me.thesis.clinic_booking.service.cache;

import me.thesis.clinic_booking.entity.store.StoreEntity;
import me.thesis.clinic_booking.service.store.StoreService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StoreCacheService {

    private static final Logger LOGGER = LoggerFactory.getLogger(StoreCacheService.class);

    @Autowired
    private StoreService storeService;

    private List<StoreEntity> listStore;

    @EventListener(ApplicationReadyEvent.class)
    public void buildCache() {
        buildStoreCache();
    }

    public void buildStoreCache() {
        LOGGER.info("Build store cache");
        this.listStore = storeService.findAll();
    }

    public StoreEntity getByIdFromCache(Long id) {
        for (StoreEntity storeEntity : this.listStore) {
            if (storeEntity.getId().equals(id)) {
                return storeEntity;
            }
        }
        LOGGER.info("Not found store with id {} from cache", id);
        return null;
    }

    public StoreEntity getByAuthIdFromCache(String authId) {
        for (StoreEntity storeEntity : this.listStore) {
            if (storeEntity.getStoreAuthId().equals(authId)) {
                return storeEntity;
            }
        }
        LOGGER.info("Not found store with authId {} from cache", authId);
        return null;
    }

}
