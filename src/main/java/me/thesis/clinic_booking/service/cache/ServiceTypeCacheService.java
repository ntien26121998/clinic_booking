package me.thesis.clinic_booking.service.cache;

import me.thesis.clinic_booking.entity.store.ServiceTypeEntity;
import me.thesis.clinic_booking.service.ServiceTypeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ServiceTypeCacheService {

    private Logger LOGGER = LoggerFactory.getLogger(ServiceTypeCacheService.class);

    @Autowired
    private ServiceTypeService serviceTypeService;

    private List<ServiceTypeEntity> listServiceType;

    @EventListener(ApplicationReadyEvent.class)
    public void buildCache() {
        buildServiceTypeCache();
    }

    public void buildServiceTypeCache() {
        LOGGER.info("Build service type cache");
        this.listServiceType = serviceTypeService.findAll();
    }

    public ServiceTypeEntity getByIdFromCache(Long id) {
        for (ServiceTypeEntity serviceTypeEntity : this.listServiceType) {
            if (serviceTypeEntity.getId().equals(id)) {
                return serviceTypeEntity;
            }
        }
        LOGGER.info("Not found service type with id {} from cache", id);
        return null;
    }

    public List<ServiceTypeEntity> getListServiceTypeFromCache() {
        return this.listServiceType;
    }

    public List<ServiceTypeEntity> getListServiceTypeByStoreIdFromCache(Long storeId) {
        return this.listServiceType.stream()
                .filter(serviceTypeEntity -> serviceTypeEntity.getStoreId().equals(storeId))
                .collect(Collectors.toList());
    }
}
