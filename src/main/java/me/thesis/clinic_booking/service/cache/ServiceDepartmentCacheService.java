package me.thesis.clinic_booking.service.cache;

import me.thesis.clinic_booking.entity.store.ServiceDepartmentEntity;
import me.thesis.clinic_booking.service.store.ServiceDepartmentService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ServiceDepartmentCacheService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ServiceDepartmentCacheService.class);

    @Autowired
    private ServiceDepartmentService serviceDepartmentService;

    private List<ServiceDepartmentEntity> listServiceDepartment;

    @EventListener(ApplicationReadyEvent.class)
    public void buildCache() {
        buildServiceDepartmentCache();
    }

    public void buildServiceDepartmentCache() {
        LOGGER.info("Build service department cache");
        this.listServiceDepartment = serviceDepartmentService.findAll();
    }

    public ServiceDepartmentEntity getByIdFromCache(Long id) {
        for (ServiceDepartmentEntity serviceDepartmentEntity : this.listServiceDepartment) {
            if (serviceDepartmentEntity.getId().equals(id)) {
                return serviceDepartmentEntity;
            }
        }
        LOGGER.info("Not found service department with id {} from cache", id);
        return null;
    }

    public ServiceDepartmentEntity getByAuthIdFromCache(String authId) {
        for (ServiceDepartmentEntity serviceDepartmentEntity : this.listServiceDepartment) {
            if (serviceDepartmentEntity.getAuthId().equals(authId)) {
                return serviceDepartmentEntity;
            }
        }
        LOGGER.info("Not found service department with authId {} from cache", authId);
        return null;
    }

    public ServiceDepartmentEntity getByAuthIdAndStoreIdFromCache(String authId, Long storeId) {
        for (ServiceDepartmentEntity serviceDepartmentEntity : this.listServiceDepartment) {
            if (serviceDepartmentEntity.getAuthId().equals(authId) && serviceDepartmentEntity.getStoreId().equals(storeId)) {
                return serviceDepartmentEntity;
            }
        }
        LOGGER.info("Not found store with authId {} and storeId {} from cache", authId, storeId);
        return null;
    }

    public List<ServiceDepartmentEntity> getByStoreId(Long storeId) {
        return this.listServiceDepartment.stream()
                .filter(serviceDepartmentEntity -> serviceDepartmentEntity.getStoreId().equals(storeId))
                .collect(Collectors.toList());
    }
}
