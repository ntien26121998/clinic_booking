package me.thesis.clinic_booking.service;

import me.thesis.clinic_booking.entity.store.ServiceTypeEntity;
import me.thesis.clinic_booking.repository.ServiceTypeRepository;
import me.thesis.clinic_booking.service.cache.ServiceTypeCacheService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class ServiceTypeService {

    @Autowired
    private ServiceTypeRepository serviceTypeRepository;

    @Autowired
    private PageService pageService;

    @Autowired
    private ServiceTypeCacheService serviceTypeCacheService;


    public Page<ServiceTypeEntity> findByStoreId(String sortDir, String sortField, int page, int size, Long storeId) {
        Pageable pageable = pageService.getPage(sortDir, sortField, page, size);
        return serviceTypeRepository.findByStoreId(storeId, pageable);
    }

    public List<ServiceTypeEntity> getListByStoreId(Long storeId) {
        return serviceTypeRepository.findByStoreId(storeId);
    }

    public List<ServiceTypeEntity> findAll() {
        return serviceTypeRepository.findAll();
    }

    public ServiceTypeEntity findById(Long id) {
        return serviceTypeRepository.findById(id).orElse(null);
    }

    public boolean save(ServiceTypeEntity serviceTypeEntityForSaving){
        if(serviceTypeEntityForSaving.getId() != null) {
            ServiceTypeEntity oldServiceTypeEntity = findById(serviceTypeEntityForSaving.getId());
            if(oldServiceTypeEntity == null){
                return false;
            }
            oldServiceTypeEntity.setName(serviceTypeEntityForSaving.getName());
            oldServiceTypeEntity.setUpdatedTime(new Date());
            oldServiceTypeEntity.setStoreId(serviceTypeEntityForSaving.getStoreId());
            saveAndCache(oldServiceTypeEntity);
        } else {
            serviceTypeEntityForSaving.setCreatedTime(new Date());
            serviceTypeEntityForSaving.setDeleted(false);
            saveAndCache(serviceTypeEntityForSaving);
        }
        return true;
    }

    public boolean delete(Long id) {
        ServiceTypeEntity oldServiceTypeEntity = findById(id);
        if(oldServiceTypeEntity == null){
            return false;
        } else {
            oldServiceTypeEntity.setDeleted(true);
            oldServiceTypeEntity.setUpdatedTime(new Date());
            saveAndCache(oldServiceTypeEntity);
            return true;
        }
    }

    private void saveAndCache(ServiceTypeEntity serviceTypeEntity){
        serviceTypeRepository.save(serviceTypeEntity);
        serviceTypeCacheService.buildServiceTypeCache();
    }
}

