package me.thesis.clinic_booking.service.security;

import me.thesis.clinic_booking.entity.UserEntity;
import me.thesis.clinic_booking.entity.enums.StatusUserType;
import me.thesis.clinic_booking.repository.UserRepository;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class AuthenticationService {
    private final UserRepository userRepository;

    private final BCryptPasswordEncoder passwordEncoder;

    public AuthenticationService(UserRepository userRepository, BCryptPasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    public UserEntity validateUser(String email, String password) {
        UserEntity userEntity = userRepository.findByEmail(email);
        if (userEntity == null) {
            return null;
        }
        boolean isMatchPassword = passwordEncoder.matches(password, userEntity.getPassword());
        boolean isActive = userEntity.getStatus().equals(StatusUserType.ACTIVE);
        return isMatchPassword && isActive ? userEntity : null ;
    }
}
