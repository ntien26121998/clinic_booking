package me.thesis.clinic_booking.service.security;

import me.thesis.clinic_booking.config.security.CustomUserDetailHolder;
import me.thesis.clinic_booking.entity.RoleEntity;
import me.thesis.clinic_booking.entity.enums.RoleType;
import me.thesis.clinic_booking.entity.store.ServiceDepartmentEntity;
import me.thesis.clinic_booking.entity.store.StoreEntity;
import me.thesis.clinic_booking.service.RoleService;
import me.thesis.clinic_booking.service.store.ServiceDepartmentService;
import me.thesis.clinic_booking.service.store.StoreService;
import org.springframework.stereotype.Service;

@Service("preAuthorizeService")
public class PreAuthorizeService {

    private final StoreService storeService;

    private final ServiceDepartmentService serviceDepartmentService;

    private final RoleService roleService;

    private final CustomUserDetailHolder customUserDetailHolder;

    public PreAuthorizeService(StoreService storeService, ServiceDepartmentService serviceDepartmentService,
                               RoleService roleService, CustomUserDetailHolder customUserDetailHolder) {
        this.storeService = storeService;
        this.serviceDepartmentService = serviceDepartmentService;
        this.roleService = roleService;
        this.customUserDetailHolder = customUserDetailHolder;
    }

    public boolean isCanAccessSystem() {
        return roleService.isSystemAdmin(customUserDetailHolder.getUserEntity().getId());
    }

    public boolean isCanAccessStore(String storeAuthId) {
        StoreEntity storeEntity = storeService.findByStoreAuthId(storeAuthId);
        Long userId = customUserDetailHolder.getUserEntity().getId();
        System.out.println(storeEntity);
        return roleService.isSystemAdmin(userId) || roleService.isStoreAdmin(userId, storeEntity.getId());
    }

    public boolean isCanAccessServiceDepartment(String serviceDepartmentAuthId) {
        ServiceDepartmentEntity serviceDepartmentEntity = serviceDepartmentService.findByAuthId(serviceDepartmentAuthId);
        Long userId = customUserDetailHolder.getUserEntity().getId();
        boolean isSystemAdmin = roleService.isSystemAdmin(userId);
        boolean isStoreAdmin = roleService.isStoreAdmin(userId, serviceDepartmentEntity.getStoreId());
        boolean isServiceDepartmentAdmin = roleService.isServiceDepartmentAdmin(userId, serviceDepartmentEntity.getStoreId(), serviceDepartmentEntity.getId());
        return isSystemAdmin || isStoreAdmin || isServiceDepartmentAdmin;
    }

    public boolean isCanAccessClientAPI(){
        for(RoleEntity roleEntity : customUserDetailHolder.getUserEntity().getRoleEntities()){
            if(roleEntity.getName().equals("ROLE_" + RoleType.CLIENT_WITH_PHONE.getValue())){
                return true;
            }
        }
        return false;
    }
}