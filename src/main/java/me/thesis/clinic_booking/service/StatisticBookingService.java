package me.thesis.clinic_booking.service;

import me.thesis.clinic_booking.dto.StatisticCategoryBookingDto;
import me.thesis.clinic_booking.repository.store.booking.CategoryBookingRepository;
import me.thesis.clinic_booking.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
public class StatisticBookingService {

    @Autowired
    private CategoryBookingRepository categoryBookingRepository;

    //statistic by number money and number choose of category per day in range
    public Map<String, Object> getStatisticInfo(Long serviceDepartmentId, Date startRangeBookingTime, Date endTimeBookingTime, List<Long> listCategoryId) {
        List<Object[]> listCategoryStatisticDataFromQuery = getDataStatisticFromDB(serviceDepartmentId, startRangeBookingTime, endTimeBookingTime, listCategoryId);

        long totalMoney = 0L;
        Set<Long> setOfBookingId = new HashSet<>();
        Map<Date, Set<Long>> mapDayStatisticAndListBookingIdOfThisDay = new LinkedHashMap<>();
        Map<Date, Long> mapDayStatisticAndNumberMoneyOfThisDay = new LinkedHashMap<>();
        List<StatisticCategoryBookingDto> statisticCategoryBookingDtoList = new ArrayList<>();

        for (Object[] arrayValue : listCategoryStatisticDataFromQuery) {
            StatisticCategoryBookingDto statisticCategoryBookingDto = convertFromQueryValue(arrayValue);
            statisticCategoryBookingDtoList.add(statisticCategoryBookingDto);

            long money = Long.parseLong(String.valueOf(statisticCategoryBookingDto.getNumberMoney()));
            totalMoney += money;

            List<Long> bookingIdsOfCategory = statisticCategoryBookingDto.getListBookingId();
            Date dayStatistic = statisticCategoryBookingDto.getBookingDate();
            Set<Long> listBookingIdOfDay = mapDayStatisticAndListBookingIdOfThisDay.getOrDefault(dayStatistic, new HashSet<>());
            listBookingIdOfDay.addAll(bookingIdsOfCategory);

            Long numberMoneyOfDay = mapDayStatisticAndNumberMoneyOfThisDay.getOrDefault(dayStatistic, 0L);
            numberMoneyOfDay += statisticCategoryBookingDto.getNumberMoney();

            setOfBookingId.addAll(bookingIdsOfCategory);
            mapDayStatisticAndListBookingIdOfThisDay.put(dayStatistic, listBookingIdOfDay);
            mapDayStatisticAndNumberMoneyOfThisDay.put(dayStatistic, numberMoneyOfDay);
        }

        List<Integer> listNumberBookingOfDays = mapDayStatisticAndListBookingIdOfThisDay.values().stream().map(Set::size).collect(Collectors.toList());

        Map<String, Object> mapCategoryNameAndListNumberStatistic = new LinkedHashMap<>();

        mapCategoryNameAndListNumberStatistic.put("numberForDays", new ArrayList<>() {{
            add(listNumberBookingOfDays);
            add(mapDayStatisticAndNumberMoneyOfThisDay.values());
        }});

        Set<Date> setDayStatistic = mapDayStatisticAndNumberMoneyOfThisDay.keySet();

        //if null: not show category statistic detail in client
        if (listCategoryId != null && !listCategoryId.isEmpty()) {
            addDefaultStatisticOnDaysNotHasStatisticForCategory(statisticCategoryBookingDtoList, setDayStatistic, mapCategoryNameAndListNumberStatistic);
            addDefaultStatisticInfoForCategoryNotHasBooking(listCategoryId, serviceDepartmentId, setDayStatistic, mapCategoryNameAndListNumberStatistic);
        }

        Map<String, Object> mapResult = new HashMap<>();
        mapResult.put("totalMoney", totalMoney);
        mapResult.put("numberBooking", setOfBookingId.size());
        mapResult.put("listDayStatistic", setDayStatistic);
        mapResult.put("mapCategoryAndListNumberStatistic", mapCategoryNameAndListNumberStatistic);
        return mapResult;
    }

    private List<Object[]> getDataStatisticFromDB(Long serviceDepartmentId, Date startRangeBookingTime, Date endTimeBookingTime, List<Long> listCategoryId) {
        //remove default id when not choosen statistic;
        if (listCategoryId != null) {
            listCategoryId.removeIf(categoryId -> categoryId.equals(0L));
            listCategoryId = listCategoryId.isEmpty() ? null : listCategoryId;
        }
        Date startOfStatisticRange = startRangeBookingTime == null ? null : DateUtils.getStartOfDay(startRangeBookingTime);
        Date endOfStatisticRange = endTimeBookingTime == null ? null : DateUtils.getEndOfDay(endTimeBookingTime);
        return categoryBookingRepository.getListCategoryStatistic(serviceDepartmentId, listCategoryId, startOfStatisticRange, endOfStatisticRange);
    }

    // problem context: list day may be 5 element, but some category may have statistic on 1 or 2 or 3 day
    // => must add defaut statistic for days when category not have statistic
    private void addDefaultStatisticOnDaysNotHasStatisticForCategory(List<StatisticCategoryBookingDto> statisticCategoryBookingDtoList,
                                                                     Set<Date> setOfDayStatistic,
                                                                     Map<String, Object> currentMapCategoryNameAndListNumberStatistic) {
        Map<String, Map<Date, StatisticCategoryBookingDto>> mapDayAndStatisticDtoByCategoryNameFromQuery =
                statisticCategoryBookingDtoList.stream()
                        .collect(Collectors.groupingBy(StatisticCategoryBookingDto::getCategoryName,
                                Collectors.toMap(StatisticCategoryBookingDto::getBookingDate, Function.identity())));
        Map<Date, StatisticCategoryBookingDto> defaultMapAllDayAndNumberStatistic =
                Collections.unmodifiableMap(getDefaultMapDayAndNumberStatistic(setOfDayStatistic));

        Map<String, Map<Date, StatisticCategoryBookingDto>> mapAllDayAndNumberStatisticByCategoryName = new HashMap<>();

        mapDayAndStatisticDtoByCategoryNameFromQuery.forEach((categoryName, mapDayAndNumberStatistic) -> {
            Map<Date, StatisticCategoryBookingDto> mapAllDayAndNumberStatistic
                    = mapAllDayAndNumberStatisticByCategoryName.getOrDefault(categoryName, new TreeMap<>(defaultMapAllDayAndNumberStatistic));

            mapAllDayAndNumberStatistic.putAll(mapDayAndNumberStatistic);
            mapAllDayAndNumberStatisticByCategoryName.put(categoryName, mapAllDayAndNumberStatistic);
        });

        // category name: [StatisticCategoryBookingDto of day 1, StatisticCategoryBookingDto of day 2, ... ], this's size is based on list day statistic
        mapAllDayAndNumberStatisticByCategoryName.forEach((categoryName, mapAllDayAndNumberStatistic) ->
                currentMapCategoryNameAndListNumberStatistic.put(categoryName, mapAllDayAndNumberStatistic.values()));

    }

    private Map<Date, StatisticCategoryBookingDto> getDefaultMapDayAndNumberStatistic(Set<Date> setDayStatistic) {
        Map<Date, StatisticCategoryBookingDto> mapDayAndNumberStatistic = new TreeMap<>();
        setDayStatistic.forEach(day -> mapDayAndNumberStatistic.put(day, new StatisticCategoryBookingDto(0L, 0L)));
        return mapDayAndNumberStatistic;
    }

    private void addDefaultStatisticInfoForCategoryNotHasBooking(List<Long> listCategoryId,
                                                                 Long serviceDepartmentId,
                                                                 Set<Date> setOfDayStatistic,
                                                                 Map<String, Object> currentMapCategoryNameAndListNumberStatistic) {
        List<String> listCategoryName = categoryBookingRepository.findCategoryNameByIdInAndServiceDepartmentIdAndIsDeleted(listCategoryId, serviceDepartmentId);
        Map<Date, StatisticCategoryBookingDto> defaultMapDayAndNumberStatistic = getDefaultMapDayAndNumberStatistic(setOfDayStatistic);
        listCategoryName.forEach(categoryName ->
                currentMapCategoryNameAndListNumberStatistic.computeIfAbsent(categoryName, mapKey -> defaultMapDayAndNumberStatistic.values()));
    }

    private StatisticCategoryBookingDto convertFromQueryValue(Object[] arrayValue) {
        StatisticCategoryBookingDto statisticCategoryBookingDto = new StatisticCategoryBookingDto();
        Long categoryId = Long.parseLong(String.valueOf(arrayValue[0]));
        statisticCategoryBookingDto.setCategoryId(categoryId);
        statisticCategoryBookingDto.setCategoryName(String.valueOf(arrayValue[1]));
        statisticCategoryBookingDto.setNumberFrameConsecutiveAutoFill(Long.parseLong(String.valueOf(arrayValue[2])));
        long money = Long.parseLong(String.valueOf(arrayValue[3]));
        statisticCategoryBookingDto.setNumberMoney(money);
        statisticCategoryBookingDto.setNumberBooking(Long.parseLong(String.valueOf(arrayValue[4])));
        statisticCategoryBookingDto.setBookingDate((Date) arrayValue[5]);
        statisticCategoryBookingDto.setListBookingId(String.valueOf(arrayValue[6]));
        return statisticCategoryBookingDto;
    }

}

