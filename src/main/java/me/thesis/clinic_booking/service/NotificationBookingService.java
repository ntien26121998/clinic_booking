package me.thesis.clinic_booking.service;

import me.thesis.clinic_booking.dto.client.NotificationBookingDto;
import me.thesis.clinic_booking.entity.NotificationBookingEntity;
import me.thesis.clinic_booking.entity.UserEntity;
import me.thesis.clinic_booking.repository.NotificationBookingRepository;
import me.thesis.clinic_booking.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class NotificationBookingService {

    @Autowired
    private NotificationBookingRepository notificationBookingRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PageService pageService;

    @Autowired
    private RoleService roleService;

    public void setLatestTimeReadNotification(UserEntity userEntity) {
        userEntity.setLatestTimeReadingNotification(new Date());
        userRepository.save(userEntity);
    }

    public NotificationBookingDto getListNotification(UserEntity userEntity, int page, Long serviceDepartmentId) {
        Pageable pageable = pageService.getPage("desc", "createdTime", page, 8);
        Page<NotificationBookingEntity> pageResult;

        Long userId = userEntity.getId();
        Long storeId = userEntity.getStoreId();

        boolean isStoreAdmin = roleService.isStoreAdmin(userId, storeId);
        boolean isSystemAdmin = roleService.isSystemAdmin(userId);

        boolean isServiceDepartmentAdmin = roleService.isServiceDepartmentAdmin(userId, storeId, serviceDepartmentId);

         if (isSystemAdmin || isStoreAdmin || isServiceDepartmentAdmin) {
            pageResult = notificationBookingRepository.findByServiceDepartmentIdAndStaffIdIsNull(serviceDepartmentId, pageable);
        } else {
            pageResult = notificationBookingRepository.findByClientPhone(userEntity.getPhone(), pageable);
        }

        NotificationBookingDto notificationBookingDto = new NotificationBookingDto();
        notificationBookingDto.setListNotification(pageResult.getContent());
        if (page == 1) {
            notificationBookingDto.setNumberUnread(getNumberUnreadNotification(userEntity, serviceDepartmentId));
        }
        return notificationBookingDto;
    }

    private Integer getNumberUnreadNotification(UserEntity userEntity, Long serviceDepartmentId) {
        Long userId = userEntity.getId();
        Long storeId = userEntity.getStoreId();
        Date latestTimeReadingNotification = userEntity.getLatestTimeReadingNotification();

        boolean isStoreAdmin = roleService.isStoreAdmin(userId, storeId);
        boolean isSystemAdmin = roleService.isSystemAdmin(userId);
        boolean isServiceDepartmentAdmin = roleService.isServiceDepartmentAdmin(userId, storeId, serviceDepartmentId);
        if (isSystemAdmin || isStoreAdmin || isServiceDepartmentAdmin) {
            latestTimeReadingNotification = latestTimeReadingNotification == null ? userEntity.getCreatedTime() : latestTimeReadingNotification;
            return notificationBookingRepository.countByServiceDepartmentIdAndStaffIdIsNullAndCreatedTimeAfter(serviceDepartmentId, latestTimeReadingNotification);

        } else {
            return notificationBookingRepository.countByClientPhone(userEntity.getPhone());
        }
    }

    @Async
    public void save(NotificationBookingEntity notificationBookingEntity) {
        notificationBookingRepository.save(notificationBookingEntity);
    }
}

