package me.thesis.clinic_booking.service;

import com.google.common.base.Objects;
import me.thesis.clinic_booking.config.security.CustomUserDetail;
import me.thesis.clinic_booking.entity.company.CompanyEntity;
import me.thesis.clinic_booking.entity.store.StoreEntity;
import me.thesis.clinic_booking.service.company.CompanyService;
import me.thesis.clinic_booking.service.store.StoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class RedirectService {

    @Autowired
    private CompanyService companyService;
    @Autowired
    private StoreService storeService;

    public String getDefaultRedirectUri(CustomUserDetail userDetails) {
        boolean hasSystemAdminRole = false;
        boolean hasCompanyAdminRole = false;
        boolean hasStoreAdminRole = false;
        String uri = null;
        Collection<GrantedAuthority> auths = userDetails.getAuthorities();
        for (GrantedAuthority auth : auths) {
            String SYSTEM_ADMIN_ROLE = "ROLE_SYSTEM_ADMIN";
            if (Objects.equal(auth.getAuthority(), SYSTEM_ADMIN_ROLE)) {
                hasSystemAdminRole = true;
            }
            String STORE_ADMIN_ROLE = "ROLE_STORE_ADMIN";
            if (Objects.equal(auth.getAuthority(), STORE_ADMIN_ROLE)) {
                hasStoreAdminRole = true;
            }
            String COMPANY_ADMIN_ROLE = "ROLE_COMPANY_ADMIN";
            if (Objects.equal(auth.getAuthority(), COMPANY_ADMIN_ROLE)) {
                hasCompanyAdminRole = true;
            }
        }
        String companyAuthId = "";
        if (userDetails.getUserEntity().getCompanyId() != null) {
            CompanyEntity companyEntity = companyService.findOne(userDetails.getUserEntity().getCompanyId());
            companyAuthId = companyEntity.getCompanyAuthId();
        }
        String storeAuthId = "";
        if (userDetails.getUserEntity().getStoreId() != null) {
            StoreEntity storeEntity = storeService.findById(userDetails.getUserEntity().getStoreId());
            storeAuthId = storeEntity.getStoreAuthId();
        }
        if (hasSystemAdminRole) {
            uri = "system/companies";
        } else if (hasCompanyAdminRole) {
            uri = "/company/" + companyAuthId + "/home";
        }else if(hasStoreAdminRole){
            uri = "/store/" + storeAuthId + "/notificationForStore";
        }
        return uri;
    }
}
