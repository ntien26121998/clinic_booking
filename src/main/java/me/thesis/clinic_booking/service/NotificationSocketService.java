package me.thesis.clinic_booking.service;

import me.thesis.clinic_booking.dto.client.NewBookingMessageDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

@Service
public class NotificationSocketService {

    @Autowired
    private SimpMessagingTemplate messagingTemplate;

    public void notifyToServiceDepartmentAdmin(NewBookingMessageDto notification, String serviceDepartmentAuthId) {
        messagingTemplate.convertAndSend("/service_department/" + serviceDepartmentAuthId, notification);
    }
}
