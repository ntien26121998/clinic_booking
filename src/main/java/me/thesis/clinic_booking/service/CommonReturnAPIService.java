package me.thesis.clinic_booking.service;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class CommonReturnAPIService {

    public ResponseEntity<String> returnOnSaveAPI(boolean isSaveSuccess){
        if (isSaveSuccess) {
            return ResponseEntity.ok("SAVE_SUCCESS");
        } else {
            return ResponseEntity.ok("BAD_REQUEST");
        }
    }

    public ResponseEntity<String> returnOnDeleteAPI(boolean isDeleteSuccess){
        if (isDeleteSuccess) {
            return ResponseEntity.ok("DELETE_SUCCESS");
        } else {
            return ResponseEntity.ok("BAD_REQUEST");
        }
    }
}