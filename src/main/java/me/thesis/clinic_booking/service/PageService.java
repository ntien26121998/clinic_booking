package me.thesis.clinic_booking.service;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

@Service
public class PageService {
    public Pageable getPage(String sortDir, String sortField, int page, int size) {
        Pageable pageable;
        if ("ASC".equalsIgnoreCase(sortDir)) {
            pageable = PageRequest.of(page - 1, size, Sort.Direction.ASC, sortField);
        } else {
            pageable = PageRequest.of(page - 1, size, Sort.Direction.DESC, sortField);
        }
        return pageable;
    }

    public Sort.Direction getDirection(String sortDir){
        return "ASC".equalsIgnoreCase(sortDir) ? Sort.Direction.ASC : Sort.Direction.DESC;
    }
}
