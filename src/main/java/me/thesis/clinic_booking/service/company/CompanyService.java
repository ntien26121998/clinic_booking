package me.thesis.clinic_booking.service.company;

import me.thesis.clinic_booking.entity.company.CompanyEntity;
import me.thesis.clinic_booking.repository.company.CompanyRepository;
import me.thesis.clinic_booking.service.BaseService;
import me.thesis.clinic_booking.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class CompanyService extends BaseService<CompanyEntity, CompanyRepository> {

    @Autowired
    private CompanyRepository companyRepository;

    @Autowired
    private RoleService roleService;

    public CompanyEntity findOne(Long id) {
        return companyRepository.findById(id).orElse(null);
    }

    public Long getIdFromAuthId(String authId) {
        return companyRepository.findByCompanyAuthId(authId).getId();
    }

    public CompanyEntity findByCompanyId(String companyId) {
        return companyRepository.findByCompanyAuthIdAndIsDeleted(companyId, false);
    }

//    public Page<CompanyEntity> findActiveCompany(Long appId, Pageable pageable) {
//        return companyRepository.findByAppIdAndIsDeleted(appId, false, pageable);
//    }

//    public boolean updateCompany(CompanyEntity companyEntity, String companyIdString) {
//        CompanyEntity oldCompany = companyRepository.findByCompanyAuthId(companyIdString);
//        if (oldCompany == null) {
//            return false;
//        }
//        oldCompany.setName(companyEntity.getName());
//        oldCompany.setFirstName(companyEntity.getFirstName());
//        oldCompany.setAddress(companyEntity.getAddress());
//        oldCompany.setUpdatedTime(new Date());
//        saveAndBuildCache(oldCompany);
//        return true;
//    }
//
//    @Transactional
//    public boolean createCompany(CompanyEntity companyEntity,String appAuthId) {
//        MobileApplicationEntity applicationEntity = appService.findByAppId(appAuthId);
//        companyEntity.setAppId(applicationEntity.getId());
//        companyEntity.setCompanyAuthId(String.valueOf((new Date()).getTime()));
//        companyEntity.setCreatedTime(new Date());
//        companyEntity.setDeleted(false);
//        saveAndBuildCache(companyEntity);
//        RoleEntity roleEntity = new RoleEntity(Constant.COMPANY_ADMIN, companyEntity.getId(), Constant.ROLE_TYPE_COMPANY);
//        roleService.saveRole(roleEntity);
//        return true;
//    }
}
