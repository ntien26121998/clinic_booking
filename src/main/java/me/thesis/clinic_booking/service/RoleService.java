package me.thesis.clinic_booking.service;

import me.thesis.clinic_booking.config.security.CustomUserDetail;
import me.thesis.clinic_booking.entity.RoleEntity;
import me.thesis.clinic_booking.entity.UserEntity;
import me.thesis.clinic_booking.repository.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;
import java.util.Set;

@Service
public class RoleService {

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private UserService userService;

    void saveRole(RoleEntity roleEntity) {
        roleRepository.save(roleEntity);
    }

    public boolean isSystemAdmin(Long userId) {
        UserEntity userEntity = userService.findByIdAndStoreIdAndServiceDepartmentId(userId, null, null);
        if (userEntity == null) {
            return false;
        }
        for (RoleEntity role : userEntity.getRoleEntities()) {
            if ("ROLE_SYSTEM_ADMIN".equals(role.getName())) {
                return true;
            }
        }
        return false;
    }

    public boolean isStoreAdmin(Long userId, Long storeId) {
        UserEntity userEntity = userService.findByIdAndStoreIdAndServiceDepartmentId(userId, storeId, null);
        if (userEntity == null) {
            return false;
        }
        for (RoleEntity role : userEntity.getRoleEntities()) {
            if ("ROLE_STORE_ADMIN".equals(role.getName())) {
                return true;
            }
        }
        return false;
    }

    public boolean isServiceDepartmentAdmin(Long userId, Long storeId, Long serviceDepartmentId) {
        UserEntity userEntity = userService.findByIdAndStoreIdAndServiceDepartmentId(userId, storeId, serviceDepartmentId);
        if (userEntity == null) {
            return false;
        }
        for (RoleEntity role : userEntity.getRoleEntities()) {
            if ("ROLE_SERVICE_DEPARTMENT_ADMIN".equals(role.getName())) {
                return true;
            }
        }
        return false;
    }

    public boolean isServiceDepartmentStaff(Long userId, Long storeId, Long serviceDepartmentId) {
        UserEntity userEntity = userService.findByIdAndStoreIdAndServiceDepartmentId(userId, storeId, serviceDepartmentId);
        if (userEntity == null) {
            return false;
        }
        for (RoleEntity role : userEntity.getRoleEntities()) {
            if ("ROLE_SERVICE_DEPARTMENT_STAFF".equals(role.getName())) {
                return true;
            }
        }
        return false;
    }

    public boolean isCashierStaff(Long userId, Long storeId, Long serviceDepartmentId) {
        UserEntity userEntity = userService.findByIdAndStoreIdAndServiceDepartmentId(userId, storeId, serviceDepartmentId);
        if (userEntity == null) {
            return false;
        }
        for (RoleEntity role : userEntity.getRoleEntities()) {
            if ("ROLE_SERVICE_DEPARTMENT_CASHIER".equals(role.getName())) {
                return true;
            }
        }
        return false;
    }

    public boolean isClientWithPhone(CustomUserDetail customUserDetail) {
        if (customUserDetail == null) return false;
        for (RoleEntity role : customUserDetail.getUserEntity().getRoleEntities()) {
            if ("ROLE_CLIENT_WITH_PHONE".equals(role.getName())) {
                return true;
            }
        }
        return false;
    }

}
