package me.thesis.clinic_booking.service;

import com.amazonaws.services.s3.model.S3Object;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.InputStream;

@Service
public class DownloadFileService {

    private static final Logger LOGGER = LoggerFactory.getLogger(DownloadFileService.class);

    @Autowired
    private UploadFileLocalService uploadFileLocalService;

    public InputStream downloadImage(String url) {
        S3Object s3Object = uploadFileLocalService.download(url.replace("https://clinic-booking.s3.us-east-2.amazonaws.com/", ""));
        LOGGER.info("Downloading file {} from s3 successfully.", s3Object.getObjectContent());
        return s3Object.getObjectContent();
    }
}
