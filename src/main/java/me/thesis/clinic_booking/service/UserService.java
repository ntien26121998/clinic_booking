package me.thesis.clinic_booking.service;

import me.thesis.clinic_booking.dto.UserDto;
import me.thesis.clinic_booking.entity.RoleEntity;
import me.thesis.clinic_booking.entity.UserEntity;
import me.thesis.clinic_booking.entity.enums.StatusUserType;
import me.thesis.clinic_booking.repository.RoleRepository;
import me.thesis.clinic_booking.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private PageService pageService;

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    @Autowired
    private UploadFileLocalService uploadFileService;

    public Page<UserEntity> findByRole(String roleNameEnum, String sortDir, String sortField, int page, int size){
        Pageable pageable =  pageService.getPage(sortDir, sortField, page, size);
        String roleName = "ROLE_" + roleNameEnum;
        return userRepository.findByRoleEntities_Name(roleName, pageable);
    }

    public Page<UserEntity> findByServiceDepartmentIdAndRole(Long serviceDepartmentId, String roleNameEnum, String sortDir, String sortField, int page, int size){
        Pageable pageable =  pageService.getPage(sortDir, sortField, page, size);
        String roleName = "ROLE_" + roleNameEnum;
        return userRepository.findByServiceDepartmentIdAndRoleEntities_Name(serviceDepartmentId, roleName, pageable);
    }

    public Page<UserEntity> findByStoreIdAndRole(Long storeId, String roleNameEnum, String sortDir, String sortField, int page, int size){
        Pageable pageable =  pageService.getPage(sortDir, sortField, page, size);
        String roleName = "ROLE_" + roleNameEnum;
        return userRepository.findByStoreIdAndRoleEntities_Name(storeId, roleName, pageable);
    }

    public List<UserEntity> findByServiceDepartmentIdAndRole(Long serviceDepartmentId, String roleNameEnum){
        String roleName = "ROLE_" + roleNameEnum;
        return userRepository.findByServiceDepartmentIdAndRoleEntities_Name(serviceDepartmentId, roleName);
    }

    public List<UserEntity> findByServiceDepartmentIdAndRoleAndStatus(Long serviceDepartmentId, String roleNameEnum, StatusUserType status){
        String roleName = "ROLE_" + roleNameEnum;
        return userRepository.findByServiceDepartmentIdAndRoleEntities_NameAndStatus(serviceDepartmentId, roleName, status);
    }

    public List<UserEntity> findByServiceDepartmentIdAndRoleAndIdNotIn(Long serviceDepartmentId, String roleNameEnum, List<Long> listId){
        String roleName = "ROLE_" + roleNameEnum;
        return userRepository.findByServiceDepartmentIdAndRoleEntities_NameAndIdNotIn(serviceDepartmentId, roleName, listId);
    }


    public UserEntity findByIdAndStoreIdAndServiceDepartmentId(Long id, Long storeId, Long serviceDepartmentId) {
        return userRepository.findByIdAndStoreIdAndServiceDepartmentId(id, storeId, serviceDepartmentId);
    }

    public boolean changeStatus(Long id, Long storeId, Long serviceDepartmentId) {
        UserEntity oldUserEntity = userRepository.findByIdAndStoreIdAndServiceDepartmentId(id, storeId, serviceDepartmentId);
        if(oldUserEntity == null){
            return false;
        }
        if(oldUserEntity.getStatus() == StatusUserType.ACTIVE) {
            oldUserEntity.setStatus(StatusUserType.IN_ACTIVE);
        } else {
            oldUserEntity.setStatus(StatusUserType.ACTIVE);
        }
        oldUserEntity.setUpdatedTime(new Date());
        userRepository.save(oldUserEntity);
        return true;
    }


    public boolean checkEmailHasBeenUsed(String email){
        return userRepository.findByEmail(email) != null;
    }

    public boolean save(UserDto userDto, HttpServletRequest request) {
        UserEntity userEntity = convertFromDto(userDto, request);
        if(userEntity == null) {
            return false;
        }
        userRepository.save(userEntity);
        return true;
    }

    public boolean update(UserDto userDto, UserEntity userEntity, HttpServletRequest request) {
        if(userDto.getAvatar() != null) {
            String url = uploadFileService.uploadFile("serviceDepartmentImages", userDto.getAvatar(), request);
            userEntity.setAvatarUrl(url);
        }
        userEntity.setUpdatedTime(new Date());
        userEntity.setPhone(userDto.getPhone());
        userEntity.setName(userDto.getName());
        userEntity.setEmail(userDto.getEmail());
        userEntity.setDescription(userDto.getDescription());
        userRepository.save(userEntity);
        return true;
    }

    public boolean delete(Long id, Long storeId, Long serviceDepartmentId) {
        UserEntity oldUserEntity = findByIdAndStoreIdAndServiceDepartmentId(id, storeId, serviceDepartmentId);
        if(oldUserEntity == null){
            return false;
        } else {
            oldUserEntity.setDeleted(true);
            oldUserEntity.setUpdatedTime(new Date());
            userRepository.save(oldUserEntity);
            return true;
        }
    }

    public UserEntity convertFromDto(UserDto userDto, HttpServletRequest request) {
        UserEntity userEntity;
        if(userDto.getId() != null){
            userEntity = findByIdAndStoreIdAndServiceDepartmentId(userDto.getId(), userDto.getStoreId(), userDto.getServiceDepartmentId());
            if(userEntity == null) {
                return null;
            }
            userEntity.setUpdatedTime(new Date());
        } else {
            userEntity = new UserEntity();
            userEntity.setCreatedTime(new Date());
        }
        Set<RoleEntity> roleEntities = new HashSet<>();
        roleEntities.add(userDto.getRoleEntity());
        userEntity.setRoleEntities(roleEntities);
        userEntity.setEmail(userDto.getEmail());
        userEntity.setName(userDto.getName());
        userEntity.setPhone(userDto.getPhone());
        userEntity.setPassword(passwordEncoder.encode(userDto.getPassword()));
        userEntity.setServiceDepartmentId(userDto.getServiceDepartmentId());
        userEntity.setStoreId(userDto.getStoreId());
        userEntity.setEnableServe(userDto.isEnableServe());
        //todo: handle save user avatar
        if (userDto.getAvatar() != null) {
            String url = uploadFileService.uploadFile("avatarUser", userDto.getAvatar(), request);
            userEntity.setAvatarUrl(url);
        }
        userEntity.setStatus(userDto.getStatus());
        return userEntity;
    }

    public UserEntity findById(Long id) {
        return userRepository.findById(id).orElse(null);
    }

    public Map<Long, String> getMapUserNameAndUserId(Collection<Long> listUserIds) {
        List<UserEntity> listUser = userRepository.getListUserByIdIn(listUserIds);
        if (listUser.isEmpty()) {
            return new HashMap<>();
        }
        return listUser.stream().collect(Collectors.toMap(UserEntity::getId, UserEntity::getName));
    }
}
