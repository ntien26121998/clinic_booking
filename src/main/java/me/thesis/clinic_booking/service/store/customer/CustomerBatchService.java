package me.thesis.clinic_booking.service.store.customer;

import me.thesis.clinic_booking.entity.store.booking.CustomerBookingEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

@Service
public class CustomerBatchService {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    public int[] batchInsert(List<CustomerBookingEntity> customerEntities, Long createdBy) {
        return this.jdbcTemplate.batchUpdate(
                "insert IGNORE into customer (name, phone, email, gender, store_id, created_time, is_patient_code_from_user, is_created_booking, created_by, patient_code ) " +
                        "values(?,?,?,?,?,?,?,?,?,?)",
                new BatchPreparedStatementSetter() {
                    public void setValues(PreparedStatement ps, int i) throws SQLException {
                        CustomerBookingEntity customer = customerEntities.get(i);
                        ps.setObject(1, customer.getName());
                        ps.setObject(2, customer.getPhone());
                        ps.setObject(3, customer.getEmail());
                        ps.setObject(4, customer.getGender());
                        ps.setObject(5, customer.getStoreId());
                        ps.setObject(6, new Date(new java.util.Date().getTime()));
                        ps.setObject(7, true);
                        ps.setObject(8, true);
                        ps.setObject(9, createdBy);
                        ps.setObject(10, customer.getPatientCode());

                    }

                    public int getBatchSize() {
                        return customerEntities.size();
                    }
                });
    }
}

