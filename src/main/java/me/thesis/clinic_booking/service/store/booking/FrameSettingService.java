package me.thesis.clinic_booking.service.store.booking;

import me.thesis.clinic_booking.entity.enums.WorkingTimeConfigType;
import me.thesis.clinic_booking.entity.store.booking.FrameSettingEntity;
import me.thesis.clinic_booking.entity.store.booking.TimeSettingEntity;
import me.thesis.clinic_booking.repository.store.booking.FrameSettingRepository;
import me.thesis.clinic_booking.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.Date;

@Service
public class FrameSettingService extends BaseConfigService<FrameSettingEntity, FrameSettingRepository> {

}
