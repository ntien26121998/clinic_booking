package me.thesis.clinic_booking.service.store.booking;

import me.thesis.clinic_booking.dto.ResponseCase;
import me.thesis.clinic_booking.dto.ServerResponseDto;
import me.thesis.clinic_booking.entity.enums.WorkingTimeConfigType;
import me.thesis.clinic_booking.entity.store.booking.BaseConfigEntity;
import me.thesis.clinic_booking.repository.store.booking.BaseConfigRepository;
import me.thesis.clinic_booking.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public abstract class BaseConfigService<T extends BaseConfigEntity, R extends BaseConfigRepository<T>> {

    @Autowired
    private R repository;

    public List<T> findListCommonConfigByServiceDepartmentId(Long serviceDepartmentId) {
        return repository.findByServiceDepartmentIdAndIsDeletedFalse(serviceDepartmentId);
    }

    public ServerResponseDto saveCommonConfig(Long serviceDepartmentId, T entityNeedBeSave, Long updatedByUserId) {
        if (!entityNeedBeSave.getStartActiveRange().before(entityNeedBeSave.getEndActiveRange())) {
            return new ServerResponseDto(ResponseCase.INVALID_DATE_RANGE);
        }
        List<T> listFromDB = findListCommonConfigByServiceDepartmentId(serviceDepartmentId);
        for (T oldEntity : listFromDB) {
            if (oldEntity.getId().equals(entityNeedBeSave.getId())) {
                continue;
            }
            if (check2DateRangeOverlap(entityNeedBeSave, oldEntity)) {
                return new ServerResponseDto(ResponseCase.DATE_MUST_NOT_OVERLAP);
            }
        }
        boolean isNew = entityNeedBeSave.getId() == null;
        if (isNew) {
            entityNeedBeSave.setServiceDepartmentId(serviceDepartmentId);
            entityNeedBeSave.setCreatedTime(new Date());
            entityNeedBeSave.setTypeConfig(WorkingTimeConfigType.COMMON_CONFIG);
        } else {
            entityNeedBeSave.setUpdatedTime(new Date());
        }
        entityNeedBeSave.setUpdatedBy(updatedByUserId);
        repository.save(entityNeedBeSave);
        return new ServerResponseDto(isNew ? ResponseCase.ADD_SUCCESS : ResponseCase.UPDATE_SUCCESS);
    }

    private boolean check2DateRangeOverlap(T firstEntity, T secondEntity) {
        Date firstStartRange = firstEntity.getStartActiveRange();
        Date firstEndRange = firstEntity.getEndActiveRange();

        Date secondStartRange = secondEntity.getStartActiveRange();
        Date secondEndRange = secondEntity.getEndActiveRange();

        // check secondStartRange in firstEntity range
        if (!firstStartRange.after(secondStartRange) && !secondStartRange.after(firstEndRange)) {
            return true;
        }
        // check secondEntity ends in firstEntity range
        if (!firstStartRange.after(secondEndRange) && !secondEndRange.after(firstEndRange)) {
            return true;
        }
        //check  firstEntity range in secondEntity range
        return secondStartRange.before(firstStartRange) && firstEndRange.before(secondEndRange);
    }

    public ServerResponseDto deleteConfig(Long serviceDepartmentId, Long configEntityId, Long updatedByUserId) {
        T configEntity = repository.findByServiceDepartmentIdAndIdAndIsDeletedFalse(serviceDepartmentId, configEntityId);
        if(configEntity == null) {
            return new ServerResponseDto(ResponseCase.DELETE_FAIL);
        } else {
            configEntity.setUpdatedTime(new Date());
            configEntity.setUpdatedBy(updatedByUserId);
            configEntity.setDeleted(true);
            repository.save(configEntity);
            return new ServerResponseDto(ResponseCase.DELETE_SUCCESS);
        }
    }

    public T findByDayAndTypeConfig(Long serviceDepartmentId, Date day) {
        return repository.findByActiveRangeAndTypeConfig(serviceDepartmentId, DateUtils.getStartOfDay(day), WorkingTimeConfigType.COMMON_CONFIG);
    }
}
