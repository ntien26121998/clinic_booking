package me.thesis.clinic_booking.service.store.booking;

import me.thesis.clinic_booking.entity.store.booking.WorkOffDaySettingEntity;
import me.thesis.clinic_booking.repository.store.booking.WorkOffDaySettingRepository;
import org.springframework.stereotype.Service;

@Service
public class WorkOffDaySettingService extends BaseConfigService<WorkOffDaySettingEntity, WorkOffDaySettingRepository> {
}
