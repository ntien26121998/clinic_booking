package me.thesis.clinic_booking.service.store;

import me.thesis.clinic_booking.dto.StoreDto;
import me.thesis.clinic_booking.entity.store.StoreEntity;
import me.thesis.clinic_booking.repository.StoreRepository;
import me.thesis.clinic_booking.service.PageService;
import me.thesis.clinic_booking.service.RoleService;
import me.thesis.clinic_booking.service.UploadFileLocalService;
import me.thesis.clinic_booking.service.cache.StoreCacheService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.List;

@Service
public class StoreService {

    @Autowired
    private StoreRepository storeRepository;

    @Autowired
    private PageService pageService;

    @Autowired
    private RoleService roleService;

    @Autowired
    private UploadFileLocalService uploadFileService;

    @Autowired
    private StoreCacheService storeCacheService;

//    public Page<StoreEntity> findByStoreTypeId(Long storeTypeId, String sortDir, String sortField, int page, int size) {
//        Pageable pageable = pageService.getPage(sortDir, sortField, page, size);
//        return storeRepository.findByStoreTypeId(storeTypeId, pageable);
//    }

    public Page<StoreEntity> findByCompanyId(Long companyId, String sortDir, String sortField, int page, int size) {
        Pageable pageable = pageService.getPage(sortDir, sortField, page, size);
        return storeRepository.findByCompanyIdAndIsDeletedFalse(companyId, pageable);
    }
    
    public List<StoreEntity> findAll() {
        return storeRepository.findAll();
    }
//    public Page<StoreEntity> findByStoreTypeAndCityAndServiceType(Long storeTypeId, Integer city, Long serviceTypeId, int page, int size) {
//        return storeRepository.findByStoreTypeAndCityAndServiceType(storeTypeId, city, serviceTypeId, PageRequest.of(page - 1, size));
//    }
//
//    public Page<StoreEntity> findWithKey(Long storeTypeId, String city, Long serviceTypeId, String searchKey,
//                                         Double startRating, Double endRating, Pageable pageable) {
//        return storeRepository.findWithKey(storeTypeId, city, serviceTypeId, searchKey, startRating, endRating, pageable);
//    }
//
    public StoreEntity findById(Long id) {
        return storeRepository.findById(id).orElse(null);
    }

    public StoreEntity findByStoreAuthId(String storeAuthId) { //todo: check các hàm gọi ở đây
        return storeRepository.findByStoreAuthIdAndIsDeletedFalse(storeAuthId);
    }

    public Long getStoreIdFromAuthId(String authId) {
        return storeRepository.getIdFromAuthId(authId, false);
    }

    public boolean save(StoreDto storeDto, HttpServletRequest request) {
        StoreEntity storeEntity = convertFromDto(storeDto, request);
        if (storeEntity == null) {
            return false;
        }
        if (storeDto.getStoreCode() == null || storeDto.getStoreCode().equals("")) {
            storeEntity.setStoreCode(getNextStoreCode(1L));
        } else {
            storeEntity.setStoreCode(storeDto.getStoreCode());
        }
        storeEntity.setDeleted(false);

        saveAndCache(storeEntity);
        return true;
    }

    private String getNextStoreCode(Long companyId) {
        StoreEntity storeEntity = storeRepository.findFirstByCompanyIdAndIsDeletedOrderByStoreCodeDesc(companyId, false);
        if (storeEntity == null) {
            return "101";
        }
        long storeCode = Long.parseLong(storeEntity.getStoreCode()) + 1;
        return String.valueOf(storeCode);
    }

    public boolean delete(String storeAuthId) {
        StoreEntity oldStoreEntity = findByStoreAuthId(storeAuthId);
        if (oldStoreEntity == null) {
            return false;
        }
        oldStoreEntity.setDeleted(true);
        oldStoreEntity.setUpdatedTime(new Date());
        saveAndCache(oldStoreEntity);
        return true;
    }

    private StoreEntity convertFromDto(StoreDto dto, HttpServletRequest request) {
        StoreEntity storeEntity;
        if (dto.getId() != null) {
            storeEntity = findById(dto.getId());
            if (storeEntity == null) {
                return null;
            }
            storeEntity.setUpdatedTime(new Date());
        } else {
            storeEntity = new StoreEntity();
            storeEntity.setCreatedTime(new Date());
            storeEntity.setStoreAuthId(String.valueOf((new Date()).getTime()));
        }
        storeEntity.setCompanyId(dto.getCompanyId());
        storeEntity.setCityName(dto.getCity());
        storeEntity.setDistrictName(dto.getDistrictName());
        storeEntity.setStoreName(dto.getName());
        storeEntity.setPhone(dto.getPhone());
        storeEntity.setAddress(dto.getAddress());
        storeEntity.setEmail(dto.getEmail());
        storeEntity.setDescription(dto.getDescription());
        if (dto.getImage() != null) {
            String url = uploadFileService.uploadFile("storeImages", dto.getImage(), request);
            storeEntity.setImageUrl(url);
        }
        return storeEntity;
    }

    private void saveAndCache(StoreEntity storeEntity) {
        storeRepository.save(storeEntity);
        storeCacheService.buildStoreCache();
    }
}
