package me.thesis.clinic_booking.service.store.booking;

import me.thesis.clinic_booking.entity.enums.WorkingTimeConfigType;
import me.thesis.clinic_booking.entity.store.booking.FrameSettingEntity;
import me.thesis.clinic_booking.entity.store.booking.TimeSettingEntity;
import me.thesis.clinic_booking.entity.store.booking.WorkOffDaySettingEntity;
import me.thesis.clinic_booking.entity.store.booking.WorkingTimeConfigEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

@Service
public class CustomWorkingTimeConfigService {
    @Autowired
    private TimeSettingService timeSettingService;

    @Autowired
    private FrameSettingService frameSettingService;

    @Autowired
    private WorkOffDaySettingService workOffDaySettingService;

    private static final Logger LOGGER = LoggerFactory.getLogger(CustomWorkingTimeConfigService.class);

    public WorkingTimeConfigEntity getCommonWorkingTimeConfigForDateRequest(Long serviceDepartment, Date date) {
        CompletableFuture<TimeSettingEntity> timeSettingCompletableFuture = getTimeSetting(serviceDepartment, date);
        CompletableFuture<FrameSettingEntity> frameSettingCompletableFuture = getFrameSetting(serviceDepartment, date);
        CompletableFuture<WorkOffDaySettingEntity> workOffDaySettingCompletableFuture = getWorkOffDaySetting(serviceDepartment, date);

        CompletableFuture.allOf(timeSettingCompletableFuture, frameSettingCompletableFuture, workOffDaySettingCompletableFuture).join();

        try {
            TimeSettingEntity timeSettingEntity = timeSettingCompletableFuture.get();
            FrameSettingEntity frameSettingEntity = frameSettingCompletableFuture.get();
            WorkOffDaySettingEntity workOffDaySettingEntity = workOffDaySettingCompletableFuture.get();

            if (timeSettingEntity == null || frameSettingEntity == null || workOffDaySettingEntity == null) {
                LOGGER.info("cant find common config on this day with time setting: {}, frame setting: {}, work off day setting: {}",
                        timeSettingEntity, frameSettingEntity, workOffDaySettingEntity);
                return null;
            }

            WorkingTimeConfigEntity commonConfig = new WorkingTimeConfigEntity();
            commonConfig.setServiceDepartmentId(serviceDepartment);
            commonConfig.setSlotTimeUnit(frameSettingEntity.getSlotTimeUnit());
            commonConfig.setSlotPerFrame(frameSettingEntity.getFrameSlot());
            commonConfig.setStartWorkingTime(timeSettingEntity.getStartWorkingTime());
            commonConfig.setStartBreakTime(timeSettingEntity.getStartBreakTime());
            commonConfig.setEndBreakTime(timeSettingEntity.getEndBreakTime());
            commonConfig.setEndWorkingTime(timeSettingEntity.getEndWorkingTime());
            commonConfig.setTypeConfig(WorkingTimeConfigType.COMMON_CONFIG);
            commonConfig.setWorkOffFixed(workOffDaySettingEntity.getWorkOffFixed());
            return commonConfig;
        } catch (ExecutionException | InterruptedException e) {
            LOGGER.error("exception when call async list config, message is: {}", e.getMessage());
            return null;
        }
    }

    @Async
    CompletableFuture<TimeSettingEntity> getTimeSetting(Long serviceDepartment, Date date) {
        return CompletableFuture.completedFuture(timeSettingService.findByDayAndTypeConfig(serviceDepartment, date));
    }

    @Async
    CompletableFuture<FrameSettingEntity> getFrameSetting(Long serviceDepartment, Date date) {
        return CompletableFuture.completedFuture(frameSettingService.findByDayAndTypeConfig(serviceDepartment, date));
    }

    @Async
    CompletableFuture<WorkOffDaySettingEntity> getWorkOffDaySetting(Long serviceDepartment, Date date) {
        return CompletableFuture.completedFuture(workOffDaySettingService.findByDayAndTypeConfig(serviceDepartment, date));
    }
}
