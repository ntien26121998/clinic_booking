package me.thesis.clinic_booking.service.store.customer;

import me.thesis.clinic_booking.config.security.CustomUserDetail;
import me.thesis.clinic_booking.dto.ImportCustomerBookingDto;
import me.thesis.clinic_booking.entity.store.booking.CustomerBookingEntity;
import me.thesis.clinic_booking.repository.store.booking.CustomerBookingRepository;
import me.thesis.clinic_booking.utils.ExcelImportUtils;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.transaction.Transactional;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Service
public class ImportCustomerService {
    private final static Logger LOGGER = LoggerFactory.getLogger(ImportCustomerService.class);
    public static final Pattern VALID_EMAIL_ADDRESS_REGEX =
            Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);

    @Autowired
    private CustomerBookingRepository customerBookingRepository;

    @Autowired
    private CustomerBatchService customerBatchService;

    @Transactional
    public boolean submitImport(List<CustomerBookingEntity> listCustomerBooking, CustomUserDetail userDetails) {
        if (listCustomerBooking.isEmpty()) {
            LOGGER.info("List Customer Booking is empty");
            return false;
        }
        customerBatchService.batchInsert(listCustomerBooking, userDetails.getUserEntity().getId());
        return true;
    }

    public ImportCustomerBookingDto readDataFromFileImport(MultipartFile fileExcelImport, Long storeId) {
        if (fileExcelImport == null) {
            return null;
        }
        List<String[]> listLineCustomerBooking = readListCustomerBookingRowValueFromExcel(fileExcelImport);
        if (listLineCustomerBooking == null || listLineCustomerBooking.isEmpty()) {
            LOGGER.error("File import is empty");
            return null;
        }
        List<CustomerBookingEntity> listCustomerBookingInDB = customerBookingRepository.findByStoreIdAndIsDeletedFalse(storeId);

        Map<Long, CustomerBookingEntity> mapCustomerExistInDbByPatientCode =
                listCustomerBookingInDB
                        .stream()
                        .collect(Collectors.toMap(CustomerBookingEntity::getPatientCode, Function.identity()));
        List<CustomerBookingEntity> customerBookingEntities = new ArrayList<>();
        List<String> listCustomerBookingError = new ArrayList<>();

        listLineCustomerBooking.forEach(lineCustomer -> {
            CustomerBookingEntity customerBookingEntity =
                    convertFromLineCustomerBooking(lineCustomer, mapCustomerExistInDbByPatientCode, storeId);
            if (customerBookingEntity == null) {
                listCustomerBookingError.add(lineCustomer[0]);
            } else {
                customerBookingEntities.add(customerBookingEntity);
            }
        });


        ImportCustomerBookingDto importCustomerBookingDto = new ImportCustomerBookingDto();
        importCustomerBookingDto.setListCustomerBooking(customerBookingEntities);
        importCustomerBookingDto.setListCustomerBookingError(listCustomerBookingError);
        return importCustomerBookingDto;
    }

    private List<String[]> readListCustomerBookingRowValueFromExcel(MultipartFile excelFile) {
        try {
            int numberColumnWantToSelect = 5;
            return ExcelImportUtils.readListRowValueWithSkipHeader(excelFile, numberColumnWantToSelect, this::isRowEmpty);

        } catch (IOException | InvalidFormatException e) {
            LOGGER.error("Cant read list customer booking from excel file with Exception {}", e.getMessage());
            return null;
        }
    }

    private boolean isRowEmpty(Row rowConfig) {
        DataFormatter dataFormatter = new DataFormatter();
        String patientCode = dataFormatter.formatCellValue(rowConfig.getCell(0)).trim();
        String customerName = dataFormatter.formatCellValue(rowConfig.getCell(1)).trim();
        String phone = dataFormatter.formatCellValue(rowConfig.getCell(2)).trim();
        String email = dataFormatter.formatCellValue(rowConfig.getCell(3)).trim();
        String gender = dataFormatter.formatCellValue(rowConfig.getCell(4)).trim();
        return patientCode.length() == 0
                && customerName.length() == 0 && phone.length() == 0
                && email.length() == 0 && gender.length() == 0;
    }

    private CustomerBookingEntity convertFromLineCustomerBooking(
            String[] lineCustomerBooking,
            Map<Long, CustomerBookingEntity> mapCustomerBookingByPatientCodeFromDb,
            Long storeId) {

        CustomerBookingEntity customerBookingEntity = new CustomerBookingEntity();
        String patientCodeStr = lineCustomerBooking[0];
        String patientName = lineCustomerBooking[1];
        String phone = lineCustomerBooking[2];
        String email = lineCustomerBooking[3];
        String genderStr = lineCustomerBooking[4];

        try {
            if (!StringUtils.hasText(patientCodeStr) || patientCodeStr.trim().length() != 7) {
                LOGGER.error("Patient code {} is invalid format", patientCodeStr);
                return null;
            }
            Long patientCode = Long.parseLong(patientCodeStr);
            if (mapCustomerBookingByPatientCodeFromDb.containsKey(patientCode)) {
                LOGGER.error("This customer has code: {} is exist in Db", patientCode);
                return null;
            }
            if (patientCode < 1000000) {
                LOGGER.error("Patient code {} is must be bigger than 1000000", patientCode);
                return null;
            }
            customerBookingEntity.setPatientCode(patientCode);
        } catch (NumberFormatException e) {
            LOGGER.error("This customer has code: {} is invalid format", patientCodeStr);
            return null;
        }

        if (StringUtils.hasText(patientName.trim())) {
            customerBookingEntity.setName(patientName);
        } else {
            LOGGER.error("Patient name {} is invalid", patientName);
            return null;
        }

        try {
            if (!StringUtils.hasText(phone) || phone.trim().length() != 10) {
                LOGGER.error("Phone Number {} is invalid format", phone);
                return null;
            }
            CustomerBookingEntity getByPhone = customerBookingRepository.findByPhoneAndStoreIdAndIsDeletedFalse(phone, storeId);
            if (getByPhone != null) {
                LOGGER.error("Phone Number {} is exist in store", phone);
                return null;
            }
            customerBookingEntity.setPhone(phone);
        } catch (NumberFormatException e) {
            LOGGER.error("This customer has code: {} is invalid format", phone);
            return null;
        }

        if(validateEmail(email)) {
            customerBookingEntity.setEmail(email);
        } else {
            LOGGER.error("Email {} is invalid", patientName);
            return null;
        }

        if(!StringUtils.hasText(genderStr)) {
            LOGGER.error("Gender {} is invalid format of patient code {}", genderStr, patientCodeStr);
            return null;
        } else {
            Integer gender = Integer.parseInt(genderStr);
            customerBookingEntity.setGender(gender);
        }

        customerBookingEntity.setStoreId(storeId);
        return customerBookingEntity;
    }

    public static boolean validateEmail(String emailStr) {
        Matcher matcher = VALID_EMAIL_ADDRESS_REGEX.matcher(emailStr);
        return matcher.find();
    }
}

