package me.thesis.clinic_booking.service.store.booking;

import me.thesis.clinic_booking.dto.ImportCategoryBookingDto;
import me.thesis.clinic_booking.entity.store.booking.category.CategoryBookingEntity;
import me.thesis.clinic_booking.repository.store.booking.CategoryBookingRepository;
import me.thesis.clinic_booking.utils.ExcelImportUtils;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.transaction.Transactional;
import java.io.IOException;
import java.util.*;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
public class CategoryBookingService {

    @Autowired
    private CategoryBookingRepository categoryBookingRepository;

    private static final Logger LOGGER = LoggerFactory.getLogger(CategoryBookingService.class);

    public List<CategoryBookingEntity> getAllByServiceDepartmentId(Long serviceDepartmentId) {
        return categoryBookingRepository.findAllByServiceDepartmentIdAndIsDeletedFalse(serviceDepartmentId);
    }

    public boolean saveCategory(Long serviceDepartmentId, CategoryBookingEntity category) {
        CategoryBookingEntity entityWithSameName = categoryBookingRepository.findByServiceDepartmentIdAndCategoryNameAndIsDeletedFalse(serviceDepartmentId, category.getCategoryName());
        Long categoryRequestId = category.getId();
        if (categoryRequestId == null && entityWithSameName != null) {
            return false;
        }
        if (categoryRequestId != null && entityWithSameName != null && !categoryRequestId.equals(entityWithSameName.getId())) {
            return false;
        }
        CategoryBookingEntity oldEntity = categoryBookingRepository.findByIdAndServiceDepartmentIdAndIsDeleted(category.getId(), serviceDepartmentId, false);
        if (oldEntity != null) {
            oldEntity.setCategoryName(category.getCategoryName());
            oldEntity.setMoneyExcludeTax(category.getMoneyExcludeTax());
            oldEntity.setNumberFrameConsecutiveAutoFill(category.getNumberFrameConsecutiveAutoFill());
            oldEntity.setUpdatedTime(new Date());
            categoryBookingRepository.save(oldEntity);
        } else {
            category.setServiceDepartmentId(serviceDepartmentId);
            category.setActive(false);
            category.setCreatedTime(new Date());
            categoryBookingRepository.save(category);
        }
        return true;
    }

    @Transactional
    public boolean submitImport(Set<CategoryBookingEntity> listCategoryBooking, Long serviceDepartmentId) {
        if (listCategoryBooking.isEmpty()) {
            LOGGER.info("listCategoryBooking is empty");
            return false;
        }
        for (CategoryBookingEntity categoryBookingEntity : listCategoryBooking) {
            categoryBookingEntity.setServiceDepartmentId(serviceDepartmentId);
            categoryBookingEntity.setActive(false);
            categoryBookingEntity.setCreatedTime(new Date());
        }
        categoryBookingRepository.saveAll(listCategoryBooking);
        return true;
    }

    public ImportCategoryBookingDto readDataFromFileImport(MultipartFile fileExcelImport, Long serviceDepartmentId) {
        if (fileExcelImport == null) {
            return null;
        }
        List<String[]> listLineCategoryBooking = readListCategoryBookingRowValueFromExcel(fileExcelImport);
        if (listLineCategoryBooking == null || listLineCategoryBooking.isEmpty()) {
            LOGGER.error("File import is empty");
            return null;
        }

        List<CategoryBookingEntity> listCategoryBookingInDB = categoryBookingRepository.findByServiceDepartmentIdAndIsDeletedFalse(serviceDepartmentId);
        BinaryOperator<CategoryBookingEntity> funcWhenDuplicateCategoryNameKey = (categoryInMap, categoryWithSameNameAfter) -> categoryWithSameNameAfter;
        Map<String, CategoryBookingEntity> mapCategoryExistInDbByCategoryName =
                listCategoryBookingInDB
                        .stream()
                        .collect(Collectors.toMap(CategoryBookingEntity::getCategoryName, Function.identity(), funcWhenDuplicateCategoryNameKey));

        List<CategoryBookingEntity> categoryBookingEntities = new ArrayList<>();
        List<String> listCategoryBookingError = new ArrayList<>();

        listLineCategoryBooking.forEach(lineCategory -> {
            CategoryBookingEntity categoryBookingEntity = convertFromLineCategoryBooking(lineCategory, mapCategoryExistInDbByCategoryName);
            if (categoryBookingEntity == null) {
                listCategoryBookingError.add(lineCategory[0]);
            } else {
                categoryBookingEntities.add(categoryBookingEntity);
            }
        });


        ImportCategoryBookingDto importCategoryBookingDto = new ImportCategoryBookingDto();
//        Collections.sort(categoryBookingEntities);
        importCategoryBookingDto.setListCategoryBooking(categoryBookingEntities);
        importCategoryBookingDto.setListCategoryBookingError(listCategoryBookingError);
        return importCategoryBookingDto;
    }

    private List<String[]> readListCategoryBookingRowValueFromExcel(MultipartFile excelFile) {
        try {
            int numberColumnWantToSelect = 3;
            return ExcelImportUtils.readListRowValueWithSkipHeader(excelFile, numberColumnWantToSelect, this::isRowEmpty);

        } catch (IOException | InvalidFormatException e) {
            LOGGER.error("Cant read list category booking from excel file with Exception {}", e.getMessage());
            return null;
        }
    }

    private CategoryBookingEntity convertFromLineCategoryBooking(String[] lineCategoryBooking,
                                                                 Map<String, CategoryBookingEntity> mapCategoryBookingByNameFromDb) {

        CategoryBookingEntity categoryBookingEntity = new CategoryBookingEntity();
        String categoryName = lineCategoryBooking[0];
        String numberFrameConsecutiveAutoFillStr = lineCategoryBooking[1];
        String moneyExcludeTaxStr = lineCategoryBooking[2];

        if (mapCategoryBookingByNameFromDb.containsKey(categoryName)) {
            LOGGER.error("This category name: {} is exist in Db", categoryName);
            return null;
        }
        if (!StringUtils.hasText(categoryName)) {
            LOGGER.error("Category name is null");
            return null;
        }
        categoryBookingEntity.setCategoryName(categoryName);

        try {
            int numberFrameConsecutiveAutoFill = Integer.parseInt(numberFrameConsecutiveAutoFillStr);
            boolean isValidNumberFrameConsecutiveAutoFill = numberFrameConsecutiveAutoFill >= 0 && numberFrameConsecutiveAutoFill <= 10;
            if (isValidNumberFrameConsecutiveAutoFill) {
                categoryBookingEntity.setNumberFrameConsecutiveAutoFill(numberFrameConsecutiveAutoFill);
            } else {
                LOGGER.error("invalid numberFrameConsecutiveAutoFill: {}", numberFrameConsecutiveAutoFill);
                return null;
            }
        } catch (Exception e) {
            LOGGER.error("This category name {}: Number frame consecutive auto fill: {} is invalid with format number", categoryName, numberFrameConsecutiveAutoFillStr);
            return null;
        }

        try {
            long moneyExcludeTax = Long.parseLong(moneyExcludeTaxStr);
            if (moneyExcludeTax < 0L) {
                LOGGER.error("This category name {}: money must greater than or equals 0", categoryName);
                return null;
            }
            categoryBookingEntity.setMoneyExcludeTax(moneyExcludeTax);
        } catch (Exception e) {
            LOGGER.error("This category name {}: Money value: {} is invalid with format number", categoryName, moneyExcludeTaxStr);
            return null;
        }

        return categoryBookingEntity;
    }

    private boolean isRowEmpty(Row rowConfig) {
        DataFormatter dataFormatter = new DataFormatter();
        String categoryName = dataFormatter.formatCellValue(rowConfig.getCell(0)).trim();
        String numberFrameConsecutiveAutoFill = dataFormatter.formatCellValue(rowConfig.getCell(1)).trim();
        return categoryName.length() == 0
                && numberFrameConsecutiveAutoFill.length() == 0;
    }


    public CategoryBookingEntity getCategoryById(Long id, Long serviceDepartmentId) {
        return categoryBookingRepository.findByIdAndServiceDepartmentIdAndIsDeleted(id, serviceDepartmentId, false);
    }

    public boolean deleteCategory(Long id, Long serviceDepartmentId) {
        CategoryBookingEntity categoryBookingEntity = categoryBookingRepository.findByIdAndServiceDepartmentIdAndIsDeleted(id, serviceDepartmentId, false);
        if (categoryBookingEntity == null) {
            return false;
        } else {
            categoryBookingEntity.setDeleted(true);
            categoryBookingEntity.setUpdatedTime(new Date());
            categoryBookingRepository.save(categoryBookingEntity);
            return true;
        }
    }

    public boolean deleteAllCategory(Long serviceDepartmentId) {
        List<CategoryBookingEntity> listCategoryBooking = categoryBookingRepository.findAllByServiceDepartmentIdAndIsDeletedFalse(serviceDepartmentId);
        for (CategoryBookingEntity entity : listCategoryBooking) {
            entity.setDeleted(true);
            entity.setUpdatedTime(new Date());
        }
        categoryBookingRepository.saveAll(listCategoryBooking);
        return true;
    }

//    @Transactional
//    public boolean updateOrder(List<SaveCategoryBookingOrderDto> listCategoryBookingOrderDto, Long storeId) {
//        if (listCategoryBookingOrderDto.isEmpty()) {
//            return false;
//        }
//        Map<Long, Integer> mapCategoryIdAndNewPosition =
//                listCategoryBookingOrderDto
//                        .stream()
//                        .collect(Collectors.toMap(SaveCategoryBookingOrderDto::getId, SaveCategoryBookingOrderDto::getPosition));
//        List<CategoryBookingEntity> listCategoryBookingNeedBeUpdatePosition = categoryBookingRepository.
//                findByIdInAndStoreIdAndIsDeleted(mapCategoryIdAndNewPosition.keySet(), storeId, false);
//
//        for (CategoryBookingEntity categoryBooking : listCategoryBookingNeedBeUpdatePosition) {
//            Long categoryId = categoryBooking.getId();
//            Integer newPosition = mapCategoryIdAndNewPosition.get(categoryId);
//            categoryBooking.setPosition(newPosition);
//            categoryBooking.setUpdatedTime(new Date());
//        }
//        categoryBookingRepository.saveAll(listCategoryBookingNeedBeUpdatePosition);
//        return true;
//    }

    public boolean changeStatus(Long categoryId, Long serviceDepartmentId) {
        CategoryBookingEntity categoryBookingEntity = categoryBookingRepository.findByIdAndServiceDepartmentIdAndIsDeleted(categoryId, serviceDepartmentId, false);
        if (categoryBookingEntity == null) {
            return false;
        } else {
            categoryBookingEntity.setActive(!categoryBookingEntity.isActive());
            categoryBookingEntity.setUpdatedTime(new Date());
            categoryBookingRepository.save(categoryBookingEntity);
            return true;
        }
    }


    public List<CategoryBookingEntity> findByServiceDepartmentIdAndIdIn(Long serviceDepartmentId, Collection<Long> listCategoryId) {
        if (listCategoryId.isEmpty()) {
            return Collections.emptyList();
        }
        return categoryBookingRepository.findByServiceDepartmentIdAndIdIn(serviceDepartmentId, listCategoryId);
    }

    public List<CategoryBookingEntity> findByServiceDepartmentIdAndActiveAndDeletedFalse(Long serviceDepartmentId) {
        return categoryBookingRepository.findByServiceDepartmentIdAndIsActiveTrueAndIsDeletedFalse(serviceDepartmentId);
    }

    public List<CategoryBookingEntity> findAllByServiceDepartmentId(Long serviceDepartmentId) {
        return categoryBookingRepository.findAllByServiceDepartmentIdAndIsDeletedFalse(serviceDepartmentId);
    }

    public List<String> findCategoryNameByIdInAndStoreIdAndIsDeleted(Long serviceDepartmentId, Collection<Long> listCategoryId) {
        if (listCategoryId.isEmpty()) {
            return Collections.emptyList();
        }
        return categoryBookingRepository.findCategoryNameByIdInAndServiceDepartmentIdAndIsDeleted(listCategoryId, serviceDepartmentId);
    }

    public Map<Long, CategoryBookingEntity> getMapCategoryById(Long serviceDepartmentId) {
        List<CategoryBookingEntity> listCategory = categoryBookingRepository.findByServiceDepartmentId(serviceDepartmentId);
        if (listCategory.isEmpty()) {
            return new HashMap<>();
        }
        return listCategory.stream().collect(Collectors.toMap(CategoryBookingEntity::getId, Function.identity()));
    }

}

