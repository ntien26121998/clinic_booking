package me.thesis.clinic_booking.service.store.booking;

import me.thesis.clinic_booking.dto.ImageServiceDto;
import me.thesis.clinic_booking.entity.store.booking.ImageEntity;
import me.thesis.clinic_booking.repository.store.booking.ImageRepository;
import me.thesis.clinic_booking.service.PageService;
import me.thesis.clinic_booking.service.UploadFileLocalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.List;

@Service
public class ImageService {

    @Autowired
    private ImageRepository imageRepository;

    @Autowired
    private PageService pageService;

    @Autowired
    private UploadFileLocalService uploadFileService;

    public boolean save(ImageServiceDto imageServiceDto, Long serviceDepartmentId, HttpServletRequest request) {
        ImageEntity image;
        MultipartFile imageFile = imageServiceDto.getImage();
        if (imageServiceDto.getImage() == null ) {
            Long imageId = imageServiceDto.getId();
            if(imageServiceDto.getId() == null) {
                return false;
            }
            if(findById(imageId, serviceDepartmentId) == null) {
                return false;
            }
            image = findById(imageId, serviceDepartmentId);
            image.setUpdatedTime(new Date());
        } else {
            image = new ImageEntity();
            String url = uploadFileService.uploadFile("serviceDepartmentImages", imageFile, request);
            image.setImageUrl(url);
            image.setCreatedTime(new Date());
            image.setServiceDepartmentId(serviceDepartmentId);
        }
        image.setTitle(imageServiceDto.getTitle());
        image.setDescription(imageServiceDto.getDescription());
        imageRepository.save(image);
        return true;
    }

    public Page<ImageEntity> findByServiceDepartmentId(Long serviceDepartmentId, String sortDir, String sortField, int page, int size) {
        Pageable pageable = pageService.getPage(sortDir, sortField, page, size);
        return imageRepository.findByServiceDepartmentId(serviceDepartmentId, pageable);
    }

    public List<ImageEntity> findByServiceDepartmentId(Long serviceDepartmentId) {
        return imageRepository.findByServiceDepartmentId(serviceDepartmentId);
    }

    public ImageEntity findById(Long imageId, Long serviceDepartmentId) {
        return imageRepository.findByIdAndServiceDepartmentId(imageId, serviceDepartmentId);
    }

    public boolean delete(Long id, Long serviceDepartmentId) {
        ImageEntity image = findById(id, serviceDepartmentId);
        if (image == null) {
            return false;
        }
        image.setDeleted(true);
        image.setUpdatedTime(new Date());
        imageRepository.save(image);
        return true;
    }
}
