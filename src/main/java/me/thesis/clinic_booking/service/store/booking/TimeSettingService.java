package me.thesis.clinic_booking.service.store.booking;

import me.thesis.clinic_booking.entity.enums.WorkingTimeConfigType;
import me.thesis.clinic_booking.entity.store.booking.TimeSettingEntity;
import me.thesis.clinic_booking.repository.store.booking.TimeSettingRepository;
import me.thesis.clinic_booking.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.Date;

@Service
public class TimeSettingService extends BaseConfigService<TimeSettingEntity, TimeSettingRepository> {
    @Autowired
    private TimeSettingRepository timeSettingRepository;

    @Override
    public TimeSettingEntity findByDayAndTypeConfig(Long serviceDepartmentId, Date day) {
        Date startOfDay = DateUtils.getStartOfDay(day);
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(startOfDay);
        return timeSettingRepository.findByActiveRangeAndTypeConfig(serviceDepartmentId, startOfDay, WorkingTimeConfigType.COMMON_CONFIG);
    }
}
