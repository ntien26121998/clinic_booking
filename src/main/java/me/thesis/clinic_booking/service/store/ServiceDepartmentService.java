package me.thesis.clinic_booking.service.store;

import me.thesis.clinic_booking.dto.ServiceDepartmentDto;
import me.thesis.clinic_booking.entity.store.ServiceDepartmentEntity;
import me.thesis.clinic_booking.repository.store.ServiceDepartmentRepository;
import me.thesis.clinic_booking.service.PageService;
import me.thesis.clinic_booking.service.UploadFileLocalService;
import me.thesis.clinic_booking.service.cache.ServiceDepartmentCacheService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class ServiceDepartmentService {

    @Autowired
    private ServiceDepartmentRepository serviceDepartmentRepository;

    @Autowired
    private PageService pageService;

    @Autowired
    private UploadFileLocalService uploadFileService;

//    @Autowired
//    private RatingService ratingService;

    @Autowired
    private ServiceDepartmentCacheService serviceDepartmentCacheService;

    public Page<ServiceDepartmentEntity> findByStoreId(String sortDir, String sortField, int page, int size, Long storeId) {
        Pageable pageable = pageService.getPage(sortDir, sortField, page, size);
        return serviceDepartmentRepository.findByStoreId(storeId, pageable);
    }

    public List<ServiceDepartmentEntity> findByNameContaining(String name) {
        return serviceDepartmentRepository.findByNameContaining(name);
    }

    public List<ServiceDepartmentEntity> findAll() {
        return serviceDepartmentRepository.findAll();
    }

    public List<ServiceDepartmentEntity> findAllByStoreId(Long storeId) {
        return serviceDepartmentRepository.findByStoreId(storeId);
    }

    public Page<ServiceDepartmentEntity> findByStoreTypeAndCityAndServiceType(Long storeId, String city,
                                                                                 int page, int size, String sortDirNumberBooking) {
        Sort sort = Sort.by(
                new Sort.Order(pageService.getDirection(sortDirNumberBooking), "numberDoneBooking"));
        PageRequest pageRequest = PageRequest.of(page - 1, size, sort);
        Page<ServiceDepartmentEntity> pageEntity = serviceDepartmentRepository.findByStoreAndCity(storeId, city, pageRequest);
        return pageEntity;
    }

//    public Page<ServiceDepartmentClientDto> findWithKey(Long storeTypeId, String city, Long serviceTypeId, String searchKey,
//                                                        Double startRating, Double endRating, Pageable pageable) {
//
//        Page<ServiceDepartmentEntity> pageEntity = serviceDepartmentRepository.findWithKey(storeTypeId, city, serviceTypeId, searchKey, startRating, endRating, pageable);
//        return pageEntity.map(this::convertFromEntityToDto);
//    }
//
//
//    public List<ServiceDepartmentClientDto> findByStoreId(Long storeId) {
//        List<ServiceDepartmentEntity> listEntities = serviceDepartmentRepository.findByStoreId(storeId);
//        List<ServiceDepartmentClientDto> listDto = new ArrayList<>();
//        for (ServiceDepartmentEntity serviceDepartmentEntity : listEntities) {
//            listDto.add(convertFromEntityToDto(serviceDepartmentEntity));
//        }
//        return listDto;
//    }

    public ServiceDepartmentEntity findById(Long id) {
        return serviceDepartmentRepository.findById(id).orElse(null);
    }

    public ServiceDepartmentEntity findByAuthId(String authId) {
        return serviceDepartmentRepository.findByAuthId(authId);
    }

    public ServiceDepartmentEntity findByAuthIdAndStoreId(String authId, Long storeId) {
        return serviceDepartmentRepository.findByAuthIdAndStoreId(authId, storeId);
    }

    public Long getServiceDepartmentIdFromAuthId(String authId) {
        return serviceDepartmentRepository.getIdFromAuthId(authId);
    }

    public boolean save(ServiceDepartmentDto serviceDepartmentDto, HttpServletRequest request) {
        ServiceDepartmentEntity serviceDepartmentEntity = convertFromDto(serviceDepartmentDto, request);
        if (serviceDepartmentEntity == null) {
            return false;
        }
        saveAndCache(serviceDepartmentEntity);
        return true;
    }

    public boolean delete(String serviceDepartmentAuthId, Long storeId) {
        ServiceDepartmentEntity oldServiceDepartmentEntityEntity = findByAuthIdAndStoreId(serviceDepartmentAuthId, storeId);
        if (oldServiceDepartmentEntityEntity == null) {
            return false;
        }
        oldServiceDepartmentEntityEntity.setDeleted(true);
        oldServiceDepartmentEntityEntity.setUpdatedTime(new Date());
        saveAndCache(oldServiceDepartmentEntityEntity);
        return true;
    }

    private ServiceDepartmentEntity convertFromDto(ServiceDepartmentDto dto, HttpServletRequest request) {
        ServiceDepartmentEntity serviceDepartmentEntity;
        if (dto.getId() != null) {
            serviceDepartmentEntity = findById(dto.getId());
            if (serviceDepartmentEntity == null) {
                return null;
            }
            serviceDepartmentEntity.setUpdatedTime(new Date());
        } else {
            serviceDepartmentEntity = new ServiceDepartmentEntity();
            serviceDepartmentEntity.setCreatedTime(new Date());
            serviceDepartmentEntity.setServiceTypeId(dto.getServiceTypeId());
            serviceDepartmentEntity.setAuthId(String.valueOf((new Date()).getTime()));
            serviceDepartmentEntity.setStoreId(dto.getStoreId());
        }
        serviceDepartmentEntity.setName(dto.getName());
        serviceDepartmentEntity.setDescription(dto.getDescription());
        serviceDepartmentEntity.setEmail(dto.getEmail());
        serviceDepartmentEntity.setPhone(dto.getPhone());
        serviceDepartmentEntity.setPriceList(dto.getPriceList());
        serviceDepartmentEntity.setCity(dto.getCity());
        if (dto.getImage() != null) {
            String url = uploadFileService.uploadFile("serviceDepartmentImages", dto.getImage(), request);
            serviceDepartmentEntity.setImageUrl(url);
        }
        return serviceDepartmentEntity;
    }

//    private ServiceDepartmentClientDto convertFromEntityToDto(ServiceDepartmentEntity serviceDepartmentEntity) {
//        ServiceDepartmentClientDto dto = new ServiceDepartmentClientDto();
//        dto.setServiceDepartment(serviceDepartmentEntity);
//        dto.setRatingDto(ratingService.findByServiceDepartment(serviceDepartmentEntity.getId()));
//        return dto;
//    }

    private void saveAndCache(ServiceDepartmentEntity serviceDepartmentEntity) {
        serviceDepartmentRepository.save(serviceDepartmentEntity);
        serviceDepartmentCacheService.buildServiceDepartmentCache();
    }

    public boolean updatePriceList(ServiceDepartmentEntity serviceDepartmentEntity, String priceList) {
        serviceDepartmentEntity.setPriceList(priceList);
        serviceDepartmentEntity.setUpdatedTime(new Date());
        saveAndCache(serviceDepartmentEntity);
        return true;
    }
}

