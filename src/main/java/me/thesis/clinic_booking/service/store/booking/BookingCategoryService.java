package me.thesis.clinic_booking.service.store.booking;

import me.thesis.clinic_booking.entity.store.booking.BookingCategoryEntity;
import me.thesis.clinic_booking.entity.store.booking.category.CategoryBookingEntity;
import me.thesis.clinic_booking.repository.store.booking.BookingCategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service
public class BookingCategoryService {
    @Autowired
    private BookingCategoryRepository bookingCategoryRepository;

    @Autowired
    private CategoryBookingService categoryBookingService;

    public List<Long> getListCategoryIdByBooking(Long bookingId) {
        return bookingCategoryRepository.findListCategoryIdByBookingId(bookingId);
    }

    public List<CategoryBookingEntity> findByBookingId(Long serviceDepartmentId, Collection<Long> categoryIds) {
        return categoryBookingService.findByServiceDepartmentIdAndIdIn(serviceDepartmentId, categoryIds);
    }

    @Transactional
    public void save(Set<Long> listCategoryIdHasBeChosen, Long bookingId, Long serviceDepartmentId) {
        Map<Long, CategoryBookingEntity> mapCategoryById = categoryBookingService.getMapCategoryById(serviceDepartmentId);
        List<BookingCategoryEntity> listBookingAndCategory = new ArrayList<>();
        for (Long categoryId : listCategoryIdHasBeChosen) {
            BookingCategoryEntity bookingCategoryEntity = new BookingCategoryEntity();
            bookingCategoryEntity.setBookingId(bookingId);
            bookingCategoryEntity.setCategoryId(categoryId);
            bookingCategoryEntity.setCreatedTime(new Date());
            bookingCategoryEntity.setMoneyExcludeTax(mapCategoryById.get(categoryId).getMoneyExcludeTax());
            listBookingAndCategory.add(bookingCategoryEntity);
        }
        bookingCategoryRepository.deleteByBookingId(bookingId);
        bookingCategoryRepository.saveAll(listBookingAndCategory);
    }
}
