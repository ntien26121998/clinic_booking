package me.thesis.clinic_booking.service.store.booking;

import me.thesis.clinic_booking.config.security.CustomUserDetail;
import me.thesis.clinic_booking.dto.*;
import me.thesis.clinic_booking.dto.client.BookingClientDetailDto;
import me.thesis.clinic_booking.dto.client.SlotBookingDto;
import me.thesis.clinic_booking.entity.RatingEntity;
import me.thesis.clinic_booking.entity.enums.StatusBookingType;
import me.thesis.clinic_booking.entity.enums.TypeStartFrame;
import me.thesis.clinic_booking.entity.enums.WorkOffDayEnum;
import me.thesis.clinic_booking.entity.store.ServiceDepartmentEntity;
import me.thesis.clinic_booking.entity.store.StoreEntity;
import me.thesis.clinic_booking.entity.store.booking.BookingEntity;
import me.thesis.clinic_booking.entity.store.booking.CustomerBookingEntity;
import me.thesis.clinic_booking.entity.store.booking.WorkingTimeConfigEntity;
import me.thesis.clinic_booking.repository.StoreRepository;
import me.thesis.clinic_booking.repository.store.booking.BookingRepository;
import me.thesis.clinic_booking.service.*;
import me.thesis.clinic_booking.service.cache.ServiceDepartmentCacheService;
import me.thesis.clinic_booking.service.store.ServiceDepartmentService;
import me.thesis.clinic_booking.utils.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toList;

@Service
public class BookingService {

    @Autowired
    private BookingRepository bookingRepository;

    @Autowired
    private WorkingTimeConfigService workingTimeConfigService;

    @Autowired
    private CustomerBookingService customerBookingService;

    @Autowired
    private UserService userService;

    @Autowired
    private PageService pageService;

    @Autowired
    private BookingCategoryService bookingCategoryService;

    @Autowired
    private NotificationSocketService notificationSocketService;

    @Autowired
    private NotificationBookingService notificationBookingService;

    @Autowired
    private RatingService ratingService;

    @Autowired
    private StoreRepository storeRepository;

    @Autowired
    private ServiceDepartmentCacheService serviceDepartmentCacheService;

    @Autowired
    private CustomWorkingTimeConfigService customWorkingTimeConfigService;

    @Autowired
    private ServiceDepartmentService serviceDepartmentService;

    @Autowired
    private CommentBookingService commentBookingService;

    private static final String ONLY_TIME_FORMAT = "HH:mm";

    private static final Logger LOGGER = LoggerFactory.getLogger(BookingService.class);

    public List<BookingEntity> findAll() {
        return bookingRepository.findAll();
    }

    public List<BookingEntity> findByDay(Date givenDate) {
        return bookingRepository.findByBookingTimeGreaterThanEqualAndBookingTimeLessThanEqual(DateUtils.getStartOfDay(givenDate), DateUtils.getEndOfDay(givenDate));
    }

    public List<BookingEntity> findByDayAndIsSendMailFalseAndStatus(Date givenDate) {
        return bookingRepository.
                findByBookingTimeGreaterThanEqualAndBookingTimeLessThanEqualAndIsSendMailToClientFalseAndStatus
                        (DateUtils.getStartOfDay(givenDate), DateUtils.getEndOfDay(givenDate), StatusBookingType.SUCCESS);
    }

    public Map<String, List<BookingEntity>> findByBookingTime(Long serviceDepartmentId, Date activeDay) {

        WorkingTimeConfigEntity specialConfigEntity = workingTimeConfigService.findSpecificDayConfigByActiveDay(serviceDepartmentId, DateUtils.getStartOfDay(activeDay));

        /* if specific config null, find common config */
        if (specialConfigEntity == null) {
//            specialConfigEntity = workingTimeConfigService.findCommonConfig(serviceDepartmentId); // may be null
            specialConfigEntity = customWorkingTimeConfigService.getCommonWorkingTimeConfigForDateRequest(serviceDepartmentId, activeDay); // may be null
        }
        /* if specialConfigEntity == null or holiday: return no booking on this day */
        if (specialConfigEntity == null || specialConfigEntity.isHoliday()) {
            return null;
        }
        Date startOfActiveDay = DateUtils.getStartOfDay(activeDay);
        Date endOfActiveDay = DateUtils.getEndOfDay(activeDay);

        List<BookingEntity> listBookingEntities = bookingRepository
                .findByServiceDepartmentIdAndBookingTimeGreaterThanEqualAndBookingTimeLessThanEqualAndIsDeletedFalse(serviceDepartmentId, startOfActiveDay, endOfActiveDay);
        listBookingEntities.forEach(bookingEntity -> {
            List<Long> listCategoryIdHasChosen = bookingCategoryService.getListCategoryIdByBooking(bookingEntity.getId());
            bookingEntity.setListCategoryIdChosen(listCategoryIdHasChosen);
            bookingEntity.setListCategory(bookingCategoryService.findByBookingId(serviceDepartmentId, listCategoryIdHasChosen));
        });
        Map<String, String> timeOfFramesInDay = workingTimeConfigService.getAllTimeOfFramesInDay(specialConfigEntity);
        Map<String, Integer> customNumberSlotOfFrame = workingTimeConfigService.getMapCustomNumberSlotPerFrames(specialConfigEntity);
        Map<String, String> timeOfFramesWithNumberSlotInDay = convertFrameTimeStrToFrameTimeWithNumberSlotStr(timeOfFramesInDay, customNumberSlotOfFrame);
        return getListBookingOfFrames(timeOfFramesWithNumberSlotInDay, listBookingEntities);
    }

    private Map<String, String> convertFrameTimeStrToFrameTimeWithNumberSlotStr(Map<String, String> timeOfFramesInDay,
                                                                                Map<String, Integer> customNumberSlotOfFrame) {
        Map<String, String> timeOfFramesWithNumberSlotInDay = new LinkedHashMap<>();
        for (Map.Entry<String, String> timeFrame : timeOfFramesInDay.entrySet()) {
            for (Map.Entry<String, Integer> frameWithSlot : customNumberSlotOfFrame.entrySet()) {
                if (timeFrame.getValue().equals(frameWithSlot.getKey())) {
                    String frameTimeWithNumberSlot = timeFrame.getValue() + "_" + frameWithSlot.getValue();
                    timeOfFramesWithNumberSlotInDay.put(timeFrame.getKey(), frameTimeWithNumberSlot);
                }
            }
        }
        return timeOfFramesWithNumberSlotInDay;
    }

    private Map<String, List<BookingEntity>> getListBookingOfFrames(Map<String, String> timeOfFramesInDay, List<BookingEntity> listBookingEntities) {
        Map<String, List<BookingEntity>> mapListBookingOfFrames = new LinkedHashMap<>();
        for (Map.Entry<String, String> timeFrame : timeOfFramesInDay.entrySet()) {
            mapListBookingOfFrames.put(timeFrame.getValue(), new ArrayList<>());

            for (BookingEntity bookingEntity : listBookingEntities) {
                DateFormat dateFormat = new SimpleDateFormat(ONLY_TIME_FORMAT);
                String bookingDate = dateFormat.format(bookingEntity.getBookingTime());
                String frameTime = bookingEntity.getFrame();
                String startFrameTime = frameTime.split("-")[0].trim();
                if (startFrameTime.equals(timeFrame.getKey())) {
                    if (mapListBookingOfFrames.get(timeFrame.getValue()) == null) {
                        mapListBookingOfFrames.put(timeFrame.getValue(), new ArrayList<>(Arrays.asList(bookingEntity)));
                    } else {
                        mapListBookingOfFrames.get(timeFrame.getValue()).add(bookingEntity);
                    }
                }
            }
        }
        return mapListBookingOfFrames;
    }

    public BookingEntity findById(Long bookingId, Long serviceDepartmentId) {
        return bookingRepository.findByIdAndServiceDepartmentId(bookingId, serviceDepartmentId);
    }

    public BookingEntity findByIdAndDeletedFalse(Long bookingId) {
        BookingEntity bookingEntity = bookingRepository.findByIdAndIsDeletedFalse(bookingId);
        ServiceDepartmentEntity serviceDepartment = serviceDepartmentService.findById(bookingEntity.getServiceDepartmentId());
        bookingEntity.setServiceDepartmentAuthId(serviceDepartment.getAuthId());
        return bookingEntity;
    }
    public DetailBookingInfoDto getBookingInfoByBookingId(Long bookingId, Long serviceDepartmentId) {
        DetailBookingInfoDto detailBookingInfoDto = bookingRepository.detailBooking(bookingId, serviceDepartmentId);
        if (detailBookingInfoDto == null) {
            return null;
        }
        List<Long> listCategoryIdHasChosen = bookingCategoryService.getListCategoryIdByBooking(bookingId);
        detailBookingInfoDto.setListCategoryIdHasChosen(listCategoryIdHasChosen);
        detailBookingInfoDto.setListCategory(bookingCategoryService.findByBookingId(serviceDepartmentId, listCategoryIdHasChosen));

        Long customerId = detailBookingInfoDto.getCustomer().getId();
        detailBookingInfoDto.setCommentBookingEntities(commentBookingService.findByCustomerId(customerId));
        return detailBookingInfoDto;
    }

    public BookingEntity findByIdAndCustomerPhone(Long bookingId, String phone) {
        return bookingRepository.findByIdAndCustomer_Phone(bookingId, phone);
    }

    public List<BookingEntity> getListBookingConsecutive (Long bookingId, String phone) {
        BookingEntity bookingEntity = findByIdAndDeletedFalse(bookingId);
        return bookingRepository
                .getListBookingConsecutiveByPhoneNumber(bookingEntity.getServiceDepartmentId(), bookingEntity.getStartFrameAndDate(), phone);
    }

    public BookingClientDetailDto getBookingByIdAndCustomerPhoneClient(Long bookingId, String phone) {
        BookingEntity bookingEntity = bookingRepository.findByIdAndCustomer_Phone(bookingId, phone);

        List<Long> listCategoryHasChosen = bookingCategoryService.getListCategoryIdByBooking(bookingId);
        BookingClientDetailDto bookingClientDetailDto = new BookingClientDetailDto();
        bookingClientDetailDto.setBookingEntity(bookingEntity);
        bookingClientDetailDto.setStoreEntity(storeRepository.findById(bookingEntity.getStoreId()).get());
        bookingClientDetailDto.setServiceDepartmentEntity(serviceDepartmentCacheService.getByIdFromCache(bookingEntity.getServiceDepartmentId()));
        bookingClientDetailDto.setListCategoryIdHasChosen(listCategoryHasChosen);
        return bookingClientDetailDto;
    }

    public void saveBooking(BookingEntity bookingEntity) {
        bookingRepository.save(bookingEntity);
    }

    public void saveBooking(List<BookingEntity> bookingEntity) {
        bookingRepository.saveAll(bookingEntity);
    }

    @Transactional
    public ServerResponseDto create(SaveBookingDto saveBookingDto, Long serviceDepartmentId, Long storeId, CustomUserDetail customUserDetails) {
        CustomerBookingEntity customer = customerBookingService.findById(saveBookingDto.getCustomerId());
        if (customer == null) {
            return new ServerResponseDto(ResponseCase.NOT_FOUND_CUSTOMER);
        }
        saveBookingDto.setServiceDepartmentId(serviceDepartmentId);
        Date startOfDay = DateUtils.getStartOfDay(saveBookingDto.getBookingDate());
        Date endOfDay = DateUtils.getEndOfDay(saveBookingDto.getBookingDate());
//        List<BookingEntity> listToSave = createListBookingConsecutive(saveBookingDto, storeId, customUserDetails.getUserEntity().getId());
        List<BookingEntity> listToSave = createListBookingConsecutive(saveBookingDto, storeId);
        if (!checkBookingHasTheSameFrameTime(listToSave, saveBookingDto.getCustomerId(), storeId, startOfDay, endOfDay, saveBookingDto.getFrame())) {
            LOGGER.info("cant create booking for customer: {}", saveBookingDto.getCustomerId());
            return new ServerResponseDto(ResponseCase.DUPLICATE_TIME_BOOKING);
        }
        if (listToSave.isEmpty()) {
            LOGGER.info("cant create booking for this day: {}, number frame consecutive: {}",
                    saveBookingDto.getBookingDate(), saveBookingDto.getNumberFrameConsecutive());
            return new ServerResponseDto(ResponseCase.ERROR, saveBookingDto.getBookingDate());
        }
        bookingRepository.saveAll(listToSave);
        if (listToSave.size() > 1) {
            listToSave.forEach(bookingConsecutive -> {
                bookingCategoryService.save(saveBookingDto.getSetCategoryBookingId(), bookingConsecutive.getId(), serviceDepartmentId);
            });
        }
        customerBookingService.updateIsCreatedBooking(saveBookingDto.getCustomerId());
        return new ServerResponseDto(ResponseCase.ADD_SUCCESS);
    }

    @Transactional
    public ServerResponseDto updateOldBooking(SaveBookingDto saveBookingDto, Long serviceDepartmentId, Long userUpdateId) {
        BookingEntity oldBooking = bookingRepository.findByIdAndServiceDepartmentIdAndIsDeleted(saveBookingDto.getId(), serviceDepartmentId, false);
        if (oldBooking == null) {
            return new ServerResponseDto(ResponseCase.NOT_FOUND_ENTITY);
        }
        int newNumberFrameConsecutive = saveBookingDto.getNumberFrameConsecutive();
        int oldNumberFrameConsecutive = oldBooking.getNumberFrameConsecutive();
        String startFrameAndDate = oldBooking.getStartFrameAndDate();
        oldBooking.setNumberFrameConsecutive(newNumberFrameConsecutive);

        List<BookingEntity> listBookingConsecutive = bookingRepository.findByStartFrameAndDate(startFrameAndDate, serviceDepartmentId);
        if (oldNumberFrameConsecutive == newNumberFrameConsecutive) {
            bookingCategoryService.save(saveBookingDto.getSetCategoryBookingId(), oldBooking.getId(), serviceDepartmentId);
            return handleWhenNotChangeNumberFrameConsecutive(listBookingConsecutive, saveBookingDto, userUpdateId);
        }

        if (oldNumberFrameConsecutive > newNumberFrameConsecutive) {
            bookingCategoryService.save(saveBookingDto.getSetCategoryBookingId(), oldBooking.getId(), serviceDepartmentId);
            return handleWhenDecreaseNumberFrameConsecutive(listBookingConsecutive, saveBookingDto, userUpdateId);
        }

        //oldNumberFrameConsecutive < newNumberFrameConsecutive
        ServerResponseDto serverResponseDto = handleWhenIncreaseNumberFrameConsecutive(oldBooking, listBookingConsecutive, saveBookingDto, userUpdateId, oldNumberFrameConsecutive);
        ResponseStatus responseStatus = serverResponseDto.getStatus();
        if (responseStatus.equals(ResponseCase.UPDATE_SUCCESS)) {
            bookingCategoryService.save(saveBookingDto.getSetCategoryBookingId(), oldBooking.getId(), serviceDepartmentId);
        }
        return serverResponseDto;
    }

    private ServerResponseDto handleWhenNotChangeNumberFrameConsecutive(List<BookingEntity> listBookingConsecutive,
                                                                        SaveBookingDto saveBookingDto,
                                                                        Long userUpdateId) {
        for (BookingEntity bookingConsecutive : listBookingConsecutive) {
            bookingConsecutive.setStatus(saveBookingDto.getStatus());
            updateNoteAndStatusBooking(bookingConsecutive, saveBookingDto, userUpdateId);
        }
        bookingRepository.saveAll(listBookingConsecutive);
        return new ServerResponseDto(ResponseCase.UPDATE_SUCCESS);
    }

    private ServerResponseDto handleWhenDecreaseNumberFrameConsecutive(List<BookingEntity> listBookingConsecutive,
                                                                       SaveBookingDto saveBookingDto,
                                                                       Long userUpdateId) {
        List<BookingEntity> listToUpdate = new ArrayList<>();
        int newNumberFrameConsecutive = saveBookingDto.getNumberFrameConsecutive();
        int indexInList = 0;
        List<Long> listIdToDeleted = new ArrayList<>();
        for (BookingEntity bookingConsecutive : listBookingConsecutive) {
            bookingConsecutive.setStatus(saveBookingDto.getStatus());
            if (indexInList < newNumberFrameConsecutive) {
                updateNoteAndStatusBooking(bookingConsecutive, saveBookingDto, userUpdateId);
                bookingConsecutive.setNumberFrameConsecutive(saveBookingDto.getNumberFrameConsecutive());
                listToUpdate.add(bookingConsecutive);
            } else {
                listIdToDeleted.add(bookingConsecutive.getId());
            }
            indexInList++;
        }
        bookingRepository.saveAll(listToUpdate);
        bookingRepository.deletedBookingNotInConsecutive(listIdToDeleted, listToUpdate.get(0).getServiceDepartmentId());
        return new ServerResponseDto(ResponseCase.UPDATE_SUCCESS);
    }

    private ServerResponseDto handleWhenIncreaseNumberFrameConsecutive(BookingEntity oldBookingTypeStartFrame,
                                                                       List<BookingEntity> listBookingConsecutive,
                                                                       SaveBookingDto saveBookingDto,
                                                                       Long userUpdateId,
                                                                       int oldNumberFrameConsecutive) {
        Long serviceDepartmentId = oldBookingTypeStartFrame.getServiceDepartmentId();
        Date startDayBookingTime = DateUtils.getStartOfDay(oldBookingTypeStartFrame.getBookingTime());
        WorkingTimeConfigEntity configOfThisDay = workingTimeConfigService.findSpecificDayConfigByActiveDay(serviceDepartmentId, startDayBookingTime);

        saveBookingDto.setFrame(oldBookingTypeStartFrame.getFrame());
        saveBookingDto.setStartDayOfBookingTime(startDayBookingTime);
        saveBookingDto.setStoreId(oldBookingTypeStartFrame.getStoreId());
        saveBookingDto.setServiceDepartmentId(oldBookingTypeStartFrame.getServiceDepartmentId());

        Map<ResponseStatus, List<String>> mapStatusAndListFrameCanConsecutive = getMapResponseOfCheckingFrameConsecutive(saveBookingDto, oldNumberFrameConsecutive, configOfThisDay);
        if (!mapStatusAndListFrameCanConsecutive.containsKey(ResponseCase.SUCCESS)) {
            ResponseStatus responseStatus = mapStatusAndListFrameCanConsecutive.keySet().stream().findFirst().orElse(ResponseCase.ERROR);
            oldBookingTypeStartFrame.setNumberFrameConsecutive(oldNumberFrameConsecutive);
            bookingRepository.save(oldBookingTypeStartFrame);
            return new ServerResponseDto(responseStatus, mapStatusAndListFrameCanConsecutive.get(responseStatus));
        }

        List<String> listFrameCanBeConsecutive = mapStatusAndListFrameCanConsecutive.get(ResponseCase.SUCCESS);
        for (BookingEntity oldBookingConsecutive : listBookingConsecutive) {
            oldBookingConsecutive.setNumberFrameConsecutive(saveBookingDto.getNumberFrameConsecutive());
            oldBookingConsecutive.setUpdatedTime(new Date());
            oldBookingConsecutive.setUpdatedBy(userUpdateId);
        }


        for (int idxOfFrame = oldNumberFrameConsecutive; idxOfFrame < listFrameCanBeConsecutive.size(); idxOfFrame++) {
            BookingEntity bookingEntity = cloneFromOldBooking(oldBookingTypeStartFrame, listFrameCanBeConsecutive.get(idxOfFrame));
            bookingEntity.setStartFrameAndDate(oldBookingTypeStartFrame.getStartFrameAndDate());
            bookingEntity.setTypeStartFrame(TypeStartFrame.CONSECUTIVE_FRAME);
            bookingEntity.setCreatedBy(oldBookingTypeStartFrame.getCreatedBy());
            updateNoteAndStatusBooking(bookingEntity, saveBookingDto, userUpdateId);
            listBookingConsecutive.add(bookingEntity);
        }

        bookingRepository.saveAll(listBookingConsecutive);
        return new ServerResponseDto(ResponseCase.UPDATE_SUCCESS);
    }

    private BookingEntity cloneFromOldBooking(BookingEntity oldBooking, String frame) {
        BookingEntity bookingEntity = new BookingEntity();
        bookingEntity.setTitle(oldBooking.getTitle());
        bookingEntity.setStoreId(oldBooking.getStoreId());
        bookingEntity.setServiceDepartmentId(oldBooking.getServiceDepartmentId());
        bookingEntity.setStatus(oldBooking.getStatus());
        bookingEntity.setStartFrameAndDate(oldBooking.getStartFrameAndDate());
        bookingEntity.setNote(oldBooking.getNote());
//        bookingEntity.setChangeReason(oldBooking.getChangeReason());
        bookingEntity.setFrame(frame);
        bookingEntity.setBookingTime(oldBooking.getBookingTime());
        CustomerBookingEntity customer = customerBookingService.findById(oldBooking.getId());
        bookingEntity.setCustomer(customer);
        bookingEntity.setUpdatedTime(new Date());
        bookingEntity.setUpdatedBy(oldBooking.getUpdatedBy());
        bookingEntity.setNumberFrameConsecutive(oldBooking.getNumberFrameConsecutive());
        bookingEntity.setCreatedTime(new Date());
        bookingEntity.setTimeStartToEnd(oldBooking.getTimeStartToEnd());
        return bookingEntity;
    }

    private void updateNoteAndStatusBooking(BookingEntity booking, SaveBookingDto updateDto, Long userUpdateId) {
        booking.setNote(updateDto.getNote());
        booking.setStatus(updateDto.getStatus());
        booking.setUpdatedTime(new Date());
        booking.setUpdatedBy(userUpdateId);
        booking.setTimeStartToEnd(updateDto.getTimeStartToEnd());
    }

//    public List<BookingEntity> createListBookingConsecutive(SaveBookingDto saveBookingDto, Long storeId, Long idWhoCreate) {
    public List<BookingEntity> createListBookingConsecutive(SaveBookingDto saveBookingDto, Long storeId) {
        Long serviceDepartmentId = saveBookingDto.getServiceDepartmentId();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");

        Date bookingTime = saveBookingDto.getBookingDate();
        Date startOfDay = DateUtils.getStartOfDay(bookingTime);
        saveBookingDto.setStartDayOfBookingTime(startOfDay);

        List<String> listFrameCanBeConsecutive = getListFrameCanConsecutive(saveBookingDto);
        if (listFrameCanBeConsecutive == null || listFrameCanBeConsecutive.isEmpty()) {
            return Collections.emptyList(); //cant create booking
        }
        if (!workingTimeConfigService.checkHasSpecialConfigOnActiveDay(serviceDepartmentId, startOfDay)) {
            WorkingTimeConfigEntity newSpecialConfig = customWorkingTimeConfigService.getCommonWorkingTimeConfigForDateRequest(serviceDepartmentId, startOfDay);
            boolean isCloneSpecialConfigFailed = newSpecialConfig == null;
            if (isCloneSpecialConfigFailed) {
                return Collections.emptyList();
            }
        }
        String timeStartToEnd = getTimeStartToEndStr(saveBookingDto.getFrame(), listFrameCanBeConsecutive);
        saveBookingDto.setTimeStartToEnd(timeStartToEnd);
        BookingEntity bookingOnFrameStart = createBookingOnFrameStart(saveBookingDto, storeId, 1L);
        if (bookingOnFrameStart == null) {
            return Collections.emptyList(); //cant create booking
        }
        String startFrameAndDate = saveBookingDto.getFrame() + "_" + dateFormat.format(bookingTime) + "_" + bookingOnFrameStart.getId();
        List<BookingEntity> listBookingConsecutive = createListBookingConsecutive(listFrameCanBeConsecutive, saveBookingDto, storeId, 1L, startFrameAndDate);
        bookingOnFrameStart.setStartFrameAndDate(startFrameAndDate);
        listBookingConsecutive.add(bookingOnFrameStart);

        if (!saveBookingDto.getSetCategoryBookingId().isEmpty()) {
            bookingCategoryService.save(saveBookingDto.getSetCategoryBookingId(), bookingOnFrameStart.getId(), serviceDepartmentId);
        }
        return listBookingConsecutive;
    }

    private List<BookingEntity> createListBookingConsecutive(List<String> listFrameCanBeConsecutive, SaveBookingDto saveBookingDto, Long storeId, Long idWhoCreate, String startFrameAndDate) {
        List<BookingEntity> listBookingConsecutive = new ArrayList<>();
        listFrameCanBeConsecutive.forEach(frame -> {
            if (frame.equals(saveBookingDto.getFrame())) {
                return;
            }
            BookingEntity bookingEntity = convertFromDto(saveBookingDto, storeId, idWhoCreate);
            bookingEntity.setStartFrameAndDate(startFrameAndDate);
            bookingEntity.setNumberFrameConsecutive(saveBookingDto.getNumberFrameConsecutive());
            bookingEntity.setFrame(frame);
            bookingEntity.setTypeStartFrame(TypeStartFrame.CONSECUTIVE_FRAME);
            bookingEntity.setTimeStartToEnd(saveBookingDto.getTimeStartToEnd());
            bookingEntity.setPayCheck(false);
            listBookingConsecutive.add(bookingEntity);
        });
        return listBookingConsecutive;
    }

    private boolean checkBookingHasTheSameFrameTime(List<BookingEntity> listBookingToSave, Long customerId,
                                                    Long storeId, Date startOfDay, Date endOfDay, String frame) {
        if(listBookingToSave == null || listBookingToSave.isEmpty()) {
            return false;
        }
        List<BookingEntity> listBookingExistInStore = bookingRepository.getListByStoreIdAndBookingTime(storeId, customerId, startOfDay, endOfDay, frame);
        return listBookingToSave.stream().anyMatch(bookingEntity -> listBookingExistInStore.contains(bookingEntity));
//        return false;
    }

    private BookingEntity createBookingOnFrameStart(SaveBookingDto saveBookingDto, Long storeId, Long idWhoCreate) {
        Date startOfDay = DateUtils.getStartOfDay(saveBookingDto.getBookingDate());
        Date endOfDay = DateUtils.getEndOfDay(saveBookingDto.getBookingDate());
        BookingEntity bookingOnFrameStart = convertFromDto(saveBookingDto, storeId, idWhoCreate);
        Date startOfDayBookingOnFameStart = DateUtils.getStartOfDay(saveBookingDto.getBookingDate());
        Date endOfDayBookingOnFameStart = DateUtils.getEndOfDay(saveBookingDto.getBookingDate());
        bookingOnFrameStart.setTypeStartFrame(TypeStartFrame.START_FRAME);
        bookingOnFrameStart.setNumberFrameConsecutive(saveBookingDto.getNumberFrameConsecutive());
        bookingOnFrameStart.setTimeStartToEnd(saveBookingDto.getTimeStartToEnd());
        bookingOnFrameStart.setPayCheck(false);
        List<BookingEntity> listBookingExistInStoreByCustomer = bookingRepository.getListByStoreIdAndBookingTime(storeId,
                saveBookingDto.getCustomerId(), startOfDay, endOfDay, saveBookingDto.getFrame());
        for (BookingEntity booking: listBookingExistInStoreByCustomer) {
            booking.setStartOfDay(booking.getBookingTime());
            booking.setEndOfDay(booking.getBookingTime());
            boolean checkCannotBookingByCustomerId = booking.getStoreId().equals(bookingOnFrameStart.getStoreId()) &&
                        booking.getCustomer().getId().equals(bookingOnFrameStart.getCustomer().getId()) &&
                        booking.getFrame().equals(bookingOnFrameStart.getFrame()) &&
                        booking.getStartOfDay().equals(startOfDayBookingOnFameStart) &&
                        booking.getEndOfDay().equals(endOfDayBookingOnFameStart);
            if (checkCannotBookingByCustomerId) {
                return null;
            }
        }
        bookingRepository.save(bookingOnFrameStart);
        return bookingOnFrameStart;
    }

    private List<String> getListFrameCanConsecutive(SaveBookingDto saveBookingDto) {

        Long serviceDepartmentId = saveBookingDto.getServiceDepartmentId();
        Date bookingTime = saveBookingDto.getBookingDate();
        WorkingTimeConfigEntity configOfDay = workingTimeConfigService.findSpecificDayConfigByActiveDay(serviceDepartmentId, DateUtils.getStartOfDay(bookingTime));
        if (configOfDay == null) {
            return Collections.emptyList();
        }
        Integer oldFrameConsecutiveForCreatingBooking = null;
        Map<ResponseStatus, List<String>> mapStatusAndListFrameCanConsecutive =
                getMapResponseOfCheckingFrameConsecutive(saveBookingDto, oldFrameConsecutiveForCreatingBooking, configOfDay);

        if (!mapStatusAndListFrameCanConsecutive.containsKey(ResponseCase.SUCCESS)) {
            ResponseStatus responseStatus = mapStatusAndListFrameCanConsecutive.keySet().stream().findFirst().orElse(ResponseCase.ERROR);
            LOGGER.info("list frame consecutive is invalid for date: {}, store: {}, response status: {} with list frame: {}",
                    bookingTime, serviceDepartmentId, responseStatus.message,
                    mapStatusAndListFrameCanConsecutive.get(responseStatus));
            return Collections.emptyList();
        }
        return mapStatusAndListFrameCanConsecutive.get(ResponseCase.SUCCESS);
    }

    private String getTimeStartToEndStr(String frameStart, List<String> listFrameConsecutive) {
        if (listFrameConsecutive.isEmpty()) return frameStart;

        else {
            String hourEnd = listFrameConsecutive.get(listFrameConsecutive.size() - 1).split("-")[1].trim();
            return frameStart.split("-")[0].trim() + " - " + hourEnd;
        }
    }

    private Map<ResponseStatus, List<String>> getMapResponseOfCheckingFrameConsecutive(SaveBookingDto saveBookingDto,
                                                                                       Integer oldNumberFrameConsecutive,
                                                                                       WorkingTimeConfigEntity configOfDay) {
        Map<ResponseStatus, List<String>> mapResult = new HashMap<>();
        Date dayNeedCheck = saveBookingDto.getStartDayOfBookingTime();
        if (saveBookingDto.getId() == null) { //create new booking
            ResponseStatus responseStatusWhenInvalidConfig = checkInvalidConfig(configOfDay, dayNeedCheck);
            if (responseStatusWhenInvalidConfig != null) {
                mapResult.put(responseStatusWhenInvalidConfig, null);
                return mapResult;
            }
        }

        List<BookingEntity> listBookingEntity = bookingRepository.findByServiceDepartmentIdAndChosenDay(saveBookingDto.getServiceDepartmentId(), dayNeedCheck, DateUtils.getEndOfDay(dayNeedCheck));
        Map<String, List<CommonBookingInfoDto>> mapListBookingByFrameWithNumberSlot = getMapListBookingByFrameWithNumberSlot(configOfDay, listBookingEntity);

        Set<String> listFrameOfDay = mapListBookingByFrameWithNumberSlot
                .keySet().stream()
                .map(frameWithNumberSlot -> frameWithNumberSlot.split("_")[0])
                .collect(Collectors.toCollection(LinkedHashSet::new));

        int newNumberFrameConsecutive = saveBookingDto.getNumberFrameConsecutive();
        List<String> listFrameConsecutive = getListFrameConsecutive(listFrameOfDay, saveBookingDto.getFrame(), newNumberFrameConsecutive);

        if (newNumberFrameConsecutive > listFrameConsecutive.size()) {
            LOGGER.error("numberFrameConsecutive param must not greater than listFrameCanBeConsecutive size");
            mapResult.put(ResponseCase.OUT_OF_FRAME_CAN_CONSECUTIVE, null);
            return mapResult;
        }

        String frameOutOfSlot = getFirstFrameOutOfNumberSlot(newNumberFrameConsecutive, oldNumberFrameConsecutive, listFrameConsecutive, mapListBookingByFrameWithNumberSlot);
        if (frameOutOfSlot != null) {
            mapResult.put(ResponseCase.OUT_OF_FRAME_CAN_CONSECUTIVE, Collections.singletonList(frameOutOfSlot));
            return mapResult;
        }

        return Map.of(ResponseCase.SUCCESS, listFrameConsecutive);
    }

    private List<String> getListFrameConsecutive(Set<String> setFrameCanBeConsecutive, String frameStart, int numberFrameConsecutiveFromRequest) {
        List<String> listFrameConsecutive = new ArrayList<>();
        boolean isAfterStartedFrame = false;
        for (String frame : setFrameCanBeConsecutive) {
            if (frame.equals(frameStart)) {
                isAfterStartedFrame = true;
            }
            if (isAfterStartedFrame) {
                listFrameConsecutive.add(frame);
            }
            if (listFrameConsecutive.size() == numberFrameConsecutiveFromRequest) {
                break;
            }
        }
        return listFrameConsecutive;
    }

    private String getFirstFrameOutOfNumberSlot(int numberFrameConsecutiveFromRequest,
                                                Integer oldNumberFrameCanConsecutive,
                                                List<String> listFrameConsecutive,
                                                Map<String, List<CommonBookingInfoDto>> mapFrameWithNumberSlotAndListBookingOfFrames) {
        //7:00 - 8:00: [5, 3]
        Map<String, int[]> mapNumberSlotConfigAndNumberBookingByFrame = getMapNumberSlotConfigAndNumberBookingByFrame(mapFrameWithNumberSlotAndListBookingOfFrames);
        for (int idx = 0; idx < numberFrameConsecutiveFromRequest; idx++) {
            boolean isOldFrameConsecutive = oldNumberFrameCanConsecutive != null && idx < oldNumberFrameCanConsecutive;
            if (isOldFrameConsecutive) { //old frame consecutive dont need check out of slot
                continue;
            }

            String currentFrameWithoutNumberSlot = listFrameConsecutive.get(idx);
            int currentNumberSlotOfFrame = mapNumberSlotConfigAndNumberBookingByFrame.get(currentFrameWithoutNumberSlot)[0];
            int numberBookingOfFrame = mapNumberSlotConfigAndNumberBookingByFrame.get(currentFrameWithoutNumberSlot)[1];
            if (numberBookingOfFrame >= currentNumberSlotOfFrame) {
                LOGGER.error("Out of booking slot at frame {}, number booking of frame: {}, currentNumberSlotOfFrame: {}",
                        currentFrameWithoutNumberSlot, numberBookingOfFrame, currentNumberSlotOfFrame);
                return currentFrameWithoutNumberSlot;
            }
        }
        return null;
    }

    private Map<String, int[]> getMapNumberSlotConfigAndNumberBookingByFrame(Map<String, List<CommonBookingInfoDto>> mapFrameWithNumberSlotAndListBookingOfFrames) {
        Map<String, int[]> mapNumberSlotConfigAndNumberBookingByFrame = new HashMap<>();
        mapFrameWithNumberSlotAndListBookingOfFrames.forEach((frameWithNumberSlot, listBooking) -> {
            String frameWithoutNumberSlot = frameWithNumberSlot.split("_")[0];
            int numberSlot = Integer.parseInt(frameWithNumberSlot.split("_")[1]);
            int numberBookingNotCancel = countBookedSlotPerFrame(listBooking);
            mapNumberSlotConfigAndNumberBookingByFrame.put(frameWithoutNumberSlot, new int[]{numberSlot, numberBookingNotCancel});
        });
        return mapNumberSlotConfigAndNumberBookingByFrame;
    }

    private ResponseStatus checkInvalidConfig(WorkingTimeConfigEntity specialConfigEntity, Date activeDay) {
        if (specialConfigEntity == null) {
            LOGGER.error("Has no config on {}", activeDay);
            return ResponseCase.HAS_NO_CONFIG;
        }
        if (specialConfigEntity.isHoliday()) {
            LOGGER.error("this day is holiday {}", activeDay);
            return ResponseCase.HOLIDAY_CONFIG;
        }
        if (workingTimeConfigService.checkActiveDayIsWorkOffDay(activeDay, specialConfigEntity.getWorkOffFixed())) {
            LOGGER.error("this day is work off day {}", activeDay);
            return ResponseCase.WORK_OFF_DAY;
        }
        return null;
    }

    @Transactional
    public boolean deleteBooking(Long bookingId, Long serviceDepartmentId, Long userIdWhoUpdate) {
        BookingEntity bookingEntity = findById(bookingId, serviceDepartmentId);
        if (bookingEntity == null) {
            return false;
        } else {
            bookingRepository.updateDeletedTrueByStoreIdAndStartFrameAndDate(serviceDepartmentId, bookingEntity.getStartFrameAndDate(), userIdWhoUpdate);
            return true;
        }
    }

    public boolean activeSpecialBooking(Long bookingId, Long serviceDepartmentId) {
        BookingEntity bookingEntity = findById(bookingId, serviceDepartmentId);
        if (bookingEntity == null) {
            return false;
        } else {
            bookingEntity.setSpecial(!bookingEntity.isSpecial());
            bookingRepository.save(bookingEntity);
            return true;
        }
    }

    // check time of booking is out of 1 hour
    // from frame, calculating endFrameTime: "07:00 - 08:30" => "08:30:00"
    public boolean isEndFrameBookingOutOfOneHour(String frame, Date dayActive) throws ParseException {
        Date begin = DateUtils.getStartOfDay(dayActive);
        String startingDate = new SimpleDateFormat("yyyy/MM/dd").format(begin);
        String endFrameTime = frame.split("-")[1].trim() + ":00";
        String endFrameDateTimeStr = startingDate + " " + endFrameTime;

        Date endFrameDateTime = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").parse(endFrameDateTimeStr);
        Date endTimeAfterOneHour = new Date(endFrameDateTime.getTime() + 60 * 60 * 1000);
        return new Date().after(endTimeAfterOneHour);
    }

    private int countBookedSlotPerFrame(List<? extends CommonBookingInfoDto> listBooking) {
        int bookedSlot = 0;
        for (CommonBookingInfoDto booking : listBooking) {
            if (booking.getStatus() != StatusBookingType.CANCELED) {
                bookedSlot++;
            }
        }
        return bookedSlot;
    }


    private boolean checkActiveDayIsWorkOffDay(Date activeDay, WorkOffDayEnum workOffDay) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(activeDay);
        int numberOfThisActiveDayInWeek = calendar.get(Calendar.DAY_OF_WEEK);
        return numberOfThisActiveDayInWeek == workOffDay.getValue();
    }

    private BookingEntity convertFromDto(SaveBookingDto saveBookingDto, Long storeId, Long userIdWhoCreate) {
        BookingEntity bookingEntity;
        Long staffIdFromDto = saveBookingDto.getStaffId();
        if (saveBookingDto.getId() != null) {
            bookingEntity = findById(saveBookingDto.getId(), saveBookingDto.getServiceDepartmentId());
            bookingEntity.setUpdatedTime(new Date());
        } else {
            bookingEntity = new BookingEntity();
            bookingEntity.setCreatedTime(new Date());
        }
//        bookingEntity.setBookingFor(saveBookingDto.getBookingFor());
//        if(saveBookingDto.getBookingFor() == 1) {
//            bookingEntity.setCustomerBookingForEmail(saveBookingDto.getCustomerBookingForEmail());
//            bookingEntity.setCustomerBookingForEmail(saveBookingDto.getCustomerBookingForEmail());
//            bookingEntity.setCustomerBookingForEmail(saveBookingDto.getCustomerBookingForEmail());
//        }
        bookingEntity.setTitle(saveBookingDto.getTitle());
        bookingEntity.setBookingTime(saveBookingDto.getBookingDate());
        bookingEntity.setStoreId(storeId);
        bookingEntity.setServiceDepartmentId(saveBookingDto.getServiceDepartmentId());
        bookingEntity.setStatus(saveBookingDto.getStatus());
        bookingEntity.setNote(saveBookingDto.getNote());
        bookingEntity.setFrame(saveBookingDto.getFrame());
        bookingEntity.setCreatedBy(userIdWhoCreate);
        CustomerBookingEntity customer = customerBookingService.findById(saveBookingDto.getCustomerId());
        bookingEntity.setCustomer(customer);
        String customerPhone = saveBookingDto.getCustomerPhone();
//        BookingCustomerEntity oldCustomer = bookingCustomerService.findByPhone(customerPhone);
//        if (oldCustomer != null) {
//            bookingEntity.setCustomer(oldCustomer);
//        } else {
//            BookingCustomerEntity customerEntity = new BookingCustomerEntity();
//            customerEntity.setEmail(saveBookingDto.getCustomerEmail());
//            customerEntity.setPhone(customerPhone);
//            customerEntity.setName(saveBookingDto.getCustomerName());
//            customerEntity.setCreatedTime(new Date());
//            bookingEntity.setCustomer(bookingCustomerService.saveCustomer(customerEntity));
//        }
        return bookingEntity;
    }

    public List<CustomerBookingEntity> findListCustomerForTypeAHead(String phoneStr, Long storeId) {
        return customerBookingService.findByPhoneContainingAndHasBookingInStore(phoneStr, storeId);
    }

    public String changeBookingStatus(Long bookingId, Long serviceDepartmentId, StatusBookingType newStatus) {
        BookingEntity bookingEntity = findById(bookingId, serviceDepartmentId);
        if (bookingEntity == null) {
            return "NOT_FOUND";
        }
        bookingEntity.setStatus(newStatus);
        bookingEntity.setUpdatedTime(new Date());
        bookingRepository.save(bookingEntity);
        return "SUCCESS";
    }

    public boolean changeStatusPaycheck(Long bookingId, Long serviceDepartmentId) {
        BookingEntity oldBookingEntity = findById(bookingId, serviceDepartmentId);
        if(oldBookingEntity == null) {
            return false;
        }
        List<BookingEntity> listBookingConsecutive =
                bookingRepository.getListConsecutiveToPayCheck(serviceDepartmentId, oldBookingEntity.getTimeStartToEnd(), oldBookingEntity.getBookingTime());

        listBookingConsecutive.forEach(bookingEntity -> {
            if(bookingEntity.isPayCheck()) {
                bookingEntity.setPayCheck(false);
            } else {
                bookingEntity.setPayCheck(true);
            }
            bookingEntity.setUpdatedTime(new Date());
        });
        bookingRepository.saveAll(listBookingConsecutive);

        return true;
    }

    public SlotBookingDto getNumberAvailableSlotOfFrames(Long serviceDepartmentId, Date activeDay) {
        Map<String, List<BookingEntity>> listBookingOfFrames = findByBookingTime(serviceDepartmentId, activeDay);
        SlotBookingDto slotBookingDto = new SlotBookingDto();
        if (listBookingOfFrames == null || activeDay.compareTo(DateUtils.getStartOfDay(new Date())) < 0) {
            LOGGER.info("list booking null or this active day parameter is before today");
            slotBookingDto.setHasAvailableSlot(false);
            return slotBookingDto;
        }

        Map<String, Integer> numberAvailableSlotOfFrames = new LinkedHashMap<>();
        WorkingTimeConfigEntity configEntity = workingTimeConfigService
                .findSpecificDayConfigByActiveDay(serviceDepartmentId, DateUtils.getStartOfDay(activeDay));
        if (checkActiveDayIsWorkOffDay(activeDay, configEntity.getWorkOffFixed()) && configEntity.getActiveDay() == null) {
            LOGGER.info("this day is work off day");
            slotBookingDto.setHasAvailableSlot(false);
        } else {
            Map<String, Integer> numberSlotOfFrame = workingTimeConfigService.getMapCustomNumberSlotPerFrames(configEntity);
            int numberAvailableSlotOfDay = 0;
            for (Map.Entry<String, List<BookingEntity>> listBookingOfFrame : listBookingOfFrames.entrySet()) {
                int numberAvailableSlotOfFrame = numberSlotOfFrame.get(listBookingOfFrame.getKey().split("_")[0]);
                numberAvailableSlotOfFrame -= countBookedSlotPerFrame(listBookingOfFrame.getValue());
                numberAvailableSlotOfDay += numberAvailableSlotOfFrame;
                numberAvailableSlotOfFrames.put(listBookingOfFrame.getKey().split("_")[0], numberAvailableSlotOfFrame);
            }
            slotBookingDto.setHasAvailableSlot(numberAvailableSlotOfDay != 0);
            slotBookingDto.setNumberAvailableSlotOfFrames(numberAvailableSlotOfFrames);
        }
        return slotBookingDto;

    }

    public Page<BookingEntity> findByCustomerPhone(String phone, Date startTime, Date endTime,
                                                   StatusBookingType status, String sortDir, String sortField, int page, int size) {
        Pageable pageable = pageService.getPage(sortDir, sortField, page, size);
        Page<BookingEntity> pageBooking = bookingRepository
                .findByCustomer_PhoneAndStatusAndTimeRange(phone, startTime, endTime, status == null ? null : status.getValue(), pageable);
        List<RatingEntity> listRating = ratingService.findByPhone(phone);
        Map<Long, List<RatingEntity>> mapRatingAndBooking = listRating.stream().collect(Collectors.groupingBy(RatingEntity::getBookingId));
        for (BookingEntity bookingEntity : pageBooking.getContent()) {
            bookingEntity.setRatedByPhone(mapRatingAndBooking.containsKey(bookingEntity.getId()));
        }
        return pageBooking;
    }

    public Page<SummaryBookingOfCustomerDto> getListBookingByCustomerIdForPopupBooking(Long storeId, Long customerId, Pageable pageable) {
        Page<Object[]> pageResult = bookingRepository.getListBookingByCustomerIdForPopupBooking(storeId, customerId, pageable);
        return pageResult.map(arrayValue -> {
            SummaryBookingOfCustomerDto summaryBookingOfCustomerDto = new SummaryBookingOfCustomerDto();
            summaryBookingOfCustomerDto.setBookingId(Long.valueOf(String.valueOf(arrayValue[0])));
            summaryBookingOfCustomerDto.setBookingTime((Date) arrayValue[1]);
            summaryBookingOfCustomerDto.setTimeStartToEnd((String) arrayValue[2]);
            summaryBookingOfCustomerDto.setListCategory((String) arrayValue[3]);
            summaryBookingOfCustomerDto.setNumberFrameConsecutive((Integer) arrayValue[4]);
            summaryBookingOfCustomerDto.setCreatedTime((Date) arrayValue[5]);
            return summaryBookingOfCustomerDto;
        });
    }

    public Map<String, List<CommonBookingInfoDto>> getMapListBookingByFrameWithNumberSlot(WorkingTimeConfigEntity configOfDay,
                                                                                          List<? extends CommonBookingInfoDto> listBooking) {
        Set<String> setOfFrameWithNumberSlot = workingTimeConfigService.getSetOfFrameWithNumberSlot(configOfDay);
        Map<String, List<CommonBookingInfoDto>> mapFrameWithNumberSlotAndListBooking = new LinkedHashMap<>();
        Map<String, List<CommonBookingInfoDto>> mapListBookingByFrame = listBooking
                .stream()
                .collect(Collectors.groupingBy(CommonBookingInfoDto::getFrame, HashMap::new, toList()));

        setOfFrameWithNumberSlot.forEach(frameWithNumberSlot -> {
            String frameWithoutNumberSlot = frameWithNumberSlot.split("_")[0];
            if (mapListBookingByFrame.containsKey(frameWithoutNumberSlot)) {
                mapFrameWithNumberSlotAndListBooking.put(frameWithNumberSlot, mapListBookingByFrame.get(frameWithoutNumberSlot));
            } else {
                mapFrameWithNumberSlotAndListBooking.put(frameWithNumberSlot, new ArrayList<>());
            }
        });
        return mapFrameWithNumberSlotAndListBooking;
    }

    public Page<BookingEntity> findByStoreIdAndCustomerId(StoreEntity storeEntity, Long customerId, Pageable pageable) {
        Page<BookingEntity> pageResult = bookingRepository
                .findByStoreIdAndCustomerIdAndTypeStartFrameAndIsDeletedFalse(
                        storeEntity.getId(), customerId, TypeStartFrame.START_FRAME, pageable);
        return pageResult.map(bookingEntity -> {
            bookingEntity.setStoreAuthId(storeEntity.getStoreAuthId());
            bookingEntity.setStoreName(storeEntity.getStoreName());
            return bookingEntity;
        });
    }
}

