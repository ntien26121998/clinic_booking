package me.thesis.clinic_booking.service.store.booking;

import me.thesis.clinic_booking.entity.enums.WorkOffDayEnum;
import me.thesis.clinic_booking.entity.enums.WorkingTimeConfigType;
import me.thesis.clinic_booking.entity.store.booking.WorkingTimeConfigEntity;
import me.thesis.clinic_booking.repository.store.booking.WorkingTimeConfigRepository;
import org.joda.time.DateTime;
import org.joda.time.Duration;
import org.joda.time.LocalTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class WorkingTimeConfigService {
    private static final String ONLY_TIME_FORMAT = "HH:mm";

    @Autowired
    private WorkingTimeConfigRepository workingTimeConfigRepository;

    @Autowired
    private CustomWorkingTimeConfigService customWorkingTimeConfigService;

    public WorkingTimeConfigEntity findCommonConfig(Long serviceDepartmentId){
        List<WorkingTimeConfigEntity> listCommonConfigEntity = workingTimeConfigRepository.findByServiceDepartmentIdAndTypeConfig(serviceDepartmentId, WorkingTimeConfigType.COMMON_CONFIG);
        return listCommonConfigEntity.size() > 0 ? listCommonConfigEntity.get(0) : null;
    }

    public WorkingTimeConfigEntity findSpecificDayConfigByActiveDay(Long serviceDepartmentId, Date activeDay){
        WorkingTimeConfigEntity specificConfigEntity = workingTimeConfigRepository.findByServiceDepartmentIdAndActiveDay(serviceDepartmentId, activeDay);
        if(specificConfigEntity == null){
            return customWorkingTimeConfigService.getCommonWorkingTimeConfigForDateRequest(serviceDepartmentId, activeDay);
        } else {
            WorkingTimeConfigEntity commonConfigEntity = customWorkingTimeConfigService.getCommonWorkingTimeConfigForDateRequest(serviceDepartmentId, activeDay);
            if(commonConfigEntity != null){
                specificConfigEntity.setWorkOffFixed(commonConfigEntity.getWorkOffFixed());
            }
            return specificConfigEntity;
        }
    }

    // lưu config
    // nếu không tìm thấy old entity theo active day thì tạo mới
    // mặc định active day của common config là null
    public boolean save(WorkingTimeConfigEntity newEntity){
        String customizeSlotPerFrame = getCustomSlotPerFrame(newEntity);
        newEntity.setCustomSlotPerFrame(customizeSlotPerFrame);
        if (newEntity.getId() != null && !("").equals(newEntity.getId())) {
            WorkingTimeConfigEntity oldEntity = workingTimeConfigRepository
                    .findByIdAndServiceDepartmentIdAndTypeConfigAndAndActiveDay(newEntity.getId(), newEntity.getServiceDepartmentId(), newEntity.getTypeConfig(), newEntity.getActiveDay());
            if(oldEntity == null){
                newEntity.setCreatedTime(new Date());
                newEntity.setId(null);
            } else {
                newEntity.setUpdatedTime(new Date());
                newEntity.setCreatedTime(oldEntity.getCreatedTime());
            }
        } else {
            newEntity.setCreatedTime(new Date());
        }
        workingTimeConfigRepository.save(newEntity);
        return true;
    }

    public Map<String, String> getAllTimeOfFramesInDay(WorkingTimeConfigEntity configEntity) {
        Map<String, String> timeOfFramesBeforeBreak = getTimeOfFrames(configEntity.getStartWorkingTime(),
                computeNumberFrames(configEntity.getStartWorkingTime(),
                        configEntity.getStartBreakTime(), configEntity.getSlotTimeUnit()),
                configEntity.getSlotTimeUnit());

        Map<String, String> timeOfFramesAfterBreak = getTimeOfFrames(configEntity.getEndBreakTime(),
                computeNumberFrames(configEntity.getEndBreakTime(),
                        configEntity.getEndWorkingTime(), configEntity.getSlotTimeUnit()),
                configEntity.getSlotTimeUnit());

        Map<String, String> timeOfFramesInDay = new LinkedHashMap<>();
        timeOfFramesInDay.putAll(timeOfFramesBeforeBreak);
        timeOfFramesInDay.putAll(timeOfFramesAfterBreak);
        return timeOfFramesInDay;
    }

    private int computeNumberFrames(String startWorkingTime, String endWorkTime, int timeSlot) {
        DateTimeFormatter formatter = org.joda.time.format.DateTimeFormat.forPattern(ONLY_TIME_FORMAT);
        DateTime timeStartWork = formatter.parseDateTime(startWorkingTime);
        DateTime timeEndWork = formatter.parseDateTime(endWorkTime);
        Duration duration = new Duration(timeStartWork, timeEndWork);
        return (int) duration.getStandardMinutes() / timeSlot;
    }

    private Map<String, String> getTimeOfFrames(String timeStartWorkStr, int numberFrame, int timeSlot) {
        DateTimeFormatter formatter = DateTimeFormat.forPattern(ONLY_TIME_FORMAT);
        DateTime timeStartWork = formatter.parseDateTime(timeStartWorkStr);
        Map<String, String> timeOfFrames = new LinkedHashMap();
        DateTime timeStartFrame = timeStartWork;
        for (int i = 0; i < numberFrame; i++) {
            LocalTime localTimeStartFrame = new LocalTime(timeStartFrame);
            DateTime endFrame = timeStartFrame.plusMinutes(timeSlot);
            LocalTime localTimeEndFrame = new LocalTime(endFrame);
            String startStr = localTimeStartFrame.toString(ONLY_TIME_FORMAT);
            String endStr = localTimeEndFrame.toString(ONLY_TIME_FORMAT);
            timeStartFrame = endFrame;
            timeOfFrames.put(startStr, startStr + " - " + endStr);
        }
        return timeOfFrames;
    }

    //customSlotPerFrameStr format: 07:00 - 08:40_3;08:40 - 10:20_3;10:20 - 12:00_3;14:00 - 15:40_3;
    public Map<String, Integer> getMapCustomNumberSlotPerFrames(WorkingTimeConfigEntity configEntity){
        String customSlotPerFrameStr = configEntity.getCustomSlotPerFrame();
        if (customSlotPerFrameStr == null) {
            customSlotPerFrameStr = computeCustomFrameAndNumberSlotStr(configEntity);
        }
        Map<String, Integer> frameAndNumberSlot = new HashMap<>();
        String[] customSlotPerFrameStrAfterSplit = customSlotPerFrameStr.split(";");
        for(String frameAndSlot : customSlotPerFrameStrAfterSplit) {
            String[] frameAndSlotAfterSplit = frameAndSlot.split("_");
            frameAndNumberSlot.put(frameAndSlotAfterSplit[0], Integer.parseInt(frameAndSlotAfterSplit[1]));
        }
        return frameAndNumberSlot;
    }

    // customSlotPerFrameStr format: 07:00 - 08:40_3;08:40 - 10:20_3;10:20 - 12:00_3;14:00 - 15:40_3;
    public Set<String> getSetOfFrameWithNumberSlot(WorkingTimeConfigEntity config) {
        String customSlotPerFrameStr = config.getCustomSlotPerFrame();
        if (customSlotPerFrameStr == null) {
            customSlotPerFrameStr = computeCustomFrameAndNumberSlotStr(config);
        }
        return Arrays.stream(customSlotPerFrameStr.split(";")).collect(Collectors.toCollection(LinkedHashSet::new));
    }

    private String computeCustomFrameAndNumberSlotStr(WorkingTimeConfigEntity configEntity) {
        Set<String> setOfFrameWithoutNumberSlot = calculateMapStartTimeAndFrameTimeRange(configEntity);
        StringBuilder listFrameAndNumberSlot = new StringBuilder();
        int slotPerFrame = configEntity.getSlotPerFrame();

        setOfFrameWithoutNumberSlot.forEach(
                frameWithoutNumberSlot -> listFrameAndNumberSlot
                        .append(frameWithoutNumberSlot)
                        .append("_")
                        .append(slotPerFrame)
                        .append(";"));
        return listFrameAndNumberSlot.toString();
    }

    public Set<String> calculateMapStartTimeAndFrameTimeRange(WorkingTimeConfigEntity configOfDay) {
        String startWorkingTime = configOfDay.getStartWorkingTime();
        String startBreakTime = configOfDay.getStartBreakTime();
        String endBreakTime = configOfDay.getEndBreakTime();
        String endWorkingTime = configOfDay.getEndWorkingTime();

        int timePerSlot = configOfDay.getSlotTimeUnit();
        int numberFrameBeforeBreakTime = computeNumberFrameDependOnTime(startWorkingTime, startBreakTime, timePerSlot);
        Set<String> setOfFrameWithoutNumberSlotBeforeBreakTime = calculateSetOfFrameWithoutNumberSlot(startWorkingTime, numberFrameBeforeBreakTime, timePerSlot);

        int numberFrameAfterBreakTime = computeNumberFrameDependOnTime(endBreakTime, endWorkingTime, timePerSlot);
        Set<String> setOfFrameWithoutNumberSlotAfterBreakTime = calculateSetOfFrameWithoutNumberSlot(endBreakTime, numberFrameAfterBreakTime, timePerSlot);
        Set<String> setOfFrameWithoutNumberSlot = new LinkedHashSet<>();
        setOfFrameWithoutNumberSlot.addAll(setOfFrameWithoutNumberSlotBeforeBreakTime);
        setOfFrameWithoutNumberSlot.addAll(setOfFrameWithoutNumberSlotAfterBreakTime);
        return setOfFrameWithoutNumberSlot;
    }

    private int computeNumberFrameDependOnTime(String startWorkingTime, String endWorkTime, int timeSlot) {
        DateTimeFormatter formatter = DateTimeFormat.forPattern(ONLY_TIME_FORMAT);
        DateTime timeStartWork = formatter.parseDateTime(startWorkingTime);
        DateTime timeEndWork = formatter.parseDateTime(endWorkTime);
        Duration duration = new Duration(timeStartWork, timeEndWork);
        return (int) duration.getStandardMinutes() / timeSlot;
    }

    private Set<String> calculateSetOfFrameWithoutNumberSlot(String timeStartWorkStr, int numberFrame, int timePerSlot) {
        DateTimeFormatter formatter = DateTimeFormat.forPattern(ONLY_TIME_FORMAT);
        DateTime timeStartWork = formatter.parseDateTime(timeStartWorkStr);
        Set<String> setOfFrameWithoutNumberSlot = new LinkedHashSet<>();
        DateTime timeStartFrame = timeStartWork;
        for (int i = 0; i < numberFrame; i++) {
            LocalTime localTimeStartFrame = new LocalTime(timeStartFrame);
            DateTime endFrame = timeStartFrame.plusMinutes(timePerSlot);
            LocalTime localTimeEndFrame = new LocalTime(endFrame);
            String startStr = localTimeStartFrame.toString(ONLY_TIME_FORMAT);
            String endStr = localTimeEndFrame.toString(ONLY_TIME_FORMAT);
            timeStartFrame = endFrame;
            setOfFrameWithoutNumberSlot.add(startStr + " - " + endStr);
        }
        return setOfFrameWithoutNumberSlot;
    }

    public WorkingTimeConfigEntity cloneCommonConfigToSpecialConfig(WorkingTimeConfigEntity commonConfig, Date activeDay){
        WorkingTimeConfigEntity specificConfig = new WorkingTimeConfigEntity();
        specificConfig.setActiveDay(activeDay);
        specificConfig.setCreatedTime(new Date());
        specificConfig.setStoreId(commonConfig.getStoreId());
        specificConfig.setServiceDepartmentId(commonConfig.getServiceDepartmentId());
        specificConfig.setSlotTimeUnit(commonConfig.getSlotTimeUnit());
        specificConfig.setSlotPerFrame(commonConfig.getSlotPerFrame());
        specificConfig.setStartWorkingTime(commonConfig.getStartWorkingTime());
        specificConfig.setStartBreakTime(commonConfig.getStartBreakTime());
        specificConfig.setEndBreakTime(commonConfig.getEndBreakTime());
        specificConfig.setEndWorkingTime(commonConfig.getEndWorkingTime());
        specificConfig.setTypeConfig(WorkingTimeConfigType.SPECIFIC_DAY_CONFIG);
        specificConfig.setCustomSlotPerFrame(computeCustomFrameAndNumberSlotStr(commonConfig));
        specificConfig.setWorkOffFixed(commonConfig.getWorkOffFixed());
//        specificConfig.setStaffChoiceType(commonConfig.getStaffChoiceType());
        return workingTimeConfigRepository.save(specificConfig);
    }


    private String getCustomSlotPerFrame(WorkingTimeConfigEntity configEntity){
        Map<String, String > listFrames = getAllTimeOfFramesInDay(configEntity);
        StringBuilder frameAndNumberSlot = new StringBuilder();
        for (Map.Entry<String, String> frame : listFrames.entrySet()) {
            frameAndNumberSlot.append(frame.getValue() + "_" + configEntity.getSlotPerFrame()+";");
        }
        return frameAndNumberSlot.toString();
    }

    // getCustomSlotPerFrame format: 07:00 - 08:40_3;08:40 - 10:20_3;
    // slot per frame split array format: [ "", "3;08:40 - 10:20_3;"]
    public boolean changeNumberSlotOfFrame(Date activeDay, Long serviceDepartmentId, String frame, String typeChange){
        WorkingTimeConfigEntity configEntity = findSpecificDayConfigByActiveDay(serviceDepartmentId, activeDay);
        if (configEntity == null) {
            return false;
        }
        if(configEntity.getActiveDay() ==  null){
            configEntity = cloneCommonConfigToSpecialConfig(configEntity, activeDay);
        }
        String currentCustomSlotPerFrameStr = configEntity.getCustomSlotPerFrame();
        String[] currentCustomSlotPerFrameStrSplit = currentCustomSlotPerFrameStr.split(frame + "_");
        String stringBeforeFrameChange = currentCustomSlotPerFrameStrSplit[0];
        String stringAfterFrameChange = currentCustomSlotPerFrameStrSplit[1].split(";", 2)[1];
        int currentSlotOfFrame = Integer.parseInt(currentCustomSlotPerFrameStrSplit[1].split(";")[0]);
        if("decrease".equals(typeChange)){
            if(currentSlotOfFrame == 0) {
                return false;
            }
            currentSlotOfFrame--;
        } else {
            currentSlotOfFrame++;
        }
        String newCustomSlotPerFrameStr = stringBeforeFrameChange + frame + "_" + currentSlotOfFrame + ";" + stringAfterFrameChange;
        configEntity.setCustomSlotPerFrame(newCustomSlotPerFrameStr);
        configEntity.setUpdatedTime(new Date());
        workingTimeConfigRepository.save(configEntity) ;
        return true;
    }

    public boolean checkHasSpecialConfigOnActiveDay(Long serviceDepartmentId, Date activeDay) {
        WorkingTimeConfigEntity specificConfigEntity = workingTimeConfigRepository.findByServiceDepartmentIdAndActiveDay(serviceDepartmentId, activeDay);
        return  specificConfigEntity != null;
    }

    public boolean checkActiveDayIsWorkOffDay(Date activeDay, WorkOffDayEnum workOffDay) {
        if (WorkOffDayEnum.NOT_CONFIG.equals(workOffDay)) {
            return false;
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(activeDay);
        int numberOfThisActiveDayInWeek = calendar.get(Calendar.DAY_OF_WEEK);
        return numberOfThisActiveDayInWeek == workOffDay.getValue();
    }
}
