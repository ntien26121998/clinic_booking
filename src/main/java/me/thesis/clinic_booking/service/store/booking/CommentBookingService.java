package me.thesis.clinic_booking.service.store.booking;

import me.thesis.clinic_booking.dto.CommentAndFileBookingDto;
import me.thesis.clinic_booking.dto.ResponseCase;
import me.thesis.clinic_booking.dto.ResponseStatus;
import me.thesis.clinic_booking.entity.enums.CommentFromType;
import me.thesis.clinic_booking.entity.enums.CommentType;
import me.thesis.clinic_booking.entity.store.booking.CommentBookingEntity;
import me.thesis.clinic_booking.repository.store.booking.CommentBookingRepository;
import me.thesis.clinic_booking.service.UploadFileLocalService;
import me.thesis.clinic_booking.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class CommentBookingService {

    @Autowired
    private CommentBookingRepository commentBookingRepository;

    @Autowired
    private UploadFileLocalService uploadFileLocalService;

    @Autowired
    private UserService userService;

    public void saveComment(CommentBookingEntity commentBookingEntity) {
        commentBookingRepository.save(commentBookingEntity);
    }

    public CommentBookingEntity findById(Long commentId, Long customerId) {
        return commentBookingRepository.findByIdAndCustomerIdAndIsDeleted(commentId, customerId, false);
    }

    public List<CommentBookingEntity> findByCustomerId(Long customerId) {
        List<CommentBookingEntity> listComment = commentBookingRepository.findByCustomerIdAndIsDeletedOrderByCreatedTimeAsc(customerId, false);
        List<Long> listUserIdUpdateComment = listComment.stream().map(CommentBookingEntity::getUpdatedBy).collect(Collectors.toList());
        if (listUserIdUpdateComment.isEmpty()) {
            return listComment;
        }
        Map<Long, String> mapUserIdAndName = userService.getMapUserNameAndUserId(listUserIdUpdateComment);
        listComment.forEach(commentBookingEntity -> commentBookingEntity.setUpdatedByName(mapUserIdAndName.get(commentBookingEntity.getUpdatedBy())));
        return listComment;
    }

    public int countNumberImagesByCustomerId(Long customerId) {
        return commentBookingRepository.countByCustomerIdAndTypeInputAndIsDeleted(customerId, CommentType.IMAGE, false);
    }

    public boolean deleteComment(Long commentId, Long customerId, Long adminId) {
        CommentBookingEntity commentBookingEntity = findById(commentId, customerId);
        if (commentBookingEntity == null) {
            return false;
        }
        commentBookingEntity.setUpdatedBy(adminId);
        commentBookingEntity.setDeleted(true);
        commentBookingEntity.setUpdatedTime(new Date());
        commentBookingRepository.save(commentBookingEntity);
        return true;
    }

    public boolean editComment(Long commentId, Long customerId, String content, Long adminId) {
        CommentBookingEntity commentBookingEntity = findById(commentId, customerId);
        if (commentBookingEntity == null) {
            return false;
        }
        commentBookingEntity.setContent(content);
        commentBookingEntity.setUpdatedTime(new Date());
        commentBookingEntity.setUpdatedBy(adminId);
        commentBookingRepository.save(commentBookingEntity);
        return true;
    }

    public ResponseStatus saveCommentAndFileAdmin(CommentAndFileBookingDto commentAndFileBookingDto, Long adminId, HttpServletRequest request) {
        Long customerId = commentAndFileBookingDto.getCustomerId();
        String newComment = commentAndFileBookingDto.getComment();
        List<MultipartFile> listAttachFiles = commentAndFileBookingDto.getListAttachFile();

        boolean isEmptyComment = ("").equals(newComment);
        boolean isEmptyListFileAttach = listAttachFiles == null || listAttachFiles.isEmpty();
        if (isEmptyComment && isEmptyListFileAttach) {
            return ResponseCase.ERROR;
        }

        List<CommentBookingEntity> listCommentBooking = new ArrayList<>();
        // save image or pdf comment handle
        if (!isEmptyListFileAttach) {
            int currentNumberFiles = countNumberImagesByCustomerId(customerId);
            int maxNumberImage = 10;
            if ((currentNumberFiles + listAttachFiles.size()) > maxNumberImage) {
                return ResponseCase.INVALID_FILES_SIZE;
            }
            for (MultipartFile fileAttach : listAttachFiles) {
                CommentBookingEntity imageOrPdfFileComment = addCommentBeforeSave(commentAndFileBookingDto, adminId);;
                checkTypeAttachFileToSave(fileAttach, imageOrPdfFileComment, request);
                listCommentBooking.add(imageOrPdfFileComment);
            }
        }
        // save text comment handle
        if(!("").equals(newComment)){
            CommentBookingEntity textComment = addCommentBeforeSave(commentAndFileBookingDto, adminId);
            textComment.setTypeInput(CommentType.TEXT);
            listCommentBooking.add(textComment);
        }
        commentBookingRepository.saveAll(listCommentBooking);
        return ResponseCase.SUCCESS;
    }

    private void checkTypeAttachFileToSave(MultipartFile fileAttach, CommentBookingEntity imageOrPdfFileComment, HttpServletRequest request) {
        if (fileAttach.getContentType().endsWith("pdf")) {
            String pdfUrl = uploadFileLocalService.uploadFile("pdfFile", fileAttach, request);
            imageOrPdfFileComment.setFileUrl(pdfUrl);
            imageOrPdfFileComment.setPdfFileName(fileAttach.getOriginalFilename());
            imageOrPdfFileComment.setTypeInput(CommentType.PDF);
        } else {
            String imageUrl = uploadFileLocalService.uploadFile("images", fileAttach, request);
            imageOrPdfFileComment.setFileUrl(imageUrl);
            imageOrPdfFileComment.setTypeInput(CommentType.IMAGE);
        }
    }

    private CommentBookingEntity addCommentBeforeSave(CommentAndFileBookingDto commentAndFileBookingDto, Long adminId){
        CommentBookingEntity comment = new CommentBookingEntity();
        comment.setTypeUser(CommentFromType.VIA_ADMIN);
        comment.setCustomerId(commentAndFileBookingDto.getCustomerId());
        comment.setBookingId(commentAndFileBookingDto.getBookingId());
        comment.setContent(commentAndFileBookingDto.getComment());
        comment.setCreatedTime(new Date());
        comment.setCreatedBy(adminId);
        comment.setUpdatedBy(adminId);
        return comment;
    }
}

