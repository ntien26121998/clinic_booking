package me.thesis.clinic_booking.service.store.booking;

import me.thesis.clinic_booking.config.security.CustomUserDetail;
import me.thesis.clinic_booking.dto.CustomerBookingForWebDto;
import me.thesis.clinic_booking.dto.ResponseCase;
import me.thesis.clinic_booking.dto.ServerResponseDto;
import me.thesis.clinic_booking.entity.store.booking.CustomerBookingEntity;
import me.thesis.clinic_booking.repository.store.booking.CustomerBookingRepository;
import me.thesis.clinic_booking.service.store.ServiceDepartmentService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class CustomerBookingService {
    @Autowired
    private CustomerBookingRepository customerBookingRepository;

    public static final Logger LOGGER = LoggerFactory.getLogger(CustomerBookingService.class);

    public List<CustomerBookingEntity> findByPhoneContainingAndHasBookingInStore(String phoneStr, Long storeId){
        return customerBookingRepository.findByPhoneContainingAndHasBookingInStore(phoneStr, storeId);
    }

    public CustomerBookingEntity findById(Long customerId) {
        return customerBookingRepository.findByIdAndIsDeleted(customerId, false);
    }

    public void updateIsCreatedBooking(Long customerId) {
        customerBookingRepository.updateIsCreatedBooking(customerId);
    }

    public CustomerBookingEntity saveCustomer(CustomerBookingEntity customerBookingEntity){
        return customerBookingRepository.save(customerBookingEntity);
    }

    public List<CustomerBookingEntity> findByNameContaining(Long storeId, String nameKeySearch) {
        List<Object[]> listDataFromDB = customerBookingRepository.findByStoreIdAndNameContaining(storeId, nameKeySearch);
        return listDataFromDB.stream().map(arrayValue -> {
            CustomerBookingEntity customerBookingEntity = new CustomerBookingEntity();
            customerBookingEntity.setId(Long.valueOf(String.valueOf(arrayValue[0])));
            customerBookingEntity.setName(String.valueOf(arrayValue[1]));
            customerBookingEntity.setPatientCode(Long.valueOf(String.valueOf(arrayValue[2])));
            customerBookingEntity.setPhone(String.valueOf(arrayValue[3]));
            String latestNote = String.valueOf(arrayValue[4]);
            customerBookingEntity.setLatestNote(latestNote.equals("null") ? "" : latestNote);

            String latestLisCategoryHasChosen = String.valueOf(arrayValue[5]);
            customerBookingEntity.setLatestListCategoryIdHasChosen(latestLisCategoryHasChosen.equals("null") ? null : latestLisCategoryHasChosen);
            return customerBookingEntity;
        }).collect(Collectors.toList());
    }

    public ServerResponseDto createNewCustomerForStore(Long storeId, String customerName, String phone, Long createdByUserId) {
        CustomerBookingEntity customer = new CustomerBookingEntity();
        customer.setName(customerName);
        customer.setPhone(phone);
        customer.setStoreId(storeId);
        customer.setCreatedTime(new Date());
        customer.setPatientCodeFromUser(false);
        customer.setCreatedBooking(false);
        customer.setCreatedBy(createdByUserId);
        int numberLoopCanTry = 0;
        boolean isDuplicateCode;
        do {
            numberLoopCanTry++;
            if (numberLoopCanTry == 1000) {
                return new ServerResponseDto(ResponseCase.ERROR);
            }
            long newPatientCode = generatePatientCodeForCustomer(storeId);
            try {
                customer.setPatientCode(newPatientCode);
                customerBookingRepository.save(customer);
                isDuplicateCode = false;
            } catch (DataIntegrityViolationException e) {
                LOGGER.error("Duplicate patient code {} with store id {}. Create new code", newPatientCode, storeId);
                isDuplicateCode = true;
            }
        } while (isDuplicateCode);
        return new ServerResponseDto(ResponseCase.ADD_SUCCESS, customer);
    }

    public ServerResponseDto updateCustomer(CustomerBookingEntity newCustomerInfo, Long currentUserId) {
        CustomerBookingEntity oldEntity = customerBookingRepository.findByIdAndIsDeleted(newCustomerInfo.getId(), false);
        if (oldEntity == null) {
            return new ServerResponseDto(ResponseCase.NOT_FOUND_ENTITY);
        }
        Long newPatientCode = newCustomerInfo.getPatientCode();
        try {
            oldEntity.setName(newCustomerInfo.getName());
            oldEntity.setUpdatedTime(new Date());
            oldEntity.setUpdatedBy(currentUserId);
            oldEntity.setPatientCode(newPatientCode);
            oldEntity.setPatientCodeFromUser(oldEntity.getPatientCode().equals(newPatientCode));
            oldEntity.setPhone(newCustomerInfo.getPhone());
            oldEntity.setEmail(newCustomerInfo.getEmail());
            oldEntity.setGender(newCustomerInfo.getGender());
            customerBookingRepository.save(oldEntity);
            return new ServerResponseDto(ResponseCase.UPDATE_SUCCESS);
        } catch (DataIntegrityViolationException e) {
            LOGGER.error("Duplicate patient code {} with customerId {}", newPatientCode, newCustomerInfo.getId());
            return new ServerResponseDto(ResponseCase.DUPLICATE);
        }
    }

    public long generatePatientCodeForCustomer(Long storeId) {
        CustomerBookingEntity entityWithLatestCode = customerBookingRepository.findTop1ByStoreIdAndIsDeletedFalseOrderByPatientCodeDesc(storeId);
        if (entityWithLatestCode == null) {
            return 1000000;
        } else {
            return entityWithLatestCode.getPatientCode() + 1;
        }
    }

    public Page<CustomerBookingForWebDto> findByStoreId(Long storeId, String nameOrCodeFilter, Pageable pageable) {
        return customerBookingRepository
                .findCustomerBookingByStoreId(storeId, nameOrCodeFilter, pageable)
                .map(this::fromQueryValue);
    }

    private CustomerBookingForWebDto fromQueryValue(Object[] arrayValue) {
        CustomerBookingForWebDto customerBookingDto = new CustomerBookingForWebDto();
        String genderStr = String.valueOf(arrayValue[4]);
        customerBookingDto.setId(Long.valueOf(String.valueOf(arrayValue[0])));
        customerBookingDto.setName(String.valueOf(arrayValue[1]));
        customerBookingDto.setEmail(String.valueOf(arrayValue[2]));
        customerBookingDto.setPhone(String.valueOf(arrayValue[3]));
        if (genderStr.equals("null") || genderStr.equals("")) {
            customerBookingDto.setGender(0);
        } else {
            customerBookingDto.setGender(Integer.parseInt(String.valueOf(arrayValue[4])));
        }
        customerBookingDto.setPatientCode(Long.valueOf(String.valueOf(arrayValue[5])));
        customerBookingDto.setNumberBookingHasBookedInStore(Long.valueOf(String.valueOf(arrayValue[6])));
        customerBookingDto.setListStoreNameHasBooked((String) arrayValue[7]);
        customerBookingDto.setStoreAuthId((String) arrayValue[8]);
        customerBookingDto.setWhoCreatedName((String) arrayValue[9]);
        customerBookingDto.setWhoUpdatedName((String) arrayValue[10]);
        customerBookingDto.setCreatedTime((Date) arrayValue[11]);
        customerBookingDto.setUpdatedTime((Date) arrayValue[12]);
        return customerBookingDto;
    }

    public boolean deleteCustomer(Long id, Long storeId, CustomUserDetail customUserDetails) {
        CustomerBookingEntity customerBookingEntity = customerBookingRepository.findByIdAndStoreIdAndIsDeleted(id, storeId, false);
        if (customerBookingEntity == null) {
            return false;
        } else {
            customerBookingEntity.setName(customerBookingEntity.getName() + "_" + customerBookingEntity.getPatientCode());
            customerBookingEntity.setPatientCode(null);
            customerBookingEntity.setDeleted(true);
            customerBookingEntity.setUpdatedBy(customUserDetails.getUserEntity().getId());
            customerBookingEntity.setUpdatedTime(new Date());
            customerBookingRepository.save(customerBookingEntity);
            return true;
        }
    }

    public CustomerBookingEntity findByPatientCodeAndStoreId(Long patientCode, Long storeId) {
        return customerBookingRepository.findByPatientCodeAndStoreIdAndIsDeletedFalse(patientCode, storeId);
    }

    public CustomerBookingEntity findByPhone(String customerPhone){
        return customerBookingRepository.findByPhone(customerPhone);
    }
}
