package me.thesis.clinic_booking.service;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import software.amazon.awssdk.core.exception.SdkClientException;
import software.amazon.awssdk.core.exception.SdkServiceException;
import software.amazon.awssdk.core.sync.RequestBody;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.model.GetUrlRequest;
import software.amazon.awssdk.services.s3.model.PutObjectRequest;
import software.amazon.awssdk.services.s3.model.PutObjectResponse;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.net.URL;
import java.nio.ByteBuffer;
import java.util.UUID;

@Service
public class UploadFileLocalService {

    private static final Logger LOGGER = LoggerFactory.getLogger(UploadFileLocalService.class);

    @Value("${folderStorageFile}")
    private String folderStorageFile;

    @Autowired
    private S3Client s3Client;

    @Value("${aws.s3.bucket}")
    private String bucket;

    @Value("${cloud.aws.credentials.access-key}")
    private String accessKey;

    @Value("${cloud.aws.credentials.secret-key}")
    private String secretKey;

    public String uploadFile(String folderName, MultipartFile multipartFile, HttpServletRequest request) {
        String name = multipartFile.getOriginalFilename();
        String randomNameOfFile = UUID.randomUUID().toString() + name.replaceAll("\\s+", "-");
        try {
            PutObjectResponse putObjectResult = s3Client.putObject(
                    PutObjectRequest.builder()
                            .bucket(bucket)
                            .key(randomNameOfFile)
                            .contentType(MediaType.ALL_VALUE.toString())
                            .contentLength((long) multipartFile.getBytes().length)
                            .build(),
                    RequestBody.fromByteBuffer(ByteBuffer.wrap(multipartFile.getBytes())));
            final URL reportUrl = s3Client.utilities().getUrl(GetUrlRequest.builder().bucket(bucket).key(randomNameOfFile).build());
            LOGGER.info("putObjectResult = " + putObjectResult);
            LOGGER.info("reportUrl = " + reportUrl);
            return reportUrl.toString();
        } catch (SdkServiceException ase) {
            LOGGER.error("Caught an AmazonServiceException, which " + "means your request made it "
                    + "to Amazon S3, but was rejected with an error response" + " for some reason.", ase);
            LOGGER.info("Error Message:    " + ase.getMessage());
            LOGGER.info("Key:       " + name);
            throw ase;
        } catch (SdkClientException ace) {
            LOGGER.error("Caught an AmazonClientException, which " + "means the client encountered "
                    + "an internal error while trying to " + "communicate with S3, "
                    + "such as not being able to access the network.", ace);
            LOGGER.error("Error Message: {}, {}", name, ace.getMessage());
            throw ace;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public S3Object download(String objectKey) {
        AmazonS3 s3Client = getAmazonClient();
        return s3Client.getObject(new GetObjectRequest(bucket, objectKey));
    }

    private AmazonS3 getAmazonClient() {
        BasicAWSCredentials credentials = new BasicAWSCredentials(accessKey, secretKey);
        return AmazonS3ClientBuilder.standard().withCredentials(new AWSStaticCredentialsProvider(credentials)).withRegion(Regions.US_EAST_2).build();
    }
}
