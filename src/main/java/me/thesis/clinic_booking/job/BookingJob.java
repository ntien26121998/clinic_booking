package me.thesis.clinic_booking.job;

import me.thesis.clinic_booking.entity.enums.StatusBookingType;
import me.thesis.clinic_booking.entity.store.booking.BookingEntity;
import me.thesis.clinic_booking.service.store.booking.BookingService;
import me.thesis.clinic_booking.utils.DateUtils;
import me.thesis.clinic_booking.utils.EmailUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Component
public class BookingJob {
    private static final Logger LOGGER = LoggerFactory.getLogger(BookingJob.class);

    @Autowired
    private BookingService bookingService;

    @Autowired
    private EmailUtils emailUtils;

    private static final SimpleDateFormat dateFormatToStr = new SimpleDateFormat("yyyy/MM/dd HH:mm");

    @Scheduled(cron = "${job.updateBookingStatus}") // begin of every hour
    public void scheduleUpdateStatus() throws ParseException {
        Date yesterday = DateUtils.getYesterdayOfGivenDate(new Date());
        List<BookingEntity> listBookingOfYesterday = bookingService.findByDay(yesterday);
        for (BookingEntity bookingEntity : listBookingOfYesterday) {
            boolean isEndFrameBookingOutOfOneHour = bookingService
                    .isEndFrameBookingOutOfOneHour(bookingEntity.getFrame(), bookingEntity.getBookingTime());
            StatusBookingType statusBooking = bookingEntity.getStatus();
            if (isEndFrameBookingOutOfOneHour && (statusBooking == StatusBookingType.SUCCESS || statusBooking == StatusBookingType.IN_PROCESS)) {
                LOGGER.info("Auto update booking status to CANCELED, frame {}", bookingEntity.getFrame());
                bookingEntity.setStatus(StatusBookingType.CANCELED);
                bookingService.saveBooking(bookingEntity);
            }
        }
    }

    @Scheduled(cron = "${job.sendMailJob}")
    public void scheduleSendMailBooking() {
        List<BookingEntity> listBookingOfToday = bookingService.findByDayAndIsSendMailFalseAndStatus(new Date());
        List<BookingEntity> listBookingOfTomorrow = bookingService.findByDayAndIsSendMailFalseAndStatus(DateUtils.getTomorrow(new Date()));
        List<BookingEntity> listAll = new ArrayList<>();
        listAll.addAll(listBookingOfToday);
        listAll.addAll(listBookingOfTomorrow);

        Map<String, String> emailAndBookingDateStr = new HashMap<>();
        for (BookingEntity bookingEntity : listAll) {
            String customerEmail = bookingEntity.getCustomer().getEmail();
            String bookingDateStr;
            if (emailAndBookingDateStr.containsKey(customerEmail)) {
                bookingDateStr = emailAndBookingDateStr.get(customerEmail) + ", " + dateFormatToStr.format(bookingEntity.getBookingTime());
            } else {
                bookingDateStr = dateFormatToStr.format(bookingEntity.getBookingTime());
            }
            emailAndBookingDateStr.put(customerEmail, bookingDateStr);
            bookingEntity.setSendMailToClient(true);
            bookingEntity.setUpdatedTime(new Date());
        }
        //send mail here
        for (Map.Entry<String, String> entry : emailAndBookingDateStr.entrySet()) {
            emailUtils.sendBookingEmail(entry.getKey(), entry.getValue());
        }
        bookingService.saveBooking(listAll);
    }
}
