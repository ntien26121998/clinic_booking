package me.thesis.clinic_booking.repository;

import me.thesis.clinic_booking.entity.RatingEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface RatingRepository extends JpaRepository<RatingEntity, Long> {

    List<RatingEntity> findByServiceDepartmentIdOrderByCreatedTimeDesc(Long serviceDepartmentId);

    List<RatingEntity> findByPhoneNumber(String phone);

    @Query("SELECT AVG(r.ratingValue) FROM RatingEntity r WHERE r.serviceDepartmentId = ?1")
    Double countAverageRating(Long serviceDepartmentId);
}
