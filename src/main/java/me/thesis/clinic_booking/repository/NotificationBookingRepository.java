package me.thesis.clinic_booking.repository;

import me.thesis.clinic_booking.entity.NotificationBookingEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Date;

public interface NotificationBookingRepository extends JpaRepository<NotificationBookingEntity, Long> {

    Page<NotificationBookingEntity> findByServiceDepartmentIdAndStaffIdIsNull(Long serviceDepartmentId, Pageable pageable);

    Page<NotificationBookingEntity> findByStaffId(Long staffId, Pageable pageable);

    Page<NotificationBookingEntity> findByClientPhone(String clientPhone, Pageable pageable);

    int countByServiceDepartmentIdAndStaffIdIsNullAndCreatedTimeAfter(Long serviceDepartmentId, Date latestTimeReadingNotification);

    int countByStaffIdAndCreatedTimeAfter(Long staffId, Date latestTimeReadingNotification);

    int countByClientPhone(String clientPhone);
}
