package me.thesis.clinic_booking.repository.company;

import me.thesis.clinic_booking.entity.company.CompanyEntity;
import me.thesis.clinic_booking.repository.BaseRepository;

import java.util.List;

public interface CompanyRepository extends BaseRepository<CompanyEntity> {

    CompanyEntity findByCompanyAuthId(String companyId);

    CompanyEntity findByCompanyAuthIdAndIsDeleted(String companyId, boolean isDeleted);
}
