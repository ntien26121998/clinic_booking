package me.thesis.clinic_booking.repository;

import me.thesis.clinic_booking.entity.RoleEntity;
import org.springframework.data.jpa.repository.Query;

import java.util.Collection;
import java.util.Set;

public interface RoleRepository extends BaseRepository<RoleEntity> {
    RoleEntity findByNameAndCompanyId(String name, Long companyId);

    RoleEntity findFirstByNameAndCompanyId(String name, Long companyId);

    RoleEntity findByNameAndStoreId(String name, Long storeId);

    Set<RoleEntity> findByNameAndStoreIdIn(String name, Collection<Long> storeId);

    @Query("select new RoleEntity(role, store.storeCode) from RoleEntity role " +
            "inner join StoreEntity store" +
            " on store.id = role.storeId and store.storeCode in (?2)  and store.isDeleted = false " +
            "where role.name = ?1 ")
    Set<RoleEntity> findByNameAndStoreCodeIn(String name, Collection<String> storeCodes);

//    Set<RoleEntity> findByName(String name);

    RoleEntity findByName(String name);
}
