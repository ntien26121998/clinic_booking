package me.thesis.clinic_booking.repository;

import me.thesis.clinic_booking.entity.store.ServiceTypeEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ServiceTypeRepository extends JpaRepository<ServiceTypeEntity, Long> {
    Page<ServiceTypeEntity> findByStoreId(Long storeId, Pageable pageable);

    List<ServiceTypeEntity> findByStoreId(Long storeId);
}
