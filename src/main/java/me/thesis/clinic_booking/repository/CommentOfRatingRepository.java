package me.thesis.clinic_booking.repository;

import me.thesis.clinic_booking.entity.CommentOfRatingEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CommentOfRatingRepository extends JpaRepository<CommentOfRatingEntity, Long> {

    List<CommentOfRatingEntity> findByRatingServiceId(Long ratingServiceId);

    List<CommentOfRatingEntity> findAllByOrderByCreatedTimeDesc();
}
