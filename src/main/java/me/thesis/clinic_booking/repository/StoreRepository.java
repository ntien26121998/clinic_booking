package me.thesis.clinic_booking.repository;

import me.thesis.clinic_booking.entity.store.StoreEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;

public interface StoreRepository extends BaseRepository<StoreEntity> {

    Page<StoreEntity> findByCompanyIdAndIsDeletedFalse(Long companyId, Pageable pageable);

    StoreEntity findByIdAndIsDeletedFalse(Long id);

    StoreEntity findByStoreAuthIdAndIsDeletedFalse(String storeAuthId);

    StoreEntity findFirstByCompanyIdAndIsDeletedOrderByStoreCodeDesc(Long companyId, boolean isDeleted);

    StoreEntity findByStoreAuthIdAndIsDeleted(String storeAuthId, boolean isDeleted);

    @Query("select s.id from StoreEntity s where s.storeAuthId = ?1 and s.isDeleted = ?2")
    Long getIdFromAuthId(String authId, boolean isDeleted);
}
