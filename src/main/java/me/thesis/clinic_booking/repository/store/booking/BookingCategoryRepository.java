package me.thesis.clinic_booking.repository.store.booking;

import me.thesis.clinic_booking.entity.store.booking.BookingCategoryEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.Collection;
import java.util.List;

public interface BookingCategoryRepository extends JpaRepository<BookingCategoryEntity, Long> {

    @Query("select distinct bc.categoryId from BookingCategoryEntity bc where bc.bookingId = ?1")
    List<Long> findListCategoryIdByBookingId(Long bookingId);

    @Modifying
    void deleteByBookingId(Long bookingId);

    @Query(value = "select cb.category_name, count(cb.id) as numberChosen, sum(bc.money_exclude_tax) as numberMoney, cb.money_exclude_tax " +
            "from booking_category bc " +
            " inner join category_booking cb on bc.category_id = cb.id where bc.booking_id in ?1 group by cb.id", nativeQuery = true)
    List<Object[]> findCategoryInfoStatistic(Collection<Long> listBookingId);
}