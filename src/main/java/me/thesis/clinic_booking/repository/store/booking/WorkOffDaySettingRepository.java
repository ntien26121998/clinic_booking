package me.thesis.clinic_booking.repository.store.booking;

import me.thesis.clinic_booking.entity.store.booking.WorkOffDaySettingEntity;

public interface WorkOffDaySettingRepository extends BaseConfigRepository<WorkOffDaySettingEntity> {
}
