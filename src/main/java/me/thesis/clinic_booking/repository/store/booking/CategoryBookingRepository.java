package me.thesis.clinic_booking.repository.store.booking;

import me.thesis.clinic_booking.entity.store.booking.category.CategoryBookingEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Collection;
import java.util.Date;
import java.util.List;

public interface CategoryBookingRepository extends JpaRepository <CategoryBookingEntity, Long> {
    List<CategoryBookingEntity> findByServiceDepartmentIdAndIsDeletedFalse(Long serviceDepartmentId);

    CategoryBookingEntity findByServiceDepartmentIdAndCategoryNameAndIsDeletedFalse(Long serviceDepartmentId, String categoryName);

    CategoryBookingEntity findByIdAndServiceDepartmentIdAndIsDeleted(Long id, Long serviceDepartmentId, boolean isDeleted);

    @Query("select c from CategoryBookingEntity c where c.id in ?1 and c.serviceDepartmentId = ?2 and c.isDeleted = ?3")
    List<CategoryBookingEntity> findByIdInAndServiceDepartmentIdAndIsDeleted(Collection<Long> id, Long serviceDepartmentId, boolean isDeleted);

    List<CategoryBookingEntity> findAllByServiceDepartmentIdAndIsDeletedFalse(Long serviceDepartmentId);

    @Query("select c.categoryName from CategoryBookingEntity c where c.id in (?1) and c.serviceDepartmentId = ?2 and c.isDeleted = false")
    List<String> findCategoryNameByIdInAndServiceDepartmentIdAndIsDeleted(Collection<Long> id, Long serviceDepartmentId);

    List<CategoryBookingEntity> findByServiceDepartmentIdAndIsActiveTrueAndIsDeletedFalse(Long serviceDepartmentId);

    List<CategoryBookingEntity> findByServiceDepartmentId(Long serviceDepartmentId);

    List<CategoryBookingEntity> findByServiceDepartmentIdAndIdIn(Long serviceDepartmentId, Collection<Long> listCategoryId);

    @Query("select distinct bc.categoryId from BookingCategoryEntity bc where bc.bookingId = ?1")
    List<Long> findListCategoryIdByBookingId(Long bookingId);

    @Query(value = "select c.id, c.category_name, c.number_frame_consecutive_auto_fill, sum(bc.money_exclude_tax) as number_money,  " +
            " count(bc.booking_id) as number_booking, cast(b.booking_time as date) as date_group_by, group_concat(concat(b.id) SEPARATOR ',')  as list_booking_id " +
            "from category_booking c " +
            "inner join booking_category bc on c.id = bc.category_id " +
            "inner join booking b on bc.booking_id = b.id and b.is_deleted = false and b.status = 4 and b.type_start_frame = 1 " + //4: Done
            "inner join customer cm on b.customer_id = cm.id and cm.is_deleted = false " +
            "and (?3 is null or b.booking_time >= ?3) and (?4 is null or b.booking_time <= ?4) " +
            "where (coalesce(?2, null) is null or c.id in (?2)) and c.service_department_id = ?1 group by c.id, cast(b.booking_time as date) order by cast(b.booking_time as date)", nativeQuery = true)
    List<Object[]> getListCategoryStatistic(Long serviceDepartmentId, Collection<Long> listCategoryId, Date startBookingTimeRange, Date endBookingTimeRange);
}
