package me.thesis.clinic_booking.repository.store;

import me.thesis.clinic_booking.entity.store.ServiceDepartmentEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ServiceDepartmentRepository extends JpaRepository<ServiceDepartmentEntity, Long> {
    Page<ServiceDepartmentEntity> findByStoreId(Long storeId, Pageable pageable);

    List<ServiceDepartmentEntity> findByStoreId(Long storeId);

    List<ServiceDepartmentEntity> findByNameContaining(String name);

    ServiceDepartmentEntity findByAuthId(String authId);

    ServiceDepartmentEntity findByAuthIdAndStoreId(String authId, Long storeId);

    @Query("select s.id from ServiceDepartmentEntity s where s.authId = ?1 and s.isDeleted = false")
    Long getIdFromAuthId(String authId);

    @Query("select distinct sd from ServiceDepartmentEntity sd, StoreEntity s " +
            "where sd.storeId = ?1 and sd.storeId = s.id " +
            "and (?2 = '0' or s.cityName = ?2)  and s.isDeleted = false and sd.isDeleted = false ")
    Page<ServiceDepartmentEntity> findByStoreAndCity(Long storeId, String city, Pageable pageable);

}
