package me.thesis.clinic_booking.repository.store.booking;

import me.thesis.clinic_booking.entity.enums.WorkingTimeConfigType;
import me.thesis.clinic_booking.entity.store.booking.TimeSettingEntity;
import org.springframework.data.jpa.repository.Query;

import java.util.Date;

public interface TimeSettingRepository extends BaseConfigRepository<TimeSettingEntity> {
    @Query("select t from TimeSettingEntity t where t.serviceDepartmentId = ?1 and t.startWorkingTime < ?2 and " +
            "t.endWorkingTime > ?2 and t.typeConfig = ?3 and t.dayInWeek = ?4 and t.isDeleted = false")
    TimeSettingEntity findByActiveRangeAndTypeConfig(Long storeId, Date startOfDate, WorkingTimeConfigType configType, int dayInWeek);
}
