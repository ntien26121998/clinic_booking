package me.thesis.clinic_booking.repository.store.booking;

import me.thesis.clinic_booking.entity.store.booking.ImageEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ImageRepository extends JpaRepository<ImageEntity, Long> {
    Page<ImageEntity> findByServiceDepartmentId(Long serviceDepartmentId, Pageable pageable);

    List<ImageEntity> findByServiceDepartmentId(Long serviceDepartmentId);

    ImageEntity findByIdAndServiceDepartmentId(Long id, Long serviceDepartmentId);
}
