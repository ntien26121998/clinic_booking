package me.thesis.clinic_booking.repository.store.booking;

import me.thesis.clinic_booking.dto.DetailBookingInfoDto;
import me.thesis.clinic_booking.entity.enums.StatusBookingType;
import me.thesis.clinic_booking.entity.enums.TypeStartFrame;
import me.thesis.clinic_booking.entity.store.booking.BookingEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.Collection;
import java.util.Date;
import java.util.List;

public interface BookingRepository extends JpaRepository<BookingEntity, Long> {

    BookingEntity findByIdAndServiceDepartmentId(Long id, Long departmentId);

    BookingEntity findByIdAndIsDeletedFalse(Long id);

    BookingEntity findByIdAndServiceDepartmentIdAndIsDeleted(Long id, Long departmentId, boolean isDeleted);

    BookingEntity findByIdAndCustomer_Phone(Long id, String phone);

    List<BookingEntity> findByServiceDepartmentId(Long storeId);

    @Query("select b from BookingEntity b where b.serviceDepartmentId = ?1 and b.startFrameAndDate = ?2 and b.customer.phone = ?3 and b.isDeleted = false")
    List<BookingEntity> getListBookingConsecutiveByPhoneNumber(Long serviceDepartmentId, String startFrameAndDate, String customerPhone);

    List<BookingEntity> findByServiceDepartmentIdAndBookingTimeGreaterThanEqualAndBookingTimeLessThanEqualAndIsDeletedFalse(Long serviceDepartmentId, Date startOfDay, Date endOfDay);

//    @Query(value = "select *, bc.money_exclude_tax, bc.category_name from booking as b left join booking_category as bc on b.id = bc.booking_id " +
//            "where b.service_department_id = ?1 and b.booking_time >= ?2 and b.booking_time <= ?3 and b.is_deleted = false ", nativeQuery = true)
//    List<BookingEntity> getListBookingByActiveDay(Long serviceDepartmentId, Date startOfDay, Date endOfDay);


    @Query("select booking from BookingEntity booking " +
            "where booking.customer.phone = ?1 " +
            "and (?2 is null or booking.bookingTime >= ?2) " +
            "and (?3 is null or booking.bookingTime <= ?3) " +
            "and booking.isDeleted = false " +
            "and (?4 is null or booking.status = ?4) " +
            "and booking.typeStartFrame = 1")
    Page<BookingEntity> findByCustomer_PhoneAndStatusAndTimeRange(String phone, Date startTime, Date endTime,
                                                                  Integer status, Pageable pageable);


//    @Query("select distinct booking.staffId from BookingEntity booking " +
//            "where booking.serviceDepartmentId = ?1 and  booking.bookingDate = ?2 " +
//            "and booking.deleted = false and (booking.status <> com.namchantran.booking_web.entity.type_enum.StatusBookingType.CANCELED )")
//    List<Long> findListStaffIdHasBookingOnFrame(Long serviceDepartmentId, Date bookingDate);
//
//
//    @Query("select booking from BookingEntity booking " +
//            "where booking.serviceDepartmentId = ?1 and (?2 is null or booking.bookingDate >= ?2) and (?3 is null or booking.bookingDate <= ?3) " +
//            "and booking.deleted = false")
//    List<BookingEntity> findByServiceDepartmentIdAndTimeRange(Long serviceDepartmentId, Date startTime, Date endTime);
//
//    @Query("select booking from BookingEntity booking " +
//            "where booking.storeId = ?1 and (?2 is null or booking.bookingDate >= ?2) and (?3 is null or booking.bookingDate <= ?3) " +
//            "and booking.deleted = false")
//    List<BookingEntity> findByStoreIdAndTimeRange(Long storeId, Date startTime, Date endTime);

    List<BookingEntity> findByBookingTimeGreaterThanEqualAndBookingTimeLessThanEqual(Date startOfDay, Date endOfDay);

    @Query(value = "select b.id, b.booking_time, b.time_start_to_end, category_info.list_category, b.number_frame_consecutive, b.created_time " +
            "from booking b " +
            "left join ( select group_concat(cb.category_name SEPARATOR ', ') as list_category, bc.booking_id as booking_id " +
            "                 from category_booking cb inner join  booking_category bc on bc.category_id = cb.id group by bc.booking_id) " +
            "             as category_info on category_info.booking_id = b.id " +
            "where b.store_id = ?1 and b.customer_id = ?2 and b.type_start_frame = 1 and b.is_deleted = false", nativeQuery = true,

            countQuery = "select count(b.id) " +
                    "from booking b " +
                    "where b.store_id = ?1 and b.customer_id = ?2 and b.type_start_frame = 1 and b.is_deleted = false")
    Page<Object[]> getListBookingByCustomerIdForPopupBooking(Long storeId, Long customerId, Pageable pageable);

    @Query("select new me.thesis.clinic_booking.dto.DetailBookingInfoDto(b, cm) " +
            "from BookingEntity b inner join CustomerBookingEntity cm on b.customer.id = cm.id and cm.isDeleted = false " +
            "where b.id = ?1 and b.isDeleted = false and b.serviceDepartmentId = ?2")
    DetailBookingInfoDto detailBooking(Long id, Long serviceDepartmentId);

    @Query("select b from BookingEntity b where b.serviceDepartmentId = ?1 " +
            "and b.bookingTime >= ?2 and b.bookingTime <= ?3 and b.isDeleted = false order by b.bookingTime asc")
    List<BookingEntity> findByServiceDepartmentIdAndChosenDay(Long serviceDepartmentId, Date startOfDay, Date endOfDay);

    @Query("select b from BookingEntity b where b.startFrameAndDate = ?1 and b.isDeleted = false and b.serviceDepartmentId = ?2 order by b.id asc")
    List<BookingEntity> findByStartFrameAndDate(String startFrameAndDate, Long serviceDepartmentId);

    @Modifying
    @Query(value = "delete from booking where booking.id in (?1) and booking.service_department_id = ?2", nativeQuery = true)
    void deletedBookingNotInConsecutive(Collection<Long> listBookingId, Long serviceDepartmentId);

    @Modifying
    @Query(value = "update booking set is_deleted = 1, updated_time = NOW(), updated_by = ?3 where service_department_id = ?1  and start_frame_and_date = ?2", nativeQuery = true)
    void updateDeletedTrueByStoreIdAndStartFrameAndDate(Long serviceDepartmentId, String startFrameAndDate, Long userIdWhoUpdate);

    @Query(value = "select * from booking as b where b.store_id = ?1 and b.customer_id = ?2 and b.booking_time >= ?3 and b.booking_time <= ?4 " +
            "and b.frame = ?5 and b.is_deleted = false", nativeQuery = true)
    List<BookingEntity> getListByStoreIdAndBookingTime(Long storeId, Long customerId, Date startOfDay, Date endOfDay, String frame);

    Page<BookingEntity> findByStoreIdAndCustomerIdAndTypeStartFrameAndIsDeletedFalse(Long storeId, Long customerId, TypeStartFrame typeStartFrame, Pageable pageable);

    @Query("select b from BookingEntity b where b.serviceDepartmentId = ?1 and b.timeStartToEnd = ?2  and b.bookingTime = ?3 and b.isDeleted = false")
    List<BookingEntity> getListConsecutiveToPayCheck(Long serviceDepartmentId, String timeStartToEnd, Date bookingTime);

    List<BookingEntity> findByBookingTimeGreaterThanEqualAndBookingTimeLessThanEqualAndIsSendMailToClientFalseAndStatus(Date startOfDay, Date endOfDay, StatusBookingType statusBookingType);

}
