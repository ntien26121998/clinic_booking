package me.thesis.clinic_booking.repository.store.booking;

import me.thesis.clinic_booking.entity.enums.CommentType;
import me.thesis.clinic_booking.entity.store.booking.CommentBookingEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CommentBookingRepository extends JpaRepository<CommentBookingEntity, Long> {

    List<CommentBookingEntity> findByCustomerIdAndIsDeletedOrderByCreatedTimeAsc(Long customerId, boolean isDeleted);

    int countByCustomerIdAndTypeInputAndIsDeleted(Long customerId, CommentType typeInput, boolean isDeleted);

    CommentBookingEntity findByIdAndCustomerIdAndIsDeleted(Long id, Long customerId, boolean isDeleted);
}
