package me.thesis.clinic_booking.repository.store.booking;

import me.thesis.clinic_booking.entity.enums.WorkingTimeConfigType;
import me.thesis.clinic_booking.entity.store.booking.WorkingTimeConfigEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Date;
import java.util.List;

public interface WorkingTimeConfigRepository extends JpaRepository<WorkingTimeConfigEntity, Long> {

    WorkingTimeConfigEntity findByIdAndServiceDepartmentIdAndTypeConfigAndAndActiveDay(Long id, Long serviceDepartmentId,
                                                                                       WorkingTimeConfigType typeConfig, Date activeDay);
    List<WorkingTimeConfigEntity> findByServiceDepartmentIdAndTypeConfig(Long serviceDepartmentId, WorkingTimeConfigType typeConfig);
    WorkingTimeConfigEntity  findByServiceDepartmentIdAndActiveDay(Long serviceDepartmentId, Date activeDay);
}
