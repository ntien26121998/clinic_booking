package me.thesis.clinic_booking.repository.store.booking;

import me.thesis.clinic_booking.entity.store.booking.FrameSettingEntity;

public interface FrameSettingRepository extends BaseConfigRepository<FrameSettingEntity> {
}
