package me.thesis.clinic_booking.repository.store.booking;

import me.thesis.clinic_booking.entity.store.booking.CustomerBookingEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Collection;
import java.util.List;

public interface CustomerBookingRepository extends JpaRepository<CustomerBookingEntity, Long> {
    CustomerBookingEntity findTop1ByStoreIdAndIsDeletedFalseOrderByPatientCodeDesc(Long storeId);

    CustomerBookingEntity findByIdAndIsDeleted(Long id, boolean isDeleted);

    CustomerBookingEntity findByIdAndStoreIdAndIsDeleted(Long id, Long storeId, boolean isDeleted);

    CustomerBookingEntity findByPatientCodeAndStoreIdAndIsDeletedFalse(Long patientCode, Long storeId);

    CustomerBookingEntity findByPhone(String phone);

    @Query(" select distinct customer from CustomerBookingEntity customer, BookingEntity booking "+
            "where customer.phone like %?1% and customer.id = booking.customer.id and booking.storeId = ?2 " +
            "and customer.isDeleted = false ")
//            "and customer.isDeleted = false and customer.phone not in (select bl.phoneNumber from BlackListEntity bl where bl.storeId = ?2 )")
    List<CustomerBookingEntity> findByPhoneContainingAndHasBookingInStore(String phoneStr, Long storeId);

    @Query(value = "Select c.id, c.name, c.patient_code, c.phone, b1.note as latest_note_of_booking, " +
            "(select group_concat(bc.category_id) from booking_category bc where bc.booking_id = b1.id) as list_category_of_latest_booking " +
            "from customer c " +
            "LEFT JOIN booking b1 " +
            "    ON b1.id = (SELECT id FROM booking b2 WHERE b2.customer_id = c.id and b2.is_deleted = false and b2.type_start_frame = 1 ORDER BY b2.created_time DESC LIMIT 1) " +
            "where c.store_id = ?1 " +
            "and c.is_deleted = false and c.name like CONCAT('%', ?2,'%') and c.is_created_booking = true LIMIT 10", nativeQuery = true)
    List<Object[]> findByStoreIdAndNameContaining(Long storeId, String nameKeySearch);

    @Modifying
    @Query(value = "update CustomerBookingEntity c set c.isCreatedBooking = true where c.id = ?1")
    void updateIsCreatedBooking(Long customerId);

    @Query(value = "select c.id, c.name, c.email, c.phone, c.gender, c.patient_code, count(b.id) as number_booking, ns.store_name, ns.store_auth_Id, " +
            "who_created.name as who_created_name, who_updated.name as who_updated_name, c.created_time, c.updated_time " +
            "from customer c inner join store ns on c.store_id = ns.id " +
            "inner join user as who_created on who_created.id = c.created_by " +
            "left join user as who_updated on who_updated.id = c.updated_by " +
            "left join booking b " +
            "    on c.id = b.customer_id and b.is_deleted = false and b.type_start_frame = 1 " +
            "where c.store_id = ?1 " +
            "and c.is_deleted = false and c.is_created_booking = true " +
            "and (?2 is null or c.name like CONCAT('%', ?2,'%') or c.patient_code like CONCAT('%', ?2,'%'))" +
            "group by c.id",

            countQuery = "Select count(c.id) from customer c " +
                    "where c.store_id = ?1 " +
                    "and c.is_deleted = false and c.is_created_booking = true " +
                    "and (?2 is null or c.name like CONCAT('%', ?2,'%') or c.patient_code like CONCAT('%', ?2,'%'))",
            nativeQuery = true)
    Page<Object[]> findCustomerBookingByStoreId(Long storeId, String nameOrCodeFilter, Pageable pageable);

    List<CustomerBookingEntity> findByStoreIdAndIsDeletedFalse(Long storeId);

    CustomerBookingEntity findByPhoneAndStoreIdAndIsDeletedFalse (String phone, Long storeId);
}
