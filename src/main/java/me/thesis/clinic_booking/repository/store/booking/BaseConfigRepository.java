package me.thesis.clinic_booking.repository.store.booking;

import me.thesis.clinic_booking.entity.enums.WorkingTimeConfigType;
import me.thesis.clinic_booking.entity.store.booking.BaseConfigEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.NoRepositoryBean;

import java.util.Date;
import java.util.List;

@NoRepositoryBean
public interface BaseConfigRepository<T extends BaseConfigEntity> extends JpaRepository<T, Long> {
    List<T> findByServiceDepartmentIdAndIsDeletedFalse(Long serviceDepartmentId);

    T findByServiceDepartmentIdAndIdAndIsDeletedFalse(Long serviceDepartmentId, Long id);

    @Query("select t from #{#entityName} t where t.serviceDepartmentId = ?1 and t.startActiveRange <= ?2 and t.endActiveRange >= ?2 " +
            "and t.typeConfig = ?3 " +
            "and t.isDeleted = false ")
    T findByActiveRangeAndTypeConfig(Long serviceDepartmentId, Date startOfDate, WorkingTimeConfigType configType);

}
