package me.thesis.clinic_booking.repository;

import me.thesis.clinic_booking.entity.UserEntity;
import me.thesis.clinic_booking.entity.enums.StatusUserType;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;

import java.util.Collection;
import java.util.List;

public interface UserRepository extends BaseRepository<UserEntity> {
    UserEntity findByEmail(String email);

    UserEntity findByEmailAndStatus(String email, StatusUserType status);

    UserEntity findByIdAndStoreIdAndServiceDepartmentId(Long id, Long storeId, Long serviceDepartmentId);

    Page<UserEntity> findByRoleEntities_Name(String roleName, Pageable pageable);

    Page<UserEntity> findByServiceDepartmentIdAndRoleEntities_Name(Long serviceDepartmentId, String roleName, Pageable pageable);

    Page<UserEntity> findByStoreIdAndRoleEntities_Name(Long storeId, String roleName, Pageable pageable);

    List<UserEntity> findByServiceDepartmentIdAndRoleEntities_Name(Long serviceDepartmentId, String roleName);

    List<UserEntity> findByServiceDepartmentIdAndRoleEntities_NameAndStatus(Long serviceDepartmentId, String roleName, StatusUserType status);

    List<UserEntity> findByServiceDepartmentIdAndRoleEntities_NameAndIdNotIn(Long serviceDepartmentId, String roleName, List<Long> listId);

    @Query("select u from UserEntity u where u.id in (?1)")
    List<UserEntity> getListUserByIdIn(Collection<Long> userIds);
}
