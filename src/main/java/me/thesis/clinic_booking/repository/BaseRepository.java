package me.thesis.clinic_booking.repository;

import me.thesis.clinic_booking.entity.BaseEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface BaseRepository<T extends BaseEntity> extends JpaRepository<T, Long> {

    Optional<T> findById(Long id);
}
