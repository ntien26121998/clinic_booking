package me.thesis.clinic_booking.utils;

import org.apache.poi.ss.usermodel.Row;

public interface IRowEmptyCheck {
    boolean isRowEmpty(Row currentRow);
}
