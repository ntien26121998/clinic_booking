package me.thesis.clinic_booking.utils;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ExcelImportUtils {

    public static List<String[]> readListRowValueWithSkipHeader(@NotNull MultipartFile excelFile,
                                                                int numberColumnWantToSelect,
                                                                IRowEmptyCheck iRowEmptyCheckToBreakLoop) throws IOException, InvalidFormatException {

        List<String[]> listRowResult = new ArrayList<>();
        Workbook workbook = WorkbookFactory.create(excelFile.getInputStream());
        Sheet datatypeSheet = workbook.getSheetAt(0);
        DataFormatter dataFormatter = new DataFormatter(); // convert data to string
        Iterator<Row> iterator = datatypeSheet.iterator();
        iterator.next();

        while (iterator.hasNext()) {
            Row currentRow = iterator.next();
            if (iRowEmptyCheckToBreakLoop.isRowEmpty(currentRow)) {
                break;
            }
            String[] rowValue = getRowValues(numberColumnWantToSelect, currentRow, dataFormatter);
            listRowResult.add(rowValue);
        }
        workbook.close();
        return listRowResult;

    }

    private static String[] getRowValues(int numberColumnWantToSelect, Row currentRow, DataFormatter dataFormatter) {
        String[] rowValue = new String[numberColumnWantToSelect];
        for (int i = 0; i < numberColumnWantToSelect; i++) {
            Cell cell = currentRow.getCell(i, Row.MissingCellPolicy.CREATE_NULL_AS_BLANK);
            rowValue[i] = dataFormatter.formatCellValue(cell);
        }
        return rowValue;
    }
}
