package me.thesis.clinic_booking.utils;

import me.thesis.clinic_booking.job.EmailTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.util.List;

@Component
public class EmailUtils {

    @Autowired
    private JavaMailSender javaMailSender;

    private static final Logger LOGGER = LoggerFactory.getLogger(EmailUtils.class);

    private String[] convertArrayListEmailToArr(List<String> listEmail) {
        String[] emailArr = new String[listEmail.size()];
        return listEmail.toArray(emailArr);
    }

    public void sendBookingEmail(String emailTo, String listBookingDate) {
        EmailTemplate emailMessage = new EmailTemplate();
        String subject = "[Physiotherapy Clinic] Thông báo về lịch đặt sắp tới";
        String sb = "<p> Bạn có lịch đặt sắp tới vào: " + listBookingDate + ". Vui lòng kiểm tra và theo dõi lịch</p>";
        emailMessage.setContent(sb);
        emailMessage.setReceiver(emailTo);
        emailMessage.setSubject(subject);
        sendEmail(emailMessage);
    }

    @Async
    public void sendEmail(EmailTemplate messageTemplate) {
        new Thread(() -> {
            try {
                LOGGER.info("Sending email {} to {} ", messageTemplate.getSubject(), messageTemplate.getReceiver());
                MimeMessage mimeMessage = javaMailSender.createMimeMessage();
                MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mimeMessage, true);
                mimeMessageHelper.setTo(messageTemplate.getReceiver());
                mimeMessageHelper.setSubject(messageTemplate.getSubject());
                mimeMessageHelper.setText(messageTemplate.getContent(), true);
                javaMailSender.send(mimeMessage);
            } catch (MessagingException e) {
                LOGGER.error("Error when sending email : ", e);
            }
        }).start();
    }
}