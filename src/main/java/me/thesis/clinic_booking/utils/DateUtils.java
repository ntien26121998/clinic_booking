package me.thesis.clinic_booking.utils;

import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.Date;

@Service
public class DateUtils {

    public static Date getStartOfDay(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTime();
    }

    public static Date getEndOfDay(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTime();
    }

    public static Date getYesterdayOfGivenDate(Date giveDate){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(giveDate);
        calendar.add(Calendar.DATE, -1);
        return calendar.getTime();
    }

    public static Date getTomorrow(Date giveDate){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(giveDate);
        calendar.add(Calendar.DATE, 1);
        return calendar.getTime();
    }
}
