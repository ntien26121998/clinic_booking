package me.thesis.clinic_booking.utils;

import me.thesis.clinic_booking.entity.enums.ValueEnum;

import javax.persistence.AttributeConverter;

public abstract class AbstractEnumConverter<T extends Enum<T> & ValueEnum<E>, E> implements AttributeConverter<T, E> {

    private final Class<T> clazz;

    public AbstractEnumConverter(Class<T> clazz) {
        this.clazz = clazz;
    }

    @Override
    public E convertToDatabaseColumn(T type) {
        if(type == null) {
            return null;
        }
        return type.getValue();
    }

    @Override
    public T convertToEntityAttribute(E value) {
        T[] enums = clazz.getEnumConstants();
        for (T type : enums) {
            if (type.getValue() == value) {
                return type;
            }
        }
        throw new IllegalArgumentException("Invalid value.");
    }
}
