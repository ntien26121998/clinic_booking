package me.thesis.clinic_booking.utils;

public class Constant {
    public static final String SYSTEM_ADMIN = "SYSTEM_ADMIN";
    public static final String COMPANY_ADMIN = "COMPANY_ADMIN";
    public static final String STORE_ADMIN = "STORE_ADMIN";
    public static final int ROLE_TYPE_COMPANY = 1;
    public static final int ROLE_TYPE_STORE = 2;
    public static final String ASC = "ASC";
}
