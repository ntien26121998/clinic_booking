package me.thesis.clinic_booking.web.controller;

import me.thesis.clinic_booking.entity.store.StoreEntity;
import me.thesis.clinic_booking.entity.store.booking.CustomerBookingEntity;
import me.thesis.clinic_booking.service.ServiceTypeService;
import me.thesis.clinic_booking.service.cache.ServiceDepartmentCacheService;
import me.thesis.clinic_booking.service.cache.StoreCacheService;
import me.thesis.clinic_booking.service.store.booking.CustomerBookingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/store/{storeAuthId}")
public class StoreAdminController {

    @Autowired
    private StoreCacheService storeCacheService;

    @Autowired
    private ServiceTypeService serviceTypeService;

    @Autowired
    private ServiceDepartmentCacheService serviceDepartmentCacheService;

    @Autowired
    private CustomerBookingService customerBookingService;

    @GetMapping("/information")
    public String storeInformationPage(@PathVariable String storeAuthId, Model model) {
        model.addAttribute("companyId", 1);
        addAttributesToModel(model, storeAuthId);
        return "system/stores/store_information";
    }

    @GetMapping("/service_department")
    public String serviceDepartmentManagementPage(@PathVariable String storeAuthId, Model model) {
        StoreEntity storeEntity = storeCacheService.getByAuthIdFromCache(storeAuthId);
        model.addAttribute("store", storeEntity);
        model.addAttribute("listServiceType", serviceTypeService.getListByStoreId(storeEntity.getId()));
        return "system/stores/service_department_management";
    }

    @GetMapping("/store_account")
    public String storeAccountManagementPage(@PathVariable String storeAuthId, Model model) {
        addAttributesToModel(model, storeAuthId);
        return "system/stores/store_account";
    }

    @GetMapping("/store_statistic")
    public String getStatisticPage(@PathVariable String storeAuthId, Model model){
        StoreEntity storeEntity = storeCacheService.getByAuthIdFromCache(storeAuthId);
        model.addAttribute("store", storeEntity);
        model.addAttribute("listServiceDepartment", serviceDepartmentCacheService.getByStoreId(storeEntity.getId()));
        return "system/stores/statistic";
    }

    @GetMapping("/store_customer")
    public String getListCustomerStore(@PathVariable String storeAuthId, Model model) {
        addAttributesToModel(model, storeAuthId);
        return "system/stores/store_customer";
    }

    @GetMapping("/listBookingOfCustomer/{storeAuthId}/{patientCode}")
    public String listCustomer(@PathVariable("storeAuthId") String storeAuthId,
                               @PathVariable Long patientCode,
                               Model model) {
        StoreEntity storeEntity = storeCacheService.getByAuthIdFromCache(storeAuthId);
        CustomerBookingEntity customer = customerBookingService.findByPatientCodeAndStoreId(patientCode, storeEntity.getId());
        customer.setStoreAuthId(storeAuthId);
        model.addAttribute("customer", customer);
        model.addAttribute("store", storeEntity);
        return "system/stores/list_booking_of_customer";
    }

    private void addAttributesToModel(Model model, String storeAuthId) {
        StoreEntity storeEntity = storeCacheService.getByAuthIdFromCache(storeAuthId);
        model.addAttribute("store", storeEntity);
    }
}