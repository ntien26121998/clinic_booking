package me.thesis.clinic_booking.web.controller;

import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class UserController {


    @GetMapping("/login")
    public String loginForm(@AuthenticationPrincipal UserDetails userDetails) {
        if(userDetails == null) {
            return "system/login";
        }
            String uri = "system/store";
            return "redirect:" + uri;
    }

    @GetMapping("/401")
    public String page401Form() {
        return "error/403";
    }
}