package me.thesis.clinic_booking.web.controller;

import me.thesis.clinic_booking.config.security.CustomUserDetail;
import me.thesis.clinic_booking.entity.store.ServiceDepartmentEntity;
import me.thesis.clinic_booking.entity.store.StoreEntity;
import me.thesis.clinic_booking.service.RoleService;
import me.thesis.clinic_booking.service.store.ServiceDepartmentService;
import me.thesis.clinic_booking.service.store.StoreService;
import me.thesis.clinic_booking.service.store.booking.WorkingTimeConfigService;
import me.thesis.clinic_booking.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Date;

@Controller
@RequestMapping("/service_department/{authId}")
public class ServiceDepartmentAdminController {

    @Autowired
    private StoreService storeService;

    @Autowired
    private ServiceDepartmentService serviceDepartmentService;

    @Autowired
    private WorkingTimeConfigService workingTimeConfigService;

    @Autowired
    private RoleService roleService;

    @GetMapping("/working_time_config")
    public String getWorkingTimeConfigurationPage(@PathVariable String authId, Model model){
        ServiceDepartmentEntity serviceDepartmentEntity = serviceDepartmentService.findByAuthId(authId);
        addAttributesToModel(model, authId);
//        model.addAttribute("commonConfig", workingTimeConfigService.findCommonConfig(serviceDepartmentEntity.getId()));
        return "system/stores/service_department/working_time_config";
    }

    @GetMapping("/information")
    public String getInformationPage(@PathVariable String authId, Model model){
        addAttributesToModel(model, authId);
        return "system/stores/service_department/service_department_information";
    }

    @GetMapping("/cashier")
    public String getCashierManagementPage(@PathVariable String authId, Model model) {
        addAttributesToModel(model, authId);
        return "system/stores/service_department/cashier_management";
    }


    @GetMapping("/booking")
    public String getBookingManagementPage(@PathVariable String authId, Model model, @AuthenticationPrincipal CustomUserDetail customUserDetail){
        boolean isCashierStaff = roleService.isCashierStaff(customUserDetail.getUserEntity().getId(), customUserDetail.getUserEntity().getStoreId(), customUserDetail.getUserEntity().getServiceDepartmentId());
        if (isCashierStaff) {
            model.addAttribute("isCashier", true);
        }
        ServiceDepartmentEntity serviceDepartmentEntity = serviceDepartmentService.findByAuthId(authId);
        addAttributesToModel(model, authId);
        model.addAttribute("specificConfig", workingTimeConfigService.findSpecificDayConfigByActiveDay(serviceDepartmentEntity.getId(), DateUtils.getStartOfDay(new Date())));
        return "system/stores/service_department/booking";
    }

    @GetMapping("/image")
    public String getImageManagementPage(@PathVariable String authId, Model model){
        addAttributesToModel(model, authId);
        return "system/stores/service_department/image_management";
    }

    @GetMapping("/statistic")
    public String getStatisticPage(@PathVariable String authId, Model model){
        addAttributesToModel(model, authId);
        return "system/stores/service_department/statistic";
    }

    private void addAttributesToModel(Model model, String authId) {
        ServiceDepartmentEntity serviceDepartmentEntity = serviceDepartmentService.findByAuthId(authId);
        StoreEntity storeEntity = storeService.findById(serviceDepartmentEntity.getStoreId());
        model.addAttribute("store", storeEntity);
        model.addAttribute("serviceDepartment", serviceDepartmentEntity);
    }
}
