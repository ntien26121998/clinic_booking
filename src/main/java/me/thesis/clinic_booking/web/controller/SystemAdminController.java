package me.thesis.clinic_booking.web.controller;

import me.thesis.clinic_booking.service.store.StoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/system")
public class SystemAdminController {
    @Autowired
    private StoreService storeService;

    @GetMapping("/store")
    public String getStoreManagementPage(Model model) {
//        model.addAttribute("listStoreType", storeTypeService.findAll());
        model.addAttribute("companyId", 1);
        return "system/store_management";
    }

//    @GetMapping("/companies")
//    public String getCompaniesPage(){
//        return "system/company/companies";
//    }


    @GetMapping("/system_account")
    public String getSystemAccountManagementPage() {
        return "system/system_account_management";
    }

    @GetMapping("/service_type")
    public String getServiceTypeManagementPage(Model model) {
        model.addAttribute("listStore", storeService.findAll());
        return "system/service_type_management";
    }

    @GetMapping("/contact")
    public String getContactManagementPage(Model model) {
        return "system/contact_management";
    }
}
