package me.thesis.clinic_booking.web.controller.client;

import me.thesis.clinic_booking.config.security.CustomUserDetail;
import me.thesis.clinic_booking.entity.RoleEntity;
import me.thesis.clinic_booking.entity.UserEntity;
import me.thesis.clinic_booking.entity.enums.RoleType;
import me.thesis.clinic_booking.entity.store.ServiceDepartmentEntity;
import me.thesis.clinic_booking.entity.store.StoreEntity;
import me.thesis.clinic_booking.entity.store.booking.CustomerBookingEntity;
import me.thesis.clinic_booking.service.RatingService;
import me.thesis.clinic_booking.service.RoleService;
import me.thesis.clinic_booking.service.cache.ServiceDepartmentCacheService;
import me.thesis.clinic_booking.service.cache.StoreCacheService;
import me.thesis.clinic_booking.service.store.ServiceDepartmentService;
import me.thesis.clinic_booking.service.store.StoreService;
import me.thesis.clinic_booking.service.store.booking.CategoryBookingService;
import me.thesis.clinic_booking.service.store.booking.CustomerBookingService;
import me.thesis.clinic_booking.service.store.booking.ImageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@Controller
@RequestMapping("/")
@PropertySource("classpath:application.properties")
public class ClientController {
    @Autowired
    private ServiceDepartmentCacheService serviceDepartmentCacheService;
    @Autowired
    private StoreCacheService storeCacheService;
    @Autowired
    private StoreService storeService;
    @Autowired
    private ServiceDepartmentService serviceDepartmentService;
    @Autowired
    private RoleService roleService;
    @Autowired
    private CustomerBookingService customerBookingService;
    @Autowired
    private RatingService ratingService;
    @Autowired
    private ImageService imageService;
    @Autowired
    private CategoryBookingService categoryBookingService;

    @GetMapping("/")
    public String getClientHomePage(Model model) {
        addAttributeToModel(model);
        return "client/homepage";
    }

//    @GetMapping("/search")
//    public String getClientHomePage(Model model, @RequestParam String searchKey) {
//        addAttributeToModel(model);
//        model.addAttribute("searchKey", searchKey);
//        return "client/search";
//    }


    @GetMapping("/services")
    public String getListServiceDepartmentPage(@RequestParam("storeId") Long storeId, Model model) {
        addAttributeToModel(model);
        model.addAttribute("listService", serviceDepartmentCacheService.getByStoreId(storeId));
        model.addAttribute("currentStore", storeCacheService.getByIdFromCache(storeId));
        return "client/service_department";
    }

    @GetMapping("/booking_history")
    public String getBookingHistoryPage(Model model, @AuthenticationPrincipal CustomUserDetail customUserDetail) {
        addAttributeToModel(model);
        if (customUserDetail == null || !roleService.isClientWithPhone(customUserDetail)) {
            model.addAttribute("status", "NO_LOGIN");
        } else {
            String customerPhone = customUserDetail.getUserEntity().getPhone();
            model.addAttribute("customer", customerBookingService.findByPhone(customerPhone));
        }
        return "client/booking_detail_client";
    }

    @GetMapping("/contact")
    public String getContactPage(Model model) {
        addAttributeToModel(model);
        return "client/contact";
    }

    @GetMapping("/services/{serviceDepartmentAuthId}")
    public String getDetailServiceDepartmentPage(@PathVariable("serviceDepartmentAuthId") String serviceDepartmentAuthId,
                                                 Model model, @AuthenticationPrincipal CustomUserDetail customUserDetail) {
        addAttributeToModel(model);
        ServiceDepartmentEntity serviceDepartmentEntity = serviceDepartmentService.findByAuthId(serviceDepartmentAuthId);
        StoreEntity storeEntity = storeService.findById(serviceDepartmentEntity.getStoreId());
        model.addAttribute("listImage", imageService.findByServiceDepartmentId(serviceDepartmentEntity.getId()));
        model.addAttribute("listCategory", categoryBookingService.findAllByServiceDepartmentId(serviceDepartmentEntity.getId()));
        model.addAttribute("serviceDepartment", serviceDepartmentEntity);
        model.addAttribute("storeAuthId", storeEntity.getStoreAuthId());
        model.addAttribute("store", storeEntity);
        model.addAttribute("ratingDto", ratingService.findByServiceDepartment(serviceDepartmentEntity.getId()));
        if (customUserDetail == null || !roleService.isClientWithPhone(customUserDetail)) {
            model.addAttribute("status", "NO_LOGIN");
        } else {
            String customerPhone = customUserDetail.getUserEntity().getPhone();
            model.addAttribute("customer", customerBookingService.findByPhone(customerPhone));
        }

        return "client/service_department_detail";
    }

    private void addAttributeToModel(Model model) {
        model.addAttribute("listStore", storeService.findAll());
        model.addAttribute("listServiceDepartment", serviceDepartmentService.findAll());
    }

    @PostMapping("/login_success")
    public String loginSuccess(@RequestParam("phoneNumber") String phoneNumber) {
        // Sign up user and login
        UserEntity user = new UserEntity();
        user.setPhone(phoneNumber);
        Set<RoleEntity> roles = new HashSet<>();
        roles.add(new RoleEntity(false, new Date(), null, RoleType.CLIENT_WITH_PHONE.getValue()));
        user.setRoleEntities(roles);

        // Create login session
        Collection<SimpleGrantedAuthority> authorities = new HashSet<>();
        user.getRoleEntities().forEach(
                roleEntity -> authorities.add(new SimpleGrantedAuthority(roleEntity.getName()))
        );
        String password = "12345678";
        CustomUserDetail userDetails = new CustomUserDetail(phoneNumber, password, authorities, user);
        System.err.println("Người dùng: " + userDetails);
        Authentication auth = new UsernamePasswordAuthenticationToken(userDetails, null, authorities);
        SecurityContextHolder.getContext().setAuthentication(auth);
        return "redirect:/";
    }
}
