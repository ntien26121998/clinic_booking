package me.thesis.clinic_booking.web.api.store.booking;

import me.thesis.clinic_booking.dto.UserDto;
import me.thesis.clinic_booking.entity.UserEntity;
import me.thesis.clinic_booking.entity.enums.RoleType;
import me.thesis.clinic_booking.entity.store.ServiceDepartmentEntity;
import me.thesis.clinic_booking.repository.RoleRepository;
import me.thesis.clinic_booking.service.CommonReturnAPIService;
import me.thesis.clinic_booking.service.UserService;
import me.thesis.clinic_booking.service.cache.ServiceDepartmentCacheService;
import me.thesis.clinic_booking.service.store.booking.BookingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/api/v1/service_department/{serviceDepartmentAuthId}/cashier")
public class CashierManagementAPI {
    @Autowired
    private BookingService bookingService;

    @Autowired
    private UserService userService;

    @Autowired
    private CommonReturnAPIService commonReturnAPIService;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private ServiceDepartmentCacheService serviceDepartmentCacheService;

    @GetMapping("/list")
    public ResponseEntity<Page<UserEntity>> getAll(@PathVariable String serviceDepartmentAuthId,
                                                   @RequestParam("page") int page,
                                                   @RequestParam("size") int size,
                                                   @RequestParam("sortDir") String sortDir,
                                                   @RequestParam("sortField") String sortField) {
        ServiceDepartmentEntity serviceDepartmentEntity = serviceDepartmentCacheService.getByAuthIdFromCache(serviceDepartmentAuthId);
        if (serviceDepartmentEntity == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return ResponseEntity.ok(userService.findByServiceDepartmentIdAndRole(serviceDepartmentEntity.getId(), RoleType.SERVICE_DEPARTMENT_CASHIER.name(), sortDir, sortField, page, size));
    }

    @GetMapping("/{id}")
    public ResponseEntity<UserEntity> getById(@PathVariable String serviceDepartmentAuthId,
                                              @PathVariable Long id) {
        ServiceDepartmentEntity serviceDepartmentEntity = serviceDepartmentCacheService.getByAuthIdFromCache(serviceDepartmentAuthId);
        if (serviceDepartmentEntity == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return ResponseEntity.ok(userService.findByIdAndStoreIdAndServiceDepartmentId(id, serviceDepartmentEntity.getStoreId(), serviceDepartmentEntity.getId()));
    }

    @PostMapping("/save")
    public ResponseEntity<String> save(@PathVariable String serviceDepartmentAuthId,
                                       @ModelAttribute UserDto userDto, HttpServletRequest request) {
        ServiceDepartmentEntity serviceDepartmentEntity = serviceDepartmentCacheService.getByAuthIdFromCache(serviceDepartmentAuthId);
        if (serviceDepartmentEntity == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        userDto.setServiceDepartmentId(serviceDepartmentEntity.getId());
        userDto.setStoreId(serviceDepartmentEntity.getStoreId());
        userDto.setRoleEntity(roleRepository.findByName("ROLE_" + RoleType.SERVICE_DEPARTMENT_CASHIER));
        userDto.setEnableServe(true);
        boolean isSaveSuccess = userService.save(userDto, request);
        return commonReturnAPIService.returnOnSaveAPI(isSaveSuccess);
    }

    @PostMapping("/delete/{id}")
    public ResponseEntity<String> delete(@PathVariable String serviceDepartmentAuthId,
                                         @PathVariable Long id) {
        ServiceDepartmentEntity serviceDepartmentEntity = serviceDepartmentCacheService.getByAuthIdFromCache(serviceDepartmentAuthId);
        if (serviceDepartmentEntity == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        boolean isDeleteSuccess = userService.delete(id, serviceDepartmentEntity.getStoreId(), serviceDepartmentEntity.getId());
        return commonReturnAPIService.returnOnDeleteAPI(isDeleteSuccess);
    }


    @PostMapping("/changeStatus/{id}")
    public ResponseEntity<String> changeUserStatus(@PathVariable String serviceDepartmentAuthId,
                                                   @PathVariable Long id) {
        ServiceDepartmentEntity serviceDepartmentEntity = serviceDepartmentCacheService.getByAuthIdFromCache(serviceDepartmentAuthId);
        if (serviceDepartmentEntity == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        boolean isSaveSuccess = userService.changeStatus(id, serviceDepartmentEntity.getStoreId(), serviceDepartmentEntity.getId());
        return commonReturnAPIService.returnOnSaveAPI(isSaveSuccess);
    }
}
