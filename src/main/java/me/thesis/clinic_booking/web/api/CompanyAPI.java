package me.thesis.clinic_booking.web.api;

import me.thesis.clinic_booking.dto.ServerResponseDto;
import me.thesis.clinic_booking.entity.company.CompanyEntity;
import me.thesis.clinic_booking.service.company.CompanyService;
import me.thesis.clinic_booking.repository.company.CompanyRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/company")
public class CompanyAPI extends BaseAPI<CompanyEntity, CompanyRepository, CompanyService> {

    @GetMapping("/{id}")
    public ResponseEntity<ServerResponseDto> findById(@PathVariable Long id) {
        return super.findById(id);
    }

    @PostMapping("/delete/{id}")
    public ResponseEntity<ServerResponseDto> deleteCompany(@PathVariable Long id) {
        return deleteEntity(id);
    }
}
