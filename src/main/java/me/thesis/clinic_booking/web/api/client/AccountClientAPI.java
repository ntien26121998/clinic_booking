package me.thesis.clinic_booking.web.api.client;

import me.thesis.clinic_booking.config.security.CustomUserDetail;
import me.thesis.clinic_booking.entity.store.booking.CustomerBookingEntity;
import me.thesis.clinic_booking.service.store.booking.CustomerBookingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

@RestController
@RequestMapping("/api/v1/client/account")
public class AccountClientAPI {
    @Autowired
    private CustomerBookingService customerBookingService;

    @PostMapping("/updateInfo")
    public ResponseEntity<String> updateInfo(@RequestBody CustomerBookingEntity bookingCustomer, @AuthenticationPrincipal CustomUserDetail customUserDetail){

        if(customUserDetail == null) {
            return ResponseEntity.ok("UPDATE_FAIL");
        }
        String currentLoggedUserPhone = customUserDetail.getUserEntity().getPhone();
        String phoneWithoutCountryCode = currentLoggedUserPhone.replace("+84", "0");
        if(!phoneWithoutCountryCode.equals(bookingCustomer.getPhone())){
            return ResponseEntity.ok("UPDATE_FAIL");
        }

        CustomerBookingEntity oldEntity = customerBookingService.findByPhone(bookingCustomer.getPhone());
        oldEntity.setUpdatedTime(new Date());
        System.out.println(bookingCustomer.getName());
        oldEntity.setName(bookingCustomer.getName());
        oldEntity.setEmail(bookingCustomer.getEmail());
        customerBookingService.saveCustomer(oldEntity);
        return ResponseEntity.ok("UPDATE_SUCCESS");
    }
}

