package me.thesis.clinic_booking.web.api.system;

import me.thesis.clinic_booking.dto.ResponseCase;
import me.thesis.clinic_booking.dto.ServerResponseDto;
import me.thesis.clinic_booking.dto.UserDto;
import me.thesis.clinic_booking.entity.UserEntity;
import me.thesis.clinic_booking.entity.enums.RoleType;
import me.thesis.clinic_booking.repository.RoleRepository;
import me.thesis.clinic_booking.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/api/v1/system/system_account")
public class SystemAccountManagementAPI {

    @Autowired
    private UserService userService;

    @Autowired
    private RoleRepository roleRepository;

    @PreAuthorize("@preAuthorizeService.isCanAccessSystem()")
    @GetMapping("/list")
    public ResponseEntity<Page<UserEntity>> getAll(@RequestParam("page") int page,
                                                   @RequestParam("size") int size,
                                                   @RequestParam("sortDir") String sortDir,
                                                   @RequestParam("sortField") String sortField) {
        return ResponseEntity.ok(userService.findByRole(RoleType.SYSTEM_ADMIN.name(), sortDir, sortField, page, size));
    }

    @PreAuthorize("@preAuthorizeService.isCanAccessSystem()")
    @GetMapping("/{id}")
    public ResponseEntity<UserEntity> getById(@PathVariable Long id) {
        return ResponseEntity.ok(userService.findByIdAndStoreIdAndServiceDepartmentId(id, null, null));
    }

    @PreAuthorize("@preAuthorizeService.isCanAccessSystem()")
    @PostMapping("/save")
    public ResponseEntity<ServerResponseDto> save(@ModelAttribute UserDto userDto, HttpServletRequest request) {
        userDto.setRoleEntity(roleRepository.findByName("ROLE_SYSTEM_ADMIN"));
        userDto.setCompanyId(1L);
        boolean isSaveSuccess = userService.save(userDto, request);
        return ResponseEntity.ok().body(new ServerResponseDto(ResponseCase.SUCCESS, isSaveSuccess));
    }

    @PreAuthorize("@preAuthorizeService.isCanAccessSystem()")
    @PostMapping("/delete/{id}")
    public ResponseEntity<ServerResponseDto> delete(@PathVariable Long id) {
        boolean isDeleteSuccess = userService.delete(id, null, null);
        return ResponseEntity.ok().body(new ServerResponseDto(ResponseCase.SUCCESS, isDeleteSuccess));
    }

    @PreAuthorize("@preAuthorizeService.isCanAccessSystem()")
    @PostMapping("/checkEmailHasBeenUsed")
    public ResponseEntity<Boolean> checkEmailHasBeenUsed(@RequestParam String email) {
        return ResponseEntity.ok(userService.checkEmailHasBeenUsed(email));
    }

    @PreAuthorize("@preAuthorizeService.isCanAccessSystem()")
    @PostMapping("/changeStatus/{id}")
    public ResponseEntity<ServerResponseDto> changeUserStatus(@PathVariable Long id) {
        boolean isSaveSuccess = userService.changeStatus(id, null, null);
        return ResponseEntity.ok().body(new ServerResponseDto(ResponseCase.SUCCESS, isSaveSuccess));
    }
}

