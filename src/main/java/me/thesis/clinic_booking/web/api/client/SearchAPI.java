package me.thesis.clinic_booking.web.api.client;

import me.thesis.clinic_booking.dto.client.ServiceDepartmentClientDto;
import me.thesis.clinic_booking.entity.store.ServiceDepartmentEntity;
import me.thesis.clinic_booking.entity.store.StoreEntity;
import me.thesis.clinic_booking.service.PageService;
import me.thesis.clinic_booking.service.store.ServiceDepartmentService;
import me.thesis.clinic_booking.service.store.StoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/v1/client/search")
public class SearchAPI {

    @Autowired
    private ServiceDepartmentService serviceDepartmentService;

    @Autowired
    private StoreService storeService;

    @Autowired
    private PageService pageService;

    @GetMapping("/listForTypeAHead")
    public ResponseEntity<List<ServiceDepartmentEntity>> findForTypeAHead(@RequestParam("searchKey") String searchKey) {
        return ResponseEntity.ok(serviceDepartmentService.findByNameContaining(searchKey));
    }

//    @GetMapping("/store")
//    public ResponseEntity<Page<StoreEntity>> findStoreBySearchKey(@RequestParam("page") int page,
//                                                                  @RequestParam("type") Long storeType,
//                                                                  @RequestParam("city") String city,
//                                                                  @RequestParam("serviceType") Long serviceType,
//                                                                  @RequestParam("searchKey") String searchKey,
//                                                                  @RequestParam("startRating") String startRating,
//                                                                  @RequestParam("endRating") String endRating,
//                                                                  @RequestParam("sortDirNumberBooking") String sortDirNumberBooking) {
//
//        Sort sort = Sort.by(new Sort.Order(pageService.getDirection(sortDirNumberBooking), "numberDoneBooking"));
//        Pageable pageable = PageRequest.of(page - 1, 9, sort);
//        return ResponseEntity.ok(storeService.findWithKey(storeType, city, serviceType, searchKey, Double.valueOf(startRating), Double.valueOf(endRating), pageable));
//    }

//    @GetMapping("/service_department")
//    public ResponseEntity<Page<ServiceDepartmentClientDto>> findServiceDepartmentBySearchKey(@RequestParam("page") int page,
//                                                                                             @RequestParam("type") Long storeType,
//                                                                                             @RequestParam("city") String city,
//                                                                                             @RequestParam("serviceType") Long serviceType,
//                                                                                             @RequestParam("searchKey") String searchKey,
//                                                                                             @RequestParam("startRating") String startRating,
//                                                                                             @RequestParam("endRating") String endRating,
//                                                                                             @RequestParam("sortDirNumberBooking") String sortDirNumberBooking) {
//        Sort sort = Sort.by(new Sort.Order(pageService.getDirection(sortDirNumberBooking), "numberDoneBooking"));
//        Pageable pageable = PageRequest.of(page - 1, 9, sort);
//        return ResponseEntity.ok(serviceDepartmentService.findWithKey(storeType, city, serviceType, searchKey, Double.valueOf(startRating), Double.valueOf(endRating), pageable));
//    }

//    @GetMapping("/service_type")
//    public ResponseEntity<List<ServiceTypeEntity>> findByStoreType(@RequestParam("storeType") Long storeType) {
//        if (storeType == 0L) return ResponseEntity.ok(new ArrayList<>());
//        return ResponseEntity.ok(serviceTypeCacheService.getListServiceTypeByStoreTypeIdFromCache(storeType));
//    }
}