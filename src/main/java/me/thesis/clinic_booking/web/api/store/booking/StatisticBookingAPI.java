package me.thesis.clinic_booking.web.api.store.booking;

import me.thesis.clinic_booking.dto.ResponseCase;
import me.thesis.clinic_booking.dto.ServerResponseDto;
import me.thesis.clinic_booking.entity.store.ServiceDepartmentEntity;
import me.thesis.clinic_booking.service.StatisticBookingService;
import me.thesis.clinic_booking.service.cache.ServiceDepartmentCacheService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/v1/service_department/{serviceDepartmentAuthId}/statisticBooking")
public class StatisticBookingAPI {

    @Autowired
    private StatisticBookingService statisticBookingService;

    @Autowired
    private ServiceDepartmentCacheService serviceDepartmentCacheService;


    @PreAuthorize("@preAuthorizeService.isCanAccessServiceDepartment(#serviceDepartmentAuthId)")
    @GetMapping("/getInfo")
    public ResponseEntity<ServerResponseDto> getStatisticInfo(@PathVariable String serviceDepartmentAuthId,
                                                              @RequestParam(required = false) Date startBookingTimeRange,
                                                              @RequestParam(required = false) Date endBookingTimeRange,
                                                              @RequestParam(required = false) String listCategoryIdStr) {
        ServiceDepartmentEntity serviceDepartmentEntity = serviceDepartmentCacheService.getByAuthIdFromCache(serviceDepartmentAuthId);
        List<Long> listCategoryId;
        if (listCategoryIdStr == null || listCategoryIdStr.equals("")) {
            listCategoryId = null;
        } else {
            listCategoryId = Arrays
                    .stream(listCategoryIdStr.split(","))
                    .map(Long::parseLong).collect(Collectors.toList());
        }

        return ResponseEntity.ok(new ServerResponseDto(ResponseCase.SUCCESS,
                statisticBookingService.getStatisticInfo(serviceDepartmentEntity.getId(), startBookingTimeRange, endBookingTimeRange, listCategoryId)));
    }
}

