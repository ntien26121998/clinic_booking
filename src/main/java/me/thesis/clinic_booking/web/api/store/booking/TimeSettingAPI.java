package me.thesis.clinic_booking.web.api.store.booking;

import me.thesis.clinic_booking.config.security.CustomUserDetail;
import me.thesis.clinic_booking.dto.ResponseCase;
import me.thesis.clinic_booking.dto.ServerResponseDto;
import me.thesis.clinic_booking.entity.store.ServiceDepartmentEntity;
import me.thesis.clinic_booking.entity.store.booking.TimeSettingEntity;
import me.thesis.clinic_booking.service.store.ServiceDepartmentService;
import me.thesis.clinic_booking.service.store.booking.TimeSettingService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/web/{serviceDepartmentAuthId}/time_setting")
public class TimeSettingAPI {

    @Autowired
    private TimeSettingService timeSettingService;

    @Autowired
    private ServiceDepartmentService serviceDepartmentService;

    private static final Logger LOGGER = LoggerFactory.getLogger(TimeSettingAPI.class);

//    @PreAuthorize("@authorizationService.hasPermissionAccessStore(#storeAuthId)")
    @GetMapping("/listTimeSetting")
    public ResponseEntity<ServerResponseDto> getListTimeSetting(@PathVariable String serviceDepartmentAuthId) {
        ServiceDepartmentEntity serviceDepartmentEntity = serviceDepartmentService.findByAuthId(serviceDepartmentAuthId);
        if (serviceDepartmentEntity == null) {
            return ResponseEntity.ok().body(new ServerResponseDto(ResponseCase.ERROR));
        }
        List<TimeSettingEntity> listTimeSetting = timeSettingService.findListCommonConfigByServiceDepartmentId(serviceDepartmentEntity.getId());
        return ResponseEntity.ok().body(new ServerResponseDto(ResponseCase.SUCCESS, listTimeSetting));
    }

//    @PreAuthorize("@authorizationService.hasPermissionAccessStore(#storeAuthId)")
    @PostMapping("/saveTimeSetting")
    public ResponseEntity<ServerResponseDto> saveTimeSetting(@PathVariable String serviceDepartmentAuthId,
                                                             @RequestBody TimeSettingEntity timeSettingEntity,
                                                             @AuthenticationPrincipal CustomUserDetail customUserDetails) {
        ServiceDepartmentEntity serviceDepartmentEntity = serviceDepartmentService.findByAuthId(serviceDepartmentAuthId);
        if (serviceDepartmentEntity == null) {
            return ResponseEntity.ok().body(new ServerResponseDto(ResponseCase.ERROR));
        }
        try {
            return ResponseEntity.ok(timeSettingService.saveCommonConfig(serviceDepartmentEntity.getId(), timeSettingEntity, customUserDetails.getUserEntity().getId()));
        } catch (Exception e) {
            LOGGER.error("exception when saving time setting: {}", e.getMessage());
            return ResponseEntity.ok(new ServerResponseDto(ResponseCase.ERROR));
        }
    }

//    @PreAuthorize("@authorizationService.hasPermissionAccessStore(#storeAuthId)")
    @PostMapping("/deleteTimeSetting")
    public ResponseEntity<ServerResponseDto> deleteTimeSetting(@PathVariable String serviceDepartmentAuthId,
                                                               @RequestParam Long timeSettingId,
                                                               @AuthenticationPrincipal CustomUserDetail customUserDetails) {
        ServiceDepartmentEntity serviceDepartmentEntity = serviceDepartmentService.findByAuthId(serviceDepartmentAuthId);
        if (serviceDepartmentEntity == null) {
            return ResponseEntity.ok().body(new ServerResponseDto(ResponseCase.ERROR));
        }
        return ResponseEntity.ok(timeSettingService.deleteConfig(serviceDepartmentEntity.getId(), timeSettingId, customUserDetails.getUserEntity().getId()));
    }
}

