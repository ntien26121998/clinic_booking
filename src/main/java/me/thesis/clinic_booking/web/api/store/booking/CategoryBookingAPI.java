package me.thesis.clinic_booking.web.api.store.booking;

import me.thesis.clinic_booking.config.security.CustomUserDetail;
import me.thesis.clinic_booking.dto.ResponseCase;
import me.thesis.clinic_booking.dto.ServerResponseDto;
import me.thesis.clinic_booking.entity.store.booking.category.CategoryBookingEntity;
import me.thesis.clinic_booking.service.store.ServiceDepartmentService;
import me.thesis.clinic_booking.service.store.booking.CategoryBookingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("/api/v1/service_department/{serviceDepartmentAuthId}/categoryBooking")
public class CategoryBookingAPI {

    @Autowired
    private CategoryBookingService categoryBookingService;

    @Autowired
    private ServiceDepartmentService serviceDepartmentService;

    @PreAuthorize("@preAuthorizeService.isCanAccessServiceDepartment(#serviceDepartmentAuthId)")
    @GetMapping("/listCategoryBooking")
    public List<CategoryBookingEntity> listWeb(@PathVariable("serviceDepartmentAuthId") String serviceDepartmentAuthId) {
        Long serviceDepartmentId = serviceDepartmentService.getServiceDepartmentIdFromAuthId(serviceDepartmentAuthId);
        if (serviceDepartmentId == null) {
            return null;
        }
        return categoryBookingService.getAllByServiceDepartmentId(serviceDepartmentId);
    }

    @PreAuthorize("@preAuthorizeService.isCanAccessServiceDepartment(#serviceDepartmentAuthId)")
    @PostMapping("/saveCategory")
    public ResponseEntity<ServerResponseDto> createCategory(@PathVariable("serviceDepartmentAuthId") String serviceDepartmentAuthId,
                                                            @RequestBody CategoryBookingEntity entity) {
        Long serviceDepartmentId = serviceDepartmentService.getServiceDepartmentIdFromAuthId(serviceDepartmentAuthId);
        if (serviceDepartmentId == null) {
            return ResponseEntity.ok(new ServerResponseDto(ResponseCase.ERROR));
        }
        boolean isSaveSuccess = categoryBookingService.saveCategory(serviceDepartmentId, entity);
        return ResponseEntity.ok(new ServerResponseDto(isSaveSuccess ? ResponseCase.ADD_SUCCESS : ResponseCase.ERROR));

    }

    @PreAuthorize("@preAuthorizeService.isCanAccessServiceDepartment(#serviceDepartmentAuthId)")
    @GetMapping("/{id}")
    public ResponseEntity<ServerResponseDto> getCategoryById(@PathVariable("serviceDepartmentAuthId") String serviceDepartmentAuthId,
                                                             @PathVariable("id") Long id) {
        Long serviceDepartmentId = serviceDepartmentService.getServiceDepartmentIdFromAuthId(serviceDepartmentAuthId);
        if (serviceDepartmentId == null) {
            return ResponseEntity.ok(new ServerResponseDto(ResponseCase.ERROR));
        }
        CategoryBookingEntity categoryBookingEntity = categoryBookingService.getCategoryById(id, serviceDepartmentId);
        return ResponseEntity.ok().body(new ServerResponseDto(ResponseCase.SUCCESS, categoryBookingEntity));
    }

    @PreAuthorize("@preAuthorizeService.isCanAccessServiceDepartment(#serviceDepartmentAuthId)")
    @PostMapping("/delete/{id}")
    public ResponseEntity<ServerResponseDto> delete(@PathVariable("serviceDepartmentAuthId") String serviceDepartmentAuthId, @PathVariable("id") Long id,
                                                    @AuthenticationPrincipal CustomUserDetail userDetails) {
        Long serviceDepartmentId = serviceDepartmentService.getServiceDepartmentIdFromAuthId(serviceDepartmentAuthId);
        if (serviceDepartmentId == null) {
            return ResponseEntity.ok(new ServerResponseDto(ResponseCase.ERROR));
        }
        boolean isDeleteSuccess = categoryBookingService.deleteCategory(id, serviceDepartmentId);
        return ResponseEntity.ok().body(new ServerResponseDto(isDeleteSuccess ? ResponseCase.SUCCESS : ResponseCase.ERROR));
    }


    @PreAuthorize("@preAuthorizeService.isCanAccessServiceDepartment(#serviceDepartmentAuthId)")
    @PostMapping("/deleteAll")
    public ResponseEntity<ServerResponseDto> deleteAll(@PathVariable("serviceDepartmentAuthId") String serviceDepartmentAuthId,
                                                       @AuthenticationPrincipal CustomUserDetail userDetails) {
        Long serviceDepartmentId = serviceDepartmentService.getServiceDepartmentIdFromAuthId(serviceDepartmentAuthId);
        if (serviceDepartmentId == null) {
            return ResponseEntity.ok(new ServerResponseDto(ResponseCase.ERROR));
        }
        boolean isDeleteAllSuccess = categoryBookingService.deleteAllCategory(serviceDepartmentId);
        return ResponseEntity.ok().body(new ServerResponseDto(isDeleteAllSuccess ? ResponseCase.SUCCESS : ResponseCase.ERROR));
    }

    @PreAuthorize("@preAuthorizeService.isCanAccessServiceDepartment(#serviceDepartmentAuthId)")
    @GetMapping("/changeStatus/{categoryId}")
    public ResponseEntity<ServerResponseDto> changeStatus(
            @PathVariable("categoryId") Long categoryId, @PathVariable("serviceDepartmentAuthId") String serviceDepartmentAuthId) {
        Long serviceDepartmentId = serviceDepartmentService.getServiceDepartmentIdFromAuthId(serviceDepartmentAuthId);
        if (serviceDepartmentId == null) {
            return ResponseEntity.ok(new ServerResponseDto(ResponseCase.ERROR));
        }
        boolean isChangeStatusSuccess = categoryBookingService.changeStatus(categoryId, serviceDepartmentId);

        return ResponseEntity.ok().body(new ServerResponseDto(isChangeStatusSuccess ? ResponseCase.SUCCESS : ResponseCase.ERROR));
    }

//    @PreAuthorize("@preAuthorizeService.isCanAccessServiceDepartment(#serviceDepartmentAuthId)")
//    @PostMapping("/updateOrder")
//    public ResponseEntity<ServerResponseDto> updateOrder(@RequestBody List<SaveCategoryBookingOrderDto> listCategoryBookingDto,
//                                                         @PathVariable("storeAuthId") String storeAuthId) {
//        Long storeId = storeService.getStoreIdFromStoreAuthId(storeAuthId);
//        if (storeId == null) {
//            return ResponseEntity.ok(new ServerResponseDto(ResponseCase.ERROR));
//        }
//        if (!categoryBookingService.updateOrder(listCategoryBookingDto, storeId)) {
//            return ResponseEntity.ok().body(new ServerResponseDto(ResponseCase.ERROR));
//        } else {
//            return ResponseEntity.ok().body(new ServerResponseDto(ResponseCase.SUCCESS));
//        }
//    }

    @PreAuthorize("@preAuthorizeService.isCanAccessServiceDepartment(#serviceDepartmentAuthId)")
    @PostMapping("/importCategory")
    public ResponseEntity<ServerResponseDto> importCategory(@PathVariable("serviceDepartmentAuthId") String serviceDepartmentAuthId,
                                                            @RequestPart MultipartFile excelFile,
                                                            @AuthenticationPrincipal CustomUserDetail userDetails) {
        Long serviceDepartmentId = serviceDepartmentService.getServiceDepartmentIdFromAuthId(serviceDepartmentAuthId);
        if (serviceDepartmentId == null) {
            return ResponseEntity.ok(new ServerResponseDto(ResponseCase.ERROR));
        }
        return ResponseEntity.ok(new ServerResponseDto(ResponseCase.SUCCESS, categoryBookingService.readDataFromFileImport(excelFile, serviceDepartmentId)));
    }

    @PreAuthorize("@preAuthorizeService.isCanAccessServiceDepartment(#serviceDepartmentAuthId)")
    @PostMapping("/submitImport")
    public ResponseEntity<ServerResponseDto> submitImport(@PathVariable String serviceDepartmentAuthId,
                                                          @RequestBody Set<CategoryBookingEntity> listCategoryBooking,
                                                          @AuthenticationPrincipal CustomUserDetail userDetails) {
        Long serviceDepartmentId = serviceDepartmentService.getServiceDepartmentIdFromAuthId(serviceDepartmentAuthId);
        if(serviceDepartmentId == null){
            return ResponseEntity.ok(new ServerResponseDto(ResponseCase.ERROR));
        }
        boolean isSaveSuccess = categoryBookingService.submitImport(listCategoryBooking, serviceDepartmentId);
        return ResponseEntity.ok(new ServerResponseDto(isSaveSuccess ? ResponseCase.SUCCESS : ResponseCase.ERROR));
    }

    @GetMapping("/findForSavingBooking")
    public ResponseEntity<ServerResponseDto> findForSavingBooking(@PathVariable String serviceDepartmentAuthId) {
        Long serviceDepartmentId = serviceDepartmentService.getServiceDepartmentIdFromAuthId(serviceDepartmentAuthId);
        return ResponseEntity.ok(new ServerResponseDto(ResponseCase.SUCCESS, categoryBookingService.findByServiceDepartmentIdAndActiveAndDeletedFalse(serviceDepartmentId)));
    }

    @PreAuthorize("@preAuthorizeService.isCanAccessServiceDepartment(#serviceDepartmentAuthId)")
    @GetMapping("/findForStatisticBooking")
    public ResponseEntity<ServerResponseDto> findForStatisticBooking(@PathVariable String serviceDepartmentAuthId) {
        Long serviceDepartmentId = serviceDepartmentService.getServiceDepartmentIdFromAuthId(serviceDepartmentAuthId);
        return ResponseEntity.ok(new ServerResponseDto(ResponseCase.SUCCESS, categoryBookingService.findAllByServiceDepartmentId(serviceDepartmentId)));
    }

}
