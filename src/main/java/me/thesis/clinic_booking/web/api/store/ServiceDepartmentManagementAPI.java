package me.thesis.clinic_booking.web.api.store;

import me.thesis.clinic_booking.dto.ResponseCase;
import me.thesis.clinic_booking.dto.ServerResponseDto;
import me.thesis.clinic_booking.dto.ServiceDepartmentDto;
import me.thesis.clinic_booking.entity.store.ServiceDepartmentEntity;
import me.thesis.clinic_booking.entity.store.StoreEntity;
import me.thesis.clinic_booking.service.CommonReturnAPIService;
import me.thesis.clinic_booking.service.cache.StoreCacheService;
import me.thesis.clinic_booking.service.store.ServiceDepartmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/api/v1/store/{storeAuthId}/service_department")
public class ServiceDepartmentManagementAPI {

    @Autowired
    private ServiceDepartmentService serviceDepartmentService;

    @Autowired
    private CommonReturnAPIService commonReturnAPIService;

    @Autowired
    private StoreCacheService storeCacheService;

    @PreAuthorize("@preAuthorizeService.isCanAccessStore(#storeAuthId)")
    @GetMapping("/list")
    public ResponseEntity<Page<ServiceDepartmentEntity>> getAllByPage(@PathVariable("storeAuthId") String storeAuthId,
                                                                      @RequestParam("page") int page,
                                                                      @RequestParam("size") int size,
                                                                      @RequestParam("sortDir") String sortDir,
                                                                      @RequestParam("sortField") String sortField) {
        StoreEntity storeEntity = storeCacheService.getByAuthIdFromCache(storeAuthId);
        return ResponseEntity.ok(serviceDepartmentService.findByStoreId(sortDir, sortField, page, size, storeEntity.getId()));
    }

    @PreAuthorize("@preAuthorizeService.isCanAccessStore(#storeAuthId)")
    @GetMapping("/{serviceDepartmentAuthId}")
    public ResponseEntity<ServiceDepartmentEntity> getByAuthId(@PathVariable("storeAuthId") String storeAuthId, @PathVariable("serviceDepartmentAuthId") String serviceDepartmentAuthId) {
        return ResponseEntity.ok(serviceDepartmentService.findByAuthId(serviceDepartmentAuthId));
    }

    @PreAuthorize("@preAuthorizeService.isCanAccessStore(#storeAuthId)")
    @PostMapping("/save")
    public ResponseEntity<ServerResponseDto> save(@PathVariable("storeAuthId") String storeAuthId,
                                                  @ModelAttribute ServiceDepartmentDto serviceDepartmentDto,
                                                  HttpServletRequest request) {
        StoreEntity storeEntity = storeCacheService.getByAuthIdFromCache(storeAuthId);
        serviceDepartmentDto.setStoreId(storeEntity.getId());
        serviceDepartmentDto.setCity(storeEntity.getCityName());
        boolean isSaveSuccess = serviceDepartmentService.save(serviceDepartmentDto, request);
        return ResponseEntity.ok().body(new ServerResponseDto(ResponseCase.ADD_SUCCESS, isSaveSuccess));
    }

    @PreAuthorize("@preAuthorizeService.isCanAccessStore(#storeAuthId)")
    @PostMapping("/delete/{serviceDepartmentAuthId}")
    public ResponseEntity<String> delete(@PathVariable("storeAuthId") String storeAuthId, @PathVariable String  serviceDepartmentAuthId) {
        StoreEntity storeEntity = storeCacheService.getByAuthIdFromCache(storeAuthId);
        boolean isDeleteSuccess = serviceDepartmentService.delete(serviceDepartmentAuthId, storeEntity.getId());
        return commonReturnAPIService.returnOnDeleteAPI(isDeleteSuccess);
    }
}
