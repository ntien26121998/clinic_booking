package me.thesis.clinic_booking.web.api.store.booking;

import me.thesis.clinic_booking.config.security.CustomUserDetail;
import me.thesis.clinic_booking.dto.ResponseCase;
import me.thesis.clinic_booking.dto.ServerResponseDto;
import me.thesis.clinic_booking.entity.store.ServiceDepartmentEntity;
import me.thesis.clinic_booking.entity.store.booking.FrameSettingEntity;
import me.thesis.clinic_booking.entity.store.booking.TimeSettingEntity;
import me.thesis.clinic_booking.service.store.ServiceDepartmentService;
import me.thesis.clinic_booking.service.store.booking.FrameSettingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/web/{serviceDepartmentAuthId}/frame_setting")
public class FrameSettingAPI {

    @Autowired
    private FrameSettingService frameSettingService;

    @Autowired
    private ServiceDepartmentService serviceDepartmentService;

//    @PreAuthorize("@authorizationService.hasPermissionAccessStore(#storeAuthId)")
    @GetMapping("/listFrameSetting")
    public ResponseEntity<ServerResponseDto> getListFrameSetting(@PathVariable String serviceDepartmentAuthId) {
        ServiceDepartmentEntity serviceDepartmentEntity = serviceDepartmentService.findByAuthId(serviceDepartmentAuthId);
        if (serviceDepartmentEntity == null) {
            return ResponseEntity.ok().body(new ServerResponseDto(ResponseCase.ERROR));
        }
        List<FrameSettingEntity> listFrameSetting = frameSettingService.findListCommonConfigByServiceDepartmentId(serviceDepartmentEntity.getId());
        return ResponseEntity.ok().body(new ServerResponseDto(ResponseCase.SUCCESS, listFrameSetting));
    }

//    @PreAuthorize("@authorizationService.hasPermissionAccessStore(#storeAuthId)")
    @PostMapping("/saveFrameSetting")
    public ResponseEntity<ServerResponseDto> saveFrameSetting(@PathVariable String serviceDepartmentAuthId,
                                                              @RequestBody FrameSettingEntity frameSettingEntity,
                                                              @AuthenticationPrincipal CustomUserDetail customUserDetails) {
        ServiceDepartmentEntity serviceDepartmentEntity = serviceDepartmentService.findByAuthId(serviceDepartmentAuthId);
        if (serviceDepartmentEntity == null) {
            return ResponseEntity.ok().body(new ServerResponseDto(ResponseCase.ERROR));
        }
        return ResponseEntity.ok(frameSettingService.saveCommonConfig(serviceDepartmentEntity.getId(), frameSettingEntity, customUserDetails.getUserEntity().getId()));
    }

//    @PreAuthorize("@authorizationService.hasPermissionAccessStore(#storeAuthId)")
    @PostMapping("/deleteFrameSetting")
    public ResponseEntity<ServerResponseDto> deleteFrameSetting(@PathVariable String serviceDepartmentAuthId,
                                                                @RequestParam Long frameSettingId,
                                                                @AuthenticationPrincipal CustomUserDetail customUserDetails) {
        ServiceDepartmentEntity serviceDepartmentEntity = serviceDepartmentService.findByAuthId(serviceDepartmentAuthId);
        if (serviceDepartmentEntity == null) {
            return ResponseEntity.ok().body(new ServerResponseDto(ResponseCase.ERROR));
        }
        return ResponseEntity.ok(frameSettingService.deleteConfig(serviceDepartmentEntity.getStoreId(), frameSettingId, customUserDetails.getUserEntity().getId()));
    }
}
