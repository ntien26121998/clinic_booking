package me.thesis.clinic_booking.web.api.store.booking;

import me.thesis.clinic_booking.dto.ResponseCase;
import me.thesis.clinic_booking.dto.ServerResponseDto;
import me.thesis.clinic_booking.dto.ServiceDepartmentDto;
import me.thesis.clinic_booking.entity.store.ServiceDepartmentEntity;
import me.thesis.clinic_booking.service.cache.ServiceDepartmentCacheService;
import me.thesis.clinic_booking.service.store.ServiceDepartmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/api/v1/service_department/{serviceDepartmentAuthId}")
public class ServiceDepartmentInformationAPI {
    @Autowired
    private ServiceDepartmentCacheService serviceDepartmentCacheService;

    @Autowired
    private ServiceDepartmentService serviceDepartmentService;

    @GetMapping("/information")
    public ResponseEntity<ServiceDepartmentEntity> getInfo(@PathVariable String serviceDepartmentAuthId){
        return ResponseEntity.ok(serviceDepartmentCacheService.getByAuthIdFromCache(serviceDepartmentAuthId));
    }

    @PostMapping("/saveInformation")
    public ResponseEntity saveInformation(@PathVariable String serviceDepartmentAuthId,
                                          @ModelAttribute ServiceDepartmentDto serviceDepartmentDto, HttpServletRequest request) {
        ServiceDepartmentEntity serviceDepartmentEntity = serviceDepartmentCacheService.getByAuthIdFromCache(serviceDepartmentAuthId);
        if(serviceDepartmentEntity == null) {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
        serviceDepartmentDto.setId(serviceDepartmentEntity.getId());
        boolean isSaveSuccess = serviceDepartmentService.save(serviceDepartmentDto, request);
        return ResponseEntity.ok().body(new ServerResponseDto(ResponseCase.SUCCESS, isSaveSuccess));
    }
}
