package me.thesis.clinic_booking.web.api.client;

import me.thesis.clinic_booking.entity.store.ServiceDepartmentEntity;
import me.thesis.clinic_booking.service.store.ServiceDepartmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/client/service_departments/")
public class ServiceDepartmentAPI {

    @Autowired
    private ServiceDepartmentService serviceDepartmentService;

    @GetMapping("/all")
    public ResponseEntity<Page<ServiceDepartmentEntity>> findByStoreTypeAndCityAndServiceType(@RequestParam("page") int page,
                                                                                              @RequestParam("storeId") Long storeId,
                                                                                              @RequestParam("city") String city,
                                                                                              @RequestParam("sortDirNumberBooking") String sortDirNumberBooking) {

        return ResponseEntity.ok(serviceDepartmentService.findByStoreTypeAndCityAndServiceType(storeId, city, page, 9, sortDirNumberBooking));
    }

    @GetMapping("/{serviceDepartmentAuthId}")
    public ResponseEntity<ServiceDepartmentEntity> findById(@PathVariable("serviceDepartmentAuthId") String serviceDepartmentAuthId) {
        return ResponseEntity.ok(serviceDepartmentService.findByAuthId(serviceDepartmentAuthId));
    }
}
