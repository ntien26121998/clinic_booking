package me.thesis.clinic_booking.web.api.store.booking;

import me.thesis.clinic_booking.dto.ImageServiceDto;
import me.thesis.clinic_booking.dto.ResponseCase;
import me.thesis.clinic_booking.dto.ServerResponseDto;
import me.thesis.clinic_booking.entity.store.ServiceDepartmentEntity;
import me.thesis.clinic_booking.entity.store.booking.ImageEntity;
import me.thesis.clinic_booking.service.CommonReturnAPIService;
import me.thesis.clinic_booking.service.cache.ServiceDepartmentCacheService;
import me.thesis.clinic_booking.service.store.booking.ImageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/api/v1/service_department/{serviceDepartmentAuthId}/image")
public class ImageAPI {

    @Autowired
    private ImageService imageService;

    @Autowired
    private CommonReturnAPIService commonReturnAPIService;


    @Autowired
    private ServiceDepartmentCacheService serviceDepartmentCacheService;

    @GetMapping("/list")
    public ResponseEntity<Page<ImageEntity>> getAll(@PathVariable String serviceDepartmentAuthId,
                                                    @RequestParam("page") int page,
                                                    @RequestParam("size") int size,
                                                    @RequestParam("sortDir") String sortDir,
                                                    @RequestParam("sortField") String sortField) {
        ServiceDepartmentEntity serviceDepartmentEntity = serviceDepartmentCacheService.getByAuthIdFromCache(serviceDepartmentAuthId);
        if (serviceDepartmentEntity == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return ResponseEntity.ok(imageService.findByServiceDepartmentId(serviceDepartmentEntity.getId(), sortDir, sortField, page, size));
    }

    @GetMapping("/{id}")
    public ResponseEntity<ImageEntity> getById(@PathVariable String serviceDepartmentAuthId,
                                                          @PathVariable Long id) {
        ServiceDepartmentEntity serviceDepartmentEntity = serviceDepartmentCacheService.getByAuthIdFromCache(serviceDepartmentAuthId);
        if (serviceDepartmentEntity == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return ResponseEntity.ok(imageService.findById(id, serviceDepartmentEntity.getId()));
    }

    @PostMapping("/save")
    public ResponseEntity<ServerResponseDto> save(@PathVariable String serviceDepartmentAuthId,
                                                  @ModelAttribute ImageServiceDto imageServiceDto, HttpServletRequest request) {
        ServiceDepartmentEntity serviceDepartmentEntity = serviceDepartmentCacheService.getByAuthIdFromCache(serviceDepartmentAuthId);
        if (serviceDepartmentEntity == null) {
            return ResponseEntity.ok().body(new ServerResponseDto(ResponseCase.ERROR));
        }

        boolean isSaveSuccess = imageService.save(imageServiceDto, serviceDepartmentEntity.getId(), request);
        return ResponseEntity.ok().body(new ServerResponseDto(isSaveSuccess ? ResponseCase.ADD_SUCCESS : ResponseCase.ERROR));
    }

    @PostMapping("/delete/{id}")
    public ResponseEntity<ServerResponseDto> delete(@PathVariable String serviceDepartmentAuthId,
                                         @PathVariable Long id) {
        ServiceDepartmentEntity serviceDepartmentEntity = serviceDepartmentCacheService.getByAuthIdFromCache(serviceDepartmentAuthId);
        if (serviceDepartmentEntity == null) {
            return ResponseEntity.ok().body(new ServerResponseDto(ResponseCase.ERROR));
        }
        boolean isDeleteSuccess = imageService.delete(id, serviceDepartmentEntity.getId());
        return ResponseEntity.ok().body(new ServerResponseDto(isDeleteSuccess ? ResponseCase.DELETE_SUCCESS : ResponseCase.ERROR));
    }
}
