package me.thesis.clinic_booking.web.api.store;

import me.thesis.clinic_booking.dto.StoreDto;
import me.thesis.clinic_booking.entity.store.StoreEntity;
import me.thesis.clinic_booking.service.CommonReturnAPIService;
import me.thesis.clinic_booking.service.cache.StoreCacheService;
import me.thesis.clinic_booking.service.store.StoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/api/v1/store/{storeAuthId}")
public class StoreInformationAPI {

    @Autowired
    private StoreService storeService;

    @Autowired
    private StoreCacheService storeCacheService;

    @Autowired
    private CommonReturnAPIService commonReturnAPIService;

    @PreAuthorize("@preAuthorizeService.isCanAccessStore(#storeAuthId)")
    @PostMapping("/saveInformation")
    public ResponseEntity saveInformation(@PathVariable String storeAuthId, @ModelAttribute StoreDto storeDto, HttpServletRequest request) {
        StoreEntity storeEntity = storeCacheService.getByAuthIdFromCache(storeAuthId);
        if(storeEntity == null) {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
        storeDto.setId(storeEntity.getId());
        storeDto.setCity(storeEntity.getCityName());
        boolean isSaveSuccess = storeService.save(storeDto, request);
        return commonReturnAPIService.returnOnSaveAPI(isSaveSuccess);
    }

    @PreAuthorize("@preAuthorizeService.isCanAccessStore(#storeAuthId)")
    @GetMapping("/information")
    public ResponseEntity<StoreEntity> getStoreInformation(@PathVariable String storeAuthId) {
        return ResponseEntity.ok(storeCacheService.getByAuthIdFromCache(storeAuthId));
    }
}

