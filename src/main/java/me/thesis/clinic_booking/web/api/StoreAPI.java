package me.thesis.clinic_booking.web.api;

import me.thesis.clinic_booking.config.security.CustomUserDetail;
import me.thesis.clinic_booking.dto.ResponseCase;
import me.thesis.clinic_booking.dto.ServerResponseDto;
import me.thesis.clinic_booking.dto.StoreDto;
import me.thesis.clinic_booking.entity.company.CompanyEntity;
import me.thesis.clinic_booking.entity.store.StoreEntity;
import me.thesis.clinic_booking.service.CommonReturnAPIService;
import me.thesis.clinic_booking.service.company.CompanyService;
import me.thesis.clinic_booking.service.store.StoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;

@RestController
@RequestMapping("api/v1/system/store")
public class StoreAPI {
    @Autowired
    private StoreService storeService;

    @Autowired
    private CompanyService companyService;

    @Autowired
    private CommonReturnAPIService commonReturnAPIService;

    @PreAuthorize("@preAuthorizeService.isCanAccessSystem()")
    @GetMapping("/list")
    public ResponseEntity<Page<StoreEntity>> getByCompanyId(@RequestParam("page") int page,
                                                            @RequestParam("size") int size,
                                                            @RequestParam("sortDir") String sortDir,
                                                            @RequestParam("sortField") String sortField,
                                                            @RequestParam("customFieldSearch") Long companyId) {
        return ResponseEntity.ok(storeService.findByCompanyId(companyId, sortDir, sortField, page, size));
    }

    @GetMapping("/{storeAuthId}")
    public ResponseEntity<StoreEntity> getByAuthId(@PathVariable String storeAuthId) {
        return ResponseEntity.ok(storeService.findByStoreAuthId(storeAuthId));
    }

    @PreAuthorize("@preAuthorizeService.isCanAccessSystem()")
    @PostMapping("/save")
    public ResponseEntity<ServerResponseDto> save(@ModelAttribute StoreDto storeDto, HttpServletRequest request) {
        storeDto.setId(storeService.getStoreIdFromAuthId(storeDto.getStoreAuthId()));
        boolean isSaveSuccess = storeService.save(storeDto, request);
        return ResponseEntity.ok().body(new ServerResponseDto(ResponseCase.ADD_SUCCESS, isSaveSuccess));
    }

    @PreAuthorize("@preAuthorizeService.isCanAccessSystem()")
    @PostMapping("/delete/{storeAuthId}")
    public ResponseEntity<String> delete(@PathVariable String storeAuthId) {
        boolean isDeleteSuccess = storeService.delete(storeAuthId);
        return commonReturnAPIService.returnOnDeleteAPI(isDeleteSuccess);
    }

}
