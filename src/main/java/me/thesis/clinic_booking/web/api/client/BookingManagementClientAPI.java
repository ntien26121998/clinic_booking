package me.thesis.clinic_booking.web.api.client;

import me.thesis.clinic_booking.config.security.CustomUserDetail;
import me.thesis.clinic_booking.dto.client.BookingClientDetailDto;
import me.thesis.clinic_booking.dto.client.SaveRatingDto;
import me.thesis.clinic_booking.entity.enums.StatusBookingType;
import me.thesis.clinic_booking.entity.store.ServiceDepartmentEntity;
import me.thesis.clinic_booking.entity.store.booking.BookingEntity;
import me.thesis.clinic_booking.service.RatingService;
import me.thesis.clinic_booking.service.cache.ServiceDepartmentCacheService;
import me.thesis.clinic_booking.service.store.booking.BookingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/api/v1/client/booking_manager")
public class BookingManagementClientAPI {

    @Autowired
    private BookingService bookingService;

    @Autowired
    private RatingService ratingService;

    @Autowired
    private ServiceDepartmentCacheService serviceDepartmentCacheService;

    @GetMapping("/list")
    public ResponseEntity<Page<BookingEntity>> getListBookingByMemberId(@RequestParam("page") int page,
                                                                        @RequestParam("size") int size,
                                                                        @RequestParam(value = "startTime", required = false) Date startTime,
                                                                        @RequestParam(value = "endTime", required = false) Date endTime,
                                                                        @RequestParam("status") StatusBookingType status,
                                                                        @AuthenticationPrincipal CustomUserDetail customUserDetail) {
        if (customUserDetail == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        String customerPhone = customUserDetail.getUserEntity().getPhone();
        return ResponseEntity.ok(bookingService.findByCustomerPhone(customerPhone, startTime, endTime, status, "DESC", "createdTime", page, size));
    }

    @GetMapping("/detail/{bookingId}")
    public ResponseEntity<BookingClientDetailDto> getById(@PathVariable Long bookingId,
                                                          @AuthenticationPrincipal CustomUserDetail customUserDetail) {
        if (customUserDetail == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        String customerPhone = customUserDetail.getUserEntity().getPhone();
        String phoneWithoutCountryCode = customerPhone.replace("+84", "0");
        return ResponseEntity.ok(bookingService.getBookingByIdAndCustomerPhoneClient(bookingId, phoneWithoutCountryCode));
    }


    @PostMapping("/rating")
    public ResponseEntity<String> ratingBookingService(@AuthenticationPrincipal CustomUserDetail customUserDetail,
                                                       @ModelAttribute SaveRatingDto ratingDto,
                                                       HttpServletRequest request) {
        if (customUserDetail == null) {
            return ResponseEntity.ok("RATING_FAILED");
        }
        ServiceDepartmentEntity serviceDepartmentEntity = serviceDepartmentCacheService.getByAuthIdFromCache(ratingDto.getServiceDepartmentAuthId());
        if (serviceDepartmentEntity == null) {
            return ResponseEntity.ok("RATING_FAILED");
        }

        String customerPhone = customUserDetail.getUserEntity().getPhone();
//        String phoneWithoutCountryCode = customerPhone.replace("+84", "0");
        ratingDto.setPhoneNumber(customerPhone);
        ratingDto.setServiceDepartmentId(serviceDepartmentEntity.getId());
        ratingDto.setStoreId(serviceDepartmentEntity.getStoreId());
        ratingService.save(ratingDto, request);
        return ResponseEntity.ok("RATING_SUCCESS");
    }

    @PostMapping("/cancelBooking/{bookingId}")
    public ResponseEntity<String> cancelBooking(@AuthenticationPrincipal CustomUserDetail customUserDetail, @PathVariable Long bookingId) {
        String customerPhone = customUserDetail.getUserEntity().getPhone();
        List<BookingEntity> listBookingConsecutive = bookingService.getListBookingConsecutive(bookingId, customerPhone);
        if (listBookingConsecutive.isEmpty()) {
            return ResponseEntity.ok("CANCEL_FAIL");
        }
        listBookingConsecutive.forEach(bookingEntity -> {
            bookingEntity.setStatus(StatusBookingType.CANCELED);
            bookingEntity.setUpdatedTime(new Date());
        });
        bookingService.saveBooking(listBookingConsecutive);
        return ResponseEntity.ok("CANCEL_SUCCESS");
    }
}

