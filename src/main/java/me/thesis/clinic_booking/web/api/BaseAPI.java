package me.thesis.clinic_booking.web.api;


import me.thesis.clinic_booking.dto.ResponseCase;
import me.thesis.clinic_booking.repository.BaseRepository;
import me.thesis.clinic_booking.dto.ServerResponseDto;
import me.thesis.clinic_booking.entity.BaseEntity;
import me.thesis.clinic_booking.service.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class BaseAPI<T extends BaseEntity, R extends BaseRepository<T>, S extends BaseService<T, R>> {

    @Autowired
    private S service;

    public ResponseEntity<ServerResponseDto> findById(Long entityId) {
        Optional<T> entityOptional = service.findById(entityId);
        if (entityOptional.isEmpty()) {
            return ResponseEntity.ok(new ServerResponseDto(ResponseCase.NOT_FOUND_ENTITY));
        } else {
            return ResponseEntity.ok(new ServerResponseDto(ResponseCase.SUCCESS, entityOptional.get()));
        }
    }

    public ResponseEntity<ServerResponseDto> deleteEntity(Long entityId) {
        boolean isDeleteSuccess = service.deleteEntity(entityId);
        if (isDeleteSuccess) {
            return ResponseEntity.ok(new ServerResponseDto(ResponseCase.DELETE_SUCCESS));
        } else {
            return ResponseEntity.ok(new ServerResponseDto(ResponseCase.DELETE_FAIL));
        }
    }

    public ResponseEntity<ServerResponseDto> saveOrUpdate(T entity) {
        boolean isSaveOrUpdateSuccess = service.saveOrUpdate(entity);
        if (isSaveOrUpdateSuccess) {
            return ResponseEntity.ok(new ServerResponseDto(ResponseCase.SUCCESS));
        } else {
            return ResponseEntity.ok(new ServerResponseDto(ResponseCase.ERROR));
        }
    }
}

