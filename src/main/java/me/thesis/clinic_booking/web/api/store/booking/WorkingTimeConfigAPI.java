package me.thesis.clinic_booking.web.api.store.booking;

import me.thesis.clinic_booking.dto.ServerResponseDto;
import me.thesis.clinic_booking.entity.store.ServiceDepartmentEntity;
import me.thesis.clinic_booking.entity.store.StoreEntity;
import me.thesis.clinic_booking.entity.store.booking.WorkingTimeConfigEntity;
import me.thesis.clinic_booking.service.cache.ServiceDepartmentCacheService;
import me.thesis.clinic_booking.service.cache.StoreCacheService;
import me.thesis.clinic_booking.service.store.booking.WorkingTimeConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Date;

@RestController
@RequestMapping("/api/v1/service_department/{serviceDepartmentAuthId}/working_time_config")
public class WorkingTimeConfigAPI {

    @Autowired
    private WorkingTimeConfigService workingTimeConfigService;

    @Autowired
    private StoreCacheService storeCacheService;

    @Autowired
    private ServiceDepartmentCacheService serviceDepartmentCacheService;


    @PostMapping("/save")
    public ResponseEntity<String> save(@PathVariable String serviceDepartmentAuthId,
                                       @RequestPart("configInfo") WorkingTimeConfigEntity workingTimeConfig,
                                       @RequestPart("storeAuthId") String storeAuthId) {
        StoreEntity storeEntity = storeCacheService.getByAuthIdFromCache(storeAuthId);
        if (storeEntity == null) return ResponseEntity.ok("BAD_REQUEST");

        workingTimeConfig.setStoreId(storeEntity.getId());
        ServiceDepartmentEntity serviceDepartmentEntity = serviceDepartmentCacheService.getByAuthIdAndStoreIdFromCache(serviceDepartmentAuthId, storeEntity.getId());

        if (serviceDepartmentEntity == null) return ResponseEntity.ok("BAD_REQUEST");

        workingTimeConfig.setServiceDepartmentId(serviceDepartmentEntity.getId());
        workingTimeConfigService.save(workingTimeConfig);
        return ResponseEntity.ok("SAVE_SUCCESS");
    }

    @PostMapping("/findByActiveDay")
    public ResponseEntity<WorkingTimeConfigEntity> getSpecificWorkingTimeActiveDay(@PathVariable String serviceDepartmentAuthId,
                                                                                   @RequestParam("activeDay") Date activeDay) {
        ServiceDepartmentEntity serviceDepartmentEntity = serviceDepartmentCacheService.getByAuthIdFromCache(serviceDepartmentAuthId);
        if (serviceDepartmentEntity != null) {
            WorkingTimeConfigEntity workingTimeConfigEntity = workingTimeConfigService.findSpecificDayConfigByActiveDay(serviceDepartmentEntity.getId(), activeDay);
            return ResponseEntity.ok().body(workingTimeConfigEntity);
        } else {
            return ResponseEntity.ok().body(null);
        }
    }

    @PostMapping("/changeNumberSlotOfFrame")
    public ResponseEntity<String> changeNumberSlotOfFrame(@PathVariable String serviceDepartmentAuthId,
                                                          @RequestParam("activeDay") Date activeDay,
                                                          @RequestParam("frame") String frame,
                                                          @RequestParam("typeChange") String typeChange) {
        ServiceDepartmentEntity serviceDepartmentEntity = serviceDepartmentCacheService.getByAuthIdFromCache(serviceDepartmentAuthId);
        boolean isChangeSuccess = workingTimeConfigService.changeNumberSlotOfFrame(activeDay, serviceDepartmentEntity.getId(), frame, typeChange);
        if (isChangeSuccess) {
            return ResponseEntity.ok().body("SUCCESS");
        } else {
            return ResponseEntity.ok().body("ERROR");
        }
    }
}