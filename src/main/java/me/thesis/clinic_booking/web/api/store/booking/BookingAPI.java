package me.thesis.clinic_booking.web.api.store.booking;

import me.thesis.clinic_booking.config.security.CustomUserDetail;
import me.thesis.clinic_booking.dto.ResponseStatus;
import me.thesis.clinic_booking.dto.*;
import me.thesis.clinic_booking.entity.enums.StatusBookingType;
import me.thesis.clinic_booking.entity.store.ServiceDepartmentEntity;
import me.thesis.clinic_booking.entity.store.StoreEntity;
import me.thesis.clinic_booking.entity.store.booking.BookingEntity;
import me.thesis.clinic_booking.entity.store.booking.CommentBookingEntity;
import me.thesis.clinic_booking.entity.store.booking.CustomerBookingEntity;
import me.thesis.clinic_booking.service.CommonReturnAPIService;
import me.thesis.clinic_booking.service.DownloadFileService;
import me.thesis.clinic_booking.service.cache.ServiceDepartmentCacheService;
import me.thesis.clinic_booking.service.store.StoreService;
import me.thesis.clinic_booking.service.store.booking.BookingService;
import me.thesis.clinic_booking.service.store.booking.CommentBookingService;
import me.thesis.clinic_booking.service.store.booking.CustomerBookingService;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.util.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URLEncoder;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/v1/service_department")
public class BookingAPI {

    @Autowired
    private ServiceDepartmentCacheService serviceDepartmentCacheService;

    @Autowired
    private BookingService bookingService;

    @Autowired
    private CommonReturnAPIService commonReturnAPIService;

    @Autowired
    private CustomerBookingService customerBookingService;

    @Autowired
    private StoreService storeService;

    @Autowired
    private CommentBookingService commentBookingService;

    @Autowired
    private DownloadFileService downloadFileService;

    @GetMapping("{bookingId}/findById")
    public ResponseEntity<ServerResponseDto> getBookingById(@PathVariable("bookingId") Long bookingId) {
        BookingEntity bookingEntity = bookingService.findByIdAndDeletedFalse(bookingId);
        return ResponseEntity.ok(new ServerResponseDto(ResponseCase.SUCCESS, bookingEntity));
    }

    @GetMapping("/{serviceDepartmentAuthId}/booking/findByActiveDay")
    public ResponseEntity<Map<String, List<BookingEntity>>> getListBookingByDay(@PathVariable("serviceDepartmentAuthId") String serviceDepartmentAuthId,
                                                                                @RequestParam("activeDay") Date activeDay) {
        ServiceDepartmentEntity serviceDepartmentEntity = serviceDepartmentCacheService.getByAuthIdFromCache(serviceDepartmentAuthId);
        if (serviceDepartmentEntity == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        Map<String, List<BookingEntity>> mapListBookingAndFramesInDay = bookingService.findByBookingTime(serviceDepartmentEntity.getId(), activeDay);
        return ResponseEntity.ok().body(mapListBookingAndFramesInDay);
    }

    //    @PreAuthorize("@authorizationService.hasPermissionAccessStoreAndHasPermission(#storeAuthId, 'IS_CAN_ACCESS_BOOKING_SCREEN')")
    @GetMapping("/{serviceDepartmentAuthId}/booking/{id}")
    public ResponseEntity<ServerResponseDto> getBookingInfoById(@PathVariable("serviceDepartmentAuthId") String serviceDepartmentAuthId,
                                                                @PathVariable("id") Long bookingId) {
        ServiceDepartmentEntity serviceDepartment = serviceDepartmentCacheService.getByAuthIdFromCache(serviceDepartmentAuthId);
        DetailBookingInfoDto detailBookingInfoDto = bookingService.getBookingInfoByBookingId(bookingId, serviceDepartment.getId());
        return ResponseEntity.ok(new ServerResponseDto(ResponseCase.SUCCESS, detailBookingInfoDto));
    }

    //    @PreAuthorize("@authorizationService.hasPermissionAccessStoreAndHasPermission(#storeAuthId, 'IS_CAN_EDIT_BOOKING_SCREEN')")
    @PostMapping("/{serviceDepartmentAuthId}/booking/create")
    public ResponseEntity<ServerResponseDto> createNewBooking(@PathVariable("serviceDepartmentAuthId") String serviceDepartmentAuthId,
                                                              @RequestBody SaveBookingDto saveBookingDto,
                                                              @AuthenticationPrincipal CustomUserDetail customUserDetails) {
        ServiceDepartmentEntity serviceDepartment = serviceDepartmentCacheService.getByAuthIdFromCache(serviceDepartmentAuthId);
        Long storeId = storeService.getStoreIdFromAuthId(saveBookingDto.getStoreAuthId());
        return ResponseEntity.ok(bookingService.create(saveBookingDto, serviceDepartment.getId(), storeId, customUserDetails));
    }

    //    @PreAuthorize("@authorizationService.hasPermissionAccessStoreAndHasPermission(#storeAuthId, 'IS_CAN_EDIT_BOOKING_SCREEN')")
    @PostMapping("/{serviceDepartmentAuthId}/booking/update")
    public ResponseEntity<ServerResponseDto> updateOldBooking(@PathVariable("serviceDepartmentAuthId") String serviceDepartmentAuthId,
                                                              @RequestBody SaveBookingDto saveBookingDto,
                                                              @AuthenticationPrincipal CustomUserDetail customUserDetails) {
        ServiceDepartmentEntity serviceDepartment = serviceDepartmentCacheService.getByAuthIdFromCache(serviceDepartmentAuthId);
        return ResponseEntity.ok(bookingService.updateOldBooking(saveBookingDto, serviceDepartment.getId(), customUserDetails.getUserEntity().getId()));
    }

    //    @PreAuthorize("@authorizationService.hasPermissionAccessStoreAndHasPermission(#storeAuthId, 'IS_CAN_EDIT_BOOKING_SCREEN')")
    @PostMapping("/{serviceDepartmentAuthId}/booking/delete")
    public ResponseEntity<ServerResponseDto> delete(@PathVariable("serviceDepartmentAuthId") String serviceDepartmentAuthId,
                                                    @RequestParam("bookingId") Long bookingId,
                                                    @AuthenticationPrincipal CustomUserDetail customUserDetails) {
        ServiceDepartmentEntity serviceDepartmentEntity = serviceDepartmentCacheService.getByAuthIdFromCache(serviceDepartmentAuthId);
        boolean isDeleteSuccess = bookingService.deleteBooking(bookingId, serviceDepartmentEntity.getId(), customUserDetails.getUserEntity().getId());
        ResponseStatus responseStatus = isDeleteSuccess ? ResponseCase.DELETE_SUCCESS : ResponseCase.DELETE_FAIL;
        return ResponseEntity.ok().body(new ServerResponseDto(responseStatus));
    }

    @PostMapping("/{serviceDepartmentAuthId}/booking/changeStatus")
    public ResponseEntity<String> changeBookingStatus(@PathVariable String serviceDepartmentAuthId,
                                                      @RequestParam("bookingId") Long bookingId, @RequestParam("statusChangeTo") StatusBookingType newStatus) {
        ServiceDepartmentEntity serviceDepartmentEntity = serviceDepartmentCacheService.getByAuthIdFromCache(serviceDepartmentAuthId);
        if (serviceDepartmentEntity == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        String responseOfSavingBooking = bookingService.changeBookingStatus(bookingId, serviceDepartmentEntity.getId(), newStatus);
        return ResponseEntity.ok(responseOfSavingBooking);
    }

    @PostMapping("/{serviceDepartmentAuthId}/booking/changeStatusPaycheck")
    public ResponseEntity<ServerResponseDto> changeBookingStatusPaycheck(@PathVariable String serviceDepartmentAuthId,
                                                      @RequestParam("bookingId") Long bookingId) {
        ServiceDepartmentEntity serviceDepartmentEntity = serviceDepartmentCacheService.getByAuthIdFromCache(serviceDepartmentAuthId);
        if (serviceDepartmentEntity == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        boolean isChangeSuccess = bookingService.changeStatusPaycheck(bookingId, serviceDepartmentEntity.getId());
        return ResponseEntity.ok().body(new ServerResponseDto(ResponseCase.SUCCESS, isChangeSuccess));
    }


    @GetMapping("/{serviceDepartmentAuthId}/booking/findCustomerByPhoneTypeahead")
    public ResponseEntity<List<CustomerBookingEntity>> getListCustomerByPhone(@PathVariable String serviceDepartmentAuthId,
                                                                              @RequestParam("phone") String phone) {
        ServiceDepartmentEntity serviceDepartmentEntity = serviceDepartmentCacheService.getByAuthIdFromCache(serviceDepartmentAuthId);
        if (serviceDepartmentEntity == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return ResponseEntity.ok().body(bookingService.findListCustomerForTypeAHead(phone, serviceDepartmentEntity.getStoreId()));
    }

    //    @PreAuthorize("@authorizationService.hasPermissionAccessStoreAndHasPermission(#storeAuthId, 'IS_CAN_ACCESS_BOOKING_SCREEN')")
    @GetMapping("/{serviceDepartmentAuthId}/booking/findCustomerByNameTypeahead")
    public ResponseEntity<ServerResponseDto> getListCustomerByName(@PathVariable("serviceDepartmentAuthId") String serviceDepartmentAuthId,
                                                                   @RequestParam("nameKeySearch") String nameKeySearch) {
        ServiceDepartmentEntity serviceDepartment = serviceDepartmentCacheService.getByAuthIdFromCache(serviceDepartmentAuthId);
        StoreEntity storeEntity = storeService.findById(serviceDepartment.getStoreId());
        if (storeEntity == null) {
            return ResponseEntity.ok(new ServerResponseDto(ResponseCase.ERROR));
        }

        if (nameKeySearch.equals("")) {
            return ResponseEntity.ok(new ServerResponseDto(ResponseCase.SUCCESS, Collections.emptyList()));
        }
        List<CustomerBookingEntity> customerBookingEntities = customerBookingService.findByNameContaining(storeEntity.getId(), nameKeySearch);
        return ResponseEntity.ok(new ServerResponseDto(ResponseCase.SUCCESS, customerBookingEntities));
    }

    //    @PreAuthorize("@authorizationService.hasPermissionAccessStoreAndHasPermission(#storeAuthId, 'IS_CAN_EDIT_BOOKING_SCREEN')")
    @PostMapping("/{serviceDepartmentAuthId}/booking/createNewCustomer")
    public ResponseEntity<ServerResponseDto> createNewCustomer(@PathVariable("serviceDepartmentAuthId") String serviceDepartmentAuthId,
                                                               @RequestParam("customerName") String customerName,
                                                               @RequestParam("customerPhone") String customerPhone,
                                                               @AuthenticationPrincipal CustomUserDetail customUserDetails) {
        ServiceDepartmentEntity serviceDepartment = serviceDepartmentCacheService.getByAuthIdFromCache(serviceDepartmentAuthId);
        StoreEntity storeEntity = storeService.findById(serviceDepartment.getStoreId());
        if (storeEntity == null) {
            return ResponseEntity.ok(new ServerResponseDto(ResponseCase.ERROR));
        }
        Long storeId = storeEntity.getId();
        return ResponseEntity.ok(customerBookingService.createNewCustomerForStore(storeId, customerName, customerPhone, customUserDetails.getUserEntity().getId()));
    }

    @PostMapping("/{serviceDepartmentAuthId}/booking/saveCommentAndFile")
    public ResponseEntity<ServerResponseDto> saveComment(@PathVariable("serviceDepartmentAuthId") String serviceDepartmentAuthId,
                                                         @ModelAttribute CommentAndFileBookingDto commentAndFileBookingDto,
                                                         @AuthenticationPrincipal CustomUserDetail customUserDetails,
                                                         HttpServletRequest request) {
        ServiceDepartmentEntity serviceDepartment = serviceDepartmentCacheService.getByAuthIdFromCache(serviceDepartmentAuthId);

        BookingEntity bookingEntity = bookingService.findById(commentAndFileBookingDto.getBookingId(), serviceDepartment.getId());
        if (bookingEntity == null) {
            return ResponseEntity.ok(new ServerResponseDto(ResponseCase.NOT_FOUND_ENTITY));
        }
        commentAndFileBookingDto.setServiceDepartmentId(serviceDepartment.getId());
        return ResponseEntity.ok(new ServerResponseDto(commentBookingService.saveCommentAndFileAdmin(commentAndFileBookingDto, customUserDetails.getUserEntity().getId(), request)));
    }

    //    @PreAuthorize("@authorizationService.hasPermissionAccessStoreAndHasPermission(#storeAuthId, 'IS_CAN_ACCESS_BOOKING_SCREEN')")
    @GetMapping("/{serviceDepartmentAuthId}/booking/commentBooking")
    public ResponseEntity<ServerResponseDto> getListCommentByBookingId(@PathVariable("serviceDepartmentAuthId") String serviceDepartmentAuthId,
                                                                       @RequestParam("customerId") Long customerId) {
        List<CommentBookingEntity> listComment = commentBookingService.findByCustomerId(customerId);
        return ResponseEntity.ok().body(new ServerResponseDto(ResponseCase.SUCCESS, listComment));
    }

    //    @PreAuthorize("@authorizationService.hasPermissionAccessStoreAndHasPermission(#storeAuthId, 'IS_CAN_EDIT_BOOKING_SCREEN')")
    @PostMapping("/{serviceDepartmentAuthId}/booking/deleteComment")
    public ResponseEntity<ServerResponseDto> deleteComment(@PathVariable("serviceDepartmentAuthId") String serviceDepartmentAuthId,
                                                           @RequestParam("customerId") Long customerId,
                                                           @RequestParam("commentId") Long commentId,
                                                           @AuthenticationPrincipal CustomUserDetail customUserDetails) {
        boolean isDeleteSuccess = commentBookingService.deleteComment(commentId, customerId, customUserDetails.getUserEntity().getId());
        ResponseStatus responseStatus = isDeleteSuccess ? ResponseCase.DELETE_SUCCESS : ResponseCase.DELETE_FAIL;
        return ResponseEntity.ok().body(new ServerResponseDto(responseStatus));
    }

    //    @PreAuthorize("@authorizationService.hasPermissionAccessStoreAndHasPermission(#storeAuthId, 'IS_CAN_EDIT_BOOKING_SCREEN')")
    @PostMapping("/{serviceDepartmentAuthId}/booking/editComment")
    public ResponseEntity<ServerResponseDto> editComment(@PathVariable("serviceDepartmentAuthId") String serviceDepartmentAuthId,
                                                         @RequestParam("customerId") Long customerId,
                                                         @RequestParam("commentId") Long commentId,
                                                         @RequestParam("content") String content,
                                                         @AuthenticationPrincipal CustomUserDetail customUserDetails) {
        boolean isEditSuccess = commentBookingService.editComment(commentId, customerId, content, customUserDetails.getUserEntity().getId());
        ResponseStatus responseStatus = isEditSuccess ? ResponseCase.SUCCESS : ResponseCase.NOT_FOUND_ENTITY;
        return ResponseEntity.ok(new ServerResponseDto(responseStatus));
    }

    //    @PreAuthorize("@authorizationService.hasPermissionAccessStoreAndHasPermission(#storeAuthId, 'IS_CAN_ACCESS_BOOKING_SCREEN')")
    @GetMapping("/{serviceDepartmentAuthId}/booking/downloadImageComment")
    public void downloadUserReceivedCoupon(@PathVariable("serviceDepartmentAuthId") Long serviceDepartmentAuthId,
                                           HttpServletResponse response,
                                           @RequestParam String imgUrl) throws IOException {
        String imageExtension = StringUtils.substringAfterLast(imgUrl, ".");
        response.setCharacterEncoding("UTF-8");
        response.setHeader("Content-type", "application/octet-stream; charset=utf-8");
        String fileName = URLEncoder.encode(imgUrl.replace("." + imageExtension, ""), "UTF-8") + "." + imageExtension;
        response.setHeader("Content-Disposition", "attachment;filename=" + fileName);
        try {
            response.getOutputStream().write(IOUtils.toByteArray(downloadFileService.downloadImage(imgUrl)));
        } catch (IOException e) {
            response.setStatus(HttpServletResponse.SC_NOT_FOUND);
        }
    }
}
