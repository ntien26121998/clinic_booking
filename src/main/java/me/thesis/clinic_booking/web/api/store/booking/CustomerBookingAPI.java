package me.thesis.clinic_booking.web.api.store.booking;

import me.thesis.clinic_booking.config.security.CustomUserDetail;
import me.thesis.clinic_booking.dto.CustomerBookingForWebDto;
import me.thesis.clinic_booking.dto.ResponseCase;
import me.thesis.clinic_booking.dto.ServerResponseDto;
import me.thesis.clinic_booking.dto.SummaryBookingOfCustomerDto;
import me.thesis.clinic_booking.entity.store.StoreEntity;
import me.thesis.clinic_booking.entity.store.booking.BookingEntity;
import me.thesis.clinic_booking.entity.store.booking.CustomerBookingEntity;
import me.thesis.clinic_booking.service.PageService;
import me.thesis.clinic_booking.service.store.ServiceDepartmentService;
import me.thesis.clinic_booking.service.store.StoreService;
import me.thesis.clinic_booking.service.store.booking.BookingService;
import me.thesis.clinic_booking.service.store.booking.CustomerBookingService;
import me.thesis.clinic_booking.service.store.customer.ImportCustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@RestController
@RequestMapping("/api/v1/store/")
public class CustomerBookingAPI {
    @Autowired
    private PageService pageService;

    @Autowired
    private ServiceDepartmentService serviceDepartmentService;

    @Autowired
    private BookingService bookingService;

    @Autowired
    private StoreService storeService;

    @Autowired
    private CustomerBookingService customerBookingService;

    @Autowired
    private ImportCustomerService importCustomerService;

//    @PreAuthorize("@authorizationService.hasPermissionAccessStoreAndHasPermission(#storeAuthId, 'IS_CAN_ACCESS_LIST_CUSTOMER_STORE_SCREEN')")
    @GetMapping("{storeAuthId}/customer/findByStoreId")
    public ResponseEntity<Page<CustomerBookingForWebDto>> getListCustomerByStoreId(@PathVariable("storeAuthId") String storeAuthId,
                                                                      @RequestParam("page") int page,
                                                                      @RequestParam("size") int size,
                                                                      @RequestParam String sortDir,
                                                                      @RequestParam String sortField,
                                                                      @RequestParam(value = "customFieldSearch", required = false) String nameOrCodeFilter) {
        Long storeId = storeService.getStoreIdFromAuthId(storeAuthId);
        Pageable pageable = pageService.getPage(sortDir, sortField, page, size);
        Page<CustomerBookingForWebDto> customerBookingEntities = customerBookingService.findByStoreId(storeId, nameOrCodeFilter, pageable);
        return ResponseEntity.ok(customerBookingEntities);
//        return ResponseEntity.ok().body(new ServerResponseDto(ResponseCase.SUCCESS, customerBookingEntities));
    }

//    @PreAuthorize("@authorizationService.hasPermissionAccessStoreAndHasPermission(#storeAuthId, 'IS_CAN_ACCESS_LIST_CUSTOMER_STORE_SCREEN')")
    @PostMapping("{storeAuthId}/customer/updateCustomer")
    public ResponseEntity<ServerResponseDto> updateCustomer(@PathVariable("storeAuthId") String storeAuthId,
                                                            @RequestBody CustomerBookingEntity customerUpdateInfo,
                                                            @AuthenticationPrincipal CustomUserDetail currentUser) {
        return ResponseEntity.ok(customerBookingService.updateCustomer(customerUpdateInfo, currentUser.getUserEntity().getId()));
    }

//    @PreAuthorize("@authorizationService.hasPermissionAccessStoreAndHasPermission(#storeAuthId, 'IS_CAN_ACCESS_LIST_CUSTOMER_STORE_SCREEN')")
    @GetMapping("{storeAuthId}/booking/getListBookingByCustomerIdAndStoreId")
    public ResponseEntity<Page<BookingEntity>> getListBookingByCustomerIdAndStoreId(@PathVariable("storeAuthId") String storeAuthId,
                                                                                  @RequestParam("customerId") Long customerId,
                                                                                  @RequestParam("page") int page,
                                                                                  @RequestParam("size") int size,
                                                                                  @RequestParam(value = "sortDir") String sortDir,
                                                                                  @RequestParam(value = "sortField") String sortField) {

        StoreEntity storeEntity = storeService.findByStoreAuthId(storeAuthId);
        Pageable pageable = pageService.getPage(sortDir, sortField, page, size);
        Page<BookingEntity> listBooking = bookingService.findByStoreIdAndCustomerId(storeEntity, customerId, pageable);
        return ResponseEntity.ok(listBooking);
    }

//    @PreAuthorize("@authorizationService.hasPermissionAccessStoreAndHasPermission(#storeAuthId, 'IS_CAN_ACCESS_BOOKING_SCREEN')")
    @GetMapping("{storeAuthId}/customer/getListBookingByCustomerIdForPopupBooking")
    public ResponseEntity<ServerResponseDto> getListBookingByCustomerIdForPopupBooking(@PathVariable("storeAuthId") String storeAuthId,
                                                                                       @RequestParam("customerId") Long customerId,
                                                                                       @RequestParam("page") int page,
                                                                                       @RequestParam("size") int size,
                                                                                       @RequestParam(value = "sortDir") String sortDir, @RequestParam(value = "sortField") String sortField) {
        Pageable pageable = pageService.getPage(sortDir, sortField, page, size);
        Long storeId = storeService.getStoreIdFromAuthId(storeAuthId);
        Page<SummaryBookingOfCustomerDto> listBooking = bookingService.getListBookingByCustomerIdForPopupBooking(storeId, customerId, pageable);
        return ResponseEntity.ok(new ServerResponseDto(ResponseCase.SUCCESS, listBooking));
    }

//    @PreAuthorize("@authorizationService.hasPermissionAccessStoreAndHasPermission(#storeAuthId, 'IS_CAN_EDIT_LIST_CUSTOMER_STORE_SCREEN')")
    @PostMapping("{storeAuthId}/customer/delete/{id}")
    public ResponseEntity<ServerResponseDto> delete(@PathVariable("storeAuthId") String storeAuthId, @PathVariable("id") Long id,
                                                    @AuthenticationPrincipal CustomUserDetail userDetails) {
        Long storeId = storeService.getStoreIdFromAuthId(storeAuthId);
        if (storeId == null) {
            return ResponseEntity.ok(new ServerResponseDto(ResponseCase.ERROR));
        }
        boolean isDeleteSuccess = customerBookingService.deleteCustomer(id, storeId, userDetails);
        return ResponseEntity.ok().body(new ServerResponseDto(isDeleteSuccess ? ResponseCase.SUCCESS : ResponseCase.ERROR));
    }

//    @PreAuthorize("@authorizationService.hasPermissionAccessStoreAndHasPermission(#storeAuthId, 'IS_CAN_EDIT_LIST_CUSTOMER_STORE_SCREEN')")
    @PostMapping("{storeAuthId}/customer/importCustomer")
    public ResponseEntity<ServerResponseDto> importCategory(@PathVariable String storeAuthId,
                                                            @RequestPart MultipartFile excelFile) {
        StoreEntity storeEntity = storeService.findByStoreAuthId(storeAuthId);
        if (storeEntity == null) {
            return ResponseEntity.ok(new ServerResponseDto(ResponseCase.ERROR));
        }
        Long storeId = storeEntity.getId();
        return ResponseEntity.ok(new ServerResponseDto(ResponseCase.SUCCESS, importCustomerService.readDataFromFileImport(excelFile, storeId)));
    }

//    @PreAuthorize("@authorizationService.hasPermissionAccessStoreAndHasPermission(#storeAuthId, 'IS_CAN_EDIT_LIST_CUSTOMER_STORE_SCREEN')")
    @PostMapping("{storeAuthId}/customer/submitImport")
    public ResponseEntity<ServerResponseDto> submitImport(@PathVariable String storeAuthId,
                                                          @RequestBody List<CustomerBookingEntity> listCustomerBooking,
                                                          @AuthenticationPrincipal CustomUserDetail userDetails) {
        Long storeId = storeService.getStoreIdFromAuthId(storeAuthId);
        if (storeId == null) {
            return ResponseEntity.ok(new ServerResponseDto(ResponseCase.ERROR));
        }
        boolean isSaveSuccess = importCustomerService.submitImport(listCustomerBooking, userDetails);
        return ResponseEntity.ok(new ServerResponseDto(isSaveSuccess ? ResponseCase.SUCCESS : ResponseCase.ERROR));
    }
}
