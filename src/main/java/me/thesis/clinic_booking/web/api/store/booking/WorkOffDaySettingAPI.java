package me.thesis.clinic_booking.web.api.store.booking;

import me.thesis.clinic_booking.config.security.CustomUserDetail;
import me.thesis.clinic_booking.dto.ResponseCase;
import me.thesis.clinic_booking.dto.ServerResponseDto;
import me.thesis.clinic_booking.entity.store.ServiceDepartmentEntity;
import me.thesis.clinic_booking.entity.store.booking.WorkOffDaySettingEntity;
import me.thesis.clinic_booking.service.store.ServiceDepartmentService;
import me.thesis.clinic_booking.service.store.booking.WorkOffDaySettingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/web/{serviceDepartmentAuthId}/work_off_day_setting")
public class WorkOffDaySettingAPI {

    @Autowired
    private WorkOffDaySettingService workOffDaySettingService;

    @Autowired
    private ServiceDepartmentService serviceDepartmentService;

//    @PreAuthorize("@authorizationService.hasPermissionAccessStore(#storeAuthId)")
    @GetMapping("/listWorkOffDaySetting")
    public ResponseEntity<ServerResponseDto> getListFrameSetting(@PathVariable String serviceDepartmentAuthId) {
        ServiceDepartmentEntity serviceDepartmentEntity = serviceDepartmentService.findByAuthId(serviceDepartmentAuthId);
        if (serviceDepartmentEntity == null) {
            return ResponseEntity.ok().body(new ServerResponseDto(ResponseCase.ERROR));
        }
        List<WorkOffDaySettingEntity> listTimeSetting = workOffDaySettingService.findListCommonConfigByServiceDepartmentId(serviceDepartmentEntity.getId());
        return ResponseEntity.ok().body(new ServerResponseDto(ResponseCase.SUCCESS, listTimeSetting));
    }

//    @PreAuthorize("@authorizationService.hasPermissionAccessStore(#storeAuthId)")
    @PostMapping("/saveSetting")
    public ResponseEntity<ServerResponseDto> saveWorkOffDaySetting(@PathVariable String serviceDepartmentAuthId,
                                                                   @RequestBody WorkOffDaySettingEntity workOffDaySettingEntity,
                                                                   @AuthenticationPrincipal CustomUserDetail customUserDetails) {
        ServiceDepartmentEntity serviceDepartmentEntity = serviceDepartmentService.findByAuthId(serviceDepartmentAuthId);
        if (serviceDepartmentEntity == null) {
            return ResponseEntity.ok().body(new ServerResponseDto(ResponseCase.ERROR));
        }
        return ResponseEntity.ok(workOffDaySettingService.saveCommonConfig(serviceDepartmentEntity.getId(), workOffDaySettingEntity, customUserDetails.getUserEntity().getId()));
    }

    @PreAuthorize("@authorizationService.hasPermissionAccessStore(#storeAuthId)")
    @PostMapping("/deleteSetting")
    public ResponseEntity<ServerResponseDto> deleteSetting(@PathVariable String serviceDepartmentAuthId,
                                                           @RequestParam Long settingId,
                                                           @AuthenticationPrincipal CustomUserDetail customUserDetails) {
        ServiceDepartmentEntity serviceDepartmentEntity = serviceDepartmentService.findByAuthId(serviceDepartmentAuthId);
        if (serviceDepartmentEntity == null) {
            return ResponseEntity.ok().body(new ServerResponseDto(ResponseCase.ERROR));
        }
        return ResponseEntity.ok(workOffDaySettingService.deleteConfig(serviceDepartmentEntity.getId(), settingId, customUserDetails.getUserEntity().getId()));
    }
}
