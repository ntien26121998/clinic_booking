package me.thesis.clinic_booking.web.api.client;

import me.thesis.clinic_booking.config.security.CustomUserDetail;
import me.thesis.clinic_booking.dto.ResponseCase;
import me.thesis.clinic_booking.dto.SaveBookingDto;
import me.thesis.clinic_booking.dto.ServerResponseDto;
import me.thesis.clinic_booking.dto.client.NewBookingMessageDto;
import me.thesis.clinic_booking.dto.client.SlotBookingDto;
import me.thesis.clinic_booking.entity.NotificationBookingEntity;
import me.thesis.clinic_booking.entity.enums.NotificationUsingForType;
import me.thesis.clinic_booking.entity.enums.StatusBookingType;
import me.thesis.clinic_booking.entity.store.ServiceDepartmentEntity;
import me.thesis.clinic_booking.entity.store.booking.CustomerBookingEntity;
import me.thesis.clinic_booking.repository.store.booking.CustomerBookingRepository;
import me.thesis.clinic_booking.service.NotificationBookingService;
import me.thesis.clinic_booking.service.NotificationSocketService;
import me.thesis.clinic_booking.service.cache.ServiceDepartmentCacheService;
import me.thesis.clinic_booking.service.store.booking.BookingService;
import me.thesis.clinic_booking.service.store.booking.CustomerBookingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

@RestController
@RequestMapping("/api/v1/client/{serviceDepartmentAuthId}/booking")
public class BookingClientAPI {

    @Autowired
    private BookingService bookingService;

    @Autowired
    private ServiceDepartmentCacheService serviceDepartmentCacheService;

    @Autowired
    private NotificationSocketService notificationSocketService;

    @Autowired
    private NotificationBookingService notificationBookingService;

    @Autowired
    private CustomerBookingService customerBookingService;

    @Autowired
    private CustomerBookingRepository customerBookingRepository;

    @GetMapping("/getListEmptySlotOfStore")
    public ResponseEntity<SlotBookingDto> getListEmptySlotOfStore(@PathVariable("serviceDepartmentAuthId") String serviceDepartmentAuthId,
                                                                  @RequestParam("activeDay") Date activeDay) {
        Long serviceDepartmentId = serviceDepartmentCacheService.getByAuthIdFromCache(serviceDepartmentAuthId).getId();
        if (serviceDepartmentId == null) return new ResponseEntity<>(HttpStatus.BAD_REQUEST);

        SlotBookingDto slotBookingDto = bookingService.getNumberAvailableSlotOfFrames(serviceDepartmentId, activeDay);
        return ResponseEntity.ok().body(slotBookingDto);
    }

    @PostMapping("save")
    public ResponseEntity<ServerResponseDto> saveBooking(@PathVariable("serviceDepartmentAuthId") String serviceDepartmentAuthId,
                                                         @RequestBody SaveBookingDto saveBookingDto, @AuthenticationPrincipal CustomUserDetail customUserDetail) {
        ServiceDepartmentEntity serviceDepartmentEntity = serviceDepartmentCacheService.getByAuthIdFromCache(serviceDepartmentAuthId);
        if (serviceDepartmentEntity == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        Long serviceDepartmentId = serviceDepartmentEntity.getId();

        String customerName = saveBookingDto.getCustomerName();
        String customerPhone = saveBookingDto.getCustomerPhone();
        String customerEmail = saveBookingDto.getCustomerEmail();

        //Create Customer
        CustomerBookingEntity oldCustomer = customerBookingService.findByPhone(customerPhone);
        if(oldCustomer == null) {
            CustomerBookingEntity customer = new CustomerBookingEntity();
            customer.setName(customerName);
            customer.setPhone(customerPhone);
            customer.setEmail(customerEmail);
            customer.setStoreId(serviceDepartmentEntity.getStoreId());
            customer.setCreatedTime(new Date());
            customer.setPatientCodeFromUser(false);
            customer.setCreatedBooking(false);
            customer.setCreatedBy(1L);
            int numberLoopCanTry = 0;
            boolean isDuplicateCode;
            do {
                numberLoopCanTry++;
                if (numberLoopCanTry == 1000) {
                    return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
                }
                long newPatientCode = customerBookingService.generatePatientCodeForCustomer(serviceDepartmentEntity.getStoreId());
                try {
                    customer.setPatientCode(newPatientCode);
                    customerBookingRepository.save(customer);
                    saveBookingDto.setCustomerId(customer.getId());
                    isDuplicateCode = false;
                } catch (DataIntegrityViolationException e) {
//                LOGGER.error("Duplicate patient code {} with store id {}. Create new code", newPatientCode, storeId);
                    isDuplicateCode = true;
                }
            } while (isDuplicateCode);
        } else {
            oldCustomer.setName(customerName);
            oldCustomer.setEmail(customerEmail);
            oldCustomer.setPhone(customerPhone);
            oldCustomer.setUpdatedTime(new Date());
            customerBookingRepository.save(oldCustomer);
            saveBookingDto.setCustomerId(oldCustomer.getId());
        }

        saveBookingDto.setStatus(StatusBookingType.WAITING_CONFIRM);
        saveBookingDto.setStoreId(serviceDepartmentEntity.getStoreId());
        saveBookingDto.setServiceDepartmentId(serviceDepartmentId);
        ServerResponseDto responseOfSavingBooking = bookingService
                .create(saveBookingDto, serviceDepartmentEntity.getId(), serviceDepartmentEntity.getStoreId(), customUserDetail);

        //send socket notification to serviceDepartmentAuthId
        NewBookingMessageDto newBookingMessageDto = new NewBookingMessageDto("Lịch mới!", saveBookingDto.getBookingDate());
        notificationSocketService.notifyToServiceDepartmentAdmin(newBookingMessageDto, serviceDepartmentAuthId);

        // async save new notification to db
        NotificationBookingEntity notificationBookingEntity = new NotificationBookingEntity();
        notificationBookingEntity.setServiceDepartmentId(serviceDepartmentId);
        notificationBookingEntity.setCreatedTime(new Date());
        notificationBookingEntity.setType(NotificationUsingForType.SERVICE_DEPARTMENT_ADMIN);

        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:ss");
        String strDate = dateFormat.format(saveBookingDto.getBookingDate());
        notificationBookingEntity.setContent("Bạn có 1 lịch mới vào ngày: " + strDate);
        notificationBookingService.save(notificationBookingEntity);

        return ResponseEntity.ok(responseOfSavingBooking);
    }

//    @GetMapping("{id}")
//    public ResponseEntity<BookingAPI> getBookingInfo(@RequestParam("serviceDepartmentAuthId") String serviceDepartmentAuthId,
//                                                     @PathVariable("id") Long bookingId) {
//        Long memberId = userInfoHolder.getUserDetails().getUserId();
//        BookingInfoDto bookingInfoDto = bookingService.getBookingInfoByBookingIdForMember(bookingId, memberId);
//        return ResponseEntity.ok(new ServerResponseDto(ResponseCase.SUCCESS, bookingInfoDto));
//    }
//
}
