package me.thesis.clinic_booking.web.api;

import me.thesis.clinic_booking.config.security.CustomUserDetail;
import me.thesis.clinic_booking.dto.client.NotificationBookingDto;
import me.thesis.clinic_booking.service.NotificationBookingService;
import me.thesis.clinic_booking.service.store.ServiceDepartmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/{serviceDepartmentAuthId}/notification_booking")
public class NotificationBookingAPI {

    @Autowired
    private NotificationBookingService notificationBookingService;

    @Autowired
    private ServiceDepartmentService serviceDepartmentService;

    @GetMapping("/list")
    public ResponseEntity<NotificationBookingDto> getListNotification(@PathVariable String serviceDepartmentAuthId, @RequestParam int page,
                                                                      @AuthenticationPrincipal CustomUserDetail userDetail) {
        Long serviceDepartmentId = serviceDepartmentService.getServiceDepartmentIdFromAuthId(serviceDepartmentAuthId);
        return ResponseEntity.ok(notificationBookingService.getListNotification(userDetail.getUserEntity(), page, serviceDepartmentId));
    }

    @PostMapping("/setLatestTimeReading")
    public ResponseEntity<?> setLatestTimeReadingNotification(@AuthenticationPrincipal CustomUserDetail userDetail) {
        notificationBookingService.setLatestTimeReadNotification(userDetail.getUserEntity());
        return ResponseEntity.ok(HttpStatus.OK);
    }
}