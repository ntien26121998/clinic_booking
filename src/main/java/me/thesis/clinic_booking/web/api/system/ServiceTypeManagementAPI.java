package me.thesis.clinic_booking.web.api.system;

import me.thesis.clinic_booking.dto.ResponseCase;
import me.thesis.clinic_booking.dto.ServerResponseDto;
import me.thesis.clinic_booking.entity.store.ServiceTypeEntity;
import me.thesis.clinic_booking.service.CommonReturnAPIService;
import me.thesis.clinic_booking.service.ServiceTypeService;
import me.thesis.clinic_booking.service.cache.ServiceTypeCacheService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/system/service_type")
public class ServiceTypeManagementAPI {

    @Autowired
    private ServiceTypeService serviceTypeService;

    @Autowired
    private CommonReturnAPIService commonReturnAPIService;

    @Autowired
    private ServiceTypeCacheService serviceTypeCacheService;

//    @PreAuthorize("@preAuthorizeService.isCanAccessSystem()")
    @GetMapping("/list")
    public ResponseEntity<Page<ServiceTypeEntity>> getPage(@RequestParam("page") int page,
                                                           @RequestParam("size") int size,
                                                           @RequestParam("sortDir") String sortDir,
                                                           @RequestParam("sortField") String sortField,
                                                           @RequestParam("customFieldSearch") Long storeId) {
        return ResponseEntity.ok(serviceTypeService.findByStoreId(sortDir, sortField, page, size, storeId));
    }

//    @PreAuthorize("@preAuthorizeService.isCanAccessSystem()")
    @GetMapping("/all")
    public ResponseEntity<List<ServiceTypeEntity>> getListAll(Long storeId) {
        return ResponseEntity.ok(serviceTypeCacheService.getListServiceTypeByStoreIdFromCache(storeId));
    }

//    @PreAuthorize("@preAuthorizeService.isCanAccessSystem()")
    @GetMapping("/{id}")
    public ResponseEntity<ServiceTypeEntity> getById(@PathVariable Long id) {
        return ResponseEntity.ok(serviceTypeCacheService.getByIdFromCache(id));
    }

//    @PreAuthorize("@preAuthorizeService.isCanAccessSystem()")
    @PostMapping("/save")
    public ResponseEntity<ServerResponseDto> save(@ModelAttribute ServiceTypeEntity serviceTypeEntity) {
        boolean isSaveSuccess = serviceTypeService.save(serviceTypeEntity);
        return ResponseEntity.ok().body(new ServerResponseDto(ResponseCase.ADD_SUCCESS, isSaveSuccess));
    }

//    @PreAuthorize("@preAuthorizeService.isCanAccessSystem()")
    @PostMapping("/delete/{id}")
    public ResponseEntity<String> delete(@PathVariable Long id) {
        boolean isDeleteSuccess = serviceTypeService.delete(id);
        return commonReturnAPIService.returnOnDeleteAPI(isDeleteSuccess);
    }
}
