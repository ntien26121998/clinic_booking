package me.thesis.clinic_booking.web.api.store;

import me.thesis.clinic_booking.dto.UserDto;
import me.thesis.clinic_booking.entity.UserEntity;
import me.thesis.clinic_booking.entity.enums.RoleType;
import me.thesis.clinic_booking.entity.store.StoreEntity;
import me.thesis.clinic_booking.repository.RoleRepository;
import me.thesis.clinic_booking.service.CommonReturnAPIService;
import me.thesis.clinic_booking.service.UserService;
import me.thesis.clinic_booking.service.cache.StoreCacheService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/api/v1/store/{storeAuthId}/store_account")
public class StoreAccountManagementAPI {

    @Autowired
    private UserService userService;

    @Autowired
    private CommonReturnAPIService commonReturnAPIService;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private StoreCacheService storeCacheService;

    @PreAuthorize("@preAuthorizeService.isCanAccessStore(#storeAuthId)")
    @GetMapping("/list")
    public ResponseEntity<Page<UserEntity>> getAll(@PathVariable String storeAuthId,
                                                   @RequestParam("page") int page,
                                                   @RequestParam("size") int size,
                                                   @RequestParam("sortDir") String sortDir,
                                                   @RequestParam("sortField") String sortField) {
        StoreEntity storeEntity = storeCacheService.getByAuthIdFromCache(storeAuthId);
        if(storeEntity == null) {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
        return ResponseEntity.ok(userService.findByStoreIdAndRole(storeEntity.getId(), RoleType.STORE_ADMIN.name(), sortDir, sortField, page, size));
    }

    @PreAuthorize("@preAuthorizeService.isCanAccessStore(#storeAuthId)")
    @GetMapping("/{id}")
    public ResponseEntity<UserEntity> getById(@PathVariable String storeAuthId,
                                              @PathVariable Long id) {
        StoreEntity storeEntity = storeCacheService.getByAuthIdFromCache(storeAuthId);
        if(storeEntity == null) {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
        return ResponseEntity.ok(userService.findByIdAndStoreIdAndServiceDepartmentId(id, storeEntity.getId(), null));
    }

    @PreAuthorize("@preAuthorizeService.isCanAccessStore(#storeAuthId)")
    @PostMapping("/save")
    public ResponseEntity<String> save(@PathVariable String storeAuthId,
                                       @ModelAttribute UserDto userDto, HttpServletRequest request) {
        Long storeId = storeCacheService.getByAuthIdFromCache(storeAuthId).getId();
        userDto.setServiceDepartmentId(null);
        userDto.setStoreId(storeId);
        userDto.setRoleEntity(roleRepository.findByName("ROLE_" + RoleType.STORE_ADMIN));
        userDto.setEnableServe(true);
        boolean isSaveSuccess = userService.save(userDto, request);
        return commonReturnAPIService.returnOnSaveAPI(isSaveSuccess);
    }

    @PreAuthorize("@preAuthorizeService.isCanAccessStore(#storeAuthId)")
    @PostMapping("/delete/{id}")
    public ResponseEntity<String> delete(@PathVariable String storeAuthId,
                                         @PathVariable Long id) {
        Long storeId = storeCacheService.getByAuthIdFromCache(storeAuthId).getId();
        boolean isDeleteSuccess = userService.delete(id, storeId, null);
        return commonReturnAPIService.returnOnDeleteAPI(isDeleteSuccess);
    }

    @PreAuthorize("@preAuthorizeService.isCanAccessStore(#storeAuthId)")
    @PostMapping("/changeStatus/{id}")
    public ResponseEntity<String> changeUserStatus(@PathVariable String storeAuthId,
                                                   @PathVariable Long id) {
        Long storeId = storeCacheService.getByAuthIdFromCache(storeAuthId).getId();
        boolean isSaveSuccess = userService.changeStatus(id, storeId, null);
        return commonReturnAPIService.returnOnSaveAPI(isSaveSuccess);
    }
}

