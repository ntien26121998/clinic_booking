$(document).ready(function () {

    let $loginBtn = $("#login_btn");
    let $emailInput = $("#email");
    let $passwordInput = $("#password");

    $loginBtn.click(function (e) {
        if ($emailInput.val() == null || $emailInput.val() == '' || $passwordInput.val() == null || $passwordInput.val() == '') {
            window.alert.show("error", "Tên đăng nhập (Email) và mật khẩu không được để trống", 2000);
        } else {
            e.preventDefault();
            let param = {
                email: $("#email").val(),
                password: $("#password").val()
            };
            $.ajax({
                type: "POST",
                url: "/login",
                data: param,
                beforeSend: function () {
                    window.loader.show();
                },
            }).done(function(response) {
                console.log(response);
                window.loader.hide();
                window.location.href = response;
            }).fail(function(response) {
                window.loader.hide();
                window.alert.show("error", "Email hoặc mật khẩu không đúng. Vui lòng thử lại !", "2000");
            });
        }

    });

    $('body').keypress(function (e) {
        if (e.which == 13) {
            $('#login_btn').click();
        }
    });

})
