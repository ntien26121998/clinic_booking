let storeAccountTable;
$(document).ready(function () {
    let deleteAccountId;
    let $deleteAccountModal = $("#delete_store_account_modal");
    let storeAuthId = $("#store_id").val();

    let configStoreAccountTableObject = {
        columnDefinitions: [
            {
                "data": "name",
                "orderable": true,
                "defaultContent": noDataHtml,
                "class": 'text-center '
            },
            {
                "data": "email",
                "orderable": true,
                "defaultContent": noDataHtml,
                "class": 'text-center '
            },
            {
                "data": "phone",
                "orderable": false,
                "defaultContent": noDataHtml,
                "class": 'text-center '
            },
            {
                "data": "createdTime",
                "orderable": true,
                "defaultContent": noDataHtml,
                "class": 'text-center '
            },
            {
                "data": "status",
                "orderable": true,
                "defaultContent": noDataHtml,
                "class": 'text-center '
            },
            {
                "data": null,
                "orderable": false,
                "defaultContent": noDataHtml,
                "class": 'text-center '
            }
        ],
        columnRender: [
            {
                "render": function (status) {
                    if (status === 'ACTIVE') {
                        return '<label class="btn btn-sm btn-success">Hoạt động</label>';
                    }
                    return '<label class="btn btn-sm btn-secondary">Không hoạt động</label>'
                },
                "targets": 4
            },
            {
                "render": function (data) {
                    let checkedStatus = (data.status === 'ACTIVE' ? "checked" : "");
                    return '<label class="switch change-status" id="' + data.id + '" style="margin-right: 5px;">' +
                        '<input type="checkbox" +  ' + checkedStatus + '  />'+
                        '<span class="slider round" id="' + data.id + '"></span>' +
                        '</label>'+
                        '<button class="btn btn-danger btn-sm btn-delete-store-account" id="' + data.id + '" data-toggle="modal" data-target="#delete_store_account_modal" >Xóa</button>';
                },
                "targets": 5
            }
        ],
        drawCallbackFunction: function () {
        },
        sortField: {column: 3, typeDir: "desc"},
        url: "/api/v1/store/"+ storeAuthId + "/store_account/list"
    };

    storeAccountTable = configDataTableServerSide(configStoreAccountTableObject, $("#table_store_account"));

    $(document).on("click",".slider",function () {
        let accountId = $(this).attr('id');
        $.ajax({
            type: "POST",
            url: "/api/v1/store/"+ storeAuthId + "/store_account/changeStatus/" + accountId,
            beforeSend: function () {
                window.loader.show();
            }
        }).done(function (response) {
            window.loader.hide();
            if (response) {
                window.alert.show("success", "Thành công", "1500");
                setTimeout(function () {
                    storeAccountTable.ajax.reload();
                }, 1000);
            } else {
                window.alert.show("error", "Có lỗi xảy ra. Vui lòng thử lại !", "1500");
            }
        }).fail(function (response) {
            window.alert.show("error", "Có lỗi xảy ra. Vui lòng thử lại !", "1500");
        });
    });

    $(document).on("click",".btn-delete-store-account",function () {
        deleteAccountId = $(this).attr('id');
    });

    $(document).on("click", "#btn_submit_delete", function () {
        $.ajax({
            type: "POST",
            url: '/api/v1/store/'+ storeAuthId + '/store_account/delete/' + deleteAccountId,
            beforeSend: function () {
                window.loader.show();
            }
        }).done(function (response) {
            deleteObjectResponseHandleOnDone(response, $deleteAccountModal, storeAccountTable);
        }).fail(function (response) {
            deleteObjectResponseHandleOnFail(response, $deleteAccountModal);
        })
    });

});
