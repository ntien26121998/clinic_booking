$(document).ready(function () {
    let storeAuthId = $("#store_id").val();
    let $formStoreAccount = $('#form_store_account');
    let $userName = $("#name");
    let $userPassword = $("#password");
    let $userEmail = $("#email");
    let $userPhone = $("#phone");
    let $repeatPassword = $("#repeat_password");
    let $modalStoreAccount = $("#modal_store_account");

    let rulesValidate = {
        name: {
            notEmpty: true,
            maxlength: 100
        },
        email: {
            notEmpty: true,
            email: true,
            maxlength: 100,
            checkEmailHasBeenUsed: ["/api/v1/system/system_account/checkEmailHasBeenUsed?email="]
        },
        phone: {
            onlyNumber: true,
            maxlength: 100
        },
        password: {
            notEmpty: true,
            maxlength: 32,
            minlength: 8
        },
        repeat_password: {
            notEmpty: true,
            equalTo: "#password"
        },
        image_avatar: {
            required: {
                depends: function () {
                    return !imageHandle.isHasImage();
                }
            }
        },
    };

    let messages = {
        email: {
            email: "Vui lòng nhập đúng địa chỉ email"
        },
        repeat_password: {
            equalTo: "Vui lòng nhắc lại đúng mật khẩu vừa nhập"
        },
        password: {
            maxlength: "Vui lòng nhập dưới 32 ký tự",
            minlength: "Vui lòng nhập ít nhất 8 ký tự"
        },
    };

    let validator = configValidate($formStoreAccount, rulesValidate, [], messages);

    $("#btn_add_store_admin").click(function () {
        $userName
            .add($userName)
            .add($userPassword)
            .add($repeatPassword)
            .add($userEmail)
            .add($userPhone)
            .val('').removeClass('error-message');
        validator.resetForm();
    });


    $("#btn_submit").click(function (e) {
        if ($formStoreAccount.valid()) {
            submitForm(e)
        }
    });

    let submitForm = function (e) {
        e.preventDefault();
        let formData = new FormData();
        formData.append("email", $userEmail.val());
        formData.append('name', $userName.val());
        formData.append('phone', $userPhone.val());
        formData.append("enableServe", "true");
        formData.append('password', $userPassword.val());
        formData.append('status', "ACTIVE");
        if ($(".file-input")[0].files.length !== 0) {
            formData.append("avatar", $("#image_avatar")[0].files[0]);
        }
        $.ajax({
            type: "POST",
            url: "/api/v1/store/"+ storeAuthId + "/store_account/save",
            data: formData,
            beforeSend: function () {
                window.loader.show();
            },
            processData: false,
            contentType: false,
        }).done(function (response) {
            saveObjectResponseHandleOnDone(response, $modalStoreAccount, storeAccountTable);
        }).fail(function (response) {
            saveObjectResponseHandleOnFail(response, $modalStoreAccount);
        });
    };

});
