$(document).ready(function () {

    let storeId = $("#store_id").val();

    let $formSaveStore = $("#form_store_info");
    let $btnSubmit = $("#btn_submit");
    let $storeName = $("#name");
    let $storePhone = $("#phone");
    let $storeDistrictName = $("#district_name");
    let $storeAddress = $("#address");
    let $storeEmail = $("#email");
    let $storeWebsite = $("#website");
    let $companyId = $("#company_id");
    let $storeCode = $("#store_code");

    initCkEditor('description');
    fillStoreInformation();

    function fillStoreInformation() {
        jQuery.get("/api/v1/store/" + storeId + "/information", function (store) {
            $storeName.val(store.storeName);
            $storePhone.val(store.phone);
            $storeDistrictName.val(store.districtName)
            $storeAddress.val(store.address);
            $storeEmail.val(store.email);
            $storeWebsite.val(store.website);
            $storeCode.val(store.storeCode);
            setTimeout(function () {
                CKEDITOR.instances['description'].setData(store.description);
            }, 1500);
            imageHandle.showImageWhenUrlNotNull(store.imageUrl);
        });
    }

    let rules = {
        name: {
            notEmpty: true, maxlength: 500
        },
        phone: {
            onlyNumber: true, maxlength: 12
        },
        email: {
            notEmpty: true,
            email: true
        },
        districtName: {
            notEmpty: true, maxlength: 500
        },
        address: {
            notEmpty: true, maxlength: 500
        },
        image_store: {
            required: {
                depends: function () {
                    return !imageHandle.isHasImage();
                }
            }
        },
        description: {required_ckEditor: true}
    };

    let messages = {
        image_store: {
            required: "Vui lòng chọn 1 ảnh đại diện cửa hàng"
        }
    };

    configValidate($formSaveStore, rules, [], messages);

    $btnSubmit.click(function () {
        if ($formSaveStore.valid()) {
            submitForm();
        }
    });

    let submitForm = function () {
        let formData = new FormData();
        formData.append("name", $storeName.val());
        formData.append('phone', $storePhone.val());
        formData.append('address', $storeAddress.val());
        formData.append('districtName', $storeDistrictName.val());
        formData.append('email', $storeEmail.val());
        formData.append('website', $storeWebsite.val());
        formData.append('description', CKEDITOR.instances['description'].getData());
        formData.append('companyId', $companyId.val());
        formData.append('storeCode', $storeCode.val());

        if ($(".file-input")[0].files.length !== 0) {
            formData.append("image", $("#image_store")[0].files[0]);
        }

        $.ajax({
            type: "POST",
            url: "/api/v1/store/" + storeId + "/saveInformation",
            data: formData,
            beforeSend: function () {
                window.loader.show();
            },
            processData: false,
            contentType: false,
        }).done(function (response) {
            window.loader.hide();
            if (response === 'SAVE_SUCCESS') {
                window.alert.show("success", "Thành công", "1500");
                setTimeout(function () {
                    window.location.reload();
                }, 1000);
            } else {
                window.alert.show("error", "Có lỗi xảy ra. Vui lòng thử lại !", "1500");
            }
        }).fail(function (response) {
            window.alert.show("error", "Có lỗi xảy ra. Vui lòng thử lại !", "1500");
        });
    };
});
