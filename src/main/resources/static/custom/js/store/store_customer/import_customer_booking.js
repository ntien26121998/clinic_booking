$(document).ready(function () {
    let storeId = $("#store_id").val();

    let $importCustomerBookingButton = $("#import_customer_btn");
    let $importCustomerBookingInput = $("#import_customer_excel_input");
    let listCustomerBooking = [];
    let importTable;

    $importCustomerBookingButton.click(function () {
        $importCustomerBookingInput.click();
    });

    $importCustomerBookingInput.change(function () {
        let fileExtension = ["xlsx", 'xls'];
        if (this.files[0].length < 1) {
            return;
        }
        if ($.inArray($importCustomerBookingInput.val().split('.').pop().toLowerCase(), fileExtension) === -1) {
            window.alert.show("error", 'Chỉ nhận tệp Excel', 1500);
            return;
        }
        importFileConfig(this.files[0]);
        $importCustomerBookingInput.val(null);
        return false;
    });

    function importFileConfig(excelFile) {
        let formData = new FormData();
        formData.append("excelFile", excelFile);

        $.ajax({
            type: "POST",
            url: "/api/v1/store/" + storeId + "/customer/importCustomer",
            data: formData,
            processData: false,
            contentType: false,
            beforeSend: function () {
                window.loader.show();
            },
        }).then(function (result) {
            window.loader.hide();
            if (result.data == null || result.data.length === 0) {
                window.alert.show("error", 'Tệp Excel trống', 1500);
            } else {
                listCustomerBooking = result.data.listCustomerBooking;
                let listCustomerBookingError = result.data.listCustomerBookingError;
                showListErrorOnTable(listCustomerBookingError);
                showResultOnImportTable(listCustomerBooking);
                $("#modal_customer_booking_import").modal("show");
            }
        })
    }

    function showListErrorOnTable(listCustomerBookingError) {
        let isHasError = listCustomerBookingError.length > 0;
        $("#text_error_customer_booking").toggleClass('hidden', !isHasError);

        if (!isHasError) {
            return;
        }
        let textError = '<span>Dưới đây là những bản ghi dữ liệu nhập sai</span><br/>';
        textError += "Mã khách hàng: ";
        textError += listCustomerBookingError.join(", ");
        $("#text_error_customer_booking").html(textError);
    }

    function showResultOnImportTable(listCustomerBookingFromImport) {
        if (importTable != null) {
            importTable.destroy();
        }

        let html = '';
        for (let i = 0; i < listCustomerBookingFromImport.length; i++) {
            let customerBooking = listCustomerBookingFromImport[i];
            html += '<tr>' +
                '<td>' + customerBooking.patientCode + '</td>' +
                '<td>' + customerBooking.name + '</td>' +
                '<td>' + customerBooking.phone + '</td>' +
                '<td>' + customerBooking.email + '</td>' +
                '<td>' + customerBooking.gender + '</td>' +
                '</tr>';
        }
        $('#table_customer_booking_import tbody tr').remove();
        $('#table_customer_booking_import tbody').append(html);

        importTable = $('#table_customer_booking_import').DataTable({
            "language": {"url": "/libs/datatable/js/vi.json"},
            "lengthMenu": [[10, 20, 50], [10, 20, 50]
            ],
            "searching": false,
        });
    }

    $(document).on("click", "#btn_submit_import_customer_booking", function () {
        if (listCustomerBooking.length === 0) {
            $("#modal_config_import").modal("hide");
            window.alert.show("error", "Tệp Excel trống", 2000);
            return;
        }
        $.ajax({
            type: "POST",
            url: "/api/v1/store/" + storeId + "/customer/submitImport",
            data: JSON.stringify(listCustomerBooking),
            processData: false,
            contentType: 'application/json',
            beforeSend: function () {
                window.loader.show();
            }
        }).done(function (response) {
            $("#modal_customer_booking_import").modal("hide");
            window.loader.hide();
            if (response.status.code === 200) {
                window.alert.show("success", "Thành công", 2000);
                setTimeout(function () {
                    location.reload();
                }, 2000);
            } else {
                window.alert.show("error", "Đã cố lỗi xảy ra, vui lòng thử lại", 2000);
            }
        }).fail(function () {
            window.loader.hide();
            window.alert.show("error", "Đã cố lỗi xảy ra, vui lòng thử lại", 2000);
        })
    });
});
