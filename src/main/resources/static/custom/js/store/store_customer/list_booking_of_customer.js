let bookingOfCustomerTable;
$(document).ready(function () {
    let storeId = $("#store_id").val();
    let customerId = $("#customer_id").val();

    let linkAPI = "/api/v1/store/" + storeId + "/booking/getListBookingByCustomerIdAndStoreId?customerId=" + customerId;

    let configBookingOfCustomerTable = {
        columnDefinitions: [
            {"data": "id", "orderable": true, "defaultContent": noDataHtml, "class": 'text-center'},
            {"data": "bookingTime", "orderable": true, "defaultContent": noDataHtml, "class": 'text-center'},
            {"data": "timeStartToEnd", "orderable": false, "defaultContent": noDataHtml, "class": 'text-center'},
            {"data": "createdTime", "orderable": true, "defaultContent": noDataHtml, "class": 'text-center'},
            {"data": null, "orderable": false, "defaultContent": noDataHtml, "class": 'text-center'}
        ],
        columnRender: [
            {
                "render": function (booking) {
                        return `<button class="btn btn-sm btn-primary btn-update-booking-customer" id="btn_update_booking_${booking.id}" store="${booking.storeAuthId}"
                          data="${booking.bookingTime}_${booking.frame}" data-toggle="modal" data-target="#modal_add_booking" style="margin-right: 5px"> Chi tiết</button>
                            <button class="btn btn-sm btn-danger btn-delete-booking" data-toggle="modal" data-target="#modal_delete_booking" store="${booking.storeAuthId}"
                          data="${booking.id}" style="margin-right: 5px">Xóa</button>`
                          //   <button class="btn btn-sm btn-warning btn-active-special-booking" store="${booking.storeAuthId}"
                          // data="${booking.id}" style="margin-right: 5px; color: white">Đặc biệt</button>

                },
                "targets": 4
            },
        ],
        drawCallbackFunction: function () {
        },
        sortField: {column: 2, typeDir: "desc"},
        url: linkAPI,
    };
    bookingOfCustomerTable = configDataTableServerSide(configBookingOfCustomerTable, $("#list_booking_table"));

    let serviceDepartmentAuthId = "";
    $(document).on('click', ".btn-update-booking-customer", function (event) {
        event.preventDefault();
        $("#customer_phone").prop("disabled", true);
        $("#note").prop("disabled", true);
        $("#btn_submit_booking").addClass("hidden");

        let bookingId = $(this).attr('id').replace("btn_update_booking_", "");

        $.ajax({
            type: "GET",
            url: "/api/v1/service_department/"+ bookingId +"/findById",
            async: false
        }).then(function (response) {
            if (response.status.code === 200) {
                serviceDepartmentAuthId = response.data.serviceDepartmentAuthId;
                Object.assign(MODAL_BOOKING_VUE.$data, MODAL_BOOKING_VUE.initData());
                $("#number_frame_consecutive").prop('disabled', true);
                $("#add_booking_form").validate().resetForm();
                MODAL_BOOKING_VUE.isFromScreenCustomer = true;
                MODAL_BOOKING_VUE.loadListCategoryBooking(serviceDepartmentAuthId);
                MODAL_BOOKING_VUE.getDetailBooking(bookingId, serviceDepartmentAuthId);
            }
        })

    })

    let currentBookingId = 0;
    let serviceDepartmentId = "";
    $(document).on('click', ".btn-delete-booking", function () {
        $("#modal_delete_booking").modal('show');
        currentBookingId = $(this).attr("data");
        $.ajax({
            type: "GET",
            url: "/api/v1/service_department/"+ currentBookingId +"/findById",
            async: false
        }).then(function (response) {
            if (response.status.code === 200) {
                serviceDepartmentId = response.data.serviceDepartmentAuthId;
            }
        })
    });
    $("#btn_submit_delete").click(function () {
        $.ajax({
            type: "POST",
            url: "/api/v1/service_department/" + serviceDepartmentId + "/booking/delete?bookingId=" + currentBookingId,
            beforeSend: function () {
                window.loader.show();
            }
        }).done(function (response) {
            $('#modal_delete_booking').modal('hide');
            window.loader.hide();
            if (response.status.code === 201) {
                window.alert.show("success", "Thành công", 2000);
                bookingOfCustomerTable.ajax.reload();
                listCustomerTable.ajax.reload();
                bookingManagement.getBookingDataAndRenderTable($("#choose_active_day").val(), false);
            } else {
                window.alert.show("error", "Đã có lỗi xảy ra, vui lòng thử lại", 2000);
            }
        });
    });

});
