let listCustomerTable;
$(document).ready(function () {
    let storeAuthId = $("#store_id").val();
    let companyId = $("#company_id").val();

    let $nameOrCodeFilter = $("#name_or_code_filter_input");

    let currentTableId = $("#list_customer_script").attr('table');


    let configStoreCustomerTableObject = {
        columnDefinitions: [
            {
                "data": "patientCode",
                "sort": "patient_code",
                "orderable": true,
                "defaultContent": noDataHtml,
                "class": 'text-center patient-code'
            },
            {
                "data": "name",
                "sort": "name",
                "orderable": true,
                "defaultContent": noDataHtml,
                "class": 'text-center customer-name'
            },
            {
                "data": "numberBookingHasBookedInStore",
                "sort": "number_booking",
                "orderable": true,
                "defaultContent": noDataHtml,
                "class": 'text-center'
            },
            {
                "data": "email",
                "orderable": false,
                "defaultContent": noDataHtml,
                "class": 'text-center email'
            },
            {
                "data": "phone",
                "orderable": false,
                "defaultContent": noDataHtml,
                "class": 'text-center phone'
            },
            {
                "data": "gender",
                "orderable": false,
                "defaultContent": noDataHtml,
                "class": 'text-center gender'
            },
            {
                "data": "whoCreatedName",
                "orderable": false,
                "defaultContent": noDataHtml,
                "class": 'text-center'
            },
            {
                "data": "createdTime",
                "sort": "created_time",
                "orderable": true,
                "defaultContent": noDataHtml,
                "class": 'text-center'
            },
            // {
            //     "data": "updatedTime",
            //     "sort": "updated_time",
            //     "orderable": true,
            //     "defaultContent": noDataHtml,
            //     "class": 'text-center'
            // },
            {
                "data": null,
                "orderable": false,
                "defaultContent": "",
                "class": 'text-center'
            }
        ],
        columnRender: [
            {
                "render": function (customer) {
                    return `<button class="btn btn-sm btn-primary edit-customer" id="customer_id_${customer.id}" type="button" data="store_auth_id_${customer.storeAuthId}">Chi tiết</button>
                                <a class="btn btn-sm btn-primary detail-customer" target="_blank" href="listBookingOfCustomer/${customer.storeAuthId}/${customer.patientCode}">Lịch sử đặt chỗ</a>`;
                // <button class="btn btn-sm btn-danger btn-delete-customer" id="customer_id_${customer.id}" type="button"data-toggle="modal" data-target="#modal_delete_customer">Xóa</button>
                },
                "targets": 8
            },
            {
                "render": function (email) {
                    if (email == null || email === "" || email === "null") {
                        return noDataHtml;
                    }
                    return email;
                },
                "targets": 3
            },
            {
                "render": function (gender) {
                    if (gender === 1) {
                        return "Nam";
                    } else if (gender === 2) {
                        return "Nữ";
                    } else {
                        return "Không xác định";
                    }
                },
                "targets": 5
            }
        ],
        drawCallbackFunction: function () {
        },
        sortField: {column: 1, typeDir: "asc"},
        url: "/api/v1/store/"+ storeAuthId + "/customer/findByStoreId",
        customFieldSearchSelector: $nameOrCodeFilter
    };

    listCustomerTable = configDataTableServerSide(configStoreCustomerTableObject, $("#customer_store_table"));

    $nameOrCodeFilter.on('keyup', function (e) {
        if (e.keyCode === 13) {
            listCustomerTable.ajax.reload();
        }
    });


    //active update customer
    let currentEditCustomerId = 0;
    $(document).on('click', ".edit-customer", function () {
        $("#modal_edit_customer").modal('show');
        currentEditCustomerId = $(this).attr("id").replace("customer_id_", '');
        let currentRow = $(this).parent().parent();
        let customerName = currentRow.find(".customer-name").text();
        let patientCode = currentRow.find(".patient-code").text();
        let email = currentRow.find(".email").text();
        let phone = currentRow.find(".phone").text();
        if (currentRow.find(".gender").text() === "Không xác định") {
            $("#gender").val(0);
        } else if (currentRow.find(".gender").text() === "Nam") {
            $("#gender").val(1);
        } else {
            $("#gender").val(2);

        }
        $("#patient_code").val(patientCode);
        $("#customer_name").val(customerName);
        if (email === "Không có dữ liệu") {
            $("#customer_email").val("");
        } else {
            $("#customer_email").val(email);
        }
        $("#customer_phone").val(phone);

        // if (!isCanEditListCustomerStoreScreen) {
        //     $("#patient_code").add("#customer_name").attr("disabled", true);
        // }
    });

    let currentCustomerId = 0;
    $(document).on('click', ".btn-delete-customer", function () {
        $("#modal_delete_customer").modal('show');
        currentCustomerId = $(this).attr("id").replace("customer_id_", '');

    });
    $("#btn_submit_delete").click(function () {
        $.ajax({
            type: "POST",
            url: "/api/v1/store/" + storeAuthId + "/customer/delete/" + currentCustomerId,
        }).then(function (response) {
            if (response.status.code === 200) {
                window.alert.show("success", "Thành công", 3000);
                setTimeout(function () {
                    $("#modal_delete_customer").modal('hide');
                    listCustomerTable.ajax.reload();
                }, 1000);
            } else {
                window.alert.show("error", "Đã có lỗi xảy ra, vui lòng thử lại", 3000);
            }
        })
    });


    $("#edit_customer_form").validate({
        errorElement: "p",
        errorClass: "error-message",
        errorPlacement: function (error, element) {
            error.insertBefore(element);
        },
        ignore: [],
        rules: {
            customer_name: {
                notEmpty: true,
            },
            patient_code: {
                required: true,
                maxlength: 7,
                min: 1000000
            },
            customer_phone: {
                onlyNumber: true,
                notEmpty: true,
                // required: true
            }
        }
    });

    $.validator.addMethod('notEmpty', function (value) {
        return value !== null && value !== undefined && value.trim() !== "";
    }, 'Vui lòng nhập thông tin');
    $.validator.addMethod('onlyNumber', function (value) {
        let regExp = /^\d+$/;
        return regExp.test(value)
    }, 'Vui lòng chỉ nhập số');

    $("#btn_submit_customer_form").click(function () {
        let customer = {
            id: currentEditCustomerId,
            name: $("#customer_name").val(),
            patientCode: $("#patient_code").val(),
            email: $("#customer_email").val(),
            phone: $("#customer_phone").val(),
            gender: $("#gender").val()
        }
        if ($("#edit_customer_form").valid()) {
            $.ajax({
                type: "POST",
                data: JSON.stringify(customer),
                processData: false,
                contentType: 'application/json',
                url: "/api/v1/store/"+ storeAuthId +"/customer/updateCustomer",
                beforeSend: function () {
                    window.loader.show();
                }
            }).then(function (response) {
                $("#modal_edit_customer").modal('hide');
                window.loader.hide();
                listCustomerTable.ajax.reload();
                let code = response.status.code;
                if (code === 203) {
                    window.alert.show("success", "Thành công", 2000);
                } else if (code === 411) {
                    window.alert.show("error", "Mã khách hàng đã tồn tại", 2000);
                } else {
                    window.alert.show("error", "Đã có lỗi xảy ra, vui lòng thử lại", 2000);
                }
            })
        }
    });
});
