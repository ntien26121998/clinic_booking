let categoryBookingTable;
let form_add_category;
$(document).ready(function () {
    let storeId = $("#store_id").val();
    let serviceDepartmentId = $("#service_department_id").val();

    form_add_category = new Vue({
        el: "#category_booking_management",
        data: {
            id: null,
            categoryName: '',
            numberFrameConsecutiveAutoFill: 0,
            moneyExcludeTax: 0,
            // position: 0,
            isActive: false,
            isUpdate: false,
            deleteId: '',
            validCategoryName: true,
            invalidMessage: "このフィールドは必須です。",
            listCategoryBooking: [],
            currentSelectedToDeleteId: '',
            currentSelectedToUpdateId: '',
            isCanEditWorkingTimeConfigScreen: '',
        },

        methods: {
            loadListCategoryBookingFromDB() {
                let self = this;
                jQuery.get("/api/v1/service_department/" + serviceDepartmentId + "/categoryBooking/listCategoryBooking", function (response) {
                    let listCategoryBooking = response;
                    self.listCategoryBooking = [];
                    if (listCategoryBooking != null) {
                        self.listCategoryBooking = listCategoryBooking;
                    }
                });
            },
            submitFormCategory(categoryId) {
                let self = this;
                let newCategory = {
                    "categoryName": self.categoryName,
                    "numberFrameConsecutiveAutoFill": self.numberFrameConsecutiveAutoFill,
                    "moneyExcludeTax": self.moneyExcludeTax,
                    "id": self.currentSelectedToUpdateId ? self.id : "0",
                    // "position": self.position,
                    "active": self.isActive
                };
                if ($("#form_add_category").valid()) {
                    $.ajax({
                        type: "POST",
                        url: "/api/v1/service_department/" + serviceDepartmentId + "/categoryBooking/saveCategory",
                        data: JSON.stringify(newCategory),
                        contentType: "application/json; charset=utf-8",
                        processData: false,
                        beforeSend: function () {
                            $("#btn_submit_add_category").attr('disabled', true);
                        }
                    }).then(function (response) {
                        $("#btn_submit_add_category").prop('disabled', false);
                        if (response.status.code === 202) {
                            window.alert.show("success", "Thêm mới thành công", 2000);
                            $("#modal_add_category").modal("hide");
                            self.loadListCategoryBookingFromDB();
                        } else if (response.status.code === 4) {
                            window.alert.show("error", "Đã có lỗi xảy ra, vui lòng thử lại", 3000);
                        } else {
                            window.alert.show("error", "Đã có lỗi xảy ra, vui lòng thử lại", 3000);
                        }
                    })
                }
            },
            getDetailCategory(categoryId) {
                let self = this;
                // self.resetForm();
                self.currentSelectedToUpdateId = categoryId;
                jQuery.ajax("/api/v1/service_department/" + serviceDepartmentId + "/categoryBooking/" + categoryId, {}).then(function (response) {
                    let categoryBooking = response.data;
                    self.id = categoryBooking.id;
                    self.categoryName = categoryBooking.categoryName;
                    self.numberFrameConsecutiveAutoFill = categoryBooking.numberFrameConsecutiveAutoFill;
                    self.moneyExcludeTax = categoryBooking.moneyExcludeTax;
                    self.isActive = categoryBooking.active;
                    $("#modal_add_category").modal('show');
                });
            },
            confirmDelete(categoryId) {
                let self = this;
                self.currentSelectedToDeleteId = categoryId;
                $("#modal_delete_category").modal("show");
            },
            submitDelete() {
                let self = this;
                $.ajax({
                    type: "POST",
                    url: "/api/v1/service_department/" + serviceDepartmentId + "/categoryBooking/delete/" + self.currentSelectedToDeleteId,
                }).then(function (response) {
                    if (response.status.code === 200) {
                        window.alert.show("success", "Xóa thành công", 2000);
                        $("#modal_delete_category").modal('hide');
                        self.loadListCategoryBookingFromDB();
                    } else {
                        window.alert.show("error", response.data, 2000);
                    }
                })
            },
            deleteAll() {
                let self = this;
                $.ajax({
                    type: "POST",
                    url: "/api/v1/service_department/" + serviceDepartmentId + "/categoryBooking/deleteAll",
                }).then(function (response) {
                    if (response.status.code === 200) {
                        window.alert.show("success", "Xóa thành công。", 2000);
                        $("#modal_delete_all_category").modal('hide');
                        self.loadListCategoryBookingFromDB();
                    } else {
                        window.alert.show("error", response.data, 2000);
                    }
                })
            },
            resetForm: function () {
                let self = this;
                self.validCategoryName = true;
                self.categoryName = '';
                self.numberFrameConsecutiveAutoFill = 0;
                self.moneyExcludeTax = 0;
                self.isActive = false;
                self.id = null;
            },
            changeStatus(categoryId) {
                let self = this;
                $.ajax({
                    type: "GET",
                    url: "/api/v1/service_department/" + serviceDepartmentId + "/categoryBooking/changeStatus/" + categoryId,
                }).then(function (response) {
                    if (response.status.code === 200) {
                        window.alert.show("success", "Thay đổi thành công", 2000);
                        self.loadListCategoryBookingFromDB();
                    } else {
                        window.alert.show("error", "Thay đổi không thành công", 2000);
                    }
                })
            },
            formatMoney(money) {
                return money.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
            },
        },
        mounted() {
            let self = this;
            self.loadListCategoryBookingFromDB();
            self.isCanEditWorkingTimeConfigScreen = $("#can_edit_working_time_config_screen").val();

            $("#form_add_category").validate({
                errorElement: "p",
                errorClass: "error-message",
                errorPlacement: function (error, element) {
                    error.insertAfter(element);
                },
                ignore: [],
                rules: {
                    categoryName: {
                        required: true,
                        validateCategoryName: true
                    },
                }

            });
            $.validator.addMethod("validateCategoryName", function (value) {
                return !(value === null || value === undefined || value.trim() === "");
            }, "Trường này là bắt buộc");

            $(document).on("click", "#add_new_category_booking", function () {
                form_add_category.resetForm();
                self.resetForm;
            });

            $(document).on("click", ".btn-save-category", function () {
                let categoryId = $(this).attr("id");
                self.submitFormCategory(categoryId);
            });

            $(document).on("click", ".btn-edit", function () {
                // self.resetForm;
                let categoryId = $(this).attr("id").replace("btn_edit_", "");
                self.getDetailCategory(categoryId);
            });

            $(document).on("click", ".btn-delete-category", function () {
                self.deleteId = $(this).attr('id').replace("btn_delete_", "");
            });

            $("#btn_submit_delete").on('click', function () {
                self.submitDelete();
            });

            $("#btn_submit_delete_all").on('click', function () {
                self.deleteAll();
            })

            $(document).on("click", ".slider", function () {
                let categoryId = $(this).attr('id');
                self.changeStatus(categoryId);
            });
        }

    })

});



