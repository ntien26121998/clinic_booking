$(document).ready(function () {
    let storeId = $("#store_id").val();
    let serviceDepartmentId = $("#service_department_id").val();
    console.log(serviceDepartmentId)
    new Vue({
        el: "#time_setting_div",
        data: {
            listTimeSetting: [],
            currentSelectedToDeleteId: ''
        },
        methods: {
            createNewTimeSetting() {
                let newTimeSetting = {
                    id: '',
                    startWorkingTime: '00:00',
                    endWorkingTime: '00:00',
                    startBreakTime: '00:00',
                    endBreakTime: '00:00',
                    startActiveRange: moment(new Date()).format("YYYY/MM/DD"),
                    endActiveRange: '9999/12/31',
                    // typeConfig: "COMMON_CONFIG"
                };
                this.listTimeSetting.push(newTimeSetting);
            },
            loadListTimeSettingFromDB() {
                let self = this;
                jQuery.get("/api/v1/web/" + serviceDepartmentId + "/time_setting/listTimeSetting", function (response) {
                    let listTimeSetting = response.data;
                    if (listTimeSetting != null) {
                        self.listTimeSetting = listTimeSetting;
                    }
                });
            },
            submitForm(indexOfForm) {
                let self = this;
                if ($("#setting_form_" + indexOfForm).valid()) {
                    $.ajax({
                        type: "POST",
                        url: "/api/v1/web/" + serviceDepartmentId + "/time_setting/saveTimeSetting",
                        data: JSON.stringify(self.listTimeSetting[indexOfForm]),
                        contentType: "application/json",
                        beforeSend: function () {
                            window.loader.show();
                            $(".btn-save-time-setting").each(function () {
                                $(this).prop("disabled", true);
                            });
                        }
                    }).then(function (response) {
                        $(".btn-save-time-setting").each(function () {
                            $(this).prop("disabled", false);
                        });
                        window.loader.hide();
                        handleResponseOnSaving(response.status.code, self.loadListTimeSettingFromDB);
                    });
                }
            },
            confirmDelete(indexOfForm) {
                let self = this;
                let timeSetting = self.listTimeSetting[indexOfForm];
                if (timeSetting.id === "") {
                    self.listTimeSetting.splice(indexOfForm, 1);
                } else {
                    self.currentSelectedToDeleteId = timeSetting.id;
                    $("#modal_delete_time_setting").modal('show');
                }
            },
            submitDelete() {
                let self = this;
                if(self.currentSelectedToDeleteId === ''){
                    return;
                }
                $.ajax({
                    type: "POST",
                    url: "/api/v1/web/" + serviceDepartmentId + "/time_setting/deleteTimeSetting?timeSettingId=" + self.currentSelectedToDeleteId,
                    beforeSend: function () {
                        window.loader.show();
                    },
                    success: function (response) {
                        $('#modal_delete_time_setting').modal('hide');
                        window.loader.hide();
                        handleResponseOnDeleting(response.status.code, self.loadListTimeSettingFromDB);
                        self.currentSelectedToDeleteId = '';
                    }
                });
            }
        },
        mounted() {
            let self = this;
            self.loadListTimeSettingFromDB();

        },
        updated() {
            let self = this;

            $(".form-time-setting").each(function () {
                // attach to all form elements on page

                let $currentForm = $(this);
                let indexOfForm = $(this).attr("id").replace("setting_form_", "");
                let currentTimeSetting = self.listTimeSetting[indexOfForm];

                let $startWorkingTime = $("#start_working_time_" + indexOfForm);
                let $startBreakTime = $("#start_break_time_" + indexOfForm);
                let $endBreakTime = $("#end_break_time_" + indexOfForm);
                let $endWorkingTime = $("#end_working_time_" + indexOfForm);
                let $startActiveRange = $("#start_active_range_" + indexOfForm);
                let $endActiveRange = $("#end_active_range_" + indexOfForm);

                configDateShowOnlyTime("start_working_time_" + indexOfForm);
                configDateShowOnlyTime("start_break_time_" + indexOfForm);
                configDateShowOnlyTime("end_break_time_" + indexOfForm);
                configDateShowOnlyTime("end_working_time_" + indexOfForm);

                configDateShowOnlyDay("start_active_range_" + indexOfForm);
                configDateShowOnlyDay("end_active_range_" + indexOfForm);

                $startWorkingTime.val(currentTimeSetting.startWorkingTime);
                $startBreakTime.val(currentTimeSetting.startBreakTime);
                $endBreakTime.val(currentTimeSetting.endBreakTime);
                $endWorkingTime.val(currentTimeSetting.endWorkingTime);
                $startActiveRange.val(currentTimeSetting.startActiveRange);
                $endActiveRange.val(currentTimeSetting.endActiveRange.includes("9999/12/31") ? "" : currentTimeSetting.endActiveRange);

                $startActiveRange.on('hide.daterangepicker', function () {
                    currentTimeSetting.startActiveRange = $(this).val();
                    $currentForm.valid();
                });

                $endActiveRange.on('hide.daterangepicker', function () {
                    currentTimeSetting.endActiveRange = $(this).val();
                    $currentForm.valid();
                }).on('cancel.daterangepicker', function() {
                    $endActiveRange.val('');
                    currentTimeSetting.endActiveRange = "9999/12/31";
                });

                $startWorkingTime.on('hide.daterangepicker', function () {
                    currentTimeSetting.startWorkingTime = $(this).val();
                    $currentForm.valid();
                });

                $startBreakTime.on('hide.daterangepicker', function () {
                    currentTimeSetting.startBreakTime = $(this).val();
                    $currentForm.valid();
                });

                $endBreakTime.on('hide.daterangepicker', function () {
                    currentTimeSetting.endBreakTime = $(this).val();
                    $currentForm.valid();
                });

                $endWorkingTime.on('hide.daterangepicker', function () {
                    currentTimeSetting.endWorkingTime = $(this).val();
                    $currentForm.valid();
                });

                $currentForm.validate({
                    errorElement: "p",
                    errorClass: "error-message",
                    errorPlacement: function (error, element) {
                        error.insertAfter(element);
                    },
                    ignore: [],
                    rules: {
                        start_working_time: {
                            required: true,
                            validateTime: true
                        },
                        end_working_time: {
                            required: true,
                            validateTime: true
                        },
                        start_break_time: {
                            required: true,
                            validateTime: true
                        },
                        end_break_time: {
                            required: true,
                            validateTime: true
                        },
                        start_active_range: {
                            required: true,
                            validateActiveTimeNotOverlapForTimeSetting: true,
                            validateDateRangeForTimeSetting: true,
                        },
                        end_active_range: {
                            validateActiveTimeNotOverlapForTimeSetting: true,
                            validateDateRangeForTimeSetting: true,
                        }
                    },
                    onfocusout: function (element) {
                        let $currentElement = $(element);
                        let arrayOfTimeInputSelector = [$startWorkingTime, $startBreakTime, $endBreakTime, $endWorkingTime];
                        if (arrayOfTimeInputSelector.indexOf($currentElement) > -1) {
                            $startWorkingTime.add($startBreakTime).add($endBreakTime).add($endWorkingTime).valid();
                        }
                    }
                });

                $.validator.addMethod('validateTime', function (value, element) {
                    let currentTimeSetting = self.listTimeSetting[$(element).attr('data')];
                    return currentTimeSetting.startWorkingTime < currentTimeSetting.startBreakTime
                        && currentTimeSetting.startBreakTime < currentTimeSetting.endBreakTime
                        && currentTimeSetting.endBreakTime < currentTimeSetting.endWorkingTime;
                }, "Thời gian bắt đầu phải trước thời gian kết thúc và bắt đầu thời gian giải lao");

                $.validator.addMethod('validateDateRangeForTimeSetting', function (value, element) {
                    let currentTimeSetting = self.listTimeSetting[$(element).attr('data')];
                    let startRangeFromFormToTime = new Date(currentTimeSetting.startActiveRange).getTime();
                    let endRangeFromFormToTime = new Date(currentTimeSetting.endActiveRange).getTime();
                    return startRangeFromFormToTime < endRangeFromFormToTime;
                }, "Ngày kết thúc không được trước ngày bắt đầu");

                $.validator.addMethod('validateActiveTimeNotOverlapForTimeSetting', function (value, element) {
                    let currentFormIndex = $(element).attr('data');
                    let currentTimeSetting = self.listTimeSetting[currentFormIndex];
                    return validateDateOverlapMethod(currentTimeSetting, self.listTimeSetting, currentFormIndex);
                }, "Ngày tháng không được trùng lặp");
            });
        },
    })
});

function validateDateOverlapMethod(currentSetting, listObjectSetting, indexOfForm) {
    let startRangeFromForm = currentSetting.startActiveRange;
    let endDateFromForm = currentSetting.endActiveRange;
    let isOverlaps = false;
    for (let i = 0; i < listObjectSetting.length; i++) {
        let objectSetting = listObjectSetting[i];
        if (i === Number.parseInt(indexOfForm) || objectSetting.id === '') {
            continue;
        }

        isOverlaps = check2ActiveRangeOverlap(startRangeFromForm, endDateFromForm, objectSetting.startActiveRange, objectSetting.endActiveRange);
        if (isOverlaps) break;
    }
    return !isOverlaps;
}

function check2ActiveRangeOverlap(startRangeFromForm, endRangeFromForm, startRangeFromEntity, endRangeFromEntity) {
    let startRangeFromFormToTime = new Date(startRangeFromForm).getTime();
    let endRangeFromFormToTime = new Date(endRangeFromForm).getTime();
    let startRangeFromEntityToTime = new Date(startRangeFromEntity).getTime();
    let endRangeFromEntityToTime = new Date(endRangeFromEntity).getTime();

    if (startRangeFromFormToTime <= startRangeFromEntityToTime && startRangeFromEntityToTime <= endRangeFromFormToTime) return true; // RangeFromEntity starts in RangeFromForm
    if (startRangeFromFormToTime <= endRangeFromEntityToTime && endRangeFromEntityToTime <= endRangeFromFormToTime) return true; // RangeFromEntity ends in RangeFromForm
    if (startRangeFromEntityToTime < startRangeFromFormToTime && endRangeFromFormToTime < endRangeFromEntityToTime) return true; // RangeFromForm in RangeFromEntity
    return false;
}

let handleResponseOnSaving = function (code, callback) {
    switch (code) {
        case 2000:
        case 203:
            window.alert.show("success", "Cập nhật cấu hình thành công", 2000);
            break;
        case 202:
            window.alert.show("success", "Cấu hình thành công", 2000);
            callback();
            break;
        case 402:
            window.alert.show("error", "Thời gian không hợp lệ", 3000);
            break;
        case 403:
            window.alert.show("error", "Thời gian bị trùng lên nhau", 3000);
            break;
        default:
            window.alert.show("error", "Có lỗi xảy ra, vui lòng kiểm tra lại thông tin", 3000);
            break;
    }
};

let handleResponseOnDeleting = function (code, callback) {
    if (code === 201) {
        window.alert.show("success", "Xóa thành công", 2000);
        callback();
    } else {
        window.alert.show("error", "Có lỗi xảy ra!", 3000);
    }
};
