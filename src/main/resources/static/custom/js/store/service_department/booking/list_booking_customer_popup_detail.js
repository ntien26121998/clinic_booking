let TABLE_BOOKING_CUSTOMER_POPUP = {};
$(document).ready(function () {

    let table;

    let getListBookingOfCustomer = function (requestData, renderFunction, customerId, storeAuthId) {
        let sortField = columnDefinitions[requestData.order[0].column].sort;
        let sortDir = requestData.order[0].dir;
        let params = {
            "page": (requestData.start / requestData.length) + 1,
            "size": requestData.length,
            "sortField": sortField,
            "sortDir": sortDir
        };
        jQuery.get("/api/v1/store/" + storeAuthId + "/customer/getListBookingByCustomerIdForPopupBooking?customerId=" + customerId, params, function (response) {
            let content = {
                "draw": requestData.draw,
                "recordsTotal": response.data.totalElements,
                "recordsFiltered": response.data.totalElements,
                "data": response.data.content
            };
            renderFunction(content);
        });
    };

    let columnDefinitions = [
        {"data": "bookingTime", sort:"booking_time", "orderable": true, "defaultContent": noDataHtml, "class": 'text-center'},
        {"data": "timeStartToEnd", "orderable": false, "defaultContent": noDataHtml, "class": 'text-center'},
        {"data": "numberFrameConsecutive", sort:"number_frame_consecutive", "orderable": true, "defaultContent": noDataHtml, "class": 'text-center'},
        {"data": "listCategory", sort:"", "orderable": false, "defaultContent": noDataHtml, "class": 'text-center'},
        {"data": "createdTime", sort:"created_time", "orderable": true, "defaultContent": noDataHtml, "class": 'text-center'},
    ];

    TABLE_BOOKING_CUSTOMER_POPUP.loadTable = function (customerId, storeAuthId) {
        if(table) {
            table.clear().destroy();
        }

        table = $("#table_day_future").DataTable({
            "language": {
                "url": "/libs/datatable/js/vi.json"
            },
            "lengthMenu": [
                [10, 20, 50],
                [10, 20, 50]
            ],

            "searching": false,
            "order": [[4, "desc"]],
            "pagingType": "full_numbers",
            "serverSide": true,
            "columns": columnDefinitions,
            "ajax": function (data, callback) {
                getListBookingOfCustomer(data, callback, customerId, storeAuthId);
            },
            "initComplete": function () {
                $("#table_day_future").wrap("<div class='table-responsive'></div>");
            },
            columnDefs: []
        });
    }
});
