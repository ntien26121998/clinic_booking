$(document).ready(function () {

    let storeAuthId = $("#store_auth_id").val();
    let $formSaveServiceDepartment = $("#form_save_service_department");
    let $btnSubmit = $("#btn_submit");
    let $serviceDepartmentNameInput = $("#service_department_name");
    let $serviceDepartmentDescriptionInput = $("#service_department_description");
    let $modalAddServiceDepartment = $("#modal_add_service_department");
    let $typeServiceDepartmentSelect =  $("#type_service");
    let $serviceDepartmentIdInput = $("#service_department_id");


    $("#btn_add_service_department").click(function () {
        resetForm();
    });

    $btnSubmit.click(function (e) {
        if($formSaveServiceDepartment.valid()) {
            submitForm(e);
        }
    });

    initCkEditor('service_department_description');

    let rules = {
        service_department_name: {
            notEmpty: true,
            maxlength: 500
        },
        description: {
            required_ckEditor: true
        },
        image_service_department: {
            required: {
                depends: function () {
                    return !imageHandle.isHasImage();
                }
            }
        }

    };

    let messages = {
        image_service_department: {
            required: "Vui lòng chọn 1 ảnh đại diện"
        }
    };

    let formValidator = configValidate($formSaveServiceDepartment, rules, [], messages);

    let submitForm = function (e) {
        e.preventDefault();
        let formData = new FormData();
        formData.append("name", $serviceDepartmentNameInput.val());
        formData.append('id', $serviceDepartmentIdInput.val());
        formData.append('serviceTypeId', $typeServiceDepartmentSelect.val());
        formData.append('description', CKEDITOR.instances['service_department_description'].getData());

        if ($(".file-input")[0].files.length !== 0) {
            formData.append("image", $("#image_service_department")[0].files[0]);
        }
        $.ajax({
            type: "POST",
            url: "/api/v1/store/" + storeAuthId + "/service_department/save",
            data: formData,
            beforeSend: function () {
                window.loader.show();
            },
            processData: false,
            contentType: false,
        }).done(function (response) {
            window.loader.hide();
            if(response.status.code === 202) {
                $modalAddServiceDepartment.modal("hide");
                window.alert.show("success", "Thành công", "1500");
                setTimeout(function (){
                    serviceDepartmentTable.ajax.reload();
                }, 1000);
            } else {
                window.alert.show("error", "Có lỗi xảy ra. Vui lòng thử lại !", "1500");
            }
        }).fail(function (response) {
            saveObjectResponseHandleOnFail(response, $modalAddServiceDepartment)
        });
    };

    function resetForm(){
        $serviceDepartmentNameInput.add($serviceDepartmentIdInput).val("");
        CKEDITOR.instances['service_department_description'].setData("");
        $('.remove-image-icon').click();
        formValidator.reset();
    }

    CKEDITOR.instances['service_department_description'].on( 'key', function() {
        setTimeout(function(){
            $formSaveServiceDepartment.valid();
        }, 5)
    });

});
