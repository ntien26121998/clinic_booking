$(document).ready(function () {
    let serviceDepartmentId = $('#service_department_id').val();
    let currentDeleteSlotFrame;

    //  button remove(decrease) slot click
    $(document).on("click",".btn-remove-slot",function() {
        currentDeleteSlotFrame = $(this).attr("id").split("_")[0];
        console.log(currentDeleteSlotFrame);
    });

    // submit remove
    $(document).on('click', '#btn_submit_delete_slot', function(){
        submitChangeSlot("decrease", currentDeleteSlotFrame);
    });

    // add new slot
    $(document).on("click",".btn-add-slot",function(e) {
        e.preventDefault();
        $(this).prop("disabled", true);
        let frame = $(this).attr("id").split("_")[0];
        if(isCurrentTimeBeforeFrameOfBookingTime(frame, $('#choose_active_day').val() )){
            return;
        }
        submitChangeSlot("increase", frame);
        setTimeout(function () {
            $(this).prop("disabled", false);
        }, 1100);
    });

    function submitChangeSlot(typeChange, frame){
        let formData = new FormData();
        formData.append("activeDay", $('#choose_active_day').val());
        formData.append("frame", frame);
        formData.append("typeChange", typeChange);
        $.ajax({
            type: "POST",
            url: "/api/v1/service_department/" + serviceDepartmentId + "/working_time_config/changeNumberSlotOfFrame",
            data: formData,
            processData: false,
            contentType: false,
            headers: {
                "Content-Type": undefined
            },
            beforeSend: function () {
                window.loader.show();
            }
        }).done(function (response) {
            window.loader.hide();
            $('#modal_delete_slot').modal('hide');
            if (response === "SUCCESS") {
                window.alert.show("success", "Thành công", 2000);

            } else {
                window.alert.show("error", "Có lỗi xảy ra. Vui lòng thử lại sau!", 2000);
            }
            bookingManagement.reloadBookingTable();
        });
    }
});
