$(document).ready(function () {
    let serviceDepartmentId = $("#service_department_id").val();

    Vue.component("comment-div", {
        props: {
            bookingId: {
                required: true,
                type: Number
            },
            customerId: {
                required: true,
                type: Number
            },
            listComment: {
                required: true,
                type: Array
            },
            isFromScreenCustomer: {
                required: true,
            }
        },
        template: `    <div id="update_comment_div">
        <div class="red-div">
            <h6 class="pull-left title-div">Nhận xét/Lưu ý</h6>
        </div>
        <div style="padding-top: 15px;">
            <div class="comment-content">
                <ul class="comment" v-for="comment in listComment">
                    <li class="left clearfix" > 
                        <div class="comment-body clearfix">
                            <div class="header">
                                <strong class="primary-font" style="font-size: 13px">{{comment.updatedByName}}
                                </strong>
                                <small class="pull-right text-muted">
                                    <span class="glyphicon glyphicon-time">{{comment.createdTime}} </span>
<!--                                    <span class="glyphicon glyphicon-time">BookingId: {{comment.bookingId}} </span>-->
                                </small>
                            </div>

                            <div v-if="comment.typeInput === 'TEXT'">
                                <i :id="'content_comment_' + comment.id" style="text-align: left; font-size: 13px" :class="isEditComment(comment.id) ? 'hidden' : ''">
                                    {{comment.content}}  
                                </i>
                                <textarea class="form-control" 
                                          :class="isEditComment(comment.id) ? 'textarea-comment-edit-' + comment.id : 'hidden textarea-comment-edit-' + comment.id"
                                          style="text-align: left; font-size: 13px !important; margin-bottom: 5px;">{{comment.content}}</textarea>
                                <div class="pull-right" v-if="isFromScreenCustomer !== true">
                                    <button class="btn btn-danger btn-sm btn-edit-comment" :class="isEditComment(comment.id) ? 'hidden' : ''" 
                                               style="margin-right: 5px" @click="editComment($event, comment.id)" type="button">
                                        Chỉnh sửa
                                    </button>
                                    <button class="btn btn-danger btn-sm btn-submit-update-comment" @click="updateComment($event, comment.id)" 
                                                 style="margin-right: 5px" :class="isEditComment(comment.id) ? '' : 'hidden'" type="button">
                                        Cập nhật
                                    </button>
                                    <button class="btn btn-danger btn-sm btn-delete-comment" type="button" 
                                            style="margin-right: 5px"
                                                   :class="isEditComment(comment.id) ? '' : 'hidden'" @click="deleteComment($event, comment.id)" >
<!--                                                :class="isEditComment(comment.id) ? 'hidden' : ''" @click="deleteComment($event, comment.id)" >-->
                                                Xóa
                                    </button>
                                    <button class="btn btn-danger btn-sm btn-cancel-edit-comment"
                                                :class="isEditComment(comment.id) ? '' : 'hidden'" @click="editCommentId = 0" type="button">
                                                Hủy bỏ
                                    </button>
                                </div>
                            </div>

                            <div v-if="comment.typeInput == 'IMAGE'">
                               <a href=""><img class="image-comment" :src="comment.fileUrl" class="image-comment"></a>
                                <button class="btn btn-danger btn-sm pull-right btn-delete-comment" style="margin-right:5px" 
                                @click="deleteComment($event, comment.id)" type="button">Xóa</button>
                            </div>
                            <div v-if="comment.typeInput == 'PDF'">
                                <a id="pdf_link" :href="comment.fileUrl">
                                    <i class="fa fa-file-pdf-o pdf-file-icon" style="color: red; font-size: 20px;"></i>
                                    <span style="font-size: 13px">{{comment.pdfFileName}}</span>
                                </a>
                                <button class="btn btn-danger btn-sm pull-right btn-delete-comment" style="margin-right:5px" 
                                @click="deleteComment($event, comment.id)" type="button">Xóa</button>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
            <div class="comment-input" style="margin: 0 10px" v-if="isFromScreenCustomer !== true">
                <textarea style="margin-bottom: 15px " rows="4" class="form-control" placeholder="Nhận xét hoặc lưu ý...." v-model="comment"></textarea>
                <div class="form-group" style="display: inline-flex">
                    <div class="" style="margin-right: 10px" >
                        <i class="fa fa-file-image-o" id="image_icon"></i>
                        <label style="color: blue; border-bottom: solid 1px blue; cursor: pointer;" id="label_add_img">Gắn tệp ảnh</label>
                        <input type="file" class="form-control pull-left" id="attach_file_booking" accept="image/*" multiple hidden>
                    </div>
                    <div class="">
                        <i class="fa fa-file-pdf-o fa-lg" id="pdf_icon"></i>
                        <label style="color: blue; border-bottom: solid 1px blue; cursor: pointer;" id="label_add_pdf">Gắn tệp PDF</label>
                        <input type="file" class="form-control pull-left" id="attach_file_pdf_booking" accept="application/pdf" multiple hidden>
                    </div>
                </div>
                
                <div class="show-image-preview" :class="isHasListImage ? '' : 'hidden'" style="margin-bottom: 5px">
                    <div class="render-image-div">
                    </div>
                    <i class="fa fa-times-circle" id="remove_all_images" style="font-size: 24px" @click="removeAllImagesRender()"></i>
                </div>
                <div class="show-pdf-preview" :class="isHasListPdf ? '' : 'hidden'">
                    <div class="render-pdf-div" style="cursor: pointer; border-radius: 5px; margin-right: 5px !important;">
                    </div>
                    <i class="fa fa-times-circle" id="remove_all_pdf" style="font-size: 24px" @click="removeAllPdfRender()"></i>
                </div>
                <button class="btn btn-primary btn-sm pull-right" id="btn-submit-send" @click="sendComment($event)" type="button"  >Lưu tệp/bình luận
                </button>
            </div>
        </div>
        <div class="modal fade" id="image_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-md"  style="min-width: 800px; min-height: 500px">
                <div class="modal-content">     
                    <div class="modal-body">
                        <img src="" class="image_preview img-fluid" style="width: 100%;" >
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-sm btn-light" role="button" @click="hideModalImage($event)">Hủy bỏ</button>
                         <button type="button" class="btn btn-sm btn-primary" role="button" @click="downloadImage($event)">Tải xuống</button>
                    </div>
                </div>
            </div>
        </div>
      </div>
  </div>
</div>
    </div>`,

        data: function () {
            return {
                comment: '',
                listFileAttachment: [],
                isHasListImage: false,
                isHasListPdf: false,
                editCommentId: 0,
                pdfFileName: '',
                isCanEditBookingScreen: ''
            }
        },
        methods: {
            isEditComment(commentId) {
                return this.editCommentId === commentId;
            },
            isNoCommentAndFile() {
                let dontHaveComment = (this.comment == null || this.comment.trim() === "");
                let dontHaveFile = this.listFileAttachment.length < 1;
                return dontHaveComment && dontHaveFile
            },
            sendComment(event) {
                let storeId = $('#store_id').val();
                let self = this;
                event.preventDefault();
                if (self.isNoCommentAndFile()) {
                    window.alert.show("error", "Vui lòng thêm tệp và nhận xét", 2000);
                    return;
                }
                let formData = new FormData();
                formData.append('comment', self.comment);
                formData.append('bookingId', self.bookingId);
                formData.append('customerId', self.customerId);
                formData.append('storeId', storeId);
                formData.append('serviceDepartmentId', serviceDepartmentId);
                let numberOfFiles = self.listFileAttachment.length;
                for (let i = 0; i < numberOfFiles; i++) {
                    formData.append("listAttachFile", self.listFileAttachment[i]);
                }
                $.ajax({
                    type: "POST",
                    url: "/api/v1/service_department/" + serviceDepartmentId + "/booking/saveCommentAndFile",
                    data: formData,
                    processData: false,
                    contentType: false,
                    beforeSend: function () {
                        window.loader.show();
                    },
                }).then(function (response) {
                    window.loader.hide();
                    switch (response.status.code) {
                        case 200:
                            window.alert.show("success", "Thành công", 2000);
                            self.loadListComment();
                            self.comment = '';
                            self.removeAllImagesRender();
                            self.removeAllPdfRender();
                            break;
                        case 100:
                            window.alert.show("error", "Đã có lỗi xảy ra, không tìm thấy lịch đặt", 2000);
                            break;
                        case 404:
                            window.alert.show("error", "Số lượng tệp tải lên quá 10 tệp", 2000);
                            break;
                        case 4:
                            window.alert.show("error", "Đã có lỗi xảy ra, vui lòng thử lại", 2000);
                            break;
                        default:
                            window.alert.show("error", "Đã có lỗi xảy ra, vui lòng thử lại", 2000);
                            break;
                    }
                })
            },
            loadListComment() {
                let self = this;
                let storeId = $('#store_id').val();
                jQuery.get("/api/v1/service_department/" + serviceDepartmentId + "/booking/commentBooking?customerId=" + self.customerId, function (response) {
                    self.$parent.listComment = response.data;
                    self.listComment = response.data;
                })
            },
            updateComment(event, commentId) {
                let storeId = $('#store_id').val();
                event.preventDefault();
                let self = this;
                let content = $('.textarea-comment-edit-' + commentId).val();
                if (content == null || content.trim() === "") {
                    window.alert.show("error", "Vui lòng nhập nhận xét, bình luận", 2000);
                    return;
                }
                let formData = new FormData();
                formData.append('commentId', commentId);
                formData.append('bookingId', self.bookingId);
                formData.append('customerId', self.customerId);
                formData.append("content", content);
                $.ajax({
                    type: "POST",
                    url: "/api/v1/service_department/" + serviceDepartmentId + "/booking/editComment",
                    data: formData,
                    processData: false,
                    contentType: false,
                    beforeSend: function () {
                        window.loader.show();
                    },
                }).then(function (response) {
                    window.loader.hide();
                    let code = response.status.code;
                    if (code === 200) { //delete success
                        window.alert.show("success", "Thành công", 2000);
                        self.loadListComment();
                    } else {
                        window.alert.show("error", "Đã có lỗi xảy ra, vui lòng thử lại", 2000);
                    }
                    self.editCommentId = 0;
                })
            },
            deleteComment(event, commentId) {
                let storeId = $('#store_id').val();
                let self = this;
                event.preventDefault();
                let formData = new FormData();
                formData.append('commentId', commentId);
                formData.append('bookingId', self.bookingId);
                formData.append('customerId', self.customerId);
                $.ajax({
                    type: "POST",
                    url: "/api/v1/service_department/" + serviceDepartmentId + "/booking/deleteComment",
                    data: formData,
                    processData: false,
                    contentType: false,
                    beforeSend: function () {
                        window.loader.show();
                    }
                }).then(function (response) {
                    window.loader.hide();
                    let code = response.status.code;
                    if (code === 201) { //delete success
                        window.alert.show("success", "Thành công", 2000);
                        self.loadListComment();
                    } else {
                        window.alert.show("error", "Đã có lỗi xảy ra, vui lòng thử lại", 2000);
                    }
                })
            },
            renderImageAfterChoosing(input) {
                let self = this;
                let isImageFile = ['image/gif', 'image/jpeg', 'image/png'];
                let listFileAttachment = self.listFileAttachment;
                for (let i = 0; i < listFileAttachment.length; i++) {
                    let fileType = listFileAttachment[i].type;
                    if (isImageFile.includes(fileType)){
                        listFileAttachment.splice(listFileAttachment[i], 1);
                    }
                }
                // self.listFileAttachment = [];
                if (input.files) {
                    let filesAmount = input.files.length;
                    $(".render-image-div").html("");
                    for (let i = 0; i < filesAmount; i++) {
                        let imageInputFile = input.files[i];
                        self.listFileAttachment.push(imageInputFile);
                        let reader = new FileReader();
                        reader.onload = function (e) {
                            $($.parseHTML(`<img class="image-render" alt="" title="${imageInputFile.name}"> `)).attr('src', e.target.result).appendTo('.render-image-div');
                        };
                        reader.readAsDataURL(input.files[i]);
                    }
                }
                self.isHasListImage = true;
            },
            renderPdfAfterChoosing(input) {
                let self = this;
                let isPdfFile = ['application/pdf'];
                let listFileAttachment = self.listFileAttachment;
                for (let i = 0; i < listFileAttachment.length; i++) {
                    let fileType = listFileAttachment[i].type;
                    if (isPdfFile.includes(fileType)){
                        listFileAttachment.splice(listFileAttachment[i], 1);
                    }
                }
                // self.listFileAttachment = [];
                if (input.files) {
                    let filesAmount = input.files.length;
                    $(".render-pdf-div").html("");
                    for (let i = 0; i < filesAmount; i++) {
                        let pdfInputFile = input.files[i];
                        this.pdfFileName = pdfInputFile.name;
                        self.listFileAttachment.push(pdfInputFile);
                        let reader = new FileReader();
                        reader.onload = function (e) {
                            $($.parseHTML(`<a class="pdf-render" alt="" title="${pdfInputFile.name}"> ${pdfInputFile.name}</a>`)).appendTo('.render-pdf-div');
                        };
                        reader.readAsDataURL(input.files[i]);
                    }
                }
                self.isHasListPdf = true;
            },
            removeAllImagesRender() {
                this.isHasListImage = false;
                this.listFileAttachment = [];
            },
            removeAllPdfRender() {
                this.isHasListPdf = false;
                this.listFileAttachment = [];
            },
            editComment(event, commentId) {
                event.preventDefault();
                this.editCommentId = commentId
            },
            hideModalImage(event){
                event.preventDefault();
                $('#image_modal').modal('hide');
            },
            downloadImage(event){
                event.preventDefault();
                let imageUrl = $(".image_preview").attr('src');
                window.location.href = "/api/v1/service_department/" + serviceDepartmentId + "/booking/downloadImageComment?imgUrl=" + imageUrl;
            }
        },
        mounted() {
            let self = this;
            let $imageIcon = $('#image_icon');
            let $pdfIcon = $('#pdf_icon');
            let $attachFileBookingInput = $('#attach_file_booking');
            let $attachFilePdfBookingInput = $('#attach_file_pdf_booking');

            self.isCanEditBookingScreen = $("#can_edit_booking_screen").val();

            $imageIcon.add("#label_add_img").on('click', function () {
                $attachFileBookingInput.click();
            });

            $pdfIcon.add("#label_add_pdf").on('click', function () {
                $attachFilePdfBookingInput.click();
            });



            $attachFileBookingInput.on('change', function (e) {
                let fileExtension = ['png', 'jpeg', 'jpg'];
                e.preventDefault();
                if (this.files.length < 1) {
                    return;
                } // alert if not valid
                if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) === -1) {
                    $attachFileBookingInput.val(null);
                    window.alert.show("error", "Chỉ chọn tệp ảnh", 2000);
                    return;
                }
                let numberFile = $attachFileBookingInput[0].files.length;
                if (numberFile > 10) {
                    window.alert.show("error", "Số lượng tệp tải lên quá 10 tệp", 2000);
                    return;
                }
                self.renderImageAfterChoosing(this);
                $attachFileBookingInput.val(null);
            });

            $attachFilePdfBookingInput.on('change', function (e) {
                let fileExtension = ['pdf'];
                e.preventDefault();
                if (this.files.length < 1) {
                    return;
                } // alert if not valid
                if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) === -1) {
                    $attachFilePdfBookingInput.val(null);
                    window.alert.show("error", "Chỉ chọn tệp PDF", 2000);
                    return;
                }
                let numberFile = $attachFilePdfBookingInput[0].files.length;
                if (numberFile > 10) {
                    window.alert.show("error", "Số lượng tệp tải lên quá 10 tệp", 2000);
                    return;
                }
                self.renderPdfAfterChoosing(this);
                $attachFilePdfBookingInput.val(null);
            });

            $(document).on('click', '.image-comment', function (e) {
                e.preventDefault();
                let self = $(this);
                $('.image_preview').attr('src', self.attr('src'));
                $('#image_modal').modal('show');
            });
        }
    })
});
