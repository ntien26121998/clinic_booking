let MODAL_BOOKING_VUE;
$(document).ready(function () {

    let serviceDepartmentId = $("#service_department_id").val();
    let storeId = $("#store_id").val();
    MODAL_BOOKING_VUE = new Vue({
        el: "#add_booking_form",
        data: function () {
            return this.initData();
        },
        watch: {
            numberFrameConsecutive() {
                if (!this.isFromScreenCustomer) {
                    this.getTimeBookingTitleAndDeadlineText($('#choose_active_day').val(), this.frameWithoutNumberSlot);
                }
            },
            currentCustomerId() {
                if (this.currentCustomerId && this.currentCustomerId !== '') {
                    TABLE_BOOKING_CUSTOMER_POPUP.loadTable(this.currentCustomerId, storeId);
                }
            }
        },
        computed: {
            listCategoryForModalBooking() {
                if (this.id === '') {
                    return this.listAllCategoryBooking
                        .filter(category => category.active && !category.deleted);
                } else {
                    return this.listAllCategoryBooking;
                }
            },
            listCategoryHasChosen() {
                return this.listAllCategoryBooking
                    .filter(category => this.listCategoryHasChosenId.indexOf(category.id) > -1);
            },
            totalMoneyFromCategoryChosen() {
                let sumMoney = this.listCategoryHasChosen
                    .map(category => category.moneyExcludeTax)
                    .reduce((prev, curr) => prev + curr, 0);
                return 'Tổng  ' + this.formatMoney(sumMoney) + "VND";
            }
        },
        methods: {
            initData() {
                return {
                    id: '',
                    timeBookingTitle: '',
                    startTimeTitle: '',
                    endTimeTitle: ',',
                    deadlineCancellation: '',
                    title: '',
                    patientCode: '',
                    customerName: '',
                    customerPhone: '',
                    status: 'SUCCESS',
                    typeBooking: "VIA_ADMIN",
                    bookingTime: '',
                    serviceDepartmentAuthId: serviceDepartmentId,
                    note: '',
                    frame: '',
                    startTimeOfBookingFrame: '',
                    frameWithoutNumberSlot: '',
                    listComment: [],
                    deleteBookingId: '',
                    isEnableCreateNewCustomer: true,
                    isChangeInputCustomerName: false,
                    currentCustomerId: '',
                    currentBookingIdChangeNote: '',
                    currentBookingNote: '',

                    numberFrameConsecutive: 1,
                    mapNumberFrameConsecutiveCanChoose: {},
                    isFromScreenCustomer: false,
                    timeStartToEnd: '',
                    listAllCategoryBooking: [],
                    listCategoryHasChosenId: [],
                    minNumberFrameConsecutive: 1,
                    customerId: '',
                    listDayFuture: [],
                    // numberFrame: 1,
                    isCanEditBookingScreen: '',
                    maxNumberSlot: 0,
                    paycheckBookingId: ''
                }
            },
            formatMoney(money) {
                return money.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
            },
            searchCustomerByNameStr(nameStr, async) {
                let $customerName = $('#customer_name');
                if ($customerName.valid()) {
                    jQuery.ajax('/api/v1/service_department/' + serviceDepartmentId + '/booking/findCustomerByNameTypeahead' + '?nameKeySearch=' + nameStr, {
                        success: function (res) {
                            res.data.unshift(null);
                            return async(res.data);
                        }
                    });
                }

            },
            searchCustomerByPhoneStr(phoneStr, async) {
            jQuery.ajax('/api/v1/service_department/' + serviceDepartmentId + '/booking/findCustomerByPhoneTypeahead' + '?phone=' + phoneStr)
                .done(function (listCustomer) {
                    return async(listCustomer);
                })
            },
            loadListCategoryBooking(serviceDepartmentId) {
                let self = this;
                jQuery.get("/api/v1/service_department/" + serviceDepartmentId + "/categoryBooking/findForSavingBooking", function (response) {
                    self.listAllCategoryBooking = response.data;
                });
            },

            changeNumberFrameConsecutive(categoryId, numberFrameConsecutiveAutoFill) {
                if (this.listCategoryHasChosenId.indexOf(categoryId) > -1) {
                    let indexOfListCategoryHasChosen = this.listCategoryHasChosenId.indexOf(categoryId);
                    let newNumberFrameConsecutive = this.numberFrameConsecutive + numberFrameConsecutiveAutoFill;
                    if (this.mapNumberFrameConsecutiveCanChoose.arrayNumber.indexOf(newNumberFrameConsecutive) > -1) {
                        this.numberFrameConsecutive = newNumberFrameConsecutive;
                        // this.minNumberFrameConsecutive = this.minNumberFrameConsecutive + numberFrameConsecutiveAutoFill;
                    } else {
                        this.listCategoryHasChosenId.splice(indexOfListCategoryHasChosen, 1);
                        window.alert.show("error", "Không đủ số khung", 2000);
                    }
                } else {
                    if (this.numberFrameConsecutive > numberFrameConsecutiveAutoFill) {
                        this.numberFrameConsecutive = this.numberFrameConsecutive - numberFrameConsecutiveAutoFill;
                        // this.minNumberFrameConsecutive = this.minNumberFrameConsecutive - numberFrameConsecutiveAutoFill;
                    }
                }
            },

            getTextNumberFrameAutoFillForCategory(numberFrameConsecutiveAutoFill) {
                return numberFrameConsecutiveAutoFill === 0 ? 'Không' : numberFrameConsecutiveAutoFill + "Khung";
            },
            getTimeBookingTitleAndDeadlineText(chosenDayValue, frameWithNumberSlot) {
                if (this.numberFrameConsecutive !== undefined) {

                }
                let check = moment(new Date(chosenDayValue), 'YYYY/MM/DD');
                let month = check.format('M');
                let day = check.format('D');
                let year = check.format('YYYY');
                let startFrameTime = frameWithNumberSlot.split(" - ")[0];
                // let listFrameWithoutNumberSlot = TABLE_BOOKING_VUE.listFrameWithoutNumberSlot;
                let listFrameWithoutNumberSlot = bookingManagement.listFrameWithoutNumberSlot;
                let frameWithoutNumberSlot = frameWithNumberSlot.split("_")[0];
                if (this.numberFrameConsecutive !== undefined) {
                    let endFrameTime = listFrameWithoutNumberSlot.slice(listFrameWithoutNumberSlot.indexOf(frameWithoutNumberSlot))[this.numberFrameConsecutive - 1].split(" - ")[1];
                    this.endTimeTitle = endFrameTime;
                    this.timeBookingTitle = day + "/" + month + "/" + year + getDayInWeekText(chosenDayValue);
                    this.startTimeTitle = startFrameTime;
                    this.timeStartToEnd = startFrameTime + " - " + endFrameTime;
                } else {
                    this.numberFrameConsecutive = 1;
                }
            },
            getTimeBookingTitleForCustomerBookingScreen(bookingDay, timeStartToEndOfBooking) {
                let check = moment(new Date(bookingDay), 'YYYY/MM/DD');
                let month = check.format('M');
                let day = check.format('D');
                let year = check.format('YYYY');
                this.timeBookingTitle = day + "/" + month + "/" + year + getDayInWeekText(bookingDay);
                this.startTimeTitle = timeStartToEndOfBooking.split(" - ")[0];
                this.endTimeTitle = timeStartToEndOfBooking.split(" - ")[1];
            },
            getDataForCreatingBooking() {
                return {
                    id: this.id,
                    customerId: this.currentCustomerId,
                    status: this.status,
                    // typeBooking: "VIA_ADMIN",
                    bookingDate: $("#choose_active_day").val() + ' ' + this.startTimeOfBookingFrame,
                    serviceDepartmentAuthId: serviceDepartmentId,
                    storeAuthId: storeId,
                    note: this.note,
                    frame: this.frameWithoutNumberSlot,
                    numberFrameConsecutive: this.numberFrameConsecutive,
                    timeStartToEnd: this.timeStartToEnd,
                    setCategoryBookingId: this.listCategoryHasChosenId,
                }
            },
            getDataForUpdatingBooking(serviceDepartmentId) {
                return {
                    id: this.id,
                    customerId: this.currentCustomerId,
                    status: this.status,
                    // typeBooking: "VIA_ADMIN",
                    serviceDepartmentAuthId: serviceDepartmentId,
                    note: this.note,
                    numberFrameConsecutive: this.numberFrameConsecutive,
                    timeStartToEnd: this.timeStartToEnd,
                    setCategoryBookingId: this.listCategoryHasChosenId
                }
            },
            createBooking() {
                let self = this;
                if (self.currentCustomerId === '') {
                    window.alert.show("error", "Vui lòng chọn một khách hàng", 2000);
                    return;
                }
                $.ajax({
                    type: "POST",
                    url: "/api/v1/service_department/" + serviceDepartmentId + "/booking/create",
                    data: JSON.stringify(self.getDataForCreatingBooking()),
                    processData: false,
                    contentType: 'application/json',
                    beforeSend: function () {
                        window.loader.show();
                    },
                    success: function (response) {
                        $('#modal_add_booking').modal('hide');
                        window.loader.hide();
                        self.responseSuccessHandle(response);
                        // TABLE_BOOKING_VUE.loadFrameAndBooking();
                        bookingManagement.getBookingDataAndRenderTable($("#choose_active_day").val(), false);
                    }
                });
            },
            updateBooking(serviceDepartmentId) {
                let self = this;
                $.ajax({
                    type: "POST",
                    url: "/api/v1/service_department/" + serviceDepartmentId + "/booking/update",
                    data: JSON.stringify(self.getDataForUpdatingBooking(serviceDepartmentId)),
                    processData: false,
                    contentType: 'application/json',
                    beforeSend: function () {
                        window.loader.show();
                    }
                }).done(function (response) {
                    $('#modal_add_booking').modal('hide');
                    window.loader.hide();
                    self.responseSuccessHandle(response);
                    bookingManagement.getBookingDataAndRenderTable($("#choose_active_day").val(), false);
                });
            },
            submitForm() {
                let self = this;
                if (self.id === '') {
                    self.createBooking();
                } else {
                    let serviceDepartmentId = $("#service_department_id").val();
                    self.updateBooking(serviceDepartmentId);
                }
            },
            getDetailBooking(idBooking, serviceDepartmentId) {
                let self = this;
                self.isEnableCreateNewCustomer = false;

                jQuery.get("/api/v1/service_department/" + serviceDepartmentId + "/booking/" + idBooking, function (response) {
                    let bookingInfo = response.data;
                    if (bookingInfo == null) {
                        window.alert.show("error", "Có lỗi xảy ra, không tìm thấy lịch đặt", 2000);
                        return false;
                    }
                    let bookingEntity = bookingInfo.bookingEntity;
                    self.id = bookingEntity.id;
                    self.patientCode = bookingInfo.customer.patientCode;
                    self.customerName = bookingInfo.customer.name;
                    self.customerPhone = bookingInfo.customer.phone;
                    self.currentCustomerId = bookingInfo.customer.id;
                    self.title = bookingEntity.title;
                    self.listComment = bookingInfo.commentBookingEntities;
                    self.note = bookingEntity.note;
                    self.status = bookingEntity.status;
                    self.bookingTime = bookingEntity.bookingTime;
                    self.numberFrameConsecutive = bookingEntity.numberFrameConsecutive;
                    self.timeStartToEnd = bookingEntity.timeStartToEnd;
                    self.customerId = bookingInfo.customer.id;

                    let listCategoryHasBeenDeletedOrNotActive = bookingInfo.listCategory.filter(categoryHasChosen => categoryHasChosen.deleted || !categoryHasChosen.active);
                    self.listAllCategoryBooking = self.listAllCategoryBooking.concat(listCategoryHasBeenDeletedOrNotActive);
                    self.listCategoryHasChosenId = bookingInfo.listCategoryIdHasChosen;
                    if (!self.isFromScreenCustomer) {
                        self.computeListNumberFrameConsecutiveCanChoose(bookingEntity.frame, bookingEntity.numberFrameConsecutive);
                    } else {
                        self.mapNumberFrameConsecutiveCanChoose = {
                            arrayNumber: []
                        };
                        self.mapNumberFrameConsecutiveCanChoose.arrayNumber.push(bookingEntity.numberFrameConsecutive);
                        self.getTimeBookingTitleForCustomerBookingScreen(bookingEntity.bookingTime, bookingEntity.timeStartToEnd);
                    }
                    self.numberFrame = bookingEntity.numberFrameConsecutive;

                    $("#modal_add_booking").modal('show');
                })
            },
            deleteBooking(bookingId) {
                let self = this;
                $.ajax({
                    type: "POST",
                    url: "/api/v1/service_department/" + $('#service_department_id').val() + "/booking/delete?bookingId=" + bookingId,
                    beforeSend: function () {
                        window.loader.show();
                    }
                }).done(function (response) {
                    $('#modal_delete_booking').modal('hide');
                    window.loader.hide();
                    self.responseSuccessHandle(response);
                    // TABLE_BOOKING_VUE.loadFrameAndBooking();
                    bookingManagement.getBookingDataAndRenderTable($("#choose_active_day").val(), false);
                });
            },
            activeSpecialBooking(bookingId, serviceDepartmentId) {
                $.ajax({
                    type: "POST",
                    url: "/api/v1/service_department/" + serviceDepartmentId + "/booking/activeSpecialBooking?bookingId=" + bookingId
                }).done(function (response) {
                    if (response.status.code === 2001) {
                        self.responseSuccessHandle(response);
                        bookingManagement.getBookingDataAndRenderTable($("#choose_active_day").val(), false);
                        return false;
                    } else {
                        window.alert.show("error", "Đã có lỗi xảy ra, vui lòng thử lại", 2000);
                    }
                });
            },
            confirmPaycheck(bookingId) {
                let self = this;
                $.ajax({
                    type: "POST",
                    url: "/api/v1/service_department/" + serviceDepartmentId + "/booking/changeStatusPaycheck?bookingId=" + bookingId
                }).done(function (response) {
                    if (response.status.code === 200) {
                        self.responseSuccessHandle(response);
                        $("#modal_paycheck").modal('hide');
                        window.loader.hide();
                        bookingManagement.getBookingDataAndRenderTable($("#choose_active_day").val(), false);
                    } else {
                        window.alert.show("error", "Đã có lỗi xảy ra, vui lòng thử lại", 2000);
                    }
                });
            },
            responseSuccessHandle(response) {
                let ADD_SUCCESS = 202, UPDATE_SUCCESS = 203, DELETE_SUCCESS = 201, DELETE_FAIL = 401, SUCCESS = 200;
                let OUT_OF_BOOKING_SLOT = 1030, EXPIRED_DATE_BOOKING = 1032, NOT_FOUND_CUSTOMER = 405;
                let HOLIDAY_CONFIG = 406, HAS_NO_CONFIG = 407, WORK_OFF_DAY = 408, DUPLICATE_TIME_BOOKING = 410;
                switch (response.status.code) {
                    case UPDATE_SUCCESS:
                    case SUCCESS:
                        window.alert.show("success", "Thành công", 2000);
                        // TABLE_BOOKING_VUE.loadFrameAndBooking();
                        break;
                    case DELETE_SUCCESS:
                        window.alert.show("success", "Thành công", 2000);
                        // TABLE_BOOKING_VUE.loadFrameAndBooking();
                        break;
                    case OUT_OF_BOOKING_SLOT:
                        window.alert.show("error", "<span style='margin: unset'>Out of slot at frame: </span></br>" + response.data[0], 5000);
                        break;
                    case HOLIDAY_CONFIG:
                        window.alert.show("error", "Hôm nay là ngày lễ", 2000);
                        break;
                    case HAS_NO_CONFIG:
                        window.alert.show("error", "Hôm nay không có cấu hình làm việc", 2000);
                        break;
                    case WORK_OFF_DAY:
                        window.alert.show("error", "Hôm nay là ngày nghỉ", 2000);
                        break;
                    case EXPIRED_DATE_BOOKING:
                        window.alert.show("error", "Hết thời gian đặt lịch, chọn ngày trước hôm nay", 2000);
                        break;
                    case NOT_FOUND_CUSTOMER:
                        window.alert.show("error", "Không tìm thấy khách hàng", 2000);
                        break;
                    case DUPLICATE_TIME_BOOKING:
                        window.alert.show("error", "Khách hàng này đã đặt lịch trùng khung thời gian", 5000);
                        break;
                    case ADD_SUCCESS:
                        let listDayError = response.data;
                        if (listDayError != null && listDayError.length > 0) {
                            window.alert.show("error", "Danh sách ngày không thể đặt trước： " + response.data.join(", "), 5000);
                        } else {
                            window.alert.show("success", "Thành công", 2000);
                        }
                        break;
                    default:
                        window.alert.show("error", "Đã có lỗi xảy ra, vui lòng thử lại", 2000);
                        break;
                }
            },
            createNewCustomer() {
                let $customerName = $("#customer_name");
                if ($customerName.val() === '' || !$customerName.valid()) {
                    window.alert.show("error", "Vui lòng nhập tên khách hàng", 3000);
                } else {
                    let self = this;
                    $.ajax({
                        type: "POST",
                        url: "/api/v1/service_department/" + serviceDepartmentId + "/booking/createNewCustomer?customerName=" + encodeURIComponent(self.customerName) + "&customerPhone=" + $("#customer_phone").val(),
                        beforeSend: function () {
                            window.loader.show();
                        },
                    }).done(function (response) {
                        self.isEnableCreateNewCustomer = false;
                        window.loader.hide();
                        if (response.status.code === 202) {
                            let customer = response.data;
                            self.patientCode = customer.patientCode;
                            self.customerName = customer.name;
                            self.customerPhone = customer.phone;
                            self.currentCustomerId = customer.id;
                            window.alert.show("success", "Thêm khách hàng mới thành công", 2000);
                        } else {
                            window.alert.show("error", "Đã có lỗi xảy ra, vui lòng thử lại", 2000);
                        }
                    });
                }

            },
            updateNoteBooking(newNote) {
                let self = this;
                let formData = new FormData();
                formData.append('newNote', newNote);
                $.ajax({
                    type: "POST",
                    url: "/api/v1/service_department/" + serviceDepartmentId + "/booking/updateNote?bookingId=" + self.currentBookingIdChangeNote,
                    data: formData,
                    processData: false,
                    contentType: false,
                    beforeSend: function () {
                        window.loader.show();
                    }
                }).done(function (response) {
                    window.loader.hide();
                    let code = response.status.code;
                    if (code === 200) {
                        // window.alert.show("success", "Thành công", 2000);
                        // TABLE_BOOKING_VUE.loadFrameAndBooking();
                    } else {
                        window.alert.show("error", "Đã có lỗi xảy ra, vui lòng thử lại", 2000);
                    }
                })
            },
            resetForm(validator, frameWithNumberSlot) {
                Object.assign(this.$data, this.initData());
                this.getTimeBookingTitleAndDeadlineText($('#choose_active_day').val(), frameWithNumberSlot);
                validator.resetForm();
                let frameWithoutNumberSlot = frameWithNumberSlot.split("_")[0];
                this.frameWithoutNumberSlot = frameWithoutNumberSlot;
                this.startTimeOfBookingFrame = frameWithoutNumberSlot.split(" - ")[0] + ':00';
                this.computeListNumberFrameConsecutiveCanChoose(frameWithoutNumberSlot);
                this.loadListCategoryBooking(serviceDepartmentId);
                $("#status_booking").prop('disabled', 'disabled');
            },

            computeListNumberFrameConsecutiveCanChoose(currentFrameWithoutNumberSlot, numberFrameConsecutiveFromOldBooking) {
                this.mapNumberFrameConsecutiveCanChoose = {
                    arrayNumber: []
                };
                let mapListBookingByFrameWithSlot = bookingManagement.mapFrameAndListBookingForTable;
                let isEnableSelect = false;
                let listFrameFromThisToAfter = [];
                let mapFrameAndListBooking = [];
                for (let frameTimeWithNumberSlot in mapListBookingByFrameWithSlot) {
                    let mapFrameAndListBookingForTable = {};
                    mapFrameAndListBookingForTable.frameTimeWithNumberSlot = frameTimeWithNumberSlot;
                    mapFrameAndListBookingForTable.frameTime = frameTimeWithNumberSlot.split("_")[0];
                    let numberSlotCanBooking = Number.parseInt(frameTimeWithNumberSlot.split("_")[1]);
                    if (numberSlotCanBooking > this.maxNumberSlot) {
                        this.maxNumberSlot = numberSlotCanBooking;
                    }
                    let listBooking = mapListBookingByFrameWithSlot[frameTimeWithNumberSlot];
                    mapFrameAndListBookingForTable.listBooking = listBooking;


                    let numberBookingNotHasStatusCanceled = listBooking.filter(booking => booking.status !== 'CANCELED').length;
                    mapFrameAndListBookingForTable.numberSlotEmpty = numberSlotCanBooking - numberBookingNotHasStatusCanceled;
                    if (mapFrameAndListBookingForTable.numberSlotEmpty < 0) {
                        mapFrameAndListBookingForTable.numberSlotEmpty = 0;
                    }
                    mapFrameAndListBooking.push(mapFrameAndListBookingForTable);
                    // self.listFrameWithoutNumberSlot.push(mapFrameAndListBookingForTable.frameTime);
                }
                mapFrameAndListBooking.forEach(frameAndListBooking => {
                    if (frameAndListBooking.frameTime === currentFrameWithoutNumberSlot) {
                        isEnableSelect = true;
                    }

                    if (isEnableSelect) {
                        listFrameFromThisToAfter.push(frameAndListBooking);
                    }
                });

                let numberFrameCanBeConsecutive;
                if (!numberFrameConsecutiveFromOldBooking) { // when create
                    let indexOfFrameHasNoEmptySlot = listFrameFromThisToAfter.findIndex(frame => frame.numberSlotEmpty === 0);
                    numberFrameCanBeConsecutive = indexOfFrameHasNoEmptySlot === -1 ? listFrameFromThisToAfter.length : indexOfFrameHasNoEmptySlot
                } else { // when update
                    let indexOfFrameHasNoEmptySlot = listFrameFromThisToAfter.slice(numberFrameConsecutiveFromOldBooking).findIndex(frame => frame.numberSlotEmpty === 0);
                    numberFrameCanBeConsecutive = indexOfFrameHasNoEmptySlot === -1 ? listFrameFromThisToAfter.length : indexOfFrameHasNoEmptySlot + numberFrameConsecutiveFromOldBooking;
                }
                let setOfFrame = new Set();
                for (let i = 1; i <= numberFrameCanBeConsecutive; i++) {
                    setOfFrame.add(i);
                }

                if (numberFrameConsecutiveFromOldBooking) {
                    for (let i = 1; i <= numberFrameConsecutiveFromOldBooking; i++) {
                        setOfFrame.add(i);
                    }
                }
                this.mapNumberFrameConsecutiveCanChoose.arrayNumber = Array.from(setOfFrame).sort(
                    function (a, b) {
                        return a - b;
                    });
            },
            autoFillCategory(listLatestCategoryHasChosenForCustomer) {
                let self = this;
                let numberFrameConsecutiveWillBeFilled = 1;

                let listCategoryIdHasInList = [];
                self.listCategoryHasChosenId = listCategoryIdHasInList;
                listLatestCategoryHasChosenForCustomer.forEach(function (categoryId) {
                    self.listAllCategoryBooking.forEach(function (category) {
                        if (category.id === categoryId) {
                            numberFrameConsecutiveWillBeFilled += category.numberFrameConsecutiveAutoFill;
                            listCategoryIdHasInList.push(categoryId);
                        }
                    });
                });
                if (self.mapNumberFrameConsecutiveCanChoose.arrayNumber.indexOf(numberFrameConsecutiveWillBeFilled) > -1) {
                    self.listCategoryHasChosenId = [];
                    self.listCategoryHasChosenId = listCategoryIdHasInList;
                    self.numberFrameConsecutive = numberFrameConsecutiveWillBeFilled;
                    // if (numberFrameConsecutiveWillBeFilled === numberFrameConsecutiveDefault) {
                    //     self.minNumberFrameConsecutive = 0;
                    // } else {
                    //     self.minNumberFrameConsecutive = numberFrameConsecutiveWillBeFilled;
                    // }
                } else if (listCategoryIdHasInList.length === 0) {
                    self.numberFrameConsecutive = numberFrameConsecutiveDefault;
                } else {
                    self.listCategoryHasChosenId = [];
                    self.numberFrameConsecutive = numberFrameConsecutiveDefault;
                    window.alert.show("error", "枠数がたりず設定できません", 3000);
                }
            },
        },
        mounted() {
            let self = this;
            self.loadListCategoryBooking(serviceDepartmentId);
            // self.isCanEditBookingScreen = $('#can_edit_booking_screen').val();

            let isCanEditBookingScreen = $('#can_edit_booking_screen').val() === 'true';

            let $customerName = $("#customer_name");
            let $addBookingForm = $("#add_booking_form");

            let $chooseActiveDay = $("#choose_active_day");


            let customerNameInput = document.getElementById('customer_name');

            customerNameInput.addEventListener('focusout', function (e) {
                self.customerName = this.value;
                $customerName.typeahead('val', self.customerName);

            });

            $(document).on('click', '.btn-add-booking', function () {
                $("#number_frame_consecutive").prop('disabled', false);
                self.resetForm(validator, $(this).attr("id"));
            });

            $(document).on('click', '#btn_submit_booking', function () {
                if ($addBookingForm.valid()) {
                    self.status = $("#status_booking").val();
                    self.submitForm();
                }
            });

            $(document).on('click', ".btn-update-booking", function () {
                let idBooking = $(this).attr("id").replace('btn_update_booking_', '');
                self.id = idBooking;
                self.resetForm(validator, $(this).attr("data"));
                $("#status_booking").removeAttr('disabled');
                self.getDetailBooking(idBooking, serviceDepartmentId);
                $("#btn_submit_booking").html("Cập nhật");
            });

            $(document).on('click', ".btn-active-special-booking", function (event) {
                let idBooking = $(this).attr("data");
                let storeIdInListBookingCustomerPage = $(this).attr("store");
                let isClickFromListBookingCustomerPage = storeIdInListBookingCustomerPage !== undefined;
                event.preventDefault();
                self.activeSpecialBooking(idBooking, isClickFromListBookingCustomerPage ? storeIdInListBookingCustomerPage : storeId);
            });


            $(document).on('click', '.btn-delete-booking', function () {
                let storeIdInListBookingCustomerPage = $(this).attr("store");
                let isClickFromListBookingCustomerPage = storeIdInListBookingCustomerPage !== undefined;
                if (isClickFromListBookingCustomerPage) {
                    $('#store_id').val(storeIdInListBookingCustomerPage);
                }
                self.deleteBookingId = $(this).attr('id');
            });

            // submit delete
            $(document).on('click', "#btn_submit_delete", function (event) {
                self.deleteBooking(self.deleteBookingId, event);
            });

            $(document).on('click', '.btn-paycheck', function () {
                self.paycheckBookingId = $(this).attr("id").replace('btn_paycheck_', '');
            })

            $(document).on('click', "#btn_submit_paycheck", function () {
                self.confirmPaycheck(self.paycheckBookingId);
            })

            let validator = $addBookingForm.validate({
                errorElement: "p",
                errorClass: "error-message",
                errorPlacement: function (error, element) {
                    error.insertAfter(element);
                },
                rules: {
                    customer_name: {
                        required: function () {
                            return self.isChangeInputCustomerName;
                        },
                    },
                    customer_phone: {
                        onlyNumber: true,
                        number: true
                    }
                },
            });

            // custom validate method
            $.validator.addMethod('onlyNumber', function (value) {
                let regExp = /^\d+$/;
                return regExp.test(value)
            }, 'Vui lòng chỉ nhập số hợp lệ');

            $.validator.addMethod('notEmpty', function (value) {
                return value !== null && value !== undefined && value.trim() !== "";
            }, 'Trường này bắt buộc');

            $customerName.typeahead({
                    hint: false,
                    highlight: true,
                    minLength: 0,
                },
                {
                    name: 'customer_name',
                    limit: 'Infinity',
                    display: function (customer) {
                        if (customer == null) return;
                        return customer.name;
                    },
                    remote: {
                        cache: false
                    },
                    source: function (str, sync, async) {
                        self.searchCustomerByNameStr(str, async);
                    },
                    templates: {
                        suggestion: function (customer) {
                            if (customer == null) { //on default
                                return '<div style="font-size: 12px" class="create-new-customer-selection"> Khách hàng mới</div>';
                            }
                            let latestNoteWith20Character = customer.latestNote;
                            if (latestNoteWith20Character == null || latestNoteWith20Character === 'null') {
                                latestNoteWith20Character = '';
                            } else if (latestNoteWith20Character.length > 20) {
                                latestNoteWith20Character = latestNoteWith20Character.substring(0, 20) + '...'
                            }
                            return `<div style="font-size: 12px" class="customer-selection">${customer.patientCode}, ${customer.name}, ${customer.phone}, <span class="latest-note">${latestNoteWith20Character}</span></div>`;
                        },
                    }
                });

            $('#modal_add_booking').on('show.bs.modal', function (e) {
                $customerName.typeahead('val', '');
            });

            $customerName.on('keyup', function (e) {
                self.patientCode = '';
                self.customerPhone = '';
                self.currentCustomerId = '';
                self.isEnableCreateNewCustomer = true;
                self.isChangeInputCustomerName = true;
                self.note = '';
                $customerName.focus();
            });

            // bind value customer name to input
            $customerName.on('typeahead:select', function (ev, customerSelected) {
                if (customerSelected != null) {
                    self.patientCode = customerSelected.patientCode;
                    self.customerName = customerSelected.name;
                    self.customerPhone = customerSelected.phone;
                    self.currentCustomerId = customerSelected.id;
                    self.note = customerSelected.latestNote;
                    self.isEnableCreateNewCustomer = false;
                    self.autoFillCategory(customerSelected.latestListCategoryIdHasChosen);
                }
                setTimeout(function () {
                    $addBookingForm.valid();
                }, 100)
            });


            $(document).on('click', '.create-new-customer-selection', function () {
                self.isEnableCreateNewCustomer = true;
                self.patientCode = '';
                self.customerPhone = '';
                self.currentCustomerId = '';
            });

            $(document).on('dblclick', '.customer-note', function () {
                $(this).removeAttr('disabled');
                self.currentBookingIdChangeNote = $(this).attr("id").replace("booking_note_", '');
                self.currentBookingNote = $(this).val();
            });

            $(document).mouseup(function (e) {
                if (self.currentBookingIdChangeNote === '') {
                    return;
                }
                let $noteTextareaClass = $(".customer-note");
                if (!$noteTextareaClass.is(e.target) && $noteTextareaClass.has(e.target).length === 0) {
                    let $currentNoteTextarea = $("#booking_note_" + self.currentBookingIdChangeNote);
                    if (self.currentBookingNote !== $currentNoteTextarea.val()) {
                        self.updateNoteBooking($currentNoteTextarea.val());
                    }
                    $currentNoteTextarea.prop('disabled', true);
                    self.currentBookingIdChangeNote = '';
                    self.currentBookingNote = '';
                }
            });

        }
    })

});

function getDayInWeekText(activeDay) {
    let dt = new Date(activeDay);
    let numberInWeek = dt.getDay();
    switch (numberInWeek) {
        case 0: // sunday
            return " (Chủ nhật)";
        case 1:
            return " (Thứ hai)";
        case 2:
            return " (Thứ ba)";
        case 3:
            return " (Thứ tư)";
        case 4:
            return " (Thứ năm)";
        case 5:
            return " (Thứ sáu)";
        case 6:
            return " (Thứ bảy)"; //saturday
    }
}
