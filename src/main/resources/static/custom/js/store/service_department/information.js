$(document).ready(function () {

    let serviceDepartmentId = $("#service_department_id").val();

    let $formSaveServiceDepartment = $("#form_service_department_info");
    let $btnSubmit = $("#btn_submit");
    let $serviceDepartmentName = $("#name");
    let $serviceDepartmentPhone = $("#phone");
    let $serviceDepartmentEmail = $("#email");

    initCkEditor('description');
    fillServiceDepartmentInformation();

    function fillServiceDepartmentInformation() {
        jQuery.get("/api/v1/service_department/" + serviceDepartmentId + "/information", function (serviceDepartment) {
            $serviceDepartmentName.val(serviceDepartment.name);
            $serviceDepartmentPhone.val(serviceDepartment.phone);
            $serviceDepartmentEmail.val(serviceDepartment.email);
            setTimeout(function () {
                CKEDITOR.instances['description'].setData(serviceDepartment.description);
            }, 500);
            imageHandle.showImageWhenUrlNotNull(serviceDepartment.imageUrl);
        });
    }

    let rules = {
        name: {
            notEmpty: true, maxlength: 500
        },
        phone: {
            onlyNumber: true, maxlength: 12
        },
        email: {
            notEmpty: true,
            email: true
        },
        image_service_department: {
            required: {
                depends: function () {
                    return !imageHandle.isHasImage();
                }
            }
        },
        description: {required_ckEditor: true}
    };

    let messages = {
        image_service_department: {
            required: "Vui lòng chọn 1 ảnh đại diện mảng dịch vụ"
        }
    };

    configValidate($formSaveServiceDepartment, rules, [], messages);

    $btnSubmit.click(function () {
        if ($formSaveServiceDepartment.valid()) {
            submitForm();
        }
    });

    let submitForm = function () {
        let formData = new FormData();
        formData.append("name", $serviceDepartmentName.val());
        formData.append('phone', $serviceDepartmentPhone.val());
        formData.append('email', $serviceDepartmentEmail.val());
        formData.append('description', CKEDITOR.instances['description'].getData());
        if ($(".file-input")[0].files.length !== 0) {
            formData.append("image", $("#image_service_department")[0].files[0]);
        }

        $.ajax({
            type: "POST",
            url: "/api/v1/service_department/" + serviceDepartmentId + "/saveInformation",
            data: formData,
            beforeSend: function () {
                window.loader.show();
            },
            processData: false,
            contentType: false,
        }).done(function (response) {
            window.loader.hide();
            if (response.status.code === 200) {
                window.alert.show("success", "Thành công", "1500");
                setTimeout(function () {
                    window.location.reload();
                }, 1000);
            } else {
                window.alert.show("error", "Có lỗi xảy ra. Vui lòng thử lại !", "1500");
            }
        }).fail(function (response) {
            console.log(response);
            window.alert.show("error", "Có lỗi xảy ra. Vui lòng thử lại !", "1500");
        });
    };
});
