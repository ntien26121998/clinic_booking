$(document).ready(function () {
    let serviceDepartmentId = $("#service_department_id").val();
    let $formImage = $('#form_image');
    let $imageTitle = $("#title");
    let $imageDescription = $("#description");
    let $imageId = $("#image_id");
    let $modalAddImage = $("#modal_image");

    let rulesValidate = {
        title: {
            notEmpty: true,
            maxlength: 50
        },
        description: {
            notEmpty: true,
            maxlength: 200
        },
        image_service: {
            required: {
                depends: function () {
                    return !imageHandle.isHasImage();
                }
            }
        }
    };

    let messages = {
        image_service: {
            required: "Vui lòng chọn 1 ảnh"
        }
    };

    let validator = configValidate($formImage, rulesValidate, [], messages);

    $("#btn_add_image").click(function () {
        $imageTitle.add($imageDescription).add($imageId).val('');
        $('.remove-image-icon').click();
        validator.reset();
    });


    $("#btn_submit").click(function (e) {
        if ($formImage.valid()) {
            submitForm(e)
        }
    });

    let submitForm = function (e) {
        e.preventDefault();
        let formData = new FormData();
        console.log($imageId.val());
        formData.append("id", $imageId.val());
        formData.append("title", $imageTitle.val());
        formData.append('description', $imageDescription.val());
        if ($(".file-input")[0].files.length !== 0) {
            formData.append("image", $("#image_service")[0].files[0]);
        }

        $.ajax({
            type: "POST",
            url: "/api/v1/service_department/" + serviceDepartmentId + "/image/save",
            data: formData,
            beforeSend: function () {
                window.loader.show();
            },
            processData: false,
            contentType: false,
        }).done(function (response) {
            console.log(response);
            saveObjectResponseHandleOnDone(response, $modalAddImage, imageTable);
            window.loader.hide();
            if(response.status.code === 202) {
                $modalAddImage.modal("hide");
                window.alert.show("success", "Thành công", "1500");
                setTimeout(function (){
                    imageTable.ajax.reload();
                }, 1000);
            } else {
                window.alert.show("error", "Có lỗi xảy ra. Vui lòng thử lại !", "1500");
            }
        }).fail(function (response) {
            saveObjectResponseHandleOnFail(response, $modalAddImage);
        });
    };

    $(document).on('click', '.btn-update-image', function () {
        let imageId = $(this).attr("id");
        jQuery.get('/api/v1/service_department/' + serviceDepartmentId + '/image/' + imageId, function (image) {
            imageHandle.showImageWhenUrlNotNull(image.imageUrl);
            $imageId.val(image.id);
            $imageTitle.val(image.title);
            $imageDescription.val(image.description);
        });

    });

});
