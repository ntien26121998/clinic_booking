$(document).ready(function () {
    let storeId = $("#store_id").val();
    let serviceDepartmentId = $("#service_department_id").val();

    new Vue({
        el: "#frame_setting_div",
        data: {
            listFrameSetting: [],
            currentSelectedToDeleteId: ''
        },
        methods: {
            createNewFrameSetting() {
                let newFrameSetting = {
                    id: '',
                    frameSlot: '0',
                    slotTimeUnit: '15',
                    startActiveRange: moment(new Date()).format("YYYY/MM/DD"),
                    endActiveRange: '9999/12/31',
                    typeConfig: "COMMON_CONFIG"
                };
                this.listFrameSetting.push(newFrameSetting);
            },
            loadListFrameSettingFromDB() {
                let self = this;
                jQuery.get("/api/v1/web/" + serviceDepartmentId + "/frame_setting/listFrameSetting", function (response) {
                    let listFrameSetting = response.data;
                    if (listFrameSetting != null) {
                        self.listFrameSetting = listFrameSetting;
                    }
                });
            },
            submitFrameSettingForm(indexOfForm) {
                let self = this;
                if ($("#frame_setting_form_" + indexOfForm).valid()) {
                    $.ajax({
                        type: "POST",
                        url: "/api/v1/web/" + serviceDepartmentId + "/frame_setting/saveFrameSetting",
                        data: JSON.stringify(self.listFrameSetting[indexOfForm]),
                        contentType: "application/json",
                        beforeSend: function () {
                            window.loader.show();
                            $(".btn-save-frame-setting").each(function () {
                                $(this).prop("disabled", true);
                            });
                        }
                    }).then(function (response) {
                        $(".btn-save-frame-setting").each(function () {
                            $(this).prop("disabled", false);
                        });
                        window.loader.hide();
                        handleResponseOnSaving(response.status.code, self.loadListFrameSettingFromDB);
                    });
                }
            },
            confirmDeleteFrameSetting(indexOfForm) {
                let self = this;
                let frameSetting = self.listFrameSetting[indexOfForm];
                if (frameSetting.id === "") {
                    self.listFrameSetting.splice(indexOfForm, 1);
                } else {
                    self.currentSelectedToDeleteId = frameSetting.id;
                    $("#modal_delete_frame_setting").modal('show');
                }
            },
            submitDeleteFrameSetting() {
                let self = this;
                if(self.currentSelectedToDeleteId === ''){
                    return;
                }
                $.ajax({
                    type: "POST",
                    url: "/api/v1/web/" + serviceDepartmentId + "/frame_setting/deleteFrameSetting?frameSettingId=" + self.currentSelectedToDeleteId,
                    beforeSend: function () {
                        window.loader.show();
                    },
                    success: function (response) {
                        $('#modal_delete_frame_setting').modal('hide');
                        window.loader.hide();
                        handleResponseOnDeleting(response.status.code, self.loadListFrameSettingFromDB);
                        self.currentSelectedToDeleteId = '';
                    }
                });
            }
        },
        mounted() {
            let self = this;
            self.loadListFrameSettingFromDB();

        },
        updated() {
            let self = this;

            $(".form-frame-setting").each(function () {
                // attach to all form elements on page

                let $currentFrameSettingForm = $(this);
                let indexOfForm = $(this).attr("id").replace("frame_setting_form_", "");
                let currentFrameSetting = self.listFrameSetting[indexOfForm];

                let $startActiveRange = $("#frame_setting_start_active_range_" + indexOfForm);
                let $endActiveRange = $("#frame_setting_end_active_range_" + indexOfForm);

                configDateShowOnlyDay("frame_setting_start_active_range_" + indexOfForm);
                configDateShowOnlyDay("frame_setting_end_active_range_" + indexOfForm);

                $startActiveRange.val(currentFrameSetting.startActiveRange);
                $endActiveRange.val(currentFrameSetting.endActiveRange.includes("9999/12/31") ? "" : currentFrameSetting.endActiveRange);

                $endActiveRange.on('hide.daterangepicker', function () {
                    currentFrameSetting.endActiveRange = $(this).val();
                    $currentFrameSettingForm.valid();
                }).on('cancel.daterangepicker', function() {
                    $endActiveRange.val('');
                    currentFrameSetting.endActiveRange = "9999/12/31";
                });

                $startActiveRange.on('hide.daterangepicker', function () {
                    currentFrameSetting.startActiveRange = $(this).val();
                    $currentFrameSettingForm.valid();
                });


                $currentFrameSettingForm.validate({
                    errorElement: "p",
                    errorClass: "error-message",
                    errorPlacement: function (error, element) {
                        error.insertAfter(element);
                    },
                    ignore: [],
                    rules: {
                        frame_slot: {
                            required: true,
                            minStrict: 0
                        },
                        slot_time_unit: {
                            required: true
                        },
                        start_active_range: {
                            required: true,
                            validateActiveTimeNotOverlapForFrameSetting: true,
                            validateDateRangeForFrameSetting: true,
                        },
                        end_active_range: {
                            validateActiveTimeNotOverlapForFrameSetting: true,
                            validateDateRangeForFrameSetting: true,
                        }
                    },
                });


                $.validator.addMethod('validateDateRangeForFrameSetting', function (value, element) {
                    let currentFrameSetting = self.listFrameSetting[$(element).attr('data')];
                    let startRangeFromFormToTime = new Date(currentFrameSetting.startActiveRange).getTime();
                    let endRangeFromFormToTime = new Date(currentFrameSetting.endActiveRange).getTime();
                    return startRangeFromFormToTime < endRangeFromFormToTime;
                }, "Ngày bắt đầu phải trước ngày kết thúc");

                $.validator.addMethod('minStrict', function (inputValue, el, minValue) {
                    return inputValue > minValue;
                }, "Vui lòng nhập giá trị lớn hơn hoặc bằng 1");

                $.validator.addMethod('validateActiveTimeNotOverlapForFrameSetting', function (value, element) {
                    let currentFormIndex = $(element).attr('data');
                    let currentFrameSetting = self.listFrameSetting[currentFormIndex];
                    return validateDateOverlapMethod(currentFrameSetting, self.listFrameSetting, currentFormIndex);
                }, "Ngày tháng không được trùng lặp");

            });
        },
    })
});
