let bookingManagement = {};
$(document).ready(function () {

    let serviceDepartmentId = $('#service_department_id').val();
    let $chooseActiveDay = $("#choose_active_day");
    let $bookingTable = $("#booking_table");
    let $isHoliday = $("#is_holiday");
    let $btnSubmitSpecificConfig = $("#btn_submit_specific_day_config");
    let $startWorkingTime = $("#start_working_time");
    let $startBreakTime = $("#start_break_time");
    let $endWorkingTime = $("#end_working_time");
    let $endBreakTime = $("#end_break_time");
    let $slotPerFrame = $("#slot_per_frame");
    let $slotTimeUnit = $("#slot_time_unit");

    let isCashier = $("#is_cashier").val();

    bookingManagement.listFrameWithoutNumberSlot = [];
    bookingManagement.mapFrameAndListBookingForTable = {};
    bookingManagement.reloadBookingTable = function reloadBookingTable() {
        setTimeout(function () {
            $chooseActiveDay.change();
        }, 500);
    };

    bookingManagement.getBookingDataAndRenderTable = function (chosenDay, isWorkOffDayAndNoSpecificConfig) {
        $.ajax({
            url: "/api/v1/service_department/" + serviceDepartmentId + "/booking/findByActiveDay?activeDay=" + chosenDay,
            type: "get",
            async: false
        }).done(function (mapFramesAndListBookingOnChosenDayDay) {
            for (let frameWithNumberSlot in mapFramesAndListBookingOnChosenDayDay) {
                let frameWithoutNumberSlot = frameWithNumberSlot.split("_")[0];
                bookingManagement.listFrameWithoutNumberSlot.push(frameWithoutNumberSlot);
            }
            bookingManagement.mapFrameAndListBookingForTable = mapFramesAndListBookingOnChosenDayDay;
            let html = getHtmlForRenderingBookingTable(mapFramesAndListBookingOnChosenDayDay, chosenDay, isWorkOffDayAndNoSpecificConfig);
            $bookingTable.children('tbody').html("").append(html);
            autoScrollTableByLocalTime();
        });
    };

    function getHtmlForRenderingBookingTable(mapFramesAndListBookingOnChosenDayDay, chosenDay, isWorkOffDayAndNoSpecificConfig) {
        // html for null booking data
        if (mapFramesAndListBookingOnChosenDayDay == null || mapFramesAndListBookingOnChosenDayDay === '') {
            return `<tr><td colspan="8" style="font-size: 13px !important;">${noDataHtml}</tr>`;
        }

        // html for work off day ( no booking and no specific config (not holiday)
        if (isWorkOffDayAndNoSpecificConfig && !checkDayHasBooking(mapFramesAndListBookingOnChosenDayDay)) {
            let html = '<tr><td colspan="8">Đây là ngày nghỉ mặc định</td></tr>';
            $btnSubmitSpecificConfig.add($isHoliday).prop("disabled", true);
            ($isHoliday).prop("checked", true).change();
            return html;
        }

        // html for has booking or available empty slot
        let htmlForDayHasBookingOrEmptySlot = '';
        for (let frameTimeWithNumberSlot in mapFramesAndListBookingOnChosenDayDay) {
            let frameTimeWithoutNumberSlot = frameTimeWithNumberSlot.split("_")[0];
            htmlForDayHasBookingOrEmptySlot += getHtmlForFrameThTag(chosenDay, frameTimeWithoutNumberSlot);

            let numberBookingPerFrame = mapFramesAndListBookingOnChosenDayDay[frameTimeWithNumberSlot].length;
            let numberSlotPerFrame = frameTimeWithNumberSlot.split("_")[1];
            let numberAvailableSlotPerFrame = numberSlotPerFrame;

            // no booking in frame
            if (numberBookingPerFrame === 0) {
                htmlForDayHasBookingOrEmptySlot += getHtmlForEmptySlotOnChosenDay(chosenDay, frameTimeWithNumberSlot, numberAvailableSlotPerFrame);
            } else {
                disableEditSpecificDayWorkingTimeConfig();
                // render html for booking row
                for (let i = 0; i < numberBookingPerFrame; i++) {
                    let bookingEntity = mapFramesAndListBookingOnChosenDayDay[frameTimeWithNumberSlot][i];
                    htmlForDayHasBookingOrEmptySlot += getHtmlForBookingEntityRow(bookingEntity, frameTimeWithoutNumberSlot);

                    if (bookingEntity.status !== "CANCELED") {
                        numberAvailableSlotPerFrame -= 1;
                    }
                }
                // append html if has empty slot
                htmlForDayHasBookingOrEmptySlot += getHtmlForEmptySlotOnChosenDay(chosenDay, frameTimeWithNumberSlot, numberAvailableSlotPerFrame);
            }
        }
        return htmlForDayHasBookingOrEmptySlot;
    }

    // render row table for place-holding booking slot when chosenDay has empty slot
    function getHtmlForEmptySlotOnChosenDay(chosenDay, frameTime, numberAvailableSlotPerFrame) {
        if (isBeforeToday(chosenDay)) {
            return '<tr><td colspan="8"><span style="margin-bottom: 5px">Trống</span></td></tr>'
        }
        if (numberAvailableSlotPerFrame <= 0) {
            return '';
        }
        if(isCurrentTimeBeforeFrameOfBookingTime(frameTime, $chooseActiveDay.val())){
            return '<tr><td colspan="8"><span style="margin-bottom: 5px">Đã hết thời gian đặt chỗ</span></td></tr>'
        }

        let html = '';
        for (let i = 0; i < numberAvailableSlotPerFrame; i++) {
            if (isCashier === 'true') {
                html += '<tr>' +
                    '</tr>'
            } else {
                html += '<tr>' +
                    '<td colspan="8">' +
                    '<button class="btn btn-sm btn-primary btn-add-booking" id="' + frameTime + '" ' +
                    'style="margin-right: 5px" data-toggle="modal" data-target="#modal_add_booking">Thêm lịch</button>' +
                    '<button class="btn btn-sm btn-danger btn-remove-slot" id="' + frameTime + '"' +
                    ' data-toggle="modal" data-target="#modal_delete_slot">Xóa chỗ</button>' +
                    '</td>' +
                    '</tr>'
            }

        }
        return html;
    }

    function disableEditSpecificDayWorkingTimeConfig() {
        $startWorkingTime
            .add($startBreakTime)
            .add($endBreakTime)
            .add($endWorkingTime)
            .add($slotPerFrame)
            .add($slotTimeUnit)
            .prop("disabled", true);

        $btnSubmitSpecificConfig.add($isHoliday).prop("disabled", true);
    }

    function checkDayHasBooking(mapListBookingAndFramesInDay) {
        for (let frameTime in mapListBookingAndFramesInDay) {
            let numberBookingPerFrame = mapListBookingAndFramesInDay[frameTime].length;
            if (numberBookingPerFrame > 0) {
                return true;
            }
        }
    }

    function getHtmlForFrameThTag(chosenDay, frameTimeWithNumberSlot) {
        let frameTimeWithoutNumberSlot = frameTimeWithNumberSlot.split('_')[0];
        if (isCashier === 'true') {
            return '<tr>' +
                '<th colspan="8" class="frame-title" id="' + frameTimeWithNumberSlot + '">' +
                '<span style="padding-left: 6.6em">' + frameTimeWithoutNumberSlot + '</span>' +
                '</th></tr>';
            ;
        } else {
            if (isCurrentTimeBeforeFrameOfBookingTime(frameTimeWithoutNumberSlot, $chooseActiveDay.val())) {
                return '<tr>' +
                    '<th colspan="8" class="frame-title" id="' + frameTimeWithNumberSlot + '">' +
                    '<span style="padding-left: 6.6em">' + frameTimeWithoutNumberSlot + '</span>' +
                    '<button class="btn-add-slot btn btn-primary btn-sm" ' +
                    'style="float: right; color: white;background: #0095ff !important;" id="' + frameTimeWithNumberSlot + '" disabled>Thêm chỗ</button>' +
                    '</th></tr>';
                ;
            } else {
                return '<tr>' +
                    '<th colspan="8" class="frame-title" id="' + frameTimeWithNumberSlot + '">' +
                    '<span style="padding-left: 6.6em">' + frameTimeWithoutNumberSlot + '</span>' +
                    '<button class="btn-add-slot btn btn-primary btn-sm" ' +
                    'style="float: right; color: white;background: #0095ff !important;" id="' + frameTimeWithNumberSlot + '">Thêm chỗ</button>' +
                    '</th></tr>';
                ;
            }
        }
    }

    function getHtmlForBookingEntityRow(bookingEntity, frameTimeWithoutNumberSlot) {
        let bookingNote = bookingEntity.note == null ? noDataHtml : bookingEntity.note;
        let staffName = bookingEntity.staffName == null ? '<i>Chưa phân công</i>' : bookingEntity.staffName;
        let totalMoney = totalMoneyFromCategoryChosen(bookingEntity.listCategory);
        let listCategoryChosen = renderListCategory(bookingEntity.listCategory);
        if (isCashier === 'true') {
            if (bookingEntity.payCheck) {
                return `<tr data="${bookingEntity.special}" class="has-booking special-${bookingEntity.special}" >
                          <td id="booking_customer_name_${bookingEntity.id}">${bookingEntity.customer.name}</td>
                          <td id="booking_customer_phone_${bookingEntity.id}">${bookingEntity.customer.phone}</td>
                          <td id="booking_list_category_${bookingEntity.id}">${listCategoryChosen}</td>
                          <td id="booking_money_exclude_tax_${bookingEntity.id}">${totalMoney}</td>
                          <td id="booking_note_${bookingEntity.id}">${bookingNote}</td>
                          <td id="booking_status_${bookingEntity.id}">${renderLabelForBookingStatus(bookingEntity.status, frameTimeWithoutNumberSlot)}</td>
                          <td id="booking_paycheck_${bookingEntity.id}">${renderLabelForPaycheckStatus(bookingEntity.payCheck)}</td>
                          <td>
                           
                          </td>
                    </tr>`;
            } else {
                return `<tr data="${bookingEntity.special}" class="has-booking special-${bookingEntity.special}" >
                          <td id="booking_customer_name_${bookingEntity.id}">${bookingEntity.customer.name}</td>
                          <td id="booking_customer_phone_${bookingEntity.id}">${bookingEntity.customer.phone}</td>
                          <td id="booking_list_category_${bookingEntity.id}">${listCategoryChosen}</td>
                          <td id="booking_money_exclude_tax_${bookingEntity.id}">${totalMoney}</td>
                          <td id="booking_note_${bookingEntity.id}">${bookingNote}</td>
                          <td id="booking_status_${bookingEntity.id}">${renderLabelForBookingStatus(bookingEntity.status, frameTimeWithoutNumberSlot)}</td>
                          <td id="booking_paycheck_${bookingEntity.id}">${renderLabelForPaycheckStatus(bookingEntity.payCheck)}</td>
                          <td>
  
                              <button class="btn btn-sm btn-info btn-paycheck" id="btn_paycheck_${bookingEntity.id}"
                             data-toggle="modal" data-target="#modal_paycheck" style="margin-right: 5px">Thanh toán</button>
                          </td>
                    </tr>`;
            }
        }
        else {
            if (bookingEntity.payCheck) {
                return `<tr data="${bookingEntity.special}" class="has-booking special-${bookingEntity.special}" >
                          <td id="booking_customer_name_${bookingEntity.id}">${bookingEntity.customer.name}</td>
                          <td id="booking_customer_phone_${bookingEntity.id}">${bookingEntity.customer.phone}</td>
                          <td id="booking_list_category_${bookingEntity.id}">${listCategoryChosen}</td>
                          <td id="booking_money_exclude_tax_${bookingEntity.id}">${totalMoney}</td>
                          <td id="booking_note_${bookingEntity.id}">${bookingNote}</td>
                          <td id="booking_status_${bookingEntity.id}">${renderLabelForBookingStatus(bookingEntity.status, frameTimeWithoutNumberSlot)}</td>
                          <td id="booking_paycheck_${bookingEntity.id}">${renderLabelForPaycheckStatus(bookingEntity.payCheck)}</td>
                          <td>
                             <button class="btn btn-sm btn-primary btn-update-booking" id="btn_update_booking_${bookingEntity.id}"  
                                    data="${frameTimeWithoutNumberSlot}" data-toggle="modal" data-target="#modal_add_booking" style="margin-right: 5px">
                                    Chi tiết</button>
                             <button class="btn btn-sm btn-danger btn-delete-booking"  
                                     data-toggle="modal" data-target="#modal_delete_booking"  id="${bookingEntity.id}" style="margin-right: 5px">Xóa</button>
                          </td>
                    </tr>`;
            } else {
                return `<tr data="${bookingEntity.special}" class="has-booking special-${bookingEntity.special}" >
                          <td id="booking_customer_name_${bookingEntity.id}">${bookingEntity.customer.name}</td>
                          <td id="booking_customer_phone_${bookingEntity.id}">${bookingEntity.customer.phone}</td>
                          <td id="booking_list_category_${bookingEntity.id}">${listCategoryChosen}</td>
                          <td id="booking_money_exclude_tax_${bookingEntity.id}">${totalMoney}</td>
                          <td id="booking_note_${bookingEntity.id}">${bookingNote}</td>
                          <td id="booking_status_${bookingEntity.id}">${renderLabelForBookingStatus(bookingEntity.status, frameTimeWithoutNumberSlot)}</td>
                          <td id="booking_paycheck_${bookingEntity.id}">${renderLabelForPaycheckStatus(bookingEntity.payCheck)}</td>
                          <td>
                             <button class="btn btn-sm btn-primary btn-update-booking" id="btn_update_booking_${bookingEntity.id}"  
                                    data="${frameTimeWithoutNumberSlot}" data-toggle="modal" data-target="#modal_add_booking" style="margin-right: 5px">
                                    Chi tiết</button>
                             <button class="btn btn-sm btn-danger btn-delete-booking"  
                                     data-toggle="modal" data-target="#modal_delete_booking"  id="${bookingEntity.id}" style="margin-right: 5px">Xóa</button>
                              <button class="btn btn-sm btn-info btn-paycheck" id="btn_paycheck_${bookingEntity.id}"
                             data-toggle="modal" data-target="#modal_paycheck" style="margin-right: 5px">Thanh toán</button>
                          </td>
                    </tr>`;
            }
        }

    }
    function renderListCategory(listCategory) {
        let html= '';
        for (let i = 0; i < listCategory.length; i++){
            if (listCategory.length <= 1) {
                html += listCategory[i].categoryName;
            } else {
                html += listCategory[i].categoryName + ',&nbsp';
            }
        }
        return html;
    }

    function totalMoneyFromCategoryChosen(listCategory) {
        let sumMoney = listCategory
            .map(category => category.moneyExcludeTax)
            .reduce((prev, curr) => prev + curr, 0);
        return formatMoney(sumMoney) + "VND";
    }

    function formatMoney(money) {
        return money.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
    }

    function renderLabelForBookingStatus(status, frame) {
        switch (status) {
            case "WAITING_CONFIRM":
                return '<span style="color: white" data=' + frame + ' class="btn btn-sm btn-warning btn-status" >Chờ xác nhận</span>';
            case "SUCCESS":
                return '<span class="btn btn-sm btn-success " data=' + frame + ' >Chờ thực hiện</span>';
            case "IN_PROCESS":
                return '<span class="btn btn-sm btn-info btn-status" data=' + frame + ' >Đang thực hiện</span>';
            case "DONE":
                return '<span class="btn btn-sm btn-danger btn-status" data=' + frame + ' >Đã hoàn thành</span> ';
            case "CANCELED":
                return '<span class="btn btn-sm btn-secondary btn-status" data=' + frame + ' >Hủy</span>';
        }
    }

    function renderLabelForPaycheckStatus(isPaycheck) {
        if (isPaycheck) {
            return '<span style="color: white" class="btn btn-sm btn-success" >Đã thanh toán</span>';
        } else {
            return '<span style="color: white" class="btn btn-sm btn-warning" >Chưa thanh toán</span>';
        }
    }

    let autoScrollTableByLocalTime = function(){
        let startOfToDay = new Date();
        startOfToDay.setHours(0, 0, 0, 0);
        if(new Date($chooseActiveDay.val()).getTime() === startOfToDay.getTime()) {
            let currentTime = moment().format("HH:mm");
            $('.frame-title').each(function() {
                let frameTime = ($(this).attr("id")); // HH:mm - HH:mm
                let startFrame = frameTime.split(" - ")[0]; // HH:mm
                let endFrame = frameTime.split(" - ")[1].split('_')[0];
                if (currentTime < endFrame) {
                    $('html, body').stop(true, false).animate({
                        scrollTop: $(this).offset().top
                    }, 1000);
                    return false;
                }
            })
        }
    };
});

function isCurrentTimeBeforeFrameOfBookingTime(frameTime, activeDay) {
    return moment(new Date()).isAfter(moment(new Date(activeDay + " " + frameTime.split("-")[0].trim())));
}

function isCurrentTimeBeforeFrameWithoutNumberSlotOfBookingTime(frameTime, activeDay) {
    return moment(new Date()).isAfter(moment(new Date(activeDay + " " + frameTime)));
}
