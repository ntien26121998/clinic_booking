let serviceDepartmentId;

function connect() {
    // Create and init the SockJS object
    let socket = new SockJS('/ws');
    let stompClient = Stomp.over(socket);

    stompClient.connect({}, function(frame) {
        stompClient.subscribe('/service_department/' + serviceDepartmentId, function(notification) {
            console.log(notification);
            notify(JSON.parse(notification.body).bookingDate);
            if(window.location.href.indexOf('service_department/'+ serviceDepartmentId +'/booking') > -1) {
                bookingManagement.reloadBookingTable();
                bookingNotificationNavBar.getListNotification(1);
            }
        });
    });
}

function notify(bookingDate) {
    let message = "Có 1 lịch đặt mới vào ngày: " + bookingDate + "!";
    window.alert.show("success", message, 2000);
}

$(document).ready(function() {
    serviceDepartmentId = $("#service_department_id").val();
    connect();
});
