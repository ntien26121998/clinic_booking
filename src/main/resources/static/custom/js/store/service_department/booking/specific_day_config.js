$(document).ready(function () {

    let serviceDepartmentId = $('#service_department_id').val();
    let enableAutoScroll = true;
    let $isHoliday = $("#is_holiday");
    let $startWorkingTime = $("#start_working_time");
    let $startBreakTime = $("#start_break_time");
    let $endWorkingTime = $("#end_working_time");
    let $endBreakTime = $("#end_break_time");
    let $slotPerFrame = $("#slot_per_frame");
    let $slotTimeUnit = $("#slot_time_unit");
    let $btnSubmitSpecificConfig = $("#btn_submit_specific_day_config");
    let $formSpecificDayWorkingTimeConfig = $("#form_specific_day_working_time_config");
    let $chooseActiveDay = $("#choose_active_day");
    let $idSpecificDayConfig = $("#id_specific_day_config");
    let $staffChoiceType = $("#staff_choice_type");
    let $workOffDay = $("#work_off_day_specific_config");

    configDateShowOnlyTime('start_working_time');
    configDateShowOnlyTime('end_working_time');
    configDateShowOnlyTime('start_break_time');
    configDateShowOnlyTime('end_break_time');
    configDateShowOnlyDay("choose_active_day");


    $chooseActiveDay.on('change',function () {
        fillSpecificDayConfigForm();
    });

    // disable/enable form input when click holiday button
    $isHoliday.on('change', function () {
        if($(this).is(':checked')) {
            $(this).parent().css({"background-color": "red", "color" : "white" });
            $startWorkingTime
                .add($startBreakTime)
                .add($endBreakTime)
                .add($endWorkingTime)
                .add($slotPerFrame)
                .add($slotTimeUnit)
                .prop("disabled", true);
            $btnSubmitSpecificConfig.add($isHoliday).prop("disabled", isBeforeToday($chooseActiveDay.val()));

            $formSpecificDayWorkingTimeConfig.valid();
            $('input').removeClass('error-message');
        }
        else {
            $(this).parent().css({"background-color": "white", "color" : "black" });
            $startWorkingTime
                .add($startBreakTime)
                .add($endBreakTime)
                .add($endWorkingTime)
                .add($slotPerFrame)
                .add($slotTimeUnit)
                .add($btnSubmitSpecificConfig)
                .add($isHoliday)
                .prop("disabled", isBeforeToday($chooseActiveDay.val()));
        }
    });


    let fillSpecificDayConfigForm = function() {
        let chosenDay = $chooseActiveDay.val();
        $.ajax({
            url : "/api/v1/service_department/" + serviceDepartmentId + "/working_time_config/findByActiveDay?activeDay=" + chosenDay,
            type : "post",
            async: false
        }).done(function (specificConfig) {
            let commonWorkOffDay;
            let dayHasSpecificConfig;
            if (specificConfig !== '') {
                $idSpecificDayConfig.val(specificConfig.id);
                $startWorkingTime.val(specificConfig.startWorkingTime);
                $endWorkingTime.val(specificConfig.endWorkingTime);
                $startBreakTime.val(specificConfig.startBreakTime);
                $endBreakTime.val(specificConfig.endBreakTime);
                $slotPerFrame.val(specificConfig.slotPerFrame);
                $slotTimeUnit.val(specificConfig.slotTimeUnit);
                $staffChoiceType.val(specificConfig.staffChoiceType);
                $isHoliday.prop("checked", specificConfig.holiday);
                commonWorkOffDay = specificConfig.workOffFixed;
                dayHasSpecificConfig = specificConfig.activeDay;
            }
            $isHoliday.change();
            bookingManagement.getBookingDataAndRenderTable(chosenDay, isWorkOffDayAndNoSpecificDayConfig(commonWorkOffDay, dayHasSpecificConfig));
        });
    };

    //validate form
    $formSpecificDayWorkingTimeConfig.validate({
        errorElement: "p",
        errorClass: "error-message",
        errorPlacement: function(error, element) {
            error.insertAfter(element);
        },
        ignore: [':disabled'],
        rules: {
            start_working_time: {
                required: true,
                validateStartWorkingTime: true
            },
            end_working_time: {
                required: true,
                validateEndWorkingTime: true
            },
            start_break_time: {
                required: true,
                validateStartBreakTime: true
            },
            end_break_time: {
                required: true,
                validateEndBreakTime: true
            },
            frame_slot: {
                required: true,
                minStrict: 0
            },
            work_off: {
                required: true,
            }
        },
        messages: {
            slot_time_unit: {
                required: "Vui lòng nhập 1 giá trị"
            },
            slot_per_frame: {
                required: "Vui lòng nhập 1 giá trị"
            },
        },
        onfocusout: function(element){
            if ($(element).is($startBreakTime) || $(element).is($startWorkingTime)
                || $(element).is($endBreakTime) || $(element).is($endWorkingTime)) {
                $startWorkingTime.add($endWorkingTime).add($startBreakTime).add($endBreakTime).valid();
            }
        }
    });

    // submit form
    $btnSubmitSpecificConfig.click(function (e) {
        if($staffChoiceType.val() === ''){
            window.alert.show("error", "Vui lòng cài đặt cấu hình chung trước!", 2000);
            return;
        }

        if($formSpecificDayWorkingTimeConfig.valid()){
            submitForm(e);
        }
    });

    let getSpecificConfigInfo = function(){
        return {
            id              : $idSpecificDayConfig.val(),
            startWorkingTime: $startWorkingTime.val(),
            endWorkingTime  : $endWorkingTime.val(),
            startBreakTime  : $startBreakTime.val(),
            endBreakTime    : $endBreakTime.val(),
            slotPerFrame    : $slotPerFrame.val(),
            slotTimeUnit    : $slotTimeUnit.val(),
            typeConfig      : "SPECIFIC_DAY_CONFIG", // 1: common, 2:specific
            holiday         : $isHoliday.is(':checked'),
            activeDay       : moment(new Date($chooseActiveDay.val())).format("YYYY/MM/DD HH:mm"),
            staffChoiceType : $staffChoiceType.val(),
            workOffFixed    : $workOffDay.val() 
        }
    };

    let submitForm = function(e){
        e.preventDefault();
        let formData = new FormData();
        formData.append("configInfo", new Blob([JSON.stringify(getSpecificConfigInfo())], {
            type: "application/json"
        }));
        formData.append("storeAuthId", $("#store_id").val());
        $.ajax({
            type: "POST",
            url: "/api/v1/service_department/" + $("#service_department_id").val() + "/working_time_config/save",
            data: formData,
            processData: false,
            contentType: false,
            headers: {
                "Content-Type": undefined
            },
            beforeSend: function () {
                window.loader.show();
            },
            success: function (response) {
                window.loader.hide();
                if (response !== "SAVE_SUCCESS") {
                    window.alert.show("error", "Có lỗi xảy ra. Vui lòng thử lại !", "1500");
                } else {
                    window.alert.show("success", "Thành công", 2000);
                    setTimeout(function(){
                        fillSpecificDayConfigForm();
                    },1000);
                }
            }
        });
        return false;
    };

    function isWorkOffDayAndNoSpecificDayConfig(commonWorkOffDay, dayHasSpecificConfig){
        if ((moment(new Date(dayHasSpecificConfig)).day() + 1) === commonWorkOffDay){
            return false;
        }
        return (moment(new Date($chooseActiveDay.val())).day() + 1) === commonWorkOffDay;
    }

    fillSpecificDayConfigForm();
});
