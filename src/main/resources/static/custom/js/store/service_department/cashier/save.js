$(document).ready(function () {
    let serviceDepartmentId = $("#service_department_id").val();
    let $formStaffAccount = $('#form_staff_account');
    let $userName = $("#name");
    let $userPassword = $("#password");
    let $userEmail = $("#email");
    let $userPhone = $("#phone");
    let $repeatPassword = $("#repeat_password");
    let $modalStaffAccount = $("#modal_staff_account");

    let rulesValidate = {
        name: {
            notEmpty: true,
            maxlength: 100
        },
        email: {
            notEmpty: true,
            email: true,
            maxlength: 100,
            checkEmailHasBeenUsed: ["/api/v1/system/system_account/checkEmailHasBeenUsed?email="]
        },
        phone: {
            onlyNumber: true,
            maxlength: 100
        },
        password: {
            notEmpty: true,
            maxlength: 32,
            minlength: 8
        },
        repeat_password: {
            notEmpty: true,
            equalTo: "#password"
        }
    };

    let messages = {
        email: {
            email: "Vui lòng nhập đúng địa chỉ email"
        },
        repeat_password: {
            equalTo: "Vui lòng nhắc lại đúng mật khẩu vừa nhập"
        },
        password: {
            maxlength: "Vui lòng nhập dưới 32 ký tự",
            minlength: "Vui lòng nhập ít nhất 8 ký tự"
        },
    };

    let validator = configValidate($formStaffAccount, rulesValidate, [], messages);

    $("#btn_add_staff").click(function () {
        $userName
            .add($userName)
            .add($userPassword)
            .add($repeatPassword)
            .add($userEmail)
            .add($userPhone)
            .val('').removeClass('error-message');
        validator.resetForm();
    });


    $("#btn_submit").click(function (e) {
        if ($formStaffAccount.valid()) {
            submitForm(e)
        }
    });

    let submitForm = function (e) {
        e.preventDefault();
        let formData = new FormData();
        formData.append("email", $userEmail.val());
        formData.append('name', $userName.val());
        formData.append('phone', $userPhone.val());
        formData.append("enableServe", "true");
        formData.append('password', $userPassword.val());
        formData.append('status', "ACTIVE");
        $.ajax({
            type: "POST",
            url: "/api/v1/service_department/" + serviceDepartmentId + "/cashier/save",
            data: formData,
            beforeSend: function () {
                window.loader.show();
            },
            processData: false,
            contentType: false,
        }).done(function (response) {
            saveObjectResponseHandleOnDone(response, $modalStaffAccount, staffTable);
        }).fail(function (response) {
            saveObjectResponseHandleOnFail(response, $modalStaffAccount);
        });
    };

});
