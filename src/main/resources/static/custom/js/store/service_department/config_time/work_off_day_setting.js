$(document).ready(function () {
    let storeId = $("#store_id").val();
    let serviceDepartmentId = $("#service_department_id").val();

    new Vue({
        el: "#work_off_day_setting_div",
        data: {
            listWorkOffDaySetting: [],
            currentSelectedToDeleteId: ''
        },
        methods: {
            createWorkOffDaySetting() {
                let newWorkOffDaySetting = {
                    id: '',
                    workOffFixed: "NOT_CONFIG",
                    startActiveRange: moment(new Date()).format("YYYY/MM/DD"),
                    endActiveRange: '9999/12/31',
                    typeConfig: "COMMON_CONFIG"
                };
                this.listWorkOffDaySetting.push(newWorkOffDaySetting);
            },
            loadListWorkOffSettingFromDB() {
                let self = this;
                jQuery.get("/api/v1/web/" + serviceDepartmentId + "/work_off_day_setting/listWorkOffDaySetting", function (response) {
                    let listWorkOffDaySetting = response.data;
                    if (listWorkOffDaySetting != null) {
                        self.listWorkOffDaySetting = listWorkOffDaySetting;
                    }
                });
            },
            submitWorkOffSettingForm(indexOfForm) {
                let self = this;
                if ($("#work_off_day_setting_form_" + indexOfForm).valid()) {
                    $.ajax({
                        type: "POST",
                        url: "/api/v1/web/" + serviceDepartmentId + "/work_off_day_setting/saveSetting",
                        data: JSON.stringify(self.listWorkOffDaySetting[indexOfForm]),
                        contentType: "application/json",
                        beforeSend: function () {
                            window.loader.show();
                            $(".btn-save-work-off-setting").each(function () {
                                $(this).prop("disabled", true);
                            });
                        }
                    }).then(function (response) {
                        $(".btn-save-work-off-setting").each(function () {
                            $(this).prop("disabled", false);
                        });
                        window.loader.hide();
                        handleResponseOnSaving(response.status.code, self.loadListWorkOffSettingFromDB);
                    });
                }
            },
            confirmDeleteWorkOffSetting(indexOfForm) {
                let self = this;
                let workOffDaySetting = self.listWorkOffDaySetting[indexOfForm];
                if (workOffDaySetting.id === "") {
                    self.listWorkOffDaySetting.splice(indexOfForm, 1);
                } else {
                    self.currentSelectedToDeleteId = workOffDaySetting.id;
                    $("#modal_delete_work_off_setting").modal('show');
                }
            },
            submitDeleteWorkOff() {
                let self = this;
                if (self.currentSelectedToDeleteId === '') {
                    return;
                }
                $.ajax({
                        type: "POST",
                        url: "/api/v1/web/" + serviceDepartmentId + "/work_off_day_setting/deleteSetting?settingId=" + self.currentSelectedToDeleteId,
                        beforeSend: function () {
                            window.loader.show();
                        }
                    }
                ).then(function (response) {
                    $('#modal_delete_work_off_setting').modal('hide');
                    window.loader.hide();
                    handleResponseOnDeleting(response.status.code, self.loadListWorkOffSettingFromDB);
                })
            }
        },
        mounted() {
            let self = this;
            self.loadListWorkOffSettingFromDB();

        },
        updated() {
            let self = this;

            $(".form-work-off_day-setting").each(function () {
                // attach to all form elements on page
                let $currentWorkOffDaySettingForm = $(this);
                let indexOfForm = $(this).attr("id").replace("work_off_day_setting_form_", "");
                let currentSetting = self.listWorkOffDaySetting[indexOfForm];

                configDateShowOnlyDay("work_off_setting_start_active_range_" + indexOfForm);
                configDateShowOnlyDay("work_off_setting_end_active_range_" + indexOfForm);

                let $startActiveRange = $("#work_off_setting_start_active_range_" + indexOfForm);
                let $endActiveRange = $("#work_off_setting_end_active_range_" + indexOfForm);

                $startActiveRange.val(currentSetting.startActiveRange);
                $endActiveRange.val(currentSetting.endActiveRange.includes("9999/12/31") ? "" : currentSetting.endActiveRange);

                $startActiveRange.on('hide.daterangepicker', function () {
                    currentSetting.startActiveRange = $(this).val();
                    $currentWorkOffDaySettingForm.valid();
                });

                $endActiveRange.on('hide.daterangepicker', function () {
                    currentSetting.endActiveRange = $(this).val();
                    $currentWorkOffDaySettingForm.valid();
                }).on('cancel.daterangepicker', function() {
                    $endActiveRange.val('');
                    currentSetting.endActiveRange = "9999/12/31";
                });

                $currentWorkOffDaySettingForm.validate({
                    errorElement: "p",
                    errorClass: "error-message",
                    errorPlacement: function (error, element) {
                        error.insertAfter(element);
                    },
                    ignore: [],
                    rules: {
                        work_off: {
                            required: true,
                        },
                        start_active_range: {
                            required: true,
                            validateActiveTimeNotOverlapWorkOffSetting: true,
                            validateDateRangeWorkOffSetting: true,
                        },
                        end_active_range: {
                            validateActiveTimeNotOverlapWorkOffSetting: true,
                            validateDateRangeWorkOffSetting: true,
                        }
                    },
                });


                $.validator.addMethod('validateActiveTimeNotOverlapWorkOffSetting', function (value, element) {
                    let currentFormIndex = $(element).attr('data');
                    let currentSetting = self.listWorkOffDaySetting[currentFormIndex];
                    return validateDateOverlapMethod(currentSetting, self.listWorkOffDaySetting, currentFormIndex);
                }, "Ngày tháng không được trùng lặp");

                $.validator.addMethod('validateDateRangeWorkOffSetting', function (value, element) {
                    let currentSetting = self.listWorkOffDaySetting[$(element).attr('data')];
                    let startRangeFromFormToTime = new Date(currentSetting.startActiveRange).getTime();
                    let endRangeFromFormToTime = new Date(currentSetting.endActiveRange).getTime();
                    return startRangeFromFormToTime < endRangeFromFormToTime;
                }, "Ngày bắt đầu phải trước ngày kết thúc");

            });
        },
    })
});
