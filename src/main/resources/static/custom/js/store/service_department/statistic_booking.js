$(document).ready(function () {
    let serviceDepartmentId = $("#service_department_id").val();

    new Vue({
        el: "#statistic_booking_div",
        data: {
            listCategory: [],
            typeStatistic: "NUMBER_BOOKING", //TOTAL_MONEY
            listCategoryIdForTypeNumberBooking: ['0'],
            listCategoryIdForTypeMoney: [],
            statisticInfo: {},
            startBookingTimeRange: moment().startOf('month').format("YYYY/MM/DD"),
            endBookingTimeRange: moment().endOf('month').format("YYYY/MM/DD"),
            typeChart: 'LINE_CHART',
            listCategoryStatisticInfo: []
        },
        computed: {
            isTypeStatisticNumberBooking() {
                return this.typeStatistic === "NUMBER_BOOKING";
            }
        },
        methods: {
            loadListCategory() {
                let self = this;
                jQuery.get("/api/v1/service_department/" + serviceDepartmentId + "/categoryBooking/findForStatisticBooking", function (response) {
                    self.listCategory = response.data;
                });
            },
            sumArray(categoryStatisticInfo) {
                let sum = 0;
                categoryStatisticInfo.data.forEach(function (categoryStatisticInfoPerDay) {
                    sum += categoryStatisticInfoPerDay[1]; //number of day
                });

                if (categoryStatisticInfo.name.includes("Tổng tiền") || categoryStatisticInfo.name.includes(('số tiền'))) { //format money
                    return this.formatMoney(sum) + " VND";
                } else {
                    return sum;
                }

            },
            formatMoney(money) {
                if (money !== undefined) {
                    return money.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
                } else {
                    return 0;
                }
            },
            formatSumOfArrayMoney(arrayNumberMoney) {
                return this.formatMoney(this.sumArray(arrayNumberMoney));
            },

            decreaseMonth() {
                this.startBookingTimeRange = moment(new Date(this.startBookingTimeRange)).add(-1, 'M').format("YYYY/MM/DD");
                let currentStartBookingDate = new Date(this.startBookingTimeRange);
                let startDayOfMonthAfterDecrease = moment(new Date(currentStartBookingDate.getFullYear(), currentStartBookingDate.getMonth(), 1)).format("YYYY/MM/DD");
                let endDayOfMonthAfterDecrease = moment(new Date(currentStartBookingDate.getFullYear(), currentStartBookingDate.getMonth() + 1, 0)).format("YYYY/MM/DD");

                this.startBookingTimeRange = startDayOfMonthAfterDecrease;
                this.endBookingTimeRange = endDayOfMonthAfterDecrease;
                $("#start_range_time_statistic").val(startDayOfMonthAfterDecrease);
                $("#end_range_time_statistic").val(endDayOfMonthAfterDecrease);

            },

            increaseMonth() {
                this.endBookingTimeRange = moment(new Date(this.endBookingTimeRange)).add(1, 'M').format("YYYY/MM/DD");

                let currentEndBookingDate = new Date(this.endBookingTimeRange);
                let startDayOfMonthAfterIncrease = moment(new Date(currentEndBookingDate.getFullYear(), currentEndBookingDate.getMonth(), 1)).format("YYYY/MM/DD");
                let endDayOfMonthAfterIncrease = moment(new Date(currentEndBookingDate.getFullYear(), currentEndBookingDate.getMonth() + 1, 0)).format("YYYY/MM/DD");

                this.startBookingTimeRange = startDayOfMonthAfterIncrease;
                this.endBookingTimeRange = endDayOfMonthAfterIncrease;
                $("#start_range_time_statistic").val(startDayOfMonthAfterIncrease);
                $("#end_range_time_statistic").val(endDayOfMonthAfterIncrease);
            },

            loadDataAndRenderChart() {
                let self = this;

                let listIdParam = [...self.listCategoryIdForTypeNumberBooking, ...self.listCategoryIdForTypeMoney];

                if (listIdParam.length === 0) {
                    window.alert.show("error", "Choose at least 1 for statistic", 3000);
                    return
                }
                let param = {
                    listCategoryIdStr: listIdParam.join(','),
                    typeStatistic: self.typeStatistic
                };
                if (self.startBookingTimeRange !== '') {
                    param.startBookingTimeRange = self.startBookingTimeRange;
                }
                if (self.endBookingTimeRange !== '') {
                    param.endBookingTimeRange = self.endBookingTimeRange;
                }

                let listNumberCategoryBookingName = [];
                $(".btn-choose-category.btn-select").each(function () {
                    listNumberCategoryBookingName.push($(this).text().trim());
                });

                window.loader.show();
                jQuery.get("/api/v1/service_department/" + serviceDepartmentId + "/statisticBooking/getInfo", param, function (response) {
                    window.loader.hide();
                    self.statisticInfo = response.data;
                    self.listCategoryStatisticInfo = [];

                    let listDayStatistic = self.statisticInfo.listDayStatistic;

                    Object.keys(self.statisticInfo.mapCategoryAndListNumberStatistic).forEach(function (categoryName) {
                        let listNumberBooking = [];
                        let listNumberMoney = [];
                        let categoryNameStatistics = self.statisticInfo.mapCategoryAndListNumberStatistic[categoryName];

                        let index = 0;
                        listDayStatistic.forEach(function (dayStatistic) {
                            let dayAndNumberBooking = [];
                            let dayAndNumberMoney = [];

                            dayAndNumberBooking.push(dayStatistic);
                            dayAndNumberMoney.push(dayStatistic);

                            if (categoryName === 'numberForDays') {
                                dayAndNumberBooking.push(categoryNameStatistics[0][index]);
                                dayAndNumberMoney.push(categoryNameStatistics[1][index]);
                            } else {
                                dayAndNumberBooking.push(categoryNameStatistics[index].numberBooking);
                                dayAndNumberMoney.push(categoryNameStatistics[index].numberMoney);
                            }
                            listNumberBooking.push(dayAndNumberBooking);
                            listNumberMoney.push(dayAndNumberMoney);
                            index++;

                        });

                        let categoryNameForStatisticMoney = categoryName === 'numberForDays' ? "Tổng tiền" : categoryName + " (số tiền)";
                        if (listNumberCategoryBookingName.indexOf(categoryNameForStatisticMoney) > -1) {
                            let statisticNumberMoneyDto = {
                                name: categoryNameForStatisticMoney,
                                data: listNumberMoney,
                                yAxis: 1
                            };
                            if (categoryName === 'numberForDays') {
                                self.listCategoryStatisticInfo.unshift(statisticNumberMoneyDto)
                            } else {
                                self.listCategoryStatisticInfo.push(statisticNumberMoneyDto)
                            }
                        }

                        let categoryNameForStatisticBooking = categoryName === 'numberForDays' ? "Số lịch đặt" : categoryName + " (số lượng)";
                        if (listNumberCategoryBookingName.indexOf(categoryNameForStatisticBooking) > -1) {
                            let statisticNumberBookingDto = {
                                name: categoryNameForStatisticBooking,
                                data: listNumberBooking,
                                dashStyle: 'shortdot',
                                yAxis: 0
                            };
                            if (categoryName === 'numberForDays') {
                                self.listCategoryStatisticInfo.unshift(statisticNumberBookingDto)
                            } else {
                                self.listCategoryStatisticInfo.push(statisticNumberBookingDto)
                            }
                        }

                    });

                    self.renderChart("container_chart_line", self.listCategoryStatisticInfo, "line");
                    self.renderChart("container_chart_column", self.listCategoryStatisticInfo, "column");
                });
            },
            renderChart(idContainer, listCategoryStatisticInfo, typeChart) {
                let self = this;

                let chart = Highcharts.chart(idContainer, {
                    dateRangeGrouping: true,
                    title: {
                        text: ''
                    },
                    yAxis: [
                        {
                            title: {
                                text: "Số lịch đặt"
                            },
                            min: 0
                        },
                        {
                            title: {
                                text: "Tổng tiền"
                            },
                            opposite: true,
                            min: 0
                        }
                    ],
                    chart: {
                        type: typeChart,
                    },
                    xAxis: {
                        categories: self.statisticInfo.listDayStatistic,
                    },

                    legend: {
                        layout: 'vertical',
                        align: 'right',
                        verticalAlign: 'middle'
                    },
                    series: listCategoryStatisticInfo,
                    responsive: {
                        rules: [{
                            condition: {
                                maxWidth: 500
                            },
                            chartOptions: {
                                legend: {
                                    layout: 'horizontal',
                                    align: 'center',
                                    verticalAlign: 'bottom'
                                }
                            }
                        }]
                    },
                    lang: {
                        noData: "Không có dữ liệu"
                    },
                    noData: {
                        style: {
                            fontWeight: 'bold',
                            fontSize: '23px',
                            color: '#303030'
                        }
                    },
                }, function () {
                    $("._hDateRangeGrouping ").each(function () {
                        if ($(this).text() === '日') {
                            $(this).text("Ngày")
                        }
                        if ($(this).text() === '週') {
                            $(this).text("Tuần")
                        }
                        if ($(this).text() === '月') {
                            $(this).text("Tháng")
                        }
                    })
                });
            },
        },
        mounted() {
            let vm = this;

            vm.loadListCategory();
            vm.loadDataAndRenderChart();

            let $startRangeTimeStatistic = $("#start_range_time_statistic");
            let $endRangeTimeStatistic = $("#end_range_time_statistic");

            configDateShowOnlyDay("start_range_time_statistic");
            configDateShowOnlyDay("end_range_time_statistic");

            $endRangeTimeStatistic.val(vm.endBookingTimeRange);
            $startRangeTimeStatistic.val(vm.startBookingTimeRange);

            $startRangeTimeStatistic.on('hide.daterangepicker', function () {
                vm.startBookingTimeRange = $(this).val();
            }).on('cancel.daterangepicker', function () {
                $startRangeTimeStatistic.val('');
                vm.startBookingTimeRange = "";
            });

            $endRangeTimeStatistic.on('hide.daterangepicker', function () {
                vm.endBookingTimeRange = $(this).val();
            }).on('cancel.daterangepicker', function () {
                $endRangeTimeStatistic.val('');
                vm.endBookingTimeRange = "";
            });

        }
    });

});
