let serviceDepartmentTable;
$(document).ready(function () {
    let storeAuthId = $("#store_auth_id").val();
    let deleteServiceDepartmentId;
    let $deleteServiceDepartmentModal = $("#delete_service_department_modal");

    let configServiceDepartmentTableObject = {
        columnDefinitions: [
            {
                "data": "imageUrl",
                "orderable": true,
                "defaultContent": noDataHtml,
                "class": 'text-center '
            },
            {
                "data": "name",
                "orderable": false,
                "defaultContent": noDataHtml,
                "class": 'text-center '
            },
            {
                "data": "serviceTypeName",
                "orderable": false,
                "defaultContent": noDataHtml,
                "class": 'text-center '
            },
            {
                "data": "createdTime",
                "orderable": true,
                "defaultContent": noDataHtml,
                "class": 'text-center '
            },
            {
                "data": null,
                "orderable": false,
                "defaultContent": noDataHtml,
                "class": 'text-center '
            }
        ],
        columnRender: [
            {
                "render": function (data) {
                    if (data === null) {
                        return noDataHtml;
                    }
                    return '<img style="width: 50px;height: 50px;" src="' + data + '"/>';
                },
                "targets": 0
            },
            {
                "render": function (data) {
                    return '<a style="margin-right: 5px;"class="btn btn-sm btn-primary" href="/service_department/' + data.authId + '/booking">Quản lý lịch đặt</a>' +
                        '<button class="btn btn-danger btn-sm btn-delete-service-department" id="' + data.authId + '" ' +
                        '     data-toggle="modal" data-target="#delete_service_department_modal" >Xóa</button>';
                },
                "targets": 4
            }
        ],
        drawCallbackFunction: function () {
        },
        sortField: {column: 3, typeDir: "desc"},
        url: '/api/v1/store/' + storeAuthId + '/service_department/list'
    };

    serviceDepartmentTable = configDataTableServerSide(configServiceDepartmentTableObject, $("#table_service_department"));

    $(document).on('click', ".btn-delete-service-department", function () {
        deleteServiceDepartmentId = $(this).attr('id');
    });

    $(document).on('click', "#btn_submit_delete", function () {
        $.ajax({
            type: "POST",
            url: '/api/v1/store/' + storeAuthId + '/service_department/delete/' + deleteServiceDepartmentId,
            beforeSend: function () {
                window.loader.show();
            }
        }).done(function(response) {
            deleteObjectResponseHandleOnDone(response, $deleteServiceDepartmentModal, serviceDepartmentTable);
        }).fail(function(response) {
            deleteObjectResponseHandleOnFail(response, $deleteServiceDepartmentModal);
        })
    });
});
