let imageTable;
$(document).ready(function () {
    let deleteImageId;
    let $deleteImageModal = $("#delete_image_modal");
    let serviceDepartmentId = $("#service_department_id").val();

    let configImageTableObject = {
        columnDefinitions: [
            {
                "data": "imageUrl",
                "orderable": true,
                "defaultContent": "<i>" + "Không có dữ liệu" + "</i>",
                "class": 'text-center '
            },
            {
                "data": "title",
                "orderable": true,
                "defaultContent": "<i>" + "Không có dữ liệu" + "</i>",
                "class": 'text-center '
            },
            {
                "data": "description",
                "orderable": false,
                "defaultContent": "<i>" + "Không có dữ liệu" + "</i>",
                "class": 'text-center '
            },
            {
                "data": "createdTime",
                "orderable": true,
                "defaultContent": "<i>" + "Không có dữ liệu" + "</i>",
                "class": 'text-center '
            },
            {
                "data": null,
                "orderable": false,
                "defaultContent": "<i>" + "Không có dữ liệu" + "</i>",
                "class": 'text-center '
            }
        ],
        columnRender: [
            {
                "render": function (data) {
                    if (data === null) {
                        return "<i>Không có dữ liệu</i>";
                    }
                    return '<img style="width: 50px;height: 50px;" src="' + data + '"/>';
                },
                "targets": 0
            },
            {
                "render": function (data) {
                    return '<button style="margin-right: 5px;"class="btn btn-sm btn-primary btn-update-image" id="' + data.id + '" ' +
                        '     data-toggle="modal" data-target="#modal_image" >Chi tiết</button>' +
                        '<button class="btn btn-danger btn-sm btn-delete-image" id="' + data.id + '" ' +
                        '     data-toggle="modal" data-target="#delete_image_modal" >Xóa</button>';
                },
                "targets": 4
            }
        ],
        drawCallbackFunction: function () {
        },
        sortField: {column: 3, typeDir: "desc"},
        url: "/api/v1/service_department/" + serviceDepartmentId + "/image/list"
    };

    imageTable = configDataTableServerSide(configImageTableObject, $("#table_image_service"));

    $(document).on("click", ".btn-delete-image", function () {
        deleteImageId = $(this).attr('id');
    });

    $(document).on("click", "#btn_submit_delete", function () {
        $.ajax({
            type: "POST",
            url: '/api/v1/service_department/' + serviceDepartmentId + '/image/delete/' + deleteImageId,
            beforeSend: function () {
                window.loader.show();
            }
        }).done(function (response) {
            deleteObjectResponseHandleOnDone(response, $deleteImageModal, imageTable);
            window.loader.hide();
            if(response.status.code === 201) {
                $deleteImageModal.modal("hide");
                window.alert.show("success", "Thành công", "1500");
                setTimeout(function (){
                    imageTable.ajax.reload();
                }, 1000);
            } else {
                window.alert.show("error", "Có lỗi xảy ra. Vui lòng thử lại !", "1500");
            }
        }).fail(function (response) {
            deleteObjectResponseHandleOnFail(response, $deleteImageModal);
        })
    });

});
