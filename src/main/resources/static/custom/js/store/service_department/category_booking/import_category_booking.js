$(document).ready(function () {
    let storeId = $("#store_id").val();
    let serviceDepartmentId = $("#service_department_id").val();


    let $importCategoryBookingButton = $("#import_category_btn");
    let $importCategoryBookingInput = $("#import_category_excel_input");
    let listCategoryBooking = [];
    let importTable;

    $importCategoryBookingButton.click(function () {
        $importCategoryBookingInput.click();
    });

    $importCategoryBookingInput.change(function () {
        let fileExtension = ["xlsx", 'xls'];
        if (this.files[0].length < 1) {
            return;
        }
        if ($.inArray($importCategoryBookingInput.val().split('.').pop().toLowerCase(), fileExtension) === -1) {
            window.alert.show("error", 'Chỉ nhận tệp Excel', 1500);
            return;
        }
        importFileConfig(this.files[0]);
        $importCategoryBookingInput.val(null);
        return false;
    });

    function importFileConfig(excelFile) {
        let formData = new FormData();
        formData.append("excelFile", excelFile);

        $.ajax({
            type: "POST",
            url: "/api/v1/service_department/" + serviceDepartmentId + "/categoryBooking/importCategory",
            data: formData,
            processData: false,
            contentType: false,
            beforeSend: function () {
                window.loader.show();
            },
        }).then(function (result) {
            window.loader.hide();
            console.log(result);
            if (result.data == null || result.data.length === 0) {
                window.alert.show("error", 'File Excel trống', 1500);
            } else {
                listCategoryBooking = result.data.listCategoryBooking;
                let listCategoryBookingError = result.data.listCategoryBookingError;
                showListErrorOnTable(listCategoryBookingError);
                showResultOnImportTable(listCategoryBooking);
                $("#modal_category_booking_import").modal("show");
            }
        })
    }

    function showListErrorOnTable(listCategoryBookingError) {
        let isHasError = listCategoryBookingError.length > 0;
        $("#text_error_category_booking").toggleClass('hidden', !isHasError);

        if (!isHasError) {
            return;
        }
        let textError = '<span>Dưới đây là những dữ liệu không hợp lệ. Chỉ có những dữ liệu hợp lệ mới hiển thị trong bảng bên dưới</span><br/>';
        textError += "Tên dịch vụ: ";
        textError += listCategoryBookingError.join(", ");
        $("#text_error_category_booking").html(textError);
    }

    function showResultOnImportTable(listCategoryBookingFromImport) {
        if (importTable != null) {
            importTable.destroy();
        }

        let html = '';
        for (let i = 0; i < listCategoryBookingFromImport.length; i++) {
            let categoryBooking = listCategoryBookingFromImport[i];
            html += '<tr>' +
                '<td>' + categoryBooking.categoryName + '</td>' +
                '<td>' + categoryBooking.numberFrameConsecutiveAutoFill + '</td>' +
                '<td>' + categoryBooking.moneyExcludeTax + '</td>' +
                '</tr>';
        }
        $('#table_category_booking_import tbody tr').remove();
        $('#table_category_booking_import tbody').append(html);

        importTable = $('#table_category_booking_import').DataTable({
            "language": {"url": "/libs/datatable/js/vi.json"},
            "lengthMenu": [[10, 20, 50], [10, 20, 50]
            ],
            "searching": false,
        });
    }

    $(document).on("click", "#btn_submit_import_category_booking", function () {
        if (listCategoryBooking.length === 0) {
            $("#modal_config_import").modal("hide");
            window.alert.show("error", "Tệp Excel không có dữ liệu", 2000);
            return;
        }
        $.ajax({
            type: "POST",
            url: "/api/v1/service_department/" + serviceDepartmentId + "/categoryBooking/submitImport",
            data: JSON.stringify(listCategoryBooking),
            processData: false,
            contentType: 'application/json',
            beforeSend: function () {
                window.loader.show();
            }
        }).done(function (response) {
            $("#modal_category_booking_import").modal("hide");
            window.loader.hide();
            if (response.status.code === 200) {
                window.alert.show("success", "Thành công", 2000);
                setTimeout(function () {
                    form_add_category.loadListCategoryBookingFromDB();
                }, 3000);
            } else {
                window.alert.show("error", "Đã có lỗi xảy ra, vui lòng kiểm tra lại", 2000);
            }
        }).fail(function () {
            window.loader.hide();
            window.alert.show("error", "Đã có lỗi xảy ra, vui lòng kiểm tra lại", 2000);
        })
    });
});
