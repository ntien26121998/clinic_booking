$(document).ready(function () {

    let $storeSelect =$("#store_select");

    let $formServiceType = $('#form_service_type');
    let $serviceTypeId = $("#service_type_id");
    let $serviceTypeName = $("#service_type_name");
    let $modalServiceType = $("#modal_service_type");
    let rulesValidate = {
        service_type_name: {
            notEmpty: true,
            maxlength: 500
        }
    };
    let validator = configValidate($formServiceType, rulesValidate);

    $("#btn_add_service_type").click(function () {
        $serviceTypeId.add($serviceTypeName).val('').removeClass('error-message');
        validator.resetForm();
    });

    $(document).on('click', ".btn-edit-service-type", function () {
        let $serviceTypeId = $(this).attr("id");
        fillServiceTypeInfoToForm($serviceTypeId);
    });

    $("#btn_submit").click(function (e) {
        if ($formServiceType.valid()) {
            submitForm(e);
        }
    });

    let submitForm = function (e) {
        e.preventDefault();
        let formData = new FormData();
        formData.append("id", $serviceTypeId.val());
        formData.append('name', $serviceTypeName.val());
        formData.append("storeId" , $storeSelect.val());

        $.ajax({
            type: "POST",
            url: "/api/v1/system/service_type/save",
            data: formData,
            beforeSend: function () {
                window.loader.show();
            },
            processData: false,
            contentType: false,
        }).done(function (response) {
            window.loader.hide();
            if(response.status.code === 202) {
                $modalServiceType.modal("hide");
                window.alert.show("success", "Thành công", "1500");
                setTimeout(function (){
                    serviceTypeTable.ajax.reload();
                }, 1000);
            } else {
                window.alert.show("error", "Có lỗi xảy ra. Vui lòng thử lại !", "1500");
            }
        }).fail(function (response) {
            saveObjectResponseHandleOnFail(response, $modalServiceType)
        });
    };

    let fillServiceTypeInfoToForm = function (serviceTypeId) {
        jQuery.get("/api/v1/system/service_type/" + serviceTypeId, function (serviceType) {
            $serviceTypeId.val(serviceType.id);
            $serviceTypeName.val(serviceType.name);
        });
    };
});
