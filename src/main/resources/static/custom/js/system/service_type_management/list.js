let serviceTypeTable;
$(document).ready(function () {
    let deleteServiceTypeId;
    let $deleteServiceTypeModal = $("#delete_service_type_modal");
    let $storeSelect = $("#store_select");
    let companyId = 1;

    let configServiceTypeTableObject = {
        columnDefinitions: [
            {
                "data": "name",
                "orderable": true,
                "defaultContent": noDataHtml,
                "class": 'text-center '
            },
            {
                "data": "createdTime",
                "orderable": true,
                "defaultContent": noDataHtml,
                "class": 'text-center '
            },
            {
                "data": "id",
                "orderable": false,
                "defaultContent": noDataHtml,
                "class": 'text-center '
            }
        ],
        columnRender: [
            {
                "render": function (data) {
                    return '<button style="margin-right: 5px;" data-toggle="modal" ' +
                        'data-target="#modal_service_type" class="btn btn-primary btn-sm btn-edit-service-type" id="' + data + '">Chi tiết</button>' +
                        '<button class="btn btn-danger btn-sm btn-delete-service-type" id="' + data + '" ' +
                        '     data-toggle="modal" data-target="#delete_service_type_modal" >Xóa</button>';
                },
                "targets": 2
            }
        ],
        sortField: {column: 1, typeDir: "desc"},
        url: "/api/v1/system/service_type/list?",
        customFieldSearchSelector: $storeSelect,
    };

    serviceTypeTable = configDataTableServerSide(configServiceTypeTableObject, $("#table_service_type"));

    $(document).on('change', $storeSelect, function () {
        serviceTypeTable.ajax.reload();
    })

    $(document).on("click", ".btn-delete-service-type",function () {
        deleteServiceTypeId = $(this).attr('id');
    });

    $(document).on("click", "#btn_submit_delete", function () {
        $("#btn_submit_delete").addClass("disabled");
        $.ajax({
            type: "POST",
            url: '/api/v1/system/service_type/delete/' + deleteServiceTypeId,
            beforeSend: function () {
                window.loader.show();
            }
        }).done(function(response) {
            deleteObjectResponseHandleOnDone(response, $deleteServiceTypeModal, serviceTypeTable);
        }).fail(function(response) {
            deleteObjectResponseHandleOnFail(response, $deleteServiceTypeModal);
        });
    });

});
