$(document).ready(function () {

    let $formSystemAccount = $('#form_system_account');
    let $userName = $("#name");
    let $userPassword = $("#password");
    let $userEmail = $("#email");
    let $userPhone = $("#phone");
    let $repeatPassword = $("#repeat_password");
    let $modalSystemAccount = $("#modal_system_account");

    let rulesValidate = {
        name: {
            notEmpty: true,
            maxlength: 100
        },
        phone: {
            onlyNumber: true,
            maxlength: 100
        },
        email: {
            notEmpty: true,
            email: true,
            maxlength: 100,
            checkEmailHasBeenUsed: ["/api/v1/system/system_account/checkEmailHasBeenUsed?email="]
        },
        password: {
            notEmpty: true,
            maxlength: 32,
            minlength: 8
        },
        repeat_password: {
            notEmpty: true,
            equalTo: "#password"
        }
    };

    let messages = {
        password: {
            maxlength: "Vui lòng nhập dưới 32 ký tự",
            minlength: "Vui lòng nhập ít nhất 8 ký tự"
        },
        email: {
            email: "Vui lòng nhập đúng địa chỉ email"
        },
        repeat_password: {
            equalTo: "Vui lòng nhắc lại đúng mật khẩu vừa nhập"
        }
    };

    let validator = configValidate($formSystemAccount, rulesValidate, [], messages);

    $("#btn_add_account").click(function () {
        $userName
            .add($userName)
            .add($userPassword)
            .add($repeatPassword)
            .add($userEmail)
            .add($userPhone)
            .val('').removeClass('error-message');
        validator.resetForm();
    });


    $("#btn_submit").click(function (e) {
        if ($formSystemAccount.valid()) {
            submitForm(e)
        }
    });

    let submitForm = function (e) {
        e.preventDefault();
        let formData = new FormData();
        formData.append("email", $userEmail.val());
        formData.append('name', $userName.val());
        formData.append('password', $userPassword.val());
        formData.append('status', "ACTIVE");
        formData.append('phone', $userPhone.val());
        if ($(".file-input")[0].files.length !== 0) {
            formData.append("avatar", $("#image_sys_avatar")[0].files[0]);
        }
        $.ajax({
            type: "POST",

            url: "/api/v1/system/system_account/save",
            data: formData,
            beforeSend: function () {
                window.loader.show();
            },
            processData: false,
            contentType: false,
        }).done(function (response) {
            // saveObjectResponseHandleOnDone(response, $modalSystemAccount, systemAccountTable);
            window.loader.hide();
            if(response.status.code === 200) {
                $("#modal_system_account").modal("hide");
                window.alert.show("success", "Thành công", "1500");
                setTimeout(function (){
                    systemAccountTable.ajax.reload();
                }, 1000);
            } else {
                window.alert.show("error", "Có lỗi xảy ra. Vui lòng thử lại !", "1500");
            }
        }).fail(function (response) {
            saveObjectResponseHandleOnFail(response, $modalSystemAccount)
        });
    };

});
