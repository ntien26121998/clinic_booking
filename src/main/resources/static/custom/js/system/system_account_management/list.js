let systemAccountTable;
$(document).ready(function () {
    let deleteAccountId;
    let $deleteAccountModal = $("#delete_system_account_modal");

    let configSystemAccountTableObject = {
        columnDefinitions: [
            {
                "data": "name",
                "orderable": true,
                "defaultContent": noDataHtml,
                "class": 'text-center '
            },
            {
                "data": "email",
                "orderable": true,
                "defaultContent": noDataHtml,
                "class": 'text-center '
            },
            {
                "data": "phone",
                "orderable": false,
                "defaultContent": noDataHtml,
                "class": 'text-center '
            },
            {
                "data": "createdTime",
                "orderable": true,
                "defaultContent": noDataHtml,
                "class": 'text-center '
            },
            {
                "data": "status",
                "orderable": true,
                "defaultContent": noDataHtml,
                "class": 'text-center '
            },
            {
                "data": null,
                "orderable": false,
                "defaultContent": noDataHtml,
                "class": 'text-center '
            }
        ],
        columnRender: [
            {
                "render": function (status) {
                    if (status === 'ACTIVE') {
                        return '<label class="btn btn-sm btn-success">Hoạt động</label>';
                    }
                    return '<label class="btn btn-sm btn-secondary">Không hoạt động</label>'
                },
                "targets": 4
            },
            {
                "render": function (data) {
                    let checkedStatus = (data.status === 'ACTIVE' ? "checked" : "");
                    return '<label class="switch change-status" id="' + data.id + '" style="margin-right: 5px;">' +
                        '<input type="checkbox" +  ' + checkedStatus + '  />'+
                        '<span class="slider round"></span>' +
                        '</label>'+
                        '<button class="btn btn-danger btn-sm btn-delete-system-account" id="' + data.id + '" data-toggle="modal" data-target="#delete_system_account_modal" >Xóa</button>';
                },
                "targets": 5
            }
        ],
        drawCallbackFunction: function () {
        },
        sortField: {column: 3, typeDir: "desc"},
        url: "/api/v1/system/system_account/list"
    };

    systemAccountTable = configDataTableServerSide(configSystemAccountTableObject, $("#table_system_account"));

    $(document).on("click",".change-status",function () {
        let accountId = $(this).attr('id');
        console.log("vao day");
        $.ajax({
            type: "POST",
            url: "/api/v1/system/system_account/changeStatus/" + accountId,
        }).done(function (response) {
            if (response) {
                window.alert.show("success", "Thành công", "1500");
                setTimeout(function () {
                    systemAccountTable.ajax.reload();
                }, 1000);
            } else {
                window.alert.show("error", "Có lỗi xảy ra. Vui lòng thử lại !", "1500");
            }
        }).fail(function (response) {
            window.alert.show("error", "Có lỗi xảy ra. Vui lòng thử lại !", "1500");
        });
    });

    $(document).on("click",".btn-delete-system-account",function () {
        deleteAccountId = $(this).attr('id');
    });

    $(document).on("click", "#btn_submit_delete", function () {
        $.ajax({
            type: "POST",
            url: '/api/v1/system/system_account/delete/' + deleteAccountId,
            beforeSend: function () {
                window.loader.show();
            }
        }).done(function (response) {
            // deleteObjectResponseHandleOnDone(response, $deleteAccountModal, systemAccountTable);
            window.loader.hide();
            if(response.status.code === 200) {
                $("#delete_system_account_modal").modal("hide");
                window.alert.show("success", "Thành công", "1500");
                setTimeout(function (){
                    systemAccountTable.ajax.reload();
                }, 1000);
            } else {
                window.alert.show("error", "Có lỗi xảy ra. Vui lòng thử lại !", "1500");
            }
        }).fail(function (response) {
            deleteObjectResponseHandleOnFail(response, $deleteAccountModal);
        })
    });

});
