let storeTable;
$(document).ready(function () {
    let deleteStoreId;
    let $deleteStoreModal = $("#delete_store_modal");
    let $companyId = $("#company_id");

    let configStoreTableObject = {
        columnDefinitions: [
            {
                "data": "storeCode",
                "orderable": false,
                "defaultContent": noDataHtml,
                "class": 'text-center '
            },
            {
                "data": "storeName",
                "orderable": true,
                "defaultContent": noDataHtml,
                "class": 'text-center '
            },
            {
                "data": "phone",
                "orderable": false,
                "defaultContent": noDataHtml,
                "class": 'text-center '
            },
            {
                "data": "cityName",
                "orderable": false,
                "defaultContent": noDataHtml,
                "class": 'text-center '
            },
            {
                "data": "districtName",
                "orderable": false,
                "defaultContent": noDataHtml,
                "class": 'text-center '
            },
            {
                "data": "address",
                "orderable": false,
                "defaultContent": noDataHtml,
                "class": 'text-center '
            },
            {
                "data": "createdTime",
                "orderable": true,
                "defaultContent": noDataHtml,
                "class": 'text-center '
            },
            {
                "data": null,
                "orderable": false,
                "defaultContent": noDataHtml,
                "class": 'text-center '
            }
        ],
        columnRender: [
            {
                "render": function (data) {
                    return '<a style="margin-right: 5px;" class="btn btn-sm btn-primary" href="/store/' + data.storeAuthId + '/service_department">Quản lý mảng dịch vụ</a>' +
                        '<button style="margin-right: 5px;" class="btn btn-primary btn-sm btn-update-store" id="' + data.storeAuthId + '" ' +
                        '     data-toggle="modal" data-target="#modal_add_store" >Chi tiết</button>'+
                        '<button class="btn btn-danger btn-sm btn-delete-store" id="' + data.storeAuthId + '" ' +
                        '     data-toggle="modal" data-target="#delete_store_modal" >Xóa</button>';
                },
                "targets": 7
            }
        ],
        drawCallbackFunction: function () {

        },
        sortField: {column: 6, typeDir: "desc"},
        url: "/api/v1/system/store/list",
        customFieldSearchSelector: $companyId,
        "initComplete": function () {
            //add div to enable scroll X
            $("#company_table").wrap("<div class='table-responsive'></div>");
        },
    };

    storeTable = configDataTableServerSide(configStoreTableObject, $("#table_store"));

    $(document).on("click", ".btn-delete-store", function () {
        deleteStoreId = $(this).attr('id');
    });

    $(document).on("click", "#btn_submit_delete",function () {
        $.ajax({
            type: "POST",
            url: '/api/v1/system/store/delete/' + deleteStoreId,
            beforeSend: function () {
                window.loader.show();
            }
        }).done(function(response) {
            deleteObjectResponseHandleOnDone(response, $deleteStoreModal, storeTable);
        }).fail(function(response) {
            deleteObjectResponseHandleOnFail(response, $deleteStoreModal);
        })
    });
});
