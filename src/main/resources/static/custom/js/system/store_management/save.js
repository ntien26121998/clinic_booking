$(document).ready(function () {

    let $formSaveStore = $("#form_save_store");
    let $btnSubmit = $("#btn_submit");
    let $storeName = $("#store_name");
    let $storeAuthId = $("#store_auth_id");
    let $storePhone = $("#phone");
    let $storeAddress = $("#address");
    let $modalAddStore = $("#modal_add_store");
    let $citySelect = $("#city_select");
    let $districtName = $("#district_name");

    $("#btn_add_store").click(function () {
        $("#store_type_name").html($("#type_store_id option:selected").text());
        $("#title_modal_add_store").html("Thêm phòng khám");
        $storeAuthId.add($storeName).add($storePhone).add($storeAddress).add($districtName).val('');
    });

    $(document).on('click', '.btn-update-store', function () {
        // $("#store_type_name").html($("#type_store_id option:selected").text());
        $("#title_modal_add_store").html("Cập nhật thông tin phòng khám");
        let storeAuthId = $(this).attr("id");
        console.log(storeAuthId);
        jQuery.get("/api/v1/system/store/" + storeAuthId, function (store) {
            $storeAuthId.val(store.storeAuthId);
            $storeName.val(store.storeName);
            $storePhone.val(store.phone);
            $storeAddress.val(store.address);
            $citySelect.val(store.cityName);
            $districtName.val(store.districtName);
        });

    });

    $btnSubmit.click(function (e) {
        if($formSaveStore.valid()) {
            submitForm(e);
        }
    });

    let rules = {
        store_name: {
            notEmpty: true,
            maxlength: 500
        },
        phone: {
            onlyNumber: true,
            maxlength: 12
        },
        address: {
            notEmpty: true,
            maxlength: 500
        },
        districtName: {
            notEmpty: true,
            maxlength: 500
        }
    };
    configValidate($formSaveStore, rules, [], {});

    let submitForm = function (e) {
        e.preventDefault();
        let companyIdDefault = 1;
        let formData = new FormData();
        if($storeAuthId.val() !== '') {
            formData.append("storeAuthId", $storeAuthId.val());
        }
        formData.append("name", $storeName.val());
        formData.append('phone', $storePhone.val());
        formData.append('address', $storeAddress.val());
        formData.append('districtName', $districtName.val());
        formData.append('city', $citySelect.val());
        formData.append('companyId', 1);

        $.ajax({
            type: "POST",
            url: "/api/v1/system/store/save",
            data: formData,
            beforeSend: function () {
                window.loader.show();
            },
            processData: false,
            contentType: false,
        }).done(function (response) {
            window.loader.hide();
            if(response.status.code === 202) {
                $modalAddStore.modal("hide");
                window.alert.show("success", "Thành công", "1500");
                setTimeout(function (){
                    storeTable.ajax.reload();
                }, 1000);
            } else {
                window.alert.show("error", "Có lỗi xảy ra. Vui lòng thử lại !", "1500");
            }
        }).fail(function (response) {
            saveObjectResponseHandleOnFail(response, $modalAddStore)
        });
    };
});
