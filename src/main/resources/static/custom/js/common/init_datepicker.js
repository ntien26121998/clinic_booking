
let configDateShowOnlyTime = function (idElement) {
    $("#" + idElement).daterangepicker({
        timePicker : true,
        singleDatePicker:true,
        timePicker24Hour : true,
        timePickerIncrement : 1,
        locale: {
            format: 'HH:mm',
            cancelLabel: 'Hủy',
            applyLabel: 'Ok'
        }
    }).on('show.daterangepicker', function(ev, picker) {
        picker.container.find(".calendar-table").hide();
    })
};

let configDateShowOnlyDay = function (idElement) {
    let format = 'YYYY/MM/DD';
    $("#" + idElement).daterangepicker({
        timePicker: false,
        singleDatePicker: true,
        autoApply: true,
        showDropdowns: true,
        locale: {
            format: format,
            cancelLabel: 'Hủy',
            applyLabel: 'Ok'
        }
    })
};

let configDateShowOnlyDayWithoutAutoApply = function (idElement) {
    let format = 'YYYY/MM/DD';
    $("#" + idElement).daterangepicker({
        timePicker: true,
        singleDatePicker: true,
        autoApply: false,
        setDate: null,
        showDropdowns: true,
        locale: {
            format: format,
            cancelLabel: 'Xóa',
            applyLabel: 'Ok'
        }
    }).on('show.daterangepicker', function(ev, picker) {
        picker.container.find(".calendar-time").hide();
    })
};
let configDateShowOnlyDayWithMinDate = function (idElement) {
    let format = 'YYYY/MM/DD'
    $("#" + idElement).daterangepicker({
        timePicker: false,
        singleDatePicker: true,
        autoApply: true,
        showDropdowns: true,
        minDate: moment(new Date()),
        locale: {
            format: format,
            cancelLabel: 'Hủy',
            applyLabel: 'Ok'
        }
    })
};
