let noDataHtml = "<i>Không có dữ liệu</i>";

window.alert = {
    show: function (type, message, interval) {
        if (type === "error") {
            $("#alert").removeClass("alert-success");
            $("#alert").addClass("alert-danger");
            $("#alert .error-icon").removeClass("hidden");
            $("#alert .success-icon").addClass("hidden");
        } else if (type === "success") {
            $("#alert").removeClass("alert-danger");
            $("#alert").addClass("alert-success");
            $("#alert .error-icon").addClass("hidden");
            $("#alert .success-icon").removeClass("hidden");
        }
        $("#alert").removeClass("hidden");
        $("#alert_message").html(message);
        $("#alert").animate({
            top: "50px"
        }, "slow");

        setTimeout(function () {
            $("#alert").animate({
                top: "-120px"
            }, "slow");
        }, interval);
    }
};

window.loader = {
    show: function () {
        $("#loader").removeClass("hidden");
    },
    hide: function () {
        $("#loader").addClass("hidden");
    }
};

// validate method
$.validator.addMethod('notEmpty', function (value) {
    return value !== null && value !== undefined && value.trim() !== "";
}, 'Vui lòng nhập trường này' );

$.validator.addMethod("required_ckEditor",
    function (value, element) {
        let data = CKEDITOR.instances[$(element).attr("id")].getData();
        return $(data).text().trim() != "" && data !== null && data !== undefined;
    }, "Vui lòng nhập trường này");

// add method validate url include http/https using regex
$.validator.addMethod('validUrl', function(value, element) {
    let regex = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;
    return regex.test(value);
}, 'Vui lòng nhập đúng định dạng HTTP/HTTPS'); //

// custom validate method
$.validator.addMethod('onlyNumber', function (value, el) {
    let regExp = /^\d+$/
    return regExp.test(value)
}, 'Vui lòng chỉ nhập ký tự số' );

$.validator.addMethod('checkEmailHasBeenUsed', function(value, element, params) {
    let result = true;
    $.ajax({
        url : params[0] + value,
        type : "post",
        async: false,
        success : function(response) {
            result = ! response;
        }
    });
    return result;
}, 'Email đã được sử dung. Vui lòng nhập email khác');

// custom validate method
$.validator.addMethod('minStrict', function (inputValue, el, minValue) {
    return inputValue > minValue;
},  "Vui lòng chọn giá trị lớn hơn 0");

$.validator.addMethod('validateStartWorkingTime', function (timeWorkingStart) {
    return timeWorkingStart < $("#end_working_time").val()
        && timeWorkingStart < $("#start_break_time").val()
        && $("#end_working_time").val() > $("#end_break_time").val()
}, "Thời gian bắt đầu làm phải trước hơn thời gian nghỉ");

$.validator.addMethod('validateEndWorkingTime', function (timeWorkingEnd) {
    return timeWorkingEnd > $("#start_working_time").val()
        && timeWorkingEnd > $("#end_break_time").val()
        && $("#start_working_time").val() < $("#start_break_time").val() ;
}, "Thời gian kết thúc làm phải sau thời gian nghỉ");

$.validator.addMethod('validateStartBreakTime', function (timeBreakStart) {
    return timeBreakStart < $("#end_break_time").val()
        && timeBreakStart > $("#start_working_time").val()
        && $("#end_break_time").val() < $("#end_working_time").val();
}, "Thời gian bắt đầu nghỉ phải sau thời gian bắt đầu làm");

$.validator.addMethod('validateEndBreakTime', function (timeBreakEnd) {
    return timeBreakEnd > $("#start_break_time").val()
        && timeBreakEnd < $("#end_working_time").val()
        && $("#start_break_time").val() > $("#start_working_time").val();
}, "Thời gian kết thúc nghỉ phải sau thời gian bắt đầu nghỉ");

let isBeforeToday = function (day) {
    let startOfToDay = new Date();
    startOfToDay.setHours(0, 0, 0, 0);
    return new Date(day).getTime() < startOfToDay.getTime();
};

let isToday = function (day) {
    let startOfToDay = new Date();
    startOfToDay.setHours(0, 0, 0, 0);
    return new Date(day).getTime() === startOfToDay.getTime();
};

