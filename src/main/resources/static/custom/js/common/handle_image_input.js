let imageHandle = {};
$(document).ready(function () {
    let $fileUploadContent = $('.file-uploader-content');
    let $removeImageIcon = $('.remove-image-icon');
    let $imageFileInput = $(".file-input");
    let $imagePreview = $('.image-preview');

//handle image
    $fileUploadContent.on('click', function () {
        $imageFileInput.click();
    });

//render image after choosing
    function readURL(input) {
        if (input.files && input.files[0]) {
            let reader = new FileReader();
            let imageInputFile = input.files[0];

            reader.onload = function (e) {
                $imagePreview.attr('src', e.target.result).removeClass('hidden');
                $imagePreview.attr('title', imageInputFile.name);
                $fileUploadContent.addClass('hidden');
                $removeImageIcon.removeClass('hidden');
            };
            reader.readAsDataURL(imageInputFile);
        }
    }

    $imageFileInput.on('change', function () {
        let fileExtension = ['png', ".jpeg", 'jpg'];
        if (this.files[0].length < 1) {
            return;
        }

        if ($.inArray($imageFileInput.val().split('.').pop().toLowerCase(), fileExtension) === -1) {
            window.alert.show("error", "Vui lòng upload hình ảnh định dạng:.png, .jpeg, .jpg", 1500);
            return;
        }
        $(this).valid();
        readURL(this);
    });

    $removeImageIcon.on('click', function () {
        $imagePreview.attr('src', "").addClass('hidden');
        $imagePreview.attr('title', "");
        $fileUploadContent.removeClass('hidden');
        $(this).addClass("hidden");
        $imageFileInput.val(null);
    });

    imageHandle.isHasImage = function(){
        let srcImageValue = $imagePreview.attr("src");
        return srcImageValue !== undefined && srcImageValue !== null && srcImageValue.trim() !== "";
    };

    imageHandle.showImageWhenUrlNotNull = function (urlImage) {
        if (urlImage !== null) {
            $imagePreview.attr('src', urlImage).removeClass('hidden');
            $removeImageIcon.removeClass("hidden");
            $fileUploadContent.addClass('hidden');
        } else {
            $imagePreview.attr('src', "").addClass('hidden');
            $removeImageIcon.addClass("hidden");
            $fileUploadContent.removeClass('hidden');
        }
    }
});


