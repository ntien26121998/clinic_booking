
let saveObjectResponseHandleOnDone = function (response, $saveModal, dataTable) {
    window.loader.hide();
    if(response === 'SAVE_SUCCESS') {
        $saveModal.modal("hide");
        window.alert.show("success", "Thành công", "1500");
        setTimeout(function (){
            dataTable.ajax.reload();
        }, 1000);
    } else {
        window.alert.show("error", "Có lỗi xảy ra. Vui lòng thử lại !", "1500");
    }
};

let saveObjectResponseHandleOnFail = function (response, $saveModal) {
    window.loader.hide();
    $saveModal.modal("hide");
    window.alert.show("error", "Có lỗi xảy ra. Vui lòng thử lại !", "1500");
};

let deleteObjectResponseHandleOnDone = function (response, $deleteModal, dataTable){
    window.loader.hide();
    if(response === 'DELETE_SUCCESS') {
        $deleteModal.modal("hide");
        window.alert.show("success", "Xóa thành công", "1500");
        setTimeout(function (){
            dataTable.ajax.reload();
        }, 1000);
    } else {
        window.alert.show("error", "Có lỗi xảy ra. Vui lòng thử lại !", "1500");
    }
    $("#btn_submit_delete").removeClass("disabled");
};

let deleteObjectResponseHandleOnFail = function(response, $deleteModal) {
    window.loader.hide();
    $deleteModal.modal("hide");
    window.alert.show("error", "Có lỗi xảy ra. Vui lòng thử lại !", "1500");
    $("#btn_submit_delete").removeClass("disabled");
};
