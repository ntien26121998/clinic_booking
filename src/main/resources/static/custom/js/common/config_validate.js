let configValidate = function ($idElement, rules, ignoreElementArray, messages) {
    return $idElement.validate({
        errorElement: "p",
        errorClass: "error-message",
        errorPlacement: function (error, element) {
            error.insertAfter(element);
        },
        ignore: ignoreElementArray,
        rules: rules,
        messages: messages,
    });
};
