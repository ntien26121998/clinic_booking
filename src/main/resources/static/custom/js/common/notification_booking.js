let bookingNotificationNavBar = {};
$(document).ready(function () {
    let serviceDepartmentId = $("#service_department_id").val();
    let $notificationMenuIcon = $(".notification-menu");
    let $listNotificationDiv = $(".list-notification-div");
    let $listNotificationContent = $(".list-notification-content");
    let $numberUnreadNotificationSpan = $(".number-unread-notification");
    let $hasNoNotificationDiv  = $(".has-no-notification");
    let currentPage = 1;

    $notificationMenuIcon.click(function () {
        $listNotificationDiv.addClass('show');

        $.post("/api/v1/" + serviceDepartmentId + "/notification_booking/setLatestTimeReading", function (response) {
        })

    });

    $(document).mouseup(function (e) {
        if (!$notificationMenuIcon.is(e.target) && !$listNotificationDiv.is(e.target)
            && $listNotificationDiv.has(e.target).length === 0 && $listNotificationDiv.has(e.target).length === 0) {
            $numberUnreadNotificationSpan.addClass('hidden');
            $listNotificationDiv.removeClass("show");
        }
    });

    $listNotificationDiv.scroll(function () {
        if (Math.abs(this.scrollHeight - $(this).scrollTop() - $(this).innerHeight()) < 1){
            currentPage++;
            $("#loader_notification").show();
            bookingNotificationNavBar.getListNotification(currentPage);
        }
    });

    bookingNotificationNavBar.getListNotification = function(page) {
        $.get("/api/v1/" + serviceDepartmentId +"/notification_booking/list?page=" + page, function (response) {
            let numberUnreadNotification = response.numberUnread;
            let listNotification = response.listNotification;

            if (numberUnreadNotification > 0) {
                $numberUnreadNotificationSpan.text(numberUnreadNotification).removeClass("hidden");
            } else {
                $numberUnreadNotificationSpan.addClass('hidden');
            }

            let html = '';
            if(listNotification.length === 0) {
                if(page === 1) {
                    $hasNoNotificationDiv.removeClass("hidden");
                }
                currentPage--;
                $("#loader_notification").hide();
            } else {
                $hasNoNotificationDiv.addClass("hidden");
                listNotification.forEach(function (notification) {
                    html += '<div class="notification-detail">\n' +
                        '<p class="notification_content">' + notification.content + '</p>\n' +
                        '<i class="time-send-notification"><span class="fa fa-clock-o" style="margin-right: 5px"></span>' + notification.createdTime + '</i>\n' +
                        '</div>'
                });
                if (page === 1) {
                    $listNotificationContent.html('');
                    $listNotificationContent.append(html);
                } else {
                    setTimeout(function () {
                        $listNotificationContent.append(html);
                        $("#loader_notification").hide();
                    }, 1500);
                }
            }
        })
    };

    bookingNotificationNavBar.getListNotification(1);
});
