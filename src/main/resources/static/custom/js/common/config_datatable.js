let configDataTableServerSide = function(configObject, $idElement) {
    let columnOfSortField = configObject.sortField.column;
    let sortDirDefault = configObject.sortField.typeDir;
    return $idElement.DataTable({
        "language": {
            "url": "/libs/datatable/js/vi.json"
        },
        "lengthMenu": [
            [10, 15, 20],
            [10, 15, 20],
        ],
        "searching": false,
        "ordering":true,
        "order": [[ columnOfSortField, sortDirDefault]],
        "serverSide": true,
        "columns": configObject.columnDefinitions,
        "ajax": function (data, callback) {
            getDataFromServerSide(columnOfSortField, data, callback, configObject.url, configObject.columnDefinitions, configObject.customFieldSearchSelector);
        },
        columnDefs: configObject.columnRender,
        "drawCallback": configObject.drawCallbackFunction
    });
};

let getDataFromServerSide = function (columnOfSortField, requestData, renderCallBackFunction, url, columnDefinitions, customFieldSearchSelector) {
    let sortField = columnDefinitions[requestData.order[0].column].data;
    let sortDirOptional = requestData.order[0].dir;
    let params = {
        page: (requestData.start / requestData.length) + 1,
        size: requestData.length,
        sortField: sortField,
        sortDir: sortDirOptional
    };
    if (customFieldSearchSelector != null && customFieldSearchSelector !== '') {
        params.customFieldSearch = customFieldSearchSelector.val();
    }
    jQuery.get(url, params, function (response) {
        let content = {
            "draw": requestData.draw,
            "recordsTotal": response.totalElements,
            "recordsFiltered": response.totalElements,
            "data": response.content
        };
        renderCallBackFunction(content);
    });
};
