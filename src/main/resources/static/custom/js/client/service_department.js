$(document).ready(function () {

    let currentStoreId = $("#currentStoreId").val();

    let $citySelect = $("#city_select");
    let $serviceSelect = $("#service_select");
    let $numberBookingDirSelect = $("#number_booking_dir_select");
    let currentPage = 1;
    let maxPages;

    let $paginationNav = $(".blog-pagination");

    $citySelect.add($serviceSelect).add($numberBookingDirSelect).on('change', function () {
        renderListServiceDepartment();
    });

    $(document).on('click', '.service_department', function () {
        let serviceDepartmentId = $(this).attr("id");
        window.location.href = $("#btn_detail_" + serviceDepartmentId).attr('href');
    });

    function getListServiceDepartmentFromServer() {
        let pageServiceDepartmentDto;
        $.ajax({
            url: "/api/v1/client/service_departments/all?page=" + currentPage + "&storeId=" + currentStoreId
                + "&city=" + $citySelect.val()
                + "&sortDirNumberBooking=" + $numberBookingDirSelect.val(),
            type: "get",
            async: false,
            beforeSend: function(){
                window.loader.show();
            }
        }).done(function (response) {
            window.loader.hide();
            console.log(response);
            pageServiceDepartmentDto = response;
        });
        return pageServiceDepartmentDto;
    }

    function renderListServiceDepartment() {
        let pageServiceDepartmentDto = getListServiceDepartmentFromServer();
        let listServiceDepartmentDto = pageServiceDepartmentDto.content;

        let textTitleListServiceDepartment = listServiceDepartmentDto.length === 0 ? 'Không có dữ liệu phù hợp' : 'Tìm kiếm dịch vụ';
        $("#title_content").text(textTitleListServiceDepartment);

        let htmlRenderListServiceDepartment = '';
        listServiceDepartmentDto.forEach(function (serviceDepartment) {
            // let serviceDepartment = serviceDepartmentDto.serviceDepartment;

            let imageUrl = serviceDepartment.imageUrl;
            if (imageUrl == null) {
                imageUrl = "/images/placeholder_image.png";
            }
            htmlRenderListServiceDepartment += '<div class="col-md-3 store-info"> ' +
                '<div class="service_department" id="' +serviceDepartment.authId +'"> ' +
                '<img src="' + imageUrl + '" alt=""> ' +
                '<div class="blog_details" style="color:black"> ' +
                '<h6 style="font-size: 15px; text-transform: uppercase">' + serviceDepartment.name + '</h6> ' +
                '<a style="font-size: 15px" href="/stores/' + serviceDepartment.storeAuthId + '">' + serviceDepartment.storeName + '</a> ' +
                '<hr style="margin: unset">'+
                '<div style="width: 112px;font-size: 15px;margin-top: 3px">Số lượt đặt: ' + serviceDepartment.numberDoneBooking + '</div>' +
                '<a href="/services/' + serviceDepartment.authId + '" style="width: 100%;" ' +
                'class="blog_btn btn_detail" id="btn_detail_' + serviceDepartment.authId + '">Đặt lịch</a> ' +
                '</div> ' +
                '</div> <' +
                '/div>';
        });
        $(".blog_item").html('').append(htmlRenderListServiceDepartment);
        maxPages = pageServiceDepartmentDto.totalPages;
        renderPagination(maxPages);
    }

    function renderPagination(totalPages) {
        if (totalPages < 2) {
            $paginationNav.addClass('hidden');
        } else {
            let htmlRenderPagination = '<ul class="pagination">' +
                '<li class="page-item">' +
                '<a class="page-link" id="previous" aria-label="Previous">&laquo;</a>' +
                '</li>';
            for (let numberPage = 1; numberPage <= totalPages; numberPage++) {
                htmlRenderPagination += '<li class="page-item">' +
                    '<a class="page-link" >' + numberPage + '</a>' +
                    '</li>';
            }
            htmlRenderPagination += '<li class="page-item"> ' +
                '<a class="page-link" aria-label="Next">&raquo;</a>' +
                '</li> ' +
                '</ul>';

            $paginationNav.removeClass('hidden');
            $paginationNav.html('').append(htmlRenderPagination);

            // active page pagination
            $(".page-link").each(function () {
                if ($(this).text() == currentPage) {
                    $(this).parent().removeClass().addClass("page-item active");
                }
            });
        }
    }

    $(document).on('click', ".page-link", function () {
        let page = $(this).text();
        if ((page === '«' && currentPage === 1) || (page === '»' && currentPage === maxPages) || (parseInt(page) === currentPage)) {
            return false;
        }
        if (page === '«') {
            currentPage -= 1;
        } else if (page === '»') {
            currentPage += 1;
        } else {
            currentPage = parseInt(page);
        }
        renderListServiceDepartment(currentPage);
    });

    renderListServiceDepartment(1);
});
