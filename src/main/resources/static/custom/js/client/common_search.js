$(document).ready(function(){

    let $inputSearchBar = $("#search_bar");

    $inputSearchBar.typeahead({
            hint: false,
            highlight: true,
            minLength: 1,
        },
        {
            limit: 100,
            name: 'search_bar',
            display: function (serviceDepartment) {
                return serviceDepartment.name.toUpperCase();
            },
            source: function (str, sync, async) {
                findStoreAndServiceDepartment(str, async);
            },
            templates: {
                suggestion: function (serviceDepartment) {
                    return '<div style="font-size: 12px">' +
                        '<div class="row">' +
                        '<img class="image-typeahead col-md-3" src="'+ serviceDepartment.imageUrl +'">'+
                        '<div class="col-md-9">'+
                        '<span class="service-department-name-typeahead">'+ serviceDepartment.name + '</span><br>'+
                        '<span class="store-name-typeahead">'+ serviceDepartment.storeName + '</span>' +
                        '</div>'+
                        '</div>' +
                        '</div>';
                },
            }
        }
    );

    function findStoreAndServiceDepartment(inputSearch, async) {
        jQuery.ajax("/api/v1/client/search/listForTypeAHead?searchKey=" + inputSearch, {
            success: function (response) {
                return async(response);
            }
        });
    }

    $inputSearchBar.on('typeahead:select', function (event, serviceDepartmentSelected) {
        window.location.href = "/services/" + serviceDepartmentSelected.authId;
    });

    $inputSearchBar.keyup(function(event) {
        if (event.keyCode === 13) {
            window.location.href = "/search?searchKey=" + $inputSearchBar.val();
        }
    });

});
