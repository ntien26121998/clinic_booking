$(document).ready(function () {
    let serviceDepartmentAuthId = $("#service_department_id").val();
    let $modalAddBooking = $("#modal_add_booking");
    let $chooseDayInput = $("#choose_day");
    let $statusLogin = $("#status");
    $(document).on('click', 'input:radio[name=booking_frame]', function () {
        changeCssWhenChoosingBookingButton();
    });
    function changeCssWhenChoosingBookingButton() {
        $('input:radio[name=booking_frame]').each(function () {
            if ($(this).is(':checked')) {
                $(this).parent().css({"background-color": "red", "color": "white", "border-color": "red !important"});
            } else {
                $(this).parent().css({"background-color": "white", "color": "black"});
            }
        });
    }

    $("#btn_booking").click(function () {
        if ($statusLogin.val() === "NO_LOGIN") {
            $modalAddBooking.modal('hide');
            $("#modal_login").modal("show");
        } else if ($("#isInBlackList").val() === "true") {
            window.alert.show("error", "Bạn nằm trong danh sách đen của phòng khám. Vui lòng liên hệ phòng khám và thử lại sau!", 2000);
            return;
        } else {
            let bookingFrame = $('input:radio[name=booking_frame]:checked').val();
            if (bookingFrame === null) {
                window.alert.show("error", "Không có lịch đặt trong ngày này. Vui lòng kiểm tra và thử lại!", 2000);
                return;
            }
            if (bookingFrame === undefined) {
                window.alert.show("error", "Vui lòng chọn 1 khung giờ đặt lịch!", 2000);
                return;
            }
            //todo: check slot here
            // $modalAddBooking.modal({backdrop: 'static', keyboard: false})
            $modalAddBooking.modal("show")
        }
        //
        // let bookingFrame = $('input:radio[name=booking_frame]:checked').val();
        // if (bookingFrame === null) {
        //     window.alert.show("error", "Không có lịch đặt trong ngày này. Vui lòng kiểm tra và thử lại!", 2000);
        //     return;
        // }
        // if (bookingFrame === undefined) {
        //     window.alert.show("error", "Vui lòng chọn 1 khung giờ đặt lịch!", 2000);
        //     return;
        // }
        // //todo: check slot here
        // // $modalAddBooking.modal({backdrop: 'static', keyboard: false})
        // $modalAddBooking.modal("show")
    });

    //
    $modalAddBooking.on('shown.bs.modal', function () {
        let $timeBookingSpan = $("#time_booking_span");
        $timeBookingSpan.html('');
        let check = moment(new Date($chooseDayInput.val()), 'YYYY/MM/DD');
        let month = check.format('M');
        let day = check.format('D');
        let year = check.format('YYYY');
        let stringTimeBooking = $('input:radio[name=booking_frame]:checked').val() + ", ngày " + day + ", tháng " + month + ", năm " + year;
        $timeBookingSpan.html(stringTimeBooking);
    });

    /*SAVE BOOKING*/
    let serviceDepartmentId = $("#service_department_id").val();
    let storeId = $("#store_id").val();
    let MODAL_BOOKING_CLIENT_VUE = new Vue({
        el: "#add_booking_client_form",
        data: function () {
            return this.initData();
        },
        watch: {
            numberFrameConsecutive() {

            },
            currentCustomerId() {
                if (this.currentCustomerId && this.currentCustomerId !== '') {
                    TABLE_BOOKING_CUSTOMER_POPUP.loadTable(this.currentCustomerId, storeId);
                }
            }
        },
        computed: {
            listCategoryForModalBooking() {
                if (this.id === '') {
                    return this.listAllCategoryBooking
                        .filter(category => category.active && !category.deleted);
                } else {
                    return this.listAllCategoryBooking;
                }
            },
            listCategoryHasChosen() {
                return this.listAllCategoryBooking
                    .filter(category => this.listCategoryHasChosenId.indexOf(category.id) > -1);
            },
            totalMoneyFromCategoryChosen() {
                let sumMoney = this.listCategoryHasChosen
                    .map(category => category.moneyExcludeTax)
                    .reduce((prev, curr) => prev + curr, 0);
                let sumTime = this.listCategoryHasChosen.map(category => category.numberFrameConsecutiveAutoFill)
                    .reduce((prev, curr) => prev + curr, 0);
                return 'Tổng  ' + this.formatMoney(sumMoney) + "VND" + " - Thời gian dự kiến hoàn thành: " + sumTime * 15 + " phút";
            }
        },
        methods: {
            initData() {
                return {
                    id: '',
                    timeBookingTitle: '',
                    startTimeTitle: '',
                    endTimeTitle: ',',
                    deadlineCancellation: '',
                    title: '',
                    patientCode: '',
                    customerName: '',
                    customerPhone: '',
                    status: 'SUCCESS',
                    typeBooking: "VIA_ADMIN",
                    bookingTime: '',
                    serviceDepartmentAuthId: serviceDepartmentId,
                    note: '',
                    frame: '',
                    startTimeOfBookingFrame: '',
                    frameWithoutNumberSlot: '',
                    listComment: [],
                    deleteBookingId: '',
                    isEnableCreateNewCustomer: true,
                    isChangeInputCustomerName: false,
                    currentCustomerId: '',
                    currentBookingIdChangeNote: '',
                    currentBookingNote: '',

                    numberFrameConsecutive: 1,
                    mapNumberFrameConsecutiveCanChoose: {},
                    isFromScreenCustomer: false,
                    timeStartToEnd: '',
                    listAllCategoryBooking: [],
                    listCategoryHasChosenId: [],
                    minNumberFrameConsecutive: 1,
                    customerId: '',
                    listDayFuture: [],
                    // numberFrame: 1,
                    isCanEditBookingScreen: '',
                    maxNumberSlot: 0
                }
            },
            formatMoney(money) {
                return money.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
            },
            loadListCategoryBooking(serviceDepartmentId) {
                let self = this;
                jQuery.get("/api/v1/service_department/" + serviceDepartmentId + "/categoryBooking/findForSavingBooking", function (response) {
                    self.listAllCategoryBooking = response.data;
                });
            },

            changeNumberFrameConsecutive(categoryId, numberFrameConsecutiveAutoFill) {
                if (this.listCategoryHasChosenId.indexOf(categoryId) > -1) {
                    let indexOfListCategoryHasChosen = this.listCategoryHasChosenId.indexOf(categoryId);
                    let newNumberFrameConsecutive = this.numberFrameConsecutive + numberFrameConsecutiveAutoFill;
                    if (this.mapNumberFrameConsecutiveCanChoose.arrayNumber.indexOf(newNumberFrameConsecutive) > -1) {
                        this.numberFrameConsecutive = newNumberFrameConsecutive;
                        // this.minNumberFrameConsecutive = this.minNumberFrameConsecutive + numberFrameConsecutiveAutoFill;
                    } else {
                        this.listCategoryHasChosenId.splice(indexOfListCategoryHasChosen, 1);
                        window.alert.show("error", "Không đủ số khung", 2000);
                    }
                } else {
                    if (this.numberFrameConsecutive > numberFrameConsecutiveAutoFill) {
                        this.numberFrameConsecutive = this.numberFrameConsecutive - numberFrameConsecutiveAutoFill;
                        // this.minNumberFrameConsecutive = this.minNumberFrameConsecutive - numberFrameConsecutiveAutoFill;
                    }
                }
            },

            getTextNumberFrameAutoFillForCategory(numberFrameConsecutiveAutoFill) {
                return numberFrameConsecutiveAutoFill === 0 ? 'Không' : numberFrameConsecutiveAutoFill*15 + " Phút";
            },

            getDataForCreatingBooking() {
                return {
                    id: this.id,
                    customerId: this.currentCustomerId,
                    status: this.status,
                    // typeBooking: "VIA_ADMIN",
                    bookingDate: $("#choose_day").val() + ' ' + this.startTimeOfBookingFrame,
                    serviceDepartmentAuthId: serviceDepartmentId,
                    storeAuthId: storeId,
                    // note: this.note,
                    note: $("#booking_note").val(),
                    frame: this.frameWithoutNumberSlot,
                    numberFrameConsecutive: this.numberFrameConsecutive,
                    timeStartToEnd: this.timeStartToEnd,
                    setCategoryBookingId: this.listCategoryHasChosenId,
                    customerName: $("#customer_name").val(),
                    customerPhone: $("#customer_phone").val(),
                    customerEmail: $("#customer_email").val()
                }
            },
            createBooking() {
                let self = this;
                $.ajax({
                    type: "POST",
                    url: "/api/v1/client/" + serviceDepartmentAuthId + "/booking/save",
                    data: JSON.stringify(self.getDataForCreatingBooking()),
                    processData: false,
                    contentType: 'application/json',
                    beforeSend: function () {
                        window.loader.show();
                    },
                    success: function (response) {
                        $('#modal_add_booking').modal('hide');
                        window.loader.hide();
                        self.responseSuccessHandle(response);
                        // TABLE_BOOKING_VUE.loadFrameAndBooking();
                        bookingManagement.getBookingDataAndRenderTable($("#choose_day").val(), false);
                    }
                });
            },
            submitForm() {
                let self = this;
                if (self.id === '') {
                    self.createBooking();
                } else {
                    let serviceDepartmentId = $("#service_department_id").val();
                    self.updateBooking(serviceDepartmentId);
                }
            },
            responseSuccessHandle(response) {
                let ADD_SUCCESS = 202, UPDATE_SUCCESS = 203, DELETE_SUCCESS = 201, DELETE_FAIL = 401;
                let OUT_OF_BOOKING_SLOT = 1030, EXPIRED_DATE_BOOKING = 1032, NOT_FOUND_CUSTOMER = 405;
                let HOLIDAY_CONFIG = 406, HAS_NO_CONFIG = 407, WORK_OFF_DAY = 408, DUPLICATE_TIME_BOOKING = 410;
                switch (response.status.code) {
                    case UPDATE_SUCCESS:
                    case DELETE_SUCCESS:
                        window.alert.show("success", "Thành công", 2000);
                        // TABLE_BOOKING_VUE.loadFrameAndBooking();
                        break;
                    case OUT_OF_BOOKING_SLOT:
                        window.alert.show("error", "<span style='margin: unset'>Out of slot at frame: </span></br>" + response.data[0], 5000);
                        break;
                    case HOLIDAY_CONFIG:
                        window.alert.show("error", "Hôm nay là ngày lễ", 2000);
                        break;
                    case HAS_NO_CONFIG:
                        window.alert.show("error", "Hôm nay không có cấu hình làm việc", 2000);
                        break;
                    case WORK_OFF_DAY:
                        window.alert.show("error", "Hôm nay là ngày nghỉ", 2000);
                        break;
                    case EXPIRED_DATE_BOOKING:
                        window.alert.show("error", "Hết thời gian đặt lịch, chọn ngày trước hôm nay", 2000);
                        break;
                    case NOT_FOUND_CUSTOMER:
                        window.alert.show("error", "Không tìm thấy khách hàng", 2000);
                        break;
                    case DUPLICATE_TIME_BOOKING:
                        window.alert.show("error", "Khách hàng này đã đặt lịch trùng khung thời gian", 5000);
                        break;
                    case ADD_SUCCESS:
                        let listDayError = response.data;
                        if (listDayError != null && listDayError.length > 0) {
                            window.alert.show("error", "Danh sách ngày không thể đặt trước： " + response.data.join(", "), 5000);
                        } else {
                            window.alert.show("success", "Thành công", 2000);
                        }
                        break;
                    default:
                        window.alert.show("error", "Đã có lỗi xảy ra, vui lòng thử lại", 2000);
                        break;
                }
            },
            resetForm(validator, frameWithNumberSlot) {
                Object.assign(this.$data, this.initData());

                let frameWithoutNumberSlot = frameWithNumberSlot.split("_")[0];
                this.frameWithoutNumberSlot = frameWithoutNumberSlot;
                this.startTimeOfBookingFrame = frameWithoutNumberSlot.split(" - ")[0] + ':00';
                this.computeListNumberFrameConsecutiveCanChoose(frameWithoutNumberSlot);
                this.loadListCategoryBooking(serviceDepartmentId);
            },

            computeListNumberFrameConsecutiveCanChoose(currentFrameWithoutNumberSlot, numberFrameConsecutiveFromOldBooking) {
                this.mapNumberFrameConsecutiveCanChoose = {
                    arrayNumber: []
                };
                let mapListBookingByFrameWithSlot = bookingManagement.mapFrameAndListBookingForTable;
                let isEnableSelect = false;
                let listFrameFromThisToAfter = [];
                let mapFrameAndListBooking = [];
                for (let frameTimeWithNumberSlot in mapListBookingByFrameWithSlot) {
                    let mapFrameAndListBookingForTable = {};
                    mapFrameAndListBookingForTable.frameTimeWithNumberSlot = frameTimeWithNumberSlot;
                    mapFrameAndListBookingForTable.frameTime = frameTimeWithNumberSlot.split("_")[0];
                    let numberSlotCanBooking = Number.parseInt(frameTimeWithNumberSlot.split("_")[1]);
                    if (numberSlotCanBooking > this.maxNumberSlot) {
                        this.maxNumberSlot = numberSlotCanBooking;
                    }
                    let listBooking = mapListBookingByFrameWithSlot[frameTimeWithNumberSlot];
                    mapFrameAndListBookingForTable.listBooking = listBooking;


                    let numberBookingNotHasStatusCanceled = listBooking.filter(booking => booking.status !== 'CANCELED').length;
                    mapFrameAndListBookingForTable.numberSlotEmpty = numberSlotCanBooking - numberBookingNotHasStatusCanceled;
                    if (mapFrameAndListBookingForTable.numberSlotEmpty < 0) {
                        mapFrameAndListBookingForTable.numberSlotEmpty = 0;
                    }
                    mapFrameAndListBooking.push(mapFrameAndListBookingForTable);
                    // self.listFrameWithoutNumberSlot.push(mapFrameAndListBookingForTable.frameTime);
                }
                mapFrameAndListBooking.forEach(frameAndListBooking => {
                    if (frameAndListBooking.frameTime === currentFrameWithoutNumberSlot) {
                        isEnableSelect = true;
                    }

                    if (isEnableSelect) {
                        listFrameFromThisToAfter.push(frameAndListBooking);
                    }
                });

                let numberFrameCanBeConsecutive;
                if (!numberFrameConsecutiveFromOldBooking) { // when create
                    let indexOfFrameHasNoEmptySlot = listFrameFromThisToAfter.findIndex(frame => frame.numberSlotEmpty === 0);
                    numberFrameCanBeConsecutive = indexOfFrameHasNoEmptySlot === -1 ? listFrameFromThisToAfter.length : indexOfFrameHasNoEmptySlot
                } else { // when update
                    let indexOfFrameHasNoEmptySlot = listFrameFromThisToAfter.slice(numberFrameConsecutiveFromOldBooking).findIndex(frame => frame.numberSlotEmpty === 0);
                    numberFrameCanBeConsecutive = indexOfFrameHasNoEmptySlot === -1 ? listFrameFromThisToAfter.length : indexOfFrameHasNoEmptySlot + numberFrameConsecutiveFromOldBooking;
                }
                let setOfFrame = new Set();
                for (let i = 1; i <= numberFrameCanBeConsecutive; i++) {
                    setOfFrame.add(i);
                }

                if (numberFrameConsecutiveFromOldBooking) {
                    for (let i = 1; i <= numberFrameConsecutiveFromOldBooking; i++) {
                        setOfFrame.add(i);
                    }
                }
                this.mapNumberFrameConsecutiveCanChoose.arrayNumber = Array.from(setOfFrame).sort(
                    function (a, b) {
                        return a - b;
                    });
            },
            autoFillCategory(listLatestCategoryHasChosenForCustomer) {
                let self = this;
                let numberFrameConsecutiveWillBeFilled = 1;

                let listCategoryIdHasInList = [];
                self.listCategoryHasChosenId = listCategoryIdHasInList;
                listLatestCategoryHasChosenForCustomer.forEach(function (categoryId) {
                    self.listAllCategoryBooking.forEach(function (category) {
                        if (category.id === categoryId) {
                            numberFrameConsecutiveWillBeFilled += category.numberFrameConsecutiveAutoFill;
                            listCategoryIdHasInList.push(categoryId);
                        }
                    });
                });
                if (self.mapNumberFrameConsecutiveCanChoose.arrayNumber.indexOf(numberFrameConsecutiveWillBeFilled) > -1) {
                    self.listCategoryHasChosenId = [];
                    self.listCategoryHasChosenId = listCategoryIdHasInList;
                    self.numberFrameConsecutive = numberFrameConsecutiveWillBeFilled;
                    // if (numberFrameConsecutiveWillBeFilled === numberFrameConsecutiveDefault) {
                    //     self.minNumberFrameConsecutive = 0;
                    // } else {
                    //     self.minNumberFrameConsecutive = numberFrameConsecutiveWillBeFilled;
                    // }
                } else if (listCategoryIdHasInList.length === 0) {
                    self.numberFrameConsecutive = numberFrameConsecutiveDefault;
                } else {
                    self.listCategoryHasChosenId = [];
                    self.numberFrameConsecutive = numberFrameConsecutiveDefault;
                    window.alert.show("error", "枠数がたりず設定できません", 3000);
                }
            },
        },
        mounted() {
            let self = this;
            bookingManagement.getBookingDataAndRenderTable($("#choose_day").val(), false);
            self.loadListCategoryBooking(serviceDepartmentId);
            self.isCanEditBookingScreen = $('#can_edit_booking_screen').val();

            let isCanEditBookingScreen = $('#can_edit_booking_screen').val() === 'true';

            let $customerName = $("#customer_name");
            let $addBookingForm = $("#add_booking_client_form");

            let $chooseActiveDay = $("#choose_active_day");


            let customerNameInput = document.getElementById('customer_name');

            customerNameInput.addEventListener('focusout', function (e) {
                self.customerName = this.value;
                $customerName.typeahead('val', self.customerName);

            });

            $(document).on('click', '#btn_booking', function () {
                let bookingFrame = $('input:radio[name=booking_frame]:checked').val();
                $("#number_frame_consecutive").prop('disabled', false);
                self.resetForm(validator, bookingFrame);
            });

            $(document).on('click', '#btn_submit_booking', function () {
                if ($addBookingForm.valid()) {
                    self.submitForm();
                }
            });

            $(document).on('click', ".btn-update-booking", function () {
                let idBooking = $(this).attr("id").replace('btn_update_booking_', '');
                self.id = idBooking;
                self.resetForm(validator, $(this).attr("data"));
                self.getDetailBooking(idBooking, serviceDepartmentId);
                $("#btn_submit_booking").html("Cập nhật");
            });

            $(document).on('click', ".btn-active-special-booking", function (event) {
                let idBooking = $(this).attr("data");
                let storeIdInListBookingCustomerPage = $(this).attr("store");
                let isClickFromListBookingCustomerPage = storeIdInListBookingCustomerPage !== undefined;
                event.preventDefault();
                self.activeSpecialBooking(idBooking, isClickFromListBookingCustomerPage ? storeIdInListBookingCustomerPage : storeId);
            });


            $(document).on('click', '.btn-delete-booking', function () {
                let storeIdInListBookingCustomerPage = $(this).attr("store");
                let isClickFromListBookingCustomerPage = storeIdInListBookingCustomerPage !== undefined;
                if (isClickFromListBookingCustomerPage) {
                    $('#store_id').val(storeIdInListBookingCustomerPage);
                }
                self.deleteBookingId = $(this).attr('id');
            });

            // submit delete
            $(document).on('click', "#btn_submit_delete", function (event) {
                self.deleteBooking(self.deleteBookingId, event);
            });

            let validator = $addBookingForm.validate({
                errorElement: "p",
                errorClass: "error-message",
                errorPlacement: function (error, element) {
                    error.insertAfter(element);
                },
                rules: {
                    customer_name: {
                        // required: function () {
                        //     return self.isChangeInputCustomerName;
                        // },
                        notEmpty: true
                    },
                    customer_phone: {
                        onlyNumber: true,
                        number: true,
                        notEmpty: true
                    }
                },
            });

            // custom validate method
            $.validator.addMethod('onlyNumber', function (value) {
                let regExp = /^\d+$/;
                return regExp.test(value)
            }, 'Vui lòng chỉ nhập số hợp lệ');

            $.validator.addMethod('notEmpty', function (value) {
                return value !== null && value !== undefined && value.trim() !== "";
            }, 'Trường này bắt buộc');

        }
    })

});

function getDayInWeekText(activeDay) {
    let dt = new Date(activeDay);
    let numberInWeek = dt.getDay();
    switch (numberInWeek) {
        case 0: // sunday
            return " (Chủ nhật)";
        case 1:
            return " (Thứ hai)";
        case 2:
            return " (Thứ ba)";
        case 3:
            return " (Thứ tư)";
        case 4:
            return " (Thứ năm)";
        case 5:
            return " (Thứ sáu)";
        case 6:
            return " (Thứ bảy)"; //saturday
    }
}
