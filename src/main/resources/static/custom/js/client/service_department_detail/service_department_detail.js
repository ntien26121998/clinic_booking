$(document).ready(function () {
    let $serviceDepartmentId = $("#service_department_id");
    let $chooseDayInput = $("#choose_day");
    let $listSlot = $(".list-slot");
    let $btnAddBooking = $("#btn_booking");
    let $outSlotDiv = $("#out_slot_div");

    $("body").tooltip({ selector: '[data-toggle=tooltip]' });

    configDateShowOnlyDayWithMinDate('choose_day');

    $chooseDayInput.change(function () {
        $(".list-slot").html('');
        renderListSlotBooking();
    });

    function renderListSlotBooking() {
        jQuery.get("/api/v1/client/" + $serviceDepartmentId.val() + "/booking/getListEmptySlotOfStore?activeDay=" + $chooseDayInput.val(),
            function (response) {
                console.log(response);
                $btnAddBooking.addClass($listSlot).addClass('hidden');
                if (!response.hasAvailableSlot) {
                    $outSlotDiv.removeClass('hidden');
                    return;
                } else {
                    let html = '';
                    let mapFrameAndSlot = response.numberAvailableSlotOfFrames;
                    for (const frameSlot of Object.keys(mapFrameAndSlot)) {
                        if (isFrameSlotOutTime(frameSlot) || mapFrameAndSlot[frameSlot] === 0) {
                            $outSlotDiv.removeClass('hidden');
                            continue;
                        }
                        $outSlotDiv.addClass('hidden');
                        $btnAddBooking.addClass($listSlot).removeClass('hidden');
                        html += '<label class="btn btn-outline-dark btn-slot" data-toggle="tooltip" data-placement="top" title="'+ mapFrameAndSlot[frameSlot] +' chỗ trống">' +
                            '<input type="radio" name="booking_frame" value="' + frameSlot + '_' + mapFrameAndSlot[frameSlot] + '" hidden>' + frameSlot + '</label>';
                    }
                    $listSlot.append(html);
                }
            })
    }

    function isFrameSlotOutTime(frameSlot) {
        let currentDate = moment(new Date()).format("YYYY/MM/DD HH:mm");
        let startFrame = $chooseDayInput.val() + " " + frameSlot.split("-")[0].trim();
        return startFrame < currentDate;
    }

    renderListSlotBooking();

    let previous=0;

    $('#comment_client').click(function(){
        var s=$(this).attr('id');
        $('#'+s).animate({'width':'200px', 'height':'80px'});
        $('#'+s).css({'cursor':'zoom-out'});
        if($('#'+previous).width()!=100)
        {
            $('#'+previous).animate({'width':'100px', 'height':'50px'});
            $('#'+previous).css({'cursor':'zoom-in'});
        }
        previous=s;
    });
});
