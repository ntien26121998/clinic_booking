function render() {
    window.recaptchaVerifier = new firebase.auth.RecaptchaVerifier('recaptcha_container');
    recaptchaVerifier.render();
}

function phoneAuth() {
    //render check captcha
    let phoneNumber = document.getElementById('phone_number').value;
    if (phoneNumber.trim() == "") {
        $("#check_empty_phone_number").show()
    } else {
        render();
        let lengthNumber = phoneNumber.length;
        phoneNumber = "+84" + phoneNumber.slice(1, lengthNumber);
        console.log(phoneNumber)
        //phone number authentication function of firebase
        // it takes two parameter first one is number,,,second one is recaptcha
        firebase.auth().signInWithPhoneNumber(phoneNumber, window.recaptchaVerifier).then(function (confirmationResult) {
            //s is in lowercase
            window.confirmationResult = confirmationResult;
            coderesult = confirmationResult;
            console.log(coderesult);
            $("#check_verify_code").show()
        }).catch(function (error) {

        });
    }
}


function verifyCode() {
    let code = document.getElementById('verify_code').value;
    coderesult.confirm(code).then(function (result) {
        let user = result.user;
        console.log(user);
        $("#verify_code_success_notification").css("display", "block")
        $("#verify_code_failed_notification").css("display", "none")

    }).catch(function (error) {
        $("#verify_code_success_notification").css("display", "none")
        $("#verify_code_failed_notification").css("display", "block")

    });
}

$(document).ready(function () {
    let $formLoginSms = $("#form_login_sms");

    $("#login_sms").on('click', function () {
        $("#modal_login").modal({
            backdrop: 'static',
            keyboard: false
        })
        resetFormRegister();
    })

    let resetFormRegister = function() {
        $("#phone_number").val("")
        $("#verify_code").val("")
        $("#name").val("")
        $("#register_username").val("")
        $("#register_password").val("")
        $("#register_password_confirm").val("")

        $("#recaptcha_container").empty();
    }

    $("#verify_phone_number_btn").click(function (e) {
        e.preventDefault();
        phoneAuth()
    })

    $("#phone_number").on("input", function () {
        $("#check_empty_phone_number").hide()
    })

    $("#verify_code_btn").click(function (e) {
        e.preventDefault();
        let code = $('#verify_code').val()
            verifyCode()
    })

    $("#btn_submit_login").on('click', function (e) {
        $("#modal_login").modal('hide');
        e.preventDefault();
        $.ajax({
            type: "POST",
            url: "/login_success?phoneNumber=" + $("#phone_number").val(),
            // data: JSON.stringify(accountKitPostRequest),
            processData: false,
            // contentType: 'application/json',
        }).done(function () {
            $("#modal_login").modal('hide');
            $("#status").val("LOGGED");
            // setTimeout(function () {
            //     window.location.reload();
            // }, 1000);
        })
    })

})