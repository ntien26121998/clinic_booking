$(document).ready(function () {
    let rules = {
        customer_email: {
            notEmpty: true,
            email: true,
            maxlength: 500
        },
        customer_name: {
            notEmpty: true,
            maxlength: 500
        },
    };

    let messages = {
        customer_email: {
            email: "Vui lòng nhập địa chỉ email"
        }
    };

    configValidate($("#form_account_client"), rules, [], messages);

    $("#btn_submit_update_client_account").click(function (e) {
        if ($("#form_account_client").valid()) {
            e.preventDefault();
            let updateInfo = {
                phone: $("#customer_phone").val(),
                name: $("#customer_name").val(),
                email: $("#customer_email").val(),
            };
            $.ajax({
                type: "POST",
                url: "/api/v1/client/account/updateInfo",
                data: JSON.stringify(updateInfo),
                processData: false,
                contentType: 'application/json',
                beforeSend: function () {
                    window.loader.show();
                },
            }).done(function (response) {
                window.loader.hide();
                console.log(response);
                if (response === "UPDATE_SUCCESS") {
                    window.alert.show("success", "Thành công!", 2000);
                    setTimeout(function () {
                        window.location.reload();
                    }, 3000)
                } else {
                    window.alert.show("error", "Có lỗi xảy ra. Vui lòng thử lại!", 2000);
                }
            })
        }
    })

});
