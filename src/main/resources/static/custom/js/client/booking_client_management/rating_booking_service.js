$(document).ready(function () {

    let $modalRating = $("#modal_rating_booking_service");
    let $commentRatingInput = $("#comment_rating");
    let currentServiceDepartmentRatingId;
    let currentBookingId;

    $(document).on('click', ".btn-rating", function () {
        resetForm();
        let serviceDepartmentName = $(this).parent().parent().parent().parent().find('.service-department-name').text();
        $("#service_department_name").html(serviceDepartmentName);
        $modalRating.modal("show");
        currentServiceDepartmentRatingId = $(this).attr("id");
        currentBookingId = $(this).attr("data");
    });


    function resetForm() {
        $('input[name="rate"]').prop('checked', false);
        $commentRatingInput.val("");
    }

    $("#btn_submit_rating").click(function () {
        let formData = new FormData();
            formData.append("bookingId", currentBookingId);
            formData.append("serviceDepartmentAuthId", currentServiceDepartmentRatingId);
            formData.append("comment", $commentRatingInput.val());
            if ($(".file-input")[0].files.length !== 0) {
                formData.append("imageCommentClient", $("#image_comment_client")[0].files[0]);
             }

        console.log(formData.get("serviceDepartmentAuthId"));
        $.ajax({
            type: "POST",
            url: "/api/v1/client/booking_manager/rating",
            data: formData,
            processData: false,
            contentType: false,
            beforeSend: function () {
                window.loader.show();
            },
        }).done(function (response) {
            window.loader.hide();
            if(response === "RATING_SUCCESS"){
                window.alert.show("success", "Thành công!", 2000);
                $modalRating.modal('hide');
            } else {
                window.alert.show("error", "Có lỗi xảy ra. Vui lòng thử lại!", 2000);
            }
            setTimeout(function () {
                tableClientBookingManagement.ajax.reload();
            }, 1000)
        })
    })
});
