let tableClientBookingManagement;

$(document).ready(function () {

    let $startRangeTime = $("#start_range_time");
    let $endRangeTime = $("#end_range_time");
    let $statusSelect = $("#booking_status_select");
    let currentCancelBookingId;
    let listAllCategoryBooking =  [];
    let listCategoryHasChosen = [];
    let bookingDate = '';
    configDateShowOnlyDayWithoutAutoApply("start_range_time");
    configDateShowOnlyDayWithoutAutoApply("end_range_time");

    $startRangeTime.on('cancel.daterangepicker', function (ev, picker) {
        $startRangeTime.val('');
    });

    $endRangeTime.on('cancel.daterangepicker', function (ev, picker) {
        $endRangeTime.val('');
    });

    let url = window.location.toString();
    if (url.match('#')) {
        $('.nav-tabs a[href="#' + url.split('#')[1] + '"]').tab('show');
    }

    // Change hash for page-reload
    $('.nav-tabs a').on('shown.bs.tab', function (e) {
        let scrollTop = $(window).scrollTop();
        window.location.hash = e.target.hash;
        $(window).scrollTop(scrollTop);
    });


    let noDataHtml = '<i>Không có dữ liệu</i>';
    let statusLogin = $("#status").val();

    function showModalWhenNotLoginOrRenderTable() {
        if (statusLogin === "NO_LOGIN") {
            $("#modal_login").modal({backdrop: 'static', keyboard: false});
        } else {
            $startRangeTime.val('');
            $endRangeTime.val('');
            renderBookingHistoryTable();
        }
    }

    showModalWhenNotLoginOrRenderTable();

    $("#btn_filter_booking").click(function () {
        tableClientBookingManagement.ajax.reload();
    });

    function renderBookingHistoryTable() {
        let columnDefinitions = [
            {
                "data": null,
                "orderable": false,
                "defaultContent": noDataHtml
            }
        ];

        let getDataFromServer = function (requestData, renderFunction) {
            let params = {
                "page": (requestData.start / requestData.length) + 1,
                "size": requestData.length,
                "status": $statusSelect.val()
            };
            if ($startRangeTime.val() !== '') {
                params.startTime = $startRangeTime.val() + " 00:00";
            }
            if ($endRangeTime.val() !== '') {
                params.endTime = $endRangeTime.val() + " 00:00";
            }

            jQuery.get("api/v1/client/booking_manager/list", params, function (response) {
                let content = {
                    "draw": requestData.draw,
                    "recordsTotal": response.totalElements,
                    "recordsFiltered": response.totalElements,
                    "data": response.content
                };
                renderFunction(content);
            });
        };

        tableClientBookingManagement = $("#booking_table").DataTable(
            {
                "language": {
                    "url": "/libs/datatable/js/vi.json"
                },
                "lengthMenu": [
                    [10, 15, 20],
                    [10, 15, 20],
                ],
                "searching": false,
                "serverSide": true,
                "columns": columnDefinitions,
                "ajax": function (data, callback) {
                    getDataFromServer(data, callback);
                },
                "columnDefs": [
                    {
                        "render": function (bookingEntity) {
                            return renderBookingHistoryInfo(bookingEntity);
                        },
                        "targets": 0
                    }
                ]
            });

        function renderBookingHistoryInfo(bookingEntity) {
            return '<div class="service-info row">\n' +
                '                <div class="col-md-2">\n' +
                '                    <img class="img-fluid" style="width: 200px;height: 120px;" src="' + bookingEntity.serviceDepartmentImageUrl + '" alt="">' +
                '                </div>\n' +
                '                <div class="col-md-4" style="border-right: solid 2px rgba(0,0,0,.02); padding-top: 20px; padding-left: 10px;">\n' +
                '                    <p class="service-department-name" style="font-size: 14px; text-transform: uppercase; font-weight: bold">' + bookingEntity.serviceDepartmentName + '</p>\n' +
                '                    <a class="btn btn-primary" style="margin-top: 12px" href="services/' + bookingEntity.serviceDepartmentAuthIdForClient + '">Chi tiết dịch vụ</a>\n' +
                '                </div>\n' +
                '                <div class="col-md-4" style="font-size: 14px; padding: 0 10px">\n' +
                '                    <ul class="list">\n' +
                '                        <li style="margin-bottom:10px;font-weight:bold">Trạng thái: ' + renderLabelForBookingStatus(bookingEntity.status) + '</li>\n' +
                '                        <li style="margin-bottom:10px;font-weight:bold">Ngày hẹn: ' + bookingEntity.bookingTime + '</li>\n' +
                '                        <li style="margin-bottom:10px;font-weight:bold">Ngày tạo: ' + bookingEntity.createdTime + '</li>\n' +
                '                    </ul>\n' +
                '                </div>\n' +
                '                <div class="col-md-2" style="padding-top: 20px">\n' +
                '                    <ul class="list">\n' +
                '                        <li style="margin-bottom: 10px;">' +
                '                              <button class="btn btn-primary btn-detail-booking"  data="' + bookingEntity.id + '" ' +
                '                               dataServiceDepartmentId = "' + bookingEntity.serviceDepartmentAuthIdForClient +' ">Chi tiết lịch đặt</button></li>\n' +
                '                        <li>' + renderButtonRating(bookingEntity) + '</li>' +
                '                    </ul>\n' +
                '                </div>\n' +
                '            </div>';
        }

        function renderButtonRating(bookingEntity) {
            if (bookingEntity.status === 'WAITING_CONFIRM' || bookingEntity.status === 'SUCCESS') {
                return '<button class="btn btn-danger btn-sm btn-cancel-booking" data="' + bookingEntity.id + '" bookingDate="' +bookingEntity.bookingTime +' ">Hủy lịch</button>' ;
            }
            if (bookingEntity.status === 'DONE' && !bookingEntity.ratedByPhone) {
                if(!isPassed7Days(bookingEntity.bookingTime)) {
                    return '<button class="btn btn-primary btn-rating" data="' + bookingEntity.id + '" id="' + bookingEntity.serviceDepartmentAuthIdForClient + '">Đánh giá dịch vụ</button>'
                } else{
                    return '<button class="btn btn-primary" disabled>Quá hạn đánh giá</button>' ;
                }

            }
            if(bookingEntity.ratedByPhone) {
                return '<button class="btn btn-primary" disabled hidden>Đã đánh giá</button>' ;
            }
            return "";
        }

        function renderLabelForBookingStatus(status) {
            switch (status) {
                case "WAITING_CONFIRM":
                    return '<span style="color: red">Chờ xác nhận</span>';
                case "SUCCESS":
                    return '<span style="color: red">Thành công</span>';
                case "IN_PROCESS":
                    return '<span style="color: red">Đang thực hiện</span>';
                case "DONE":
                    return '<span style="color: red">Đã hoàn thành</span> ';
                case "CANCELED":
                    return '<span style="color: red">Đã hủy</span>';
            }
        }
    }

    function isPassed7Days(bookingTime) {
        let dateBefore7Days = moment(Date.now() - 7 * 24 * 3600 * 1000).format('YYYY/MM/DD HH:ss').replace("-", "");
        return bookingTime.replace("-", "") < dateBefore7Days;
    }

    $(document).on('click', '.btn-cancel-booking',function () {
        currentCancelBookingId = $(this).attr('data');
        bookingDate = $(this).attr('bookingDate').split(" ")[0];
        $("#cancel_booking_modal").modal("show");
    });

    let listCategoryHasChosenId = [];
    $(document).on('click', '.btn-detail-booking',function () {
        let serviceDepartmentId = $(this).attr('dataServiceDepartmentId');
        jQuery.get("/api/v1/service_department/" + serviceDepartmentId + "/categoryBooking/findForSavingBooking", function (response) {
            listAllCategoryBooking = response.data;
        });

        let bookingId = $(this).attr('data');
        jQuery.get("api/v1/client/booking_manager/detail/" + bookingId, function (bookingDetailDto) {
            console.log("detail",bookingDetailDto);
            let bookingEntity = bookingDetailDto.bookingEntity;
            let store = bookingDetailDto.storeEntity;
            let serviceDepartment = bookingDetailDto.serviceDepartmentEntity;
            $("#service_department_name_info").html(bookingEntity.serviceDepartmentName);
            $("#customer_email_info").val(bookingEntity.customer.email);
            $("#customer_name_info").val(bookingEntity.customer.name);
            $("#customer_phone_info").val(bookingEntity.customer.phone);
            $("#booking_note_info").val(bookingEntity.note);
            $("#address").html("Địa chỉ: " + store.address);
            $("#service_department_phone").html("SĐT: " + serviceDepartment.phone);
            $("#service_department_email").html("Email: " + serviceDepartment.email);
            $("#time_booking_span").html(bookingEntity.bookingTime);

            listCategoryHasChosenId = bookingDetailDto.listCategoryIdHasChosen;

            if (listCategoryHasChosenId.length > 0) {
                $(".title-time-and-fee").removeClass('hidden');
                $(".list-category").removeClass('hidden');
            } else {
                $(".title-time-and-fee").addClass('hidden');
                $(".list-category").addClass('hidden');
            }

            listCategoryHasChosen = listAllCategoryBooking.filter(category => listCategoryHasChosenId.indexOf(category.id) > -1);
            let htmlCategory = '';
            for (let i = 0; i < listCategoryHasChosen.length; i ++) {
                if (listCategoryHasChosen.length <= 1) {
                    $("#list_category").html(listCategoryHasChosen[i].categoryName)
                } else {
                    htmlCategory += listCategoryHasChosen[i].categoryName + ', ';
                    $("#list_category").html(htmlCategory);
                }
            }

            let sumMoney = listCategoryHasChosen
                .map(category => category.moneyExcludeTax)
                .reduce((prev, curr) => prev + curr, 0);
            $("#time_and_fee").html(formatMoney(sumMoney) + " VND");
        });



        $("#modal_add_booking").modal("show");
    });

    function formatMoney(money) {
        return money.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
    }

    $(document).on('click', '#btn_submit_cancel_booking', function () {
        $.ajax({
            type: "POST",
            url: '/api/v1/client/booking_manager/cancelBooking/' + currentCancelBookingId,
            beforeSend: function () {
                window.loader.show();
            }
        }).done(function(response) {
            window.loader.hide();
            if(response === 'CANCEL_SUCCESS') {
                $("#cancel_booking_modal").modal("hide");
                window.alert.show("success", "Hủy thành công", "1500");
                setTimeout(function (){
                    tableClientBookingManagement.ajax.reload();
                }, 1000);
            } else {
                window.alert.show("error", "Có lỗi xảy ra. Vui lòng thử lại !", "1500");
            }

        })
    })

});
